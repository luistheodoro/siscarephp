<?php

// EXIBE TODOS COLABORADORES DE TODAS EQUIPES
function exibir_equipe($mysqli,$equipe_nome,$equipe,$active,$usuario, $servico){
	
	$query = "SELECT COUNT(*) AS qnt FROM ".$_SESSION['user_Servico']."_usuarios ";
	$mysql_query = mysqli_query($mysqli,$query);
	$max_user = mysqli_fetch_array($mysql_query);
	
?>
	
	<!-- EQUIPE -> LISTA ESPECIALIDADE -->
	<div id="tab-<?php echo $equipe; ?>" class="tab-pane fade in <?php echo $active; ?>">
		
		<!-- ACESSO PARA CRIAR NOVOS USUARIOS -->
		<?php if( acesso_cadastrar_perfil($mysqli,$usuario,$equipe) == TRUE AND $max_user['qnt'] < $servico['geral']['usuarios'] ){ ?>
		
			<a href="cadastrar_usuario.php" class="pull-right label label-primary hidden-xs">
				<h4> &nbsp; <i class="fa fa-plus"></i> Novo colaborador &nbsp; </h4>
			</a>
			
			<a href="cadastrar_usuario.php" class="pull-right label label-primary hidden-lg hidden-md hidden-sm">
				<h4> &nbsp; <i class="fa fa-plus"></i> &nbsp; </h4>
			</a>
			
		<?php } ?>
		
		
		<div class="panel-heading">
			<h2><?php echo $equipe_nome; ?></h2>
		</div>
		
		
		<div class="panel-body">
		
			<div id="tabela_<?php echo $equipe; ?>" >
				<table class="table table-hover">
					
					<thead>
						<tr>
							<th><b>#</b></th>
							<th><b>Colaborador</b></th>
							<th><b>Cargo</b></th>
							<th class="text-center"><b>Perfil</b></th>
						</tr>
					</thead>
					
					<tbody>
	
					<?php
					
						$active = "";
						
						$query = "SELECT * FROM ".$_SESSION['user_Servico']."_usuarios WHERE codigo_servico = '".$_SESSION['user_Servico']."' AND equipe = '$equipe' ORDER BY nome";
						$mysql_query = mysqli_query($mysqli,$query);
						
						$i = 0;
						while( $colaboradores = mysqli_fetch_array($mysql_query) ){
							
							$i++;
							
					?>
							
							<tr onclick="location.href = 'perfil.php?id=<?php echo $colaboradores['id_usuario']; ?>';" style="cursor: pointer;">
								<td><?php echo $i; ?></td>
								<td><?php echo $colaboradores['nome']; ?></td>
								<td><?php echo $colaboradores['cargo']; ?></td>
								<td class="text-center"><i class="fa fa-user"></i></td>
							</tr>
							
					<?php
					
						}
						
					?>
					
					</tbody>
				</table>
				<!-- FIM DA TABELA -->
				
			</div>
			
			<?php
			if($i==0){
				echo "
					<script> document.getElementById('tabela_".$equipe."').style.display = 'none'; </script>
					<h4>Nenhum colaborador cadastrado!</h4><br><br>";
			}
			?>
			
		</div>
	</div>
	
<?php
	
}

?>