﻿<?php

	// EXIBE TODOS OS PACIENTES DE DETERMINADA ESPECIALIDADE
	function exibir_pacientes_especialidades($mysqli, $equipe_nome, $equipe, $acesso){
	
?>
		
		<!-- PACIENTE -> LISTA ESPECIALIDADE -->
		<div id="tab-<?php echo $equipe; ?>" class="tab-pane fade in">
			
			<!-- ACESSO PARA CADASTRAR NOVOS PACIENTES -->
			<?php 
			
				if( $acesso['cadastrar_paciente'] == TRUE ){
				
			?>
			
				<a href="cadastrar_paciente.php" class="pull-right label label-primary hidden-xs">
					<h4> &nbsp; <i class="fa fa-plus"></i> Novo paciente &nbsp; </h4>
				</a>
				
				<a href="cadastrar_paciente.php" class="pull-right label label-primary hidden-sm hidden-md hidden-lg">
					<h4> &nbsp; <i class="fa fa-plus"></i> &nbsp; </h4>
				</a>
				
			<?php } ?>
			
			
			<div class="panel-heading">
				<h2><?php echo $equipe_nome; ?></h2>
			</div>
			
			
			<div class="panel-body">
			
				<div class="clearfix"></div>
				
				<div id="tabela_<?php echo $equipe; ?>" >
					
					<style>
					#tabela_pacientes_espec td:nth-child(6),
					#tabela_pacientes_espec th:nth-child(6) {
						text-align : center;
					}
					</style>

					<table id="tabela_pacientes_<?php echo $equipe; ?>" class="table hover row-border compact table-condensed">
						<thead>
							<tr class="headings">
								<th class="hidden-xs">#</th>
								<th>Paciente</th>
								<th class="hidden-xs">Responsável</th>
								<th class="hidden-xs">Última conduta</th>
								<th class="hidden-xs">Data</th>
								<th class="hidden-xs">Objetivo</th>
							</tr>
						</thead>

						<tbody>
						
						<?php
						
							$active = "";
							
							
							$query = "SELECT * FROM ".$_SESSION['user_Servico']."_paciente_geral WHERE status = 'ativo' AND $equipe='1' ORDER BY nome";
							$mysql_query = mysqli_query($mysqli,$query);
							
							$i = 0;
							while ( $paciente_geral = mysqli_fetch_assoc($mysql_query) ){
								
								$i++;
								
								
								// BUSCA DADOS DO PACIENTE
								$paciente = busca_paciente($mysqli, $paciente_geral['id']);
								
								
								// BUSCA A ULTIMA CONDUTA DESSA EQUIPE
								if($paciente['conduta']['fon'] == FALSE){
									$paciente['conduta']['fon']['conduta'] = 'N/A';
									$tempo = '-';
								}else{
									$d = new DateFormatter($paciente['conduta']['fon']['data']);
									$tempo = $d->formattedInterval();
								}
								
								
								// BUSCA O RESPONSAVEL POR ESSE PACIENTE NESSA EQUIPE
								$query_resp = "SELECT nome FROM ".$_SESSION['user_Servico']."_usuarios WHERE id_usuario = '".$paciente[$equipe]['responsavel']."' LIMIT 1";
								$mysql_query_resp = mysqli_query($mysqli,$query_resp);
								$responsavel = mysqli_fetch_assoc($mysql_query_resp);
								
								
								// RECUPERA O OBJETIVO DO PACIENTE
								$objetivo = busca_objetivo($mysqli,$paciente_geral['id']);
								
								if($objetivo){
									
									if( $objetivo['previsao_objetivo'] > date("Y-m-d") ){
										$status = "<i class='fa fa-circle text-success'></i>";
									}else{
										$status = "<i class='fa fa-circle text-danger'></i>";
									}
								
								}else{
								
									$status = "<small>Nenhum objetivo</small>";
									
								}
								
								?>
								
								<tr onclick="location.href = 'paciente.php?id=<?php echo $paciente['geral']['id']; ?>';" style="cursor:pointer">
									<td class="hidden-xs"><?php echo $i; ?></td>
									<td><?php echo $paciente['geral']['nome']; ?></td>
									<td class="hidden-xs"><?php echo $responsavel['nome']; ?></td>
									<td class="hidden-xs"><?php echo $paciente['conduta']['fon']['conduta']; ?></td>
									<td class="hidden-xs"><?php echo $tempo; ?></td>
									<td class="hidden-xs last"><?php echo $status; ?></td>
								</tr>
								
						<?php
							}
						?>
						</tbody>

					</table>
					
					
					
					
					<!-- FIM DA TABELA -->
					
				</div>
				
				<?php
				if($i==0){
					echo "
						<script> document.getElementById('tabela_".$equipe."').style.display = 'none'; </script>
						<h3>Nenhum paciente em tratamento!</h3><br><br>";
				}
				?>
				
			</div>
		</div>
		
		
<?php
	
	}

?>