﻿<?php


	// EXIBE TODOS OS PACIENTES QUE EU ATENDO
	function exibir_meus_pacientes($mysqli, $usuario){
		
?>
		
		<!-- PACIENTE -> LISTA ESPECIALIDADE -->
		<div id="tab-meus-pacientes" class="tab-pane fade in active">
			
			<!-- ACESSO PARA CADASTRAR NOVOS PACIENTES -->
			<?php 
				
				if( $usuario['acesso']['cadastrar_paciente'] == TRUE ){
				
			?>
			
				<a href="cadastrar_paciente.php" class="pull-right label label-primary hidden-xs">
					<h4> &nbsp; <i class="fa fa-plus"></i> Novo paciente &nbsp; </h4>
				</a>
				
				<a href="cadastrar_paciente.php" class="pull-right label label-primary hidden-sm hidden-md hidden-lg">
					<h4> &nbsp; <i class="fa fa-plus"></i> &nbsp; </h4>
				</a>
				
			<?php } ?>
			
			
			<div class="panel-heading">
				<h2>Meus pacientes</h2>
			</div>
			
			
			<div class="panel-body">
			
				<div id="tabela_meus_pacientes" >
					<table id="tabela_pacientes_meus" class="table table-hover responsive table-condensed">
						
						<thead>
							<tr>
								<th class="hidden-xs">#</th>
								<th>Paciente</th>
								<th>Diagnóstico</th>
								<th class="hidden-xs">Última conduta</th>
								<th class="hidden-xs">Data</th>
							</tr>
						</thead>
						
						<tbody>
		
						<?php
						
							$active = "";
							
							
							//$query = "SELECT * FROM ".$_SESSION['user_Servico']."_paciente_geral WHERE status = 'ativo' AND $equipe='1' ORDER BY nome";
							
							$tabela_paciente 	= $_SESSION['user_Servico']."_paciente_geral";
							$tabela_equipe		= $_SESSION['user_Servico']."_".$usuario['geral']['equipe'];
							
							$query_1 = " 	SELECT 
												".$tabela_paciente.".id as id
											FROM 
												".$tabela_paciente.", ".$tabela_equipe."
											WHERE 
												".$tabela_paciente.".id = ".$tabela_equipe.".id_paciente AND
												".$tabela_equipe.".responsavel = '".$usuario['geral']['id_usuario']."' AND 
												".$tabela_equipe.".data_termino IS NULL AND 
												".$tabela_paciente.".status = 'ativo' AND 
												".$tabela_paciente.".".$usuario['geral']['equipe']." = '1' 
											ORDER BY nome
											";
											
											
							$mysql_query_1 = mysqli_query($mysqli,$query_1);
							
							
							$i = 0;
							while ( $paciente_geral = mysqli_fetch_assoc($mysql_query_1) ){
								
								$i++;
								
								
								// Busca dados do paciente
								$paciente = busca_paciente($mysqli, $paciente_geral['id']);
								
								
								if($paciente['conduta']['fon'] == FALSE){
									$paciente['conduta']['fon']['conduta'] = 'N/A';
									$tempo = '-';
								}else{
									$d = new DateFormatter($paciente['conduta']['fon']['data']);
									$tempo = $d->formattedInterval();
								}
								
								
								?>
								
								<tr onclick="location.href = 'paciente.php?id=<?php echo $paciente['geral']['id']; ?>';" style="cursor: pointer;">
									<td class="hidden-xs"><?php echo $i; ?></td>
									<td><?php echo $paciente['geral']['nome']; ?></td>
									<td><?php echo $paciente['geral']['diagnostico']; ?></td>
									<td class="hidden-xs"><?php echo $paciente['conduta']['fon']['conduta']; ?></td>
									<td class="hidden-xs"><?php echo $tempo; ?></td>
								</tr>
								
						<?php
							}
						?>
						
						</tbody>
					</table>
					<!-- FIM DA TABELA -->
					
				</div>
				
				<?php
				
				if($i==0){
				
					echo "
						<script> document.getElementById('tabela_meus_pacientes').style.display = 'none'; </script>
						<h3>Nenhum paciente em atendimento!</h3><br><br>
						";
						
				}
				
				?>
				
			</div>
		</div>
	
<?php
	
	}

?>