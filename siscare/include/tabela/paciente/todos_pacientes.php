﻿<?php

	// EXIBE PERFIL DE TODOS OS PACIENTES
	function exibir_pacientes_perfil($mysqli, $acesso){
		
?>
	
		<!-- PACIENTE -> LISTA PERFIL -->
		<div id="tab-pacientes" class="tab-pane fade in">
			
			<!-- ACESSO PARA CADASTRAR NOVOS PACIENTES -->
			<?php 
			
				if( $acesso['cadastrar_paciente'] == TRUE ){
				
			?>
			
				<a href="cadastrar_paciente.php" class="pull-right label label-primary hidden-xs">
					<h4> &nbsp; <i class="fa fa-plus"></i> Novo paciente &nbsp; </h4>
				</a>
				
				<a href="cadastrar_paciente.php" class="pull-right label label-primary hidden-sm hidden-md hidden-lg">
					<h4> &nbsp; <i class="fa fa-plus"></i> &nbsp; </h4>
				</a>
				
			<?php } ?>
			
			
			<div class="panel-heading">
				<h2>Pacientes cadastrados</h2>
			</div>
			
			
			<div class="panel-body">
				
				<!-- DADOS BASICOS DOS PACIENTES -->
				<div id="tabela_pacientes" >
				<table id="tabela_pacientes_todos" class="table hover row-border compact table-condensed">
					
					<thead>
						<tr>
							<th>#</th>
							<th>Paciente</th>
							
							<?php
							
							$query = "SELECT * FROM servicos WHERE codigo_servico = '".$_SESSION['user_Servico']."' LIMIT 1";
							$mysql_query = mysqli_query($mysqli,$query);
							$servicos = mysqli_fetch_assoc($mysql_query);
							
							if($servicos['enf']==1){
								echo "<th class='text-center hidden-xs'>Enfermagem</th>";
							}
							if($servicos['fim']==1){
								echo "<th class='text-center hidden-xs'>Fisioterapia Motora</th>";
							}
							if($servicos['fir']==1){
								echo "<th class='text-center hidden-xs'>Fisioterapia Respiratória</th>";
							}
							if($servicos['fon']==1){
								echo "<th class='text-center hidden-xs'>Fonoaudiologia</th>";
							}
							if($servicos['med']==1){
								echo "<th class='text-center hidden-xs'>Equipe médica</th>";
							}
							if($servicos['nut']==1){
								echo "<th class='text-center hidden-xs'>Nutricionista</th>";
							}
							
							?>
							
						</tr>
					</thead>
					
					<tbody>
						
						<?php
						
						$query = "SELECT id, nome, enf, fon, med, nut, fim, fir FROM ".$_SESSION['user_Servico']."_paciente_geral WHERE status = 'ativo' ORDER BY nome";
						$mysql_query = mysqli_query($mysqli,$query);
						
						$i = 0;
						while ( $paciente_geral = mysqli_fetch_assoc($mysql_query) ){
							
							$i++;
							
						?>
							
							<!-- DADOS DA TABELA PARA CADA PACIENTE -->
							<tr onclick="location.href = 'paciente.php?id=<?php echo $paciente_geral['id']; ?>';" style="cursor: pointer;">
								<td><?php echo $i; ?></td>
								<td><?php echo $paciente_geral['nome']; ?></td>
								
								
								<?php
								
								if($servicos['enf']==1){
									echo "<td class='text-center hidden-xs'>";
									if($paciente_geral['enf']==1){ echo "<i class='fa fa-check'></i>"; }
									echo "</td>";
								}
								if($servicos['fim']==1){
									echo "<td class='text-center hidden-xs'>";
									if($paciente_geral['fim']==1){ echo "<i class='fa fa-check'></i>"; }
									echo "</td>";
								}
								if($servicos['fir']==1){
									echo "<td class='text-center hidden-xs'>";
									if($paciente_geral['fir']==1){ echo "<i class='fa fa-check'></i>"; }
									echo "</td>";
								}
								if($servicos['fon']==1){
									echo "<td class='text-center hidden-xs'>";
									if($paciente_geral['fon']==1){ echo "<i class='fa fa-check'></i>"; }
									echo "</td>";
								}
								if($servicos['med']==1){
									echo "<td class='text-center hidden-xs'>";
									if($paciente_geral['med']==1){ echo "<i class='fa fa-check'></i>"; }
									echo "</td>";
								}
								if($servicos['nut']==1){
									echo "<td class='text-center hidden-xs'>";
									if($paciente_geral['nut']==1){ echo "<i class='fa fa-check'></i>"; }
									echo "</td>";
								}
								
								?>
							</tr>
							
						<?php
						}
						?>
						
					</tbody>
					
				</table>
				</div>
				
				<?php
				
				if($i==0){
				
					echo "
						<script> document.getElementById('tabela_pacientes').style.display = 'none'; </script>
						<h3>Nenhum paciente em tratamento!</h3>
						";
						
				}
				
				?>
				
			</div>
			
		</div>
		
<?php
	
	}

?>