﻿<!-- MODAL EDITAR OBJETIVO -->
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		
		<div class="modal-content">
			
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					Editar Objetivo
				</h4>
			</div>
			
			
			<div class="modal-body">
			<form id="form-editar" data-parsley-validate class="form-horizontal" method="post" action="sql/objetivo/editar.php">
					
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 controls">
					
					
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 controls">
					
						<h3> Objetivo </h3>
						<h4 id='modalDescricao'> Descrição do objetivo </h4>
						<p id='modalPaciente'>
							Nome do paciente <br>
							Objetivo iniciado em: <i>25/2/16</i>
						</p>
						<br>
						
					</div>
					
					
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 controls">
						
						<!-- STATUS -->
						<label class="control-label" for="inputStatus"> Status </label>
						<input class="form-control" type="text" id="inputStatus" name="inputStatus" placeholder="Qual o status atual do paciente?">
						<br>
						
					</div>
					
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 controls">
						
						<!-- PROGRESSO -->
						<label class="control-label" for="inputProgresso"> Progresso </label>
						<select class="form-control" id="inputProgresso" name="inputProgresso" required>
							<option value="0"> 		0% concluído 	</option>
							<option value="10"> 	10% concluído 	</option>
							<option value="20"> 	20% concluído 	</option>
							<option value="30"> 	30% concluído 	</option>
							<option value="40"> 	40% concluído 	</option>
							<option value="50"> 	50% concluído 	</option>
							<option value="60"> 	60% concluído 	</option>
							<option value="70"> 	70% concluído 	</option>
							<option value="80"> 	80% concluído 	</option>
							<option value="90"> 	90% concluído 	</option>
						</select>
						<br>
						
					</div>
					
					<input type="hidden" value="" name="inputIdObjetivo" 	id="inputIdObjetivo">
					
				</div>
				
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				<button type="submit" class="btn btn-primary">Salvar</button>
			</div>
			
			</form>
			
		</div>
		
	</div>
</div>