﻿<!-- MODAL CRIAR OBJETIVO -->
<div class="modal fade modal-criar-objetivo" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		
		<div class="modal-content">
			
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">
					Criar Objetivo
				</h4>
			</div>
			
			
			<div class="modal-body">
			<form id="form-criar" data-parsley-validate class="form-horizontal" method="post" action="sql/objetivo/criar.php">
					
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 controls">
					
					
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 controls">
						<h3> Objetivo </h3>
						<br>
					</div>
					
					
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 controls">
					
						<!-- PACIENTE -->
						<label class="control-label" for="inputPaciente"> Paciente </label>
						<select class="form-control" id="inputPaciente" name="inputPaciente" required>
							
							<?php
							
							$query = "	SELECT 
											nome, id 
										FROM 
											".$_SESSION['user_Servico']."_paciente_geral 
										WHERE
											".$usuario['geral']['equipe']." = '1' 
										ORDER BY nome
									";
									
							foreach($mysqli->query($query) as $row) {  
							
								$query_2 = "	SELECT 
													COUNT(*) as total
												FROM 
													".$_SESSION['user_Servico']."_objetivo 
												WHERE
													id_paciente = '".$row['id']."' AND
													equipe = '".$usuario['geral']['equipe']."' AND
													progresso < '100'
											";
								
								$mysql_query_2 = mysqli_query($mysqli,$query_2) or exit(mysql_error());
								$total_2 = mysqli_fetch_assoc($mysql_query_2);
								
								if( $total_2['total'] == 0 ){
									echo "<option value='".$row['id']."'>".$row['nome']."</option>";
								}
								
							}
							
							
							
							?>
							
						</select>
						<br>
						
						
					</div>
					
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 controls">
						
						<!-- OBJETIVO -->
						<label class="control-label" for="inputObjetivo"> Objetivo </label>
						
						<?php
						if( $usuario['geral']['equipe'] == 'fon' ){
						?>
							
							<select class="form-control" id="inputObjetivo" name="inputObjetivo" required>
								<option value=''>										Qual o objetivo?</option>
								<option value='Retirada de VAA'>						Retirada de VAA</option>
								<option value='Progressão de dieta'>					Progressão de dieta</option>
								<option value='Manter dieta liberada'>					Manter dieta liberada</option>
								<option value='Sistematizar a deglutição de saliva'>	Sistematizar a deglutição de saliva</option>
								<option value='Iniciar treino via oral'>				Iniciar treino via oral</option>
								<option value='Liberar treino via oral'>				Liberar treino via oral</option>
								<option value='Liberar dieta'>							Liberar dieta</option>
								<option value='Alta fonoaudiologica'>					Alta fonoaudiológica</option>
							</select>
							
						<?php	}else{	?>
						
							<input class="form-control" type="text" id="inputObjetivo" name="inputObjetivo" placeholder="Qual o objetivo?">
							
						<?php	}	?>
						
						<br>
						
					</div>
					
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 controls">
						
						<!-- META -->
						<label class="control-label" for="inputMeta"> Meta </label>
						<input class="form-control" type="date" id="inputMeta" name="inputMeta">
						<br>
						
					</div>
					
					<!-- EQUIPE -->
					<input class="form-control" type="hidden" id="inputEquipe" name="inputEquipe" value="<?php echo $usuario['geral']['equipe']; ?>" >
					
				</div>
				
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				<button type="submit" class="btn btn-primary">Criar</button>
			</div>
			
			</form>
			
		</div>
		
	</div>
</div>