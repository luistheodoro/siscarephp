<?php
// MODAL - CRIAR ATENDIMENTO
function criar_avaliacao_fir($mysqli,$paciente){
?>

<!-- MODAL -> Avaliação FONOAUDIOLOGIA -->
<div class="modal fade modal-avaliacao-fir" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		
		<div class="modal-content">
			
			
			<!-- TITULO -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Nova Avaliação</h4>
			</div>
			
			
			<div class="modal-body">
			<form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/fir/avaliacao/criar.php">
				
				<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 controls">
					
					<h3> <?php echo $paciente['geral']['nome']; ?> </h3>
					<br>
					
					
					<h3> Nível de consciencia </h3>
					
					<!-- NIVEL DE CONSCIENCIA -->
					<label class="control-label" for="inputEscalaRamsay"> Escala Ramsay</label>
					<select class="form-control" id="inputEscalaRamsay" name="inputEscalaRamsay">
						
						<option value="N/A"> 
							N/A
						</option>
						<option value="Grau 1"> 
							Grau 1
						</option>
						<option value="Grau 2"> 
							Grau 2
						</option>
						<option value="Grau 3"> 
							Grau 3
						</option>
						<option value="Grau 4"> 
							Grau 4
						</option>
						<option value="Grau 5"> 
							Grau 5
						</option>
						<option value="Grau 6"> 
							Grau 6
						</option>
						
					</select>
					<br>
					<div id="comentarioRamsay"><h3><?php echo $paciente['escala']['Ramsay']; ?></h3></div>
					<br>
					
					<label class="control-label" for="inputEscalaRASS"> Escala RASS</label>
					<select class="form-control" id="inputEscalaRASS" name="inputEscalaRASS">
						
						<option value="N/A"> 
							N/A
						</option>
						<option value="+4"> 
							+4
						</option>
						<option value="+3"> 
							+3
						</option>
						<option value="+2"> 
							+2
						</option>
						<option value="+1"> 
							+1
						</option>
						<option value="0"> 
							0
						</option>
						<option value="-1"> 
							-1
						</option>
						<option value="-2"> 
							-2
						</option>
						<option value="-3"> 
							-3
						</option>
						<option value="-4"> 
							-4
						</option>
						<option value="-5"> 
							-5
						</option>
						
					</select>
					<br>
					<div id="comentarioRASS"><h3><?php echo $paciente['escala']['RASS']; ?></h3></div>
					
					
					<!-- DIVISOR -->
					<div class="col-sm-12 controls">
						<hr/>
					</div>
					
					
					<!-- FREQUENCIA RESPIRATORIA -->
					<label class="control-label" for="inputFrequenciaRespiratoria"> Frequencia respiratória</label>
					<select class="form-control" id="inputFrequenciaRespiratoria" name="inputFrequenciaRespiratoria">
						
						<option value="Eupneico"> 
							Eupneico
						</option>
						<option value="Taquipneico"> 
							Taquipneico
						</option>
						<option value="Bradipneico"> 
							Bradipneico
						</option>
						
					</select>
					<br>
					
					
					<!-- PADRÃO RESPIRATORIO -->
					<label class="control-label" for="inputPadraoRespiratorio"> Padrão respiratório </label>
					<select class="form-control" id="inputPadraoRespiratorio" name="inputPadraoRespiratorio">
					
						<option value="Satisfatório">
							Satisfatório
						</option>
						<option value="Esforço expiratório">
							Esforço expiratório
						</option>
						<option value="Cheyne Stokes">
							Cheyne Stokes
						</option>
						<option value="Superficial">
							Superficial
						</option>
						
					</select>
					<br>
					
					<!-- MOTIVO DA Avaliação -->
					<label class="control-label" for="inputMotivoAvaliacao"> Motivo da Avaliação </label>
					<textarea class="form-control" name="inputMotivoAvaliacao" id="inputMotivoAvaliacao" placeholder="Breve descrição sobre o motivo da Avaliação"></textarea>
					<br>
					
					<!-- COMENTARIO -->
					<label class="control-label" for="inputComentario"> Comentários </label>
					<textarea class="form-control" name="inputComentario" id="inputComentario" placeholder="Informações adicionais da Avaliação"></textarea>
					<br>
					
					<!-- FREQUÊNCIA -->
					<label class="control-label" for="inputFrequencia"> Frequência dos atendimentos </label>
					<select class="form-control" id="inputFrequencia" name="inputFrequencia" required>
						
						<option value="1x por semana">
							1x por semana
						</option>
						
						<option value="2x por semana">
							2x por semana
						</option>
						
						<option value="3x por semana">
							3x por semana
						</option>
						
						<option value="4x por semana">
							4x por semana
						</option>
						
						<option value="Diário">
							Diário
						</option>
						
						<option value="1x por mês">
							1x por mês
						</option>
						
						<option value="2x por mês">
							2x por mês
						</option>
						
						<option value="3x por mês">
							3x por mês
						</option>
						
						<option value="1x a cada 2 meses">
							1x a cada 2 meses
						</option>
						
					</select>
					<br>
					
					<!-- DIVISOR -->
					<div class="col-sm-12 controls">
						<hr/>
					</div>
					
					<h3> Escala de avaliação </h3>
					
					<label class="control-label" for="inputBraden"> BRADEN </label>
					<input class="form-control" type="text" name="inputBraden" id="inputBraden" value="<?php echo $paciente['escala']['BRADEN']; ?>">
					<br>
					
					<!-- DIVISOR -->
					<div class="col-sm-12 controls">
						<hr/>
					</div>
				
					<h3> Terapia </h3>
					<br>
					
					<!-- TERAPIA -->
					<label class="control-label" for="inputTerapiaPrincipal"> Terapia principal </label>
					<select class="form-control" id="inputTerapiaPrincipal" name="inputTerapiaPrincipal" required>
						<option>Qual foi a terapia?</option>
						<option value="Estimulo de deglutição de saliva">	Estimulo de deglutição de saliva	</option>
						<option value="Treino via oral">					Treino via oral						</option>
						<option value="Exercício para deglutição">			Exercício para deglutição			</option>
						<option value="Exercício para voz">					Exercício para voz					</option>
						<option value="Exercício para motricidade">			Exercício para motricidade			</option>
						<option value="Exercício para linguagem">			Exercício para linguagem			</option>
						<option value="Acompanhamento de refeição">			Acompanhamento de refeição			</option>
						<option value="Orientação">							Orientação							</option>
					</select>
					<br>
					
					<!-- TERAPIA ADICIONAL -->
					<label class="control-label" for="inputTerapia"> Terapias adicionais </label>
					<br>
					<div class="checkbox">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_1" id="adicional_1">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Estimulo de deglutição de saliva
							</label>
							<br><br>
							
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_2" id="adicional_2">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Treino via oral
							</label>
							<br><br>
							
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_3" id="adicional_3">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Exercício para deglutição
							</label>
							<br><br>
							
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_4" id="adicional_4">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Exercício para voz
							</label>
							
						</div>
						
						<div class="col-xs-12 col-sm-12 hidden-md hidden-lg">
							<br>
						</div>
						
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_5" id="adicional_5">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Exercício para motricidade
							</label>
							<br><br>
							
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_6" id="adicional_6">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Exercício para linguagem
							</label>
							<br><br>
							
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_7" id="adicional_7">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Acompanhamento de refeição
							</label>
							<br><br>
							
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_8" id="adicional_8">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Orientação
							</label>
							
						</div>
					</div>
					<br><br><br>
					
					<!-- DIVISOR -->
					<div class="col-sm-12 controls">
						<hr/>
					</div>
					
					<h3> Conduta </h3>
					
					<!-- CONDUTA -->
					<label class="control-label" for="inputConduta"> Conduta </label>
					<select class="form-control" id="inputConduta" name="inputConduta" required onchange="selecionaConduta(this)">
						<option value="Mantida">						Mantida							</option>
						<option value="Liberação de pequeno volume">	Liberação de pequeno volume		</option>
						<option value="Liberação de dieta">				Liberação de dieta				</option>
						<option value="Liberação de dieta por prazer">	Liberação de dieta por prazer	</option>
						<option value="Evolução de Consistência">		Evolução de Consistência		</option>
						<option value="Regressão de Consistência">		Regressão de Consistência		</option>
						<option value="Suspensão de dieta via oral">	Suspensão de dieta via oral		</option>
						<option value="Uso de espessante">				Uso de espessante				</option>
						<option value="Gerenciamento">					Gerenciamento					</option>
					</select>
					<br>
					
					
					<!-- ADAPTAÇÃO DE DIETA -->
					<label class="control-label" for="inputDieta" id="label_dieta" style="display:none"> Consistência de dieta </label>
					<select class="form-control" id="inputDieta" name="inputDieta" style="display:none">
						
						<option value="Pastosa heterogênea"> 
							Pastosa heterogênea
						</option>
						<option value="Pastosa homogênea"> 
							Pastosa homogênea 
						</option>
						<option value="Pastosa (semissólida)"> 
							Pastosa (semissólida)
						</option>
						<option value="Branda"> 
							Branda
						</option>
						<option value="Geral"> 
							Geral
						</option>
						
					</select>
					<br>
					
					
					<!-- ADAPTAÇÃO DE LIQUIDOS -->
					<label class="control-label" for="inputLiquido" id="label_liquido" style="display:none"> Consistência de Líquidos </label>
					<select class="form-control" id="inputLiquido" name="inputLiquido" style="display:none">
						
						<option value="Líquido Fino">
							Líquido Fino
						</option>
						<option value="Líquido Néctar">
							Líquido Néctar
						</option>
						<option value="Líquido Mel">
							Líquido Mel 
						</option>
						<option value="Líquido Pudim">
							Líquido Pudim
						</option>
						<option value="Suspenso">
							Suspenso 
						</option>
						
					</select>
					<br>
					
					<!-- ESCALA ASHA NOMS -->
					<label class="control-label" for="inputASHA_NOMS"> ASHA NOMS </label>
					<div id="comentarioASHA"><h3><?php echo $paciente['escala']['ASHA_NOMS']; ?></h3></div>
					
					<input type="hidden" name="vaa_ASHA_NOMS" 	id="vaa_ASHA_NOMS" 	value="<?php echo $paciente['espec']['VAA']; ?>">
					<input type="hidden" name="ASHA_atual" 		id="ASHA_atual" 	value="">
					<input type="hidden" name="inputASHA_NOMS" 	id="inputASHA_NOMS" value="">
					
					
					<input type="hidden" value="<?php echo $paciente['geral']['id']; ?>" name="inputIDPaciente" id="inputIDPaciente">
					
					<!-- BOT�O DE ENVIAR -->
					<input type="submit" value=" &nbsp; Submeter Avaliação &nbsp; " class="btn btn-primary pull-right">
					
				</div>
			
			</form>
			
			</div>
		</div>
		
	</div>
</div>


	<script>

		document.getElementById("inputDieta").addEventListener("change", ASHA);
		document.getElementById("inputLiquido").addEventListener("change", ASHA);

		document.getElementById("inputDietaAtual").addEventListener("change", ASHA);
		document.getElementById("inputLiquidoAtual").addEventListener("change", ASHA);

		document.getElementById("inputTerapiaPrincipal").addEventListener("change", ASHA);

		document.getElementById("inputConduta").addEventListener("change", ASHA);
		document.getElementById("inputConduta").addEventListener("change", Conduta2);
		
		
		
		//------------------------------------------------------------------------------------
		
		document.getElementById("inputEscalaRASS").addEventListener("change", RASS);
		document.getElementById("inputEscalaRamsay").addEventListener("change", Ramsay);
		
		
	</script>

	
<?php
	
}

?>