	
	function RASS(){
		
		
		Escala_RASS	= document.getElementById("inputEscalaRASS").value
		
		
		
		switch (Escala_RASS){
		
			case "+4":
				comentario = "<b>Combativo</b> Francamente combativo, violento, levando a perigo imediato da equipe de saúde.";
				break;
				
			case "+3":
				comentario = "<b>Muito agitado</b> Agressivo, pode puxar tubos e cateteres.";
				break;
				
			case "+2":
				comentario = "<b>Agitado</b> Movimentos não-intencionais freqüentes, briga com o respirador (se estiver em ventilação mecânica).";
				break;
			
			case "+1":
				comentario = "<b>Inquieto</b> Ansioso, inquieto, mas não agressivo.";
				break;
				
			case "0":
				comentario = "<b>Alerta e calmo</b>";
				break;
				
			case "-1":
				comentario = "<b>Torporoso</b> Não completamente alerta, mas mantém olhos abertos e contato ocular ao estímulo verbal por > 10seg.";
				break;
				
			case "-2":
				comentario = "<b>Sedado leve</b> Acorda rapidamente, e mantém contato ocular ao estímulo verbal por < 10seg.";
				break;
				
			case "-3":
				comentario = "<b>Sedado moderado</b> Movimento ou abertura dos olhos, mas sem contato ocular com o examinador.";
				break;
				
			case "-4":
				comentario = "<b>Sedado profundamente</b> Sem resposta ao estímulo verbal, mas tem movimentos ou abertura ocular ao estímulo tátil / físico.";
				break;
				
			case "-5":
				comentario = "<b>Coma</b> Sem resposta aos estímulos verbais ou exame físico.";
				break;
				
			case "N/A":
				comentario = "Não Avaliado";
				break;
				
		}

		
		document.getElementById("comentarioRASS").innerHTML = "<h3>" + Escala_RASS + "</h3> <i>" + comentario + "</i>";
		
		
	}
	
	
	function Ramsay(){
		
		
		Escala_Ramsay	= document.getElementById("inputEscalaRamsay").value
		
		
		
		switch (Escala_Ramsay){
		
			case "Grau 1":
				comentario = "Paciente ansioso, agitado.";
				break;
				
			case "Grau 2":
				comentario = "Paciente cooperativo, orientado, tranquilo.";
				break;
				
			case "Grau 3":
				comentario = "Paciente sonolento, atendendo aos comandos.";
				break;
			
			case "Grau 4":
				comentario = "Paciente dormindo, responde rapidamente ao estímulo glabelar ou ao estímulo sonoro vigoroso.";
				break;
				
			case "Grau 5":
				comentario = "Paciente ormindo, responde lentamente ao estímulo glabelar ou ao estímulo sonoro vigoroso";
				break;
				
			case "Grau 6":
				comentario = "Paciente dormindo, sem resposta.";
				break;
				
			case "N/A":
				comentario = "Não Avaliado";
				break;
				
		}

		
		document.getElementById("comentarioRamsay").innerHTML = "<h3>" + Escala_Ramsay + "</h3> <i>" + comentario + "</i>";
		
		
	}