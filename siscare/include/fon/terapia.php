﻿<?php
// MODAL - CRIAR ATENDIMENTO
function criar_atendimento_fon($mysqli,$paciente){
?>


<!-- MODAL -> NOVO ATENDIMENTO FONOAUDIOLOGIA -->
<div class="modal fade modal-fon" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		
		<div class="modal-content">
			
			
			<!-- TITULO -->
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Novo atendimento</h4>
			</div>
			
			
			<div class="modal-body">
			<form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/fon/terapia/criar.php">
					
				<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12 controls">
					
					<h3> Atendimento fonoaudiológico </h3>
					
					<div class="row">
						<div class="col-md-8 col-sm-12 col-xs-12 controls">
							
							<!-- TERAPIA -->
							<label class="control-label" for="inputTerapiaPrincipal"> Terapia principal </label>
							<select class="form-control" id="inputTerapiaPrincipal" name="inputTerapiaPrincipal" required>
								<option>Qual foi a terapia?</option>
								<option value="Estimulo de deglutição de saliva">	Estimulo de deglutição de saliva	</option>
								<option value="Treino via oral">					Treino via oral						</option>
								<option value="Exercício para deglutição">			Exercício para deglutição			</option>
								<option value="Exercício para voz">					Exercício para voz					</option>
								<option value="Exercício para motricidade">			Exercício para motricidade			</option>
								<option value="Exercício para linguagem">			Exercício para linguagem			</option>
								<option value="Acompanhamento de refeição">			Acompanhamento de refeição			</option>
								<option value="Orientação">							Orientação							</option>
							</select>
							<br>
							
						</div>
						
						<div class="col-md-4 col-sm-12 col-xs-12 controls">
						
							<label class="control-label" for="inputDataTerapia"> Data da terapia </label>
							<input class="form-control" type="date" name="inputDataTerapia" id="inputDataTerapia" value="<?php echo date('Y-m-d'); ?>" ></input>
							<br>
							
						</div>
					</div>
					
					<!-- TERAPIA ADICIONAL -->
					<label class="control-label" for="inputTerapia"> Terapias adicionais </label>
					<br>
					<div class="checkbox">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_1" id="adicional_1">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Estimulo de deglutição de saliva
							</label>
							<br><br>
							
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_2" id="adicional_2">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Treino via oral
							</label>
							<br><br>
							
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_3" id="adicional_3">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Exercício para deglutição
							</label>
							<br><br>
							
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_4" id="adicional_4">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Exercício para voz
							</label>
							
						</div>
						
						<div class="col-xs-12 col-sm-12 hidden-md hidden-lg">
							<br>
						</div>
						
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_5" id="adicional_5">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Exercício para motricidade
							</label>
							<br><br>
							
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_6" id="adicional_6">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Exercício para linguagem
							</label>
							<br><br>
							
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_7" id="adicional_7">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Acompanhamento de refeição
							</label>
							<br><br>
							
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="adicional_8" id="adicional_8">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Orientação
							</label>
							
						</div>
					</div>
					<br>
					
					<!-- O QUE FOI AVALIADO -->
					<label class="control-label" for="inputTerapia"><br> O que foi acompanhado? </label>
					<br>
					<div class="checkbox">
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="aval_dieta" id="aval_dieta">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Refeição
							</label>
							<br><br>
							
						</div>
						
						<div class="col-xs-12 col-sm-12 hidden-md hidden-lg">
							<br>
						</div>
						
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						
							<label class="hover">
								<div class="icheckbox_flat-green checked hover" style="position: relative;">
									<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" 
											name="aval_liquido" id="aval_liquido">
									<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
								</div> 
								Liquido
							</label>
							<br><br>
							
						</div>
					</div>
					<br><br><br>
					
					
					<!-- EVOLUÇÃO -->
					<label class="control-label" for="inputEvolucao"> Evolução </label>
					<textarea class="form-control" rows="5" id="inputEvolucao" name="inputEvolucao" placeholder=""></textarea>
					<br>
					
					<!-- DIVISOR -->
					<div class="col-sm-12 controls">
						<hr/>
					</div>
					
					<h3> Escala de avaliação </h3>
					
					<label class="control-label" for="inputBraden"> BRADEN </label>
					<input class="form-control" type="text" name="inputBraden" id="inputBraden" value="<?php echo $paciente['escala']['BRADEN']; ?>">
					<br>
					
					
					<!-- DIVISOR -->
					<div class="col-sm-12 controls">
						<hr/>
					</div>
					
					
					<h3> Conduta </h3>
					
					<!-- CONDUTA -->
					<label class="control-label" for="inputConduta"> Conduta </label>
					<select class="form-control" id="inputConduta" name="inputConduta" required onchange="selecionaConduta2(this)">
						<option value="Mantida">						Mantida							</option>
						<option value="Liberação de pequeno volume">	Liberação de pequeno volume		</option>
						<option value="Liberação de dieta">				Liberação de dieta				</option>
						<option value="Liberação de dieta por prazer">	Liberação de dieta por prazer	</option>
						<option value="Evolução de consistência">		Evolução de consistência		</option>
						<option value="Regressão de consistência">		Regressão de consistência		</option>
						<option value="Suspensão de dieta via oral">	Suspensão de dieta via oral		</option>
						<option value="Uso de espessante">				Uso de espessante				</option>
						<option value="Gerenciamento">					Gerenciamento					</option>
					</select>
					<br>
					
					
					<!-- ADAPTAÇÃO DE DIETA -->
					<label class="control-label" for="inputDieta"> Consistência de dieta </label>
					<input disabled="disabled" class="form-control" type="text" id="dietaAtual" name="dietaAtual" placeholder="Dieta atual">
					
					<select class="form-control" id="inputDieta" name="inputDieta" style="display: none">
						<option 
							<?php if($paciente['espec']['dieta']=='Pastosa homogênea'){echo " selected='selected' ";} ?> value="Pastosa homogênea"> 
							Pastosa homogênea 
						</option>
						<option 
							<?php if($paciente['espec']['dieta']=='Pastosa heterogênea'){echo " selected='selected' ";} ?> value="Pastosa heterogênea"> 
							Pastosa heterogênea 
						</option>
						<option 
							<?php if($paciente['espec']['dieta']=='Pastosa (semissólida)'){echo " selected='selected' ";} ?> value="Pastosa (semissólida)"> 
							Pastosa (semissólida)
						</option>
						<option 
							<?php if($paciente['espec']['dieta']=='Branda'){echo " selected='selected' ";} ?> value="Branda"> 
							Branda
						</option>
						<option 
							<?php if($paciente['espec']['dieta']=='Geral'){echo " selected='selected' ";} ?> value="Geral"> 
							Geral 
						</option>
					</select>
					<br>
					
					
					<!-- ADAPTAÇÃO DE LIQUIDOS -->
					<label class="control-label" for="inputLiquido"> Consistência de líquidos </label>
					<input disabled="disabled" class="form-control" type="text" id="liquidoAtual" name="liquidoAtual" placeholder="Liquido atual">
					
					<select class="form-control" id="inputLiquido" name="inputLiquido"  style="display: none">
						<option 
							<?php if($paciente['espec']['liquido']=='Líquido Fino'){echo " selected='selected' ";} ?> value="Líquido Fino">
							Líquido Fino
						</option>
						<option
							<?php if($paciente['espec']['liquido']=='Líquido Néctar'){echo " selected='selected' ";} ?> value="Líquido Néctar">
							Líquido Néctar
						</option>
						<option
							<?php if($paciente['espec']['liquido']=='Líquido Mel'){echo " selected='selected' ";} ?> value="Líquido Mel">
							Líquido Mel
						</option>
						<option
							<?php if($paciente['espec']['liquido']=='Líquido Pudim'){echo " selected='selected' ";} ?> value="Líquido Pudim">
							Líquido Pudim
						</option>
						<option
							<?php if($paciente['espec']['liquido']=='Suspenso'){echo " selected='selected' ";} ?> value="Suspenso">
							Suspenso
						</option>
					</select>
					<br>
					
					
					<!-- COMENTARIO -->
					<label class="control-label" for="inputLiquido"> Comentários sobra a conduta </label>
					<textarea disabled="disabled" class="form-control" id="comentarioAtual">Conduta mantida.</textarea>
					
					<textarea class="form-control" id="inputComentario" name="inputComentario" style="display: none"></textarea>
					<br>
					
					
					<!-- ESCALA ASHA NOMS -->
					<label class="control-label" for="inputASHA_NOMS"> ASHA NOMS </label>
					<div id="comentarioASHA"><h3><?php echo $paciente['escala']['ASHA_NOMS']; ?></h3></div>
					
					<input type="hidden" name="vaa_ASHA_NOMS" 	id="vaa_ASHA_NOMS" 	value="<?php echo $paciente['espec']['VAA']; ?>">
					<input type="hidden" name="ASHA_atual" 		id="ASHA_atual" 	value="<?php echo $paciente['escala']['ASHA_NOMS']; ?>">
					<input type="hidden" name="inputASHA_NOMS" 	id="inputASHA_NOMS" value="<?php echo $paciente['escala']['ASHA_NOMS']; ?>">
					
					
					<input type="hidden" value="fon" name="inputEquipe" id="inputEquipe">
					<input type="hidden" value="<?php echo $paciente['geral']['id']; ?>" name="inputIDPaciente" id="inputIDPaciente">
					
					<br>
					
				</div>
					
				
				
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				<button type="submit" class="btn btn-primary">Criar terapia</button>
			</div>
			
			</form>
			
			
		</div>
	</div>
</div>


	<script>

		document.getElementById("inputDieta").addEventListener("change", ASHA);
		document.getElementById("inputLiquido").addEventListener("change", ASHA);

		document.getElementById("inputTerapiaPrincipal").addEventListener("change", ASHA);

		document.getElementById("inputConduta").addEventListener("change", ASHA);
		document.getElementById("inputConduta").addEventListener("change", Conduta);
		
	</script>
	
	
	
<?php
	
}

?>