﻿<ul class="list-unstyled timeline">
	
	<?php
	
	// BUSCA AS ULTIMAS CONDUTAS
	$query = "SELECT * FROM ".$_SESSION['user_Servico']."_terapia WHERE equipe='fon' AND id_paciente = '".$paciente['geral']['id']."' ORDER BY data desc ";
	$mysql_query = mysqli_query($mysqli,$query);
	
	while ( $historio = mysqli_fetch_array($mysql_query) ){
		
		// FORMATA A DATA
		$d = new DateFormatter($historio['data']);
		$tempo = $d->formattedInterval();
		
		// LIMITA O TAMANHO DOS TEXTOS
		$comentario = limitarTexto($historio['comentario'], $limite = 100);
		$evolucao = limitarTexto($historio['evolucao'], $limite = 100);
			
		// VERIFICA SE A CONDUTA FOI MANTIDA
		if( $historio['conduta'] != 'Mantida' ){
			
			
			// PROCURA O NOME DA FONOAUDIOLOGA RESPONSAVEL
			$query_fono = "SELECT nome FROM ".$_SESSION['user_Servico']."_usuarios WHERE id_usuario='".$historio['id_usuario']."' ";
			$mysql_query_fono = mysqli_query($mysqli,$query_fono);
			$nome_responsavel = mysqli_fetch_assoc($mysql_query_fono);
			
			
			// IMPRIME O RESULTADO DA CONDUTA
			echo "
				<li>
					<div class='block_2'>
						<div class='block_content'>
							<h2 class='title'>
								<a>Conduta: ".$historio['conduta']."</a>
							</h2>
							<div class='byline'>
								<span>".$tempo."</span> por <a href='perfil.php?id=".$historio['id_usuario']."'>".$nome_responsavel['nome']."</a>
							</div>
							<p class='excerpt'>
								".$comentario." <br> 
								<a class='pull-right' tabindex='-1' role='menuitem' data-toggle='modal_ver_terapia' href='include/geral/terapia.php?id=".$historio['id']."' style='cursor: pointer;'>
									Ver&nbsp;Mais <i class='fa fa-long-arrow-right'></i> 
								</a>
							</p>
						</div>
					</div>
				</li>
			";
			
		}
		
		// IMPRIME A TERAPIA
		echo "
			<li>
				<div class='block_2'>
					<div class='block_content'>
						<h2 class='title'>
							<a>Atendimento: ".$historio['terapia']."</a>
						</h2>
						<div class='byline'>
							<span>".$tempo."</span> por <a href='perfil.php?id=".$historio['id_usuario']."'>".$nome_responsavel['nome']."</a>
						</div>
						<p class='excerpt'>
							".$historio['terapia_2']."<br>".$evolucao." <br> 
							<a class='pull-right' tabindex='-1' role='menuitem' data-toggle='modal_ver_terapia' href='include/geral/terapia.php?id=".$historio['id']."' style='cursor: pointer;'>
								Ver&nbsp;Mais <i class='fa fa-long-arrow-right'></i> 
							</a>
						</p>
					</div>
				</div>
			</li>
		";
		
		
	}
	
	
	?>
	
</ul>