﻿<?php
// MODAL - ALTA FONOAUDIOLOGIA
function alta_fon($mysqli,$paciente){
?>

<!-- MODAL ALTA -> FONOAUDIOLOGIA -->
<div class="modal fade modal-alta-fon" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		
		<div class="modal-content">

			<form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/fon/alta/alta.php">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Alta Fonoaudiológica</h4>
				</div>
			
				<div class="modal-body">
				
					<h3> Deseja dar alta fonoaudiológica para <?php echo $paciente['geral']['nome']; ?>? </h3>
					
					<input type="hidden" value="fon" name="inputEquipe" id="inputEquipe">
					<input type="hidden" value="alta" name="inputAlta" id="inputAlta">
					<input type="hidden" value="<?php echo $paciente['geral']['id']; ?>" name="inputIDPaciente" id="inputIDPaciente">
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Confirmar</button>
				</div>
			
			</form>
			
			
		</div>
	</div>
</div>


<?php
	
}


// MODAL - SUSPENSÃO FONOAUDIOLOGIA
function suspensao_fon($mysqli,$paciente){
?>

<!-- MODAL SUSPENSÃO -> FONOAUDIOLOGIA -->
<div class="modal fade modal-suspensao-fon" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		
		<div class="modal-content">
		
			<form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/fon/alta/alta.php">
			
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Suspensão do atendimento de fonoaudiologia</h4>
				</div>
				
				<div class="modal-body">
					
					<h3> Deseja suspender o atendimento fonoaudiológico para <?php echo $paciente['geral']['nome']; ?>? </h3>
					
					<input type="hidden" value="fon" name="inputEquipe" id="inputEquipe">
					<input type="hidden" value="suspensão" name="inputAlta" id="inputAlta">
					<input type="hidden" value="<?php echo $paciente['geral']['id']; ?>" name="inputIDPaciente" id="inputIDPaciente">
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Confirmar</button>
				</div>
			
			</form>
			
			
		</div>
		
	</div>
</div>


<?php
	
}

?>