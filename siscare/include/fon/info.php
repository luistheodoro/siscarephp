﻿<?php
// DADOS DO PACIENTE
function paciente_fon($mysqli,$acesso,$paciente,$tab){
?>
	
	<div id="tab-fon" class="tab-pane fade in <?php if ($tab=='fon'){ echo "active"; } ?>">
		
		<!-- PACIENTE -> FONOAUDIOLOGIA -->
		<div class="row">
			
			<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 controls">
				
				<!-- DADOS BASICOS -->
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 controls">
					<br>
					
					<span class="title"><b>Fonoaudiólogia Responsável</b></span>
					<p>
						<?php 
						$query = "SELECT nome FROM ".$_SESSION['user_Servico']."_usuarios WHERE id_usuario='".$paciente['fon']['responsavel']."' ";
						$mysql_query = mysqli_query($mysqli,$query);
						$nome_responsavel = mysqli_fetch_assoc($mysql_query);
						echo $nome_responsavel['nome'];
						?>
					</p>
					
					<span class="title"><b>Frequência de atendimento</b></span>
					<p><?php echo $paciente['fon']['frequencia']; ?></p>
					
					
					<?php
						// BUSCA AS ULTIMAS CONDUTAS
						$query = "SELECT * FROM ".$_SESSION['user_Servico']."_terapia WHERE equipe='fon' AND id_paciente = '".$paciente['geral']['id']."' AND aval_dieta='1' ORDER BY data desc LIMIT 1";
						$mysql_query = mysqli_query($mysqli,$query);
						$aval_dieta = mysqli_fetch_assoc($mysql_query);
						
						if($aval_dieta['data']<>""){
							
							// FORMATA A DATA
							$d = new DateFormatter($aval_dieta['data']);
							$tempo = $d->formattedInterval();
							
							// VERIFICA HA QUANTO TEMPO FOI A ULTIMA AVALIAÇÃO DE DIETA
							$date1 = new DateTime(date('Y-m-d', strtotime($aval_dieta['data'])));
							$date2 = new DateTime(date('Y-m-d'));
							$diff = $date1->diff($date2)->days;

							if($diff > 30){
								echo '
								<span class="title"><b>Última avaliação de dieta</b></span>
								<p class="red">'.$tempo.'</p>
								';
							}else{
								echo '
								<span class="title"><b>Última avaliação de dieta</b></span>
								<p>'.$tempo.'</p>
								';
							}
							
						}
					?>
					
					<?php
						// BUSCA AS ULTIMAS CONDUTAS
						$query = "SELECT * FROM ".$_SESSION['user_Servico']."_terapia WHERE equipe='fon' AND id_paciente = '".$paciente['geral']['id']."' AND aval_liquido='1' ORDER BY data desc LIMIT 1";
						$mysql_query = mysqli_query($mysqli,$query);
						$aval_liquido = mysqli_fetch_assoc($mysql_query);
						
						if($aval_liquido['data']<>""){
							
							// FORMATA A DATA
							$d = new DateFormatter($aval_liquido['data']);
							$tempo = $d->formattedInterval();		
							
							// VERIFICA HA QUANTO TEMPO FOI A ULTIMA AVALIAÇÃO DE LIQUIDO
							$date1 = new DateTime(date('Y-m-d', strtotime($aval_liquido['data'])));
							$date2 = new DateTime(date('Y-m-d'));
							$diff = $date1->diff($date2)->days;
							
							if($diff > 30){
								echo '
								<span class="title"><b>Última avaliação de líquido</b></span>
								<p class="red">'.$tempo.'</p>
								';
							}else{
								echo '
								<span class="title"><b>Última avaliação de líquido</b></span>
								<p>'.$tempo.'</p>
								';
							}
							
						}
					?>
					
					
				</div>
				
				<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 controls">
					
					<br>
					<?php
					if($paciente['espec']['VAA'] != NULL){
						echo '
							<span class="title"><b>Via Alternativa de Alimentação</b></span>
							<p>'.$paciente['espec']['VAA'].'</p>
						';
					}
					
					if($paciente['espec']['TQT'] != NULL){
						echo '
							<span class="title"><b>Traqueostomia</b></span>
							<p>'.$paciente['espec']['TQT'].'</p>
						';
					}
					
					if($paciente['espec']['dieta'] != NULL){
						echo '
							<span class="title"><b>Dieta liberada</b></span>
							<p>'.$paciente['espec']['dieta'].'</p>
						';
					}
					
					if($paciente['espec']['liquido'] != NULL){
						echo '
							<span class="title"><b>Consistência do líquido</b></span>
							<p>'.$paciente['espec']['liquido'].'</p>
						';
					}
					
					?>
					
				</div>
				
			</div>
			
			
			<!-- ALERTAS -->
			<div class="col-lg-4 col-md-6 col-sm-6 col-xs-12 controls">
				<br>
				
				<?php
					
				$query = "	SELECT * FROM 
								".$_SESSION['user_Servico']."_risco 
							WHERE
								id_paciente = '".$paciente['geral']['id']."' AND
								equipe = 'fon'
							ORDER BY 
								id DESC 
							";
				
				foreach($mysqli->query($query) as $risco) {  
				
					echo"
						<div class='alert alert-danger alert-dismissible fade in' role='alert'>
							<h4>".$risco['risco']."</h4>
							<h5>".equipe($risco['equipe'],2)."</h5>
						</div>
					";
					
				} 
				
				?>
				
			</div>
			
			
			<!-- DIVISOR -->
			<div class="col-sm-12 controls">
				<hr/>
			</div>
			
			
			<div class="col-lg-6 col-md-6 col-sm-7 col-xs-12 col-lg-offset-1 col-md-offset-1">
				<div class="x_panel">
					
					<!-- HISTORICO DE CONDUTAS FONOAUDIOLOGICAS -->
					<div class="x_title">
						<h2>Histórico <small>Condutas fonoaudiológicas</small></h2>
						
						<div class="clearfix"></div>
					</div>
					
					<div class="x_content hidden-sm hidden-xs y-scroll" style="height:350px">
						<?php include("include/fon/historico_md.php"); ?>
					</div>
					
					<div class="x_content hidden-md hidden-lg y-scroll" style="height:250px">
						<?php include("include/fon/historico_xs.php"); ?>
					</div>
					
				</div>
			</div>
			
			<div class="col-lg-4 col-md-4 col-sm-5 col-xs-12">
				<div class="x_panel ui-ribbon-container ">
					
					<!-- OBJETIVOS -->
					<div class="x_title">
						<h2>Objetivos</h2>
						<div class="clearfix"></div>
					</div>
					
					<div class="x_content">
						
						<?php
						
						$query = "	SELECT 
										* 
									FROM 
										".$_SESSION['user_Servico']."_objetivo 
									WHERE 
										id_paciente = '".$paciente['geral']['id']."' AND
										equipe	= 'fon' AND
										fim_objetivo IS NULL
									ORDER BY 
										inicio_objetivo
								";
						$mysql_query = mysqli_query($mysqli,$query);
						
						$i = 1;
						while( $objetivo = mysqli_fetch_array($mysql_query) ){
							
							// FORMATA A DATA
							$d = new DateFormatter($objetivo['previsao_objetivo']);
							$tempo = $d->formattedInterval();
							
							if( $objetivo['previsao_objetivo'] > date("Y-m-d h:i:sa") ){
								
								$tag = "<span class='label label-success pull-right' style='font-weight:100'> &nbsp; No Prazo &nbsp; </span>";
								
							}else{
								$tag = "<span class='label label-danger pull-right' style='font-weight:100'> &nbsp; Atrasado &nbsp; </span>";
							}
							
							echo "
								<span class='title'><b>Objetivo</b></span>
								<p>".$objetivo['objetivo']."</p>
								<span class='title'><b>Prazo</b></span>
								<p>".$tempo." ".$tag."</p>
							";
							
							$i++;
							
						} 
						
						if($i==1){
							echo "<p>Nenhum objetivo estabelecido</p>";
							echo "<a href='objetivos.php' class='btn btn-primary' >Adicionar objetivo</a>";
						}
						
						echo "
							<div class='divider'></div>
							<p><b>Objetivos completos</b></p>
						";
						
						$query = "	SELECT 
										* 
									FROM 
										".$_SESSION['user_Servico']."_objetivo 
									WHERE 
										id_paciente = '".$paciente['geral']['id']."' AND
										equipe	= 'fon' AND
										fim_objetivo >= inicio_objetivo
									ORDER BY 
										inicio_objetivo
								";
						$mysql_query = mysqli_query($mysqli,$query);
						
						$i = 1;
						while( $objetivo = mysqli_fetch_array($mysql_query) ){
							
							// FORMATA A DATA
							$d = new DateFormatter($objetivo['previsao_objetivo']);
							$tempo = $d->formattedInterval();
							
							echo "<p>".$objetivo['objetivo']."	<i class='fa fa-check pull-right'></i></p>";
							$i++;
							
						} 
						
						if($i==1){
							echo "<p>Nenhum objetivo finalizado</p>";
						}
						
						?>
						
					</div>
					
				</div>
			</div>
			
		</div>
	
	</div>
	
<?php
}
?>