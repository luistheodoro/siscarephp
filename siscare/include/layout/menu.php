<div class="col-md-3 left_col">
	
	<div class="left_col scroll-view">
		
		<!-- NOME DO SITE -->
		<div class="navbar nav_title" style="border: 0;">
			<a class="site_title" href="home.php"><i class="fa fa-cloud round_icon"></i> <span><i>SIS</i><b>CARE</b></span></a>
		</div>
		
		<div class="clearfix"></div>

		<!-- IMAGEM DO PERFIL -->
		<a href="perfil.php">
		<div class="profile">
			
			<div class="profile_pic">
				<img src="<?php echo $usuario['geral']['avatar']; ?>" alt="..." class="img-thumbnail profile_img">
			</div>
			
			<div class="profile_info">
				<span>Bem-vindo(a),</span>
				<h2><?php echo $_SESSION['user_Nome']; ?></h2>
				<br><br>
			</div>
			
		</div>
		</a>

		<br>

		<!-- MENU -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			
			<div class="menu_section">
				
			<h3>Menu</h3>
			
				<ul class="nav side-menu">
					
					<!-- HOME -->
					<li <?php if ($menu == "home") { echo "class='current-page'"; } ?> >
						<a href="home.php">
							<i class="fa fa-home fa-fw"></i>
							<span class="menu-title">Home</span>
						</a>
					</li>
					
					<!-- PACIENTES -->
					<li <?php if ($menu == "pacientes") { echo "class='current-page'"; } ?> >
						<a href="pacientes.php">
							<i class="fa fa-wheelchair fa-fw"></i>
							<span class="menu-title">Pacientes</span>
						</a>
					</li>
					
					<!-- EQUIPES -->
					<li <?php if ($menu == "equipes") { echo "class='current-page'"; } ?> >
						<a href="equipes.php">
							<i class="fa fa-group fa-fw"></i>
							<span class="menu-title">Equipes</span>
						</a>
					</li>
					
					<!-- OBJETIVOS -->
					<li <?php if ($menu == "objetivos") { echo "class='current-page'"; } ?> >
						<a href="objetivos.php">
							<i class="fa fa-dot-circle-o fa-fw"></i>
							<span class="menu-title">Objetivos</span>
						</a>
					</li>
					
					<!-- INDICADORES -->
					<li>
						<a href="indicadores.php?t=equipe&id=<?php echo $usuario['geral']['equipe']; ?>">
							<i class="fa fa-line-chart fa-fw"></i>
							<span class="menu-title">Indicadores</span>
						</a>
					</li>
					
					<!-- ATENDIMENTOS -->
					<?php 
						if($usuario['acesso']['ver_atendimentos']>=3){
					?>
					<li <?php if ($menu == "atendimentos") { echo "class='current-page'"; } ?> >
						<a href="atendimentos.php">
							<i class="fa fa-medkit fa-fw"></i>
							<span class="menu-title">Atendimentos</span>
						</a>
					</li>
					<?php } ?>
					
					<!-- FINANCEIRO -->
					<?php 
						if($usuario['acesso']['ver_atendimentos']>=3){
					?>
					<li <?php if ($menu == "financeiro") { echo "class='current-page'"; } ?> >
						<a href="financeiro.php">
							<i class="fa fa-dollar fa-fw"></i>
							<span class="menu-title">Financeiro</span>
						</a>
					</li>
					<?php } ?>
					
					<!-- TI -->
					<?php if( $usuario['acesso']["TI"] == 1 ){ ?>
						
						<li <?php if ($menu == "TI") { echo "class='current-page'"; } ?> >
							<a href="ti.php">
								<i class="fa fa-laptop fa-fw"></i>
								<span class="menu-title">TI</span>
							</a>
						</li>
					
					<?php } ?>
				</ul>
			</div>
			
		</div>
		
	</div>
</div>