<div class="top_nav">

	<div class="nav_menu">
		<nav class="" role="navigation">
			
			<!-- BOTÃO DE TOGGLE DO MENU -->
			<div class="nav toggle">
				<a id="menu_toggle"><i class="fa fa-bars"></i></a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				
				
				<!-- MENU DE TOPO -->
				<li class="">
					
					<!-- NOME/IMAGEM DO USUARIO -->
					<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<img src="<?php echo $usuario['geral']['avatar']; ?>" alt=""><?php echo $_SESSION['user_Nome']; ?>
						<span class=" fa fa-angle-down"></span>
					</a>
					
					<ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
						
						<!-- PERFIL -->
						<li>
							<a href="perfil.php">Perfil</a>
						</li>
						
						<!-- CONFIGURAÇÕES -->
						<li>
							<a href="configuracao.php"> Configurações </a>
						</li>
						
						<!-- AJUDA -->
						<li>
							<a href="#"> Ajuda</a>
						</li>
						
						<?php
							if($usuario['acesso']['TI'] == 4){
								echo '
									<!-- RESTRITO -->
									<li>
										<a href="restrito.php"> Restrito</a>
									</li>
								';
							}
						?>
						
						<!-- SAIR -->
						<li>
							<a href="config/logout.php"><i class="fa fa-sign-out pull-right"></i> Sair</a>
						</li>
						
					</ul>
					
				</li>
				
				
				<!-- MENSAGENS RÁPIDAS -->
				<?php /*
				<li role="presentation" class="dropdown hidden-xs">
					
					<!-- BOTÃO DE ACESSO -->
					<a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-envelope-o"></i>
						<span class="badge bg-green">6</span>
					</a>
					
					<ul id="menu1" class="dropdown-menu list-unstyled msg_list animated fadeInDown" role="menu">
						
						<!-- MENSAGEM -->
						<li>
							<a>
								<span class="image">
									<img src="<?php echo $usuario['geral']['imagem']; ?>" alt="Profile Image" />
								</span>
								<span>
									<span>John Smith</span>
									<span class="time">3 mins ago</span>
								</span>
								<span class="message">
									Film festivals used to be do-or-die moments for movie makers. They were where... 
								</span>
							</a>
						</li>
						<li>
							<a>
								<span class="image">
									<img src="<?php echo $usuario['geral']['imagem']; ?>" alt="Profile Image" />
								</span>
								<span>
									<span>John Smith</span>
									<span class="time">3 mins ago</span>
								</span>
								<span class="message">
									Film festivals used to be do-or-die moments for movie makers. They were where... 
								</span>
							</a>
						</li>
						<li>
							<a>
								<span class="image">
									<img src="<?php echo $usuario['geral']['imagem']; ?>" alt="Profile Image" />
								</span>
								<span>
									<span>John Smith</span>
									<span class="time">3 mins ago</span>
								</span>
								<span class="message">
									Film festivals used to be do-or-die moments for movie makers. They were where... 
								</span>
							</a>
						</li>
						
						<!-- VER TODAS -->
						<li>
							<div class="text-center">
								<a>
									<strong><a href="inbox.html">See All Alerts</strong>
									<i class="fa fa-angle-right"></i>
								</a>
							</div>
						</li>
						
					</ul>
					
				</li>
				*/ ?>

			</ul>
		</nav>
	</div>

</div>