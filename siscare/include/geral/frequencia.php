﻿<?php
// MODAL - CRIAR ATENDIMENTO
function editar_frequencia($mysqli,$paciente){
?>

<!-- MODAL NOVO ATENDIMENTO -> FONOAUDIOLOGIA -->
<div class="modal fade modal-frequencia" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Editar frequência de atendimento</h4>
			</div>
			
			
			<div class="modal-body">
			<form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/geral/frequencia/editar.php">
					
					<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 controls">
						
						<div class="row">
							
							
							<div class="col-md-12 col-xs-12">
								<h3> Frequência </h3>
							</div>
							
							<div class="col-md-12 col-xs-12">
							
								<!-- FREQUÊNCIA -->
								<label class="control-label" for="inputFrequencia"> Escolher frequência </label>
								<select class="form-control" id="inputFrequencia" name="inputFrequencia" required>
									
									<option 
										<?php if($paciente['fon']['frequencia']=='1x por semana'){echo " selected='selected' ";} ?> 
										value="1x por semana">
										1x por semana
									</option>
									
									<option 
										<?php if($paciente['fon']['frequencia']=='2x por semana'){echo " selected='selected' ";} ?> 
										value="2x por semana">
										2x por semana
									</option>
									
									<option 
										<?php if($paciente['fon']['frequencia']=='3x por semana'){echo " selected='selected' ";} ?> 
										value="3x por semana">
										3x por semana
									</option>
									
									<option 
										<?php if($paciente['fon']['frequencia']=='4x por semana'){echo " selected='selected' ";} ?> 
										value="4x por semana">
										4x por semana
									</option>
									
									<option 
										<?php if($paciente['fon']['frequencia']=='Diário'){echo " selected='selected' ";} ?> 
										value="Diário">
										Diário
									</option>
									
									<option 
										<?php if($paciente['fon']['frequencia']=='1x por mês'){echo " selected='selected' ";} ?> 
										value="1x por mês">
										1x por mês
									</option>
									
									<option 
										<?php if($paciente['fon']['frequencia']=='2x por mês'){echo " selected='selected' ";} ?> 
										value="2x por mês">
										2x por mês
									</option>
									
									<option 
										<?php if($paciente['fon']['frequencia']=='3x por mês'){echo " selected='selected' ";} ?> 
										value="3x por mês">
										3x por mês
									</option>
									
									<option 
										<?php if($paciente['fon']['frequencia']=='1x a cada 2 meses'){echo " selected='selected' ";} ?> 
										value="1x a cada 2 meses">
										1x a cada 2 meses
									</option>
									
								</select>
								<br>
							
							</div>
							
							
						</div>
						
						<input type="hidden" value="fon" name="inputEquipe" id="inputEquipe">
						<input type="hidden" value="<?php echo $paciente['geral']['id']; ?>" name="inputIDPaciente" id="inputIDPaciente">
						
					</div>
					
					
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				<button type="submit" class="btn btn-primary">Salvar</button>
			</div>
			
			</form>
			
			
		</div>
	</div>
</div>

	
<?php
	
}

?>