﻿<?php
// MODAL - CRIAR ATENDIMENTO
function editar_espec($mysqli,$usuario,$paciente,$servico){
?>

<!-- MODAL NOVO ATENDIMENTO -> FONOAUDIOLOGIA -->
<div class="modal fade modal-espec" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			
			
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Editar especificidades</h4>
			</div>
			
			
			<div class="modal-body">
			<form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/geral/espec/editar.php">
					
					<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 controls">
						
						<div class="row">
							
							
							<div class="col-md-12 col-xs-12">
								<h3> Geral </h3>
							</div>
							
							
							<div class="col-md-4 col-xs-12">
								<br>
								
								<!-- DEGENERATIVO -->
								<label class="hover">
									<div class="icheckbox_flat-green checked hover" style="position: relative;">
										<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" value="1"
												name="inputDegenerativo" id="inputDegenerativo" <?php if($paciente['espec']['degenerativo']=='1'){echo " checked ";} ?> >
										<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
									</div> 
									<font weight='normal'> &nbsp; Degenerativo </font>
								</label>
								
							</div>
							
							<div class="col-md-4 col-xs-12">
								<br>
							
								<!-- PALIATIVO -->
								<label class="hover">
									<div class="icheckbox_flat-green checked hover" style="position: relative;">
										<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" value="1"
												name="inputPaliativo" id="inputPaliativo" <?php if($paciente['espec']['paliativo']=='1'){echo " checked ";} ?> >
										<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
									</div> 
									<font weight='normal'> &nbsp; Paliativo </font>
								</label>
								
							</div>
							
							<div class="col-md-4 col-xs-12">
								<br>
							
								<!-- CRITÉRIO DE TERMINALIDADE -->
								<label class="hover">
									<div class="icheckbox_flat-green checked hover" style="position: relative;">
										<input class="flat" style="position: absolute; opacity: 0;" type="checkbox" value="1"
												name="inputTerminalidade" id="inputTerminalidade" <?php if($paciente['espec']['terminalidade']=='1'){echo " checked ";} ?> >
										<ins class="iCheck-helper" style="background: rgb(255, 255, 255); margin: 0px; padding: 0px; border: 0px; border-image: none; left: 0%; top: 0%; width: 100%; height: 100%; display: block; position: absolute; opacity: 0;"></ins>
									</div> 
									<font weight='normal'> &nbsp; Critério de terminalidade </font>
								</label>
								
							</div>
							
							<div class="col-md-12 col-xs-12">
								<br>
								
								<!-- PROGNOSTICO -->
								<label class="control-label" for="inputPrognostico"> Prognóstico </label>
								<select class="form-control" id="inputPrognostico" name="inputPrognostico" required>
									
									<option 
										<?php if( $paciente['espec']['prognostico'] == "N/A" ){echo " selected='selected' ";} ?> 
										value="N/A">
										N/A
									</option>
									
									<option 
										<?php if($paciente['espec']['prognostico']=='Restrito'){echo " selected='selected' ";} ?> 
										value="Restrito">
										Restrito
									</option>
									
									<option 
										<?php if($paciente['espec']['prognostico']=='Favorável'){echo " selected='selected' ";} ?> 
										value="Favorável">
										Favorável
									</option>
									
								</select>
								<br>
								
							</div>
							
							
							<div class="col-md-12 col-xs-12">
								<h3> Enfermagem / Médico </h3>
							</div>
							
							<div class="col-md-12 col-xs-12">
							
								<!-- VAA -->
								<label class="control-label" for="inputVAA"> Via Alternativa de Alimentação </label>
								<select class="form-control" id="inputVAA" name="inputVAA">
									
									<option value=""> Qual a via de alimentação? </option>
									
									<option 
										<?php if($paciente['espec']['VAA']=='Via oral'){echo " selected='selected' ";} ?> 
										value="Via oral">
										Via oral
									</option>
									
									<option 
										<?php if($paciente['espec']['VAA']=='Via mista'){echo " selected='selected' ";} ?> 
										value="Via mista">
										Via mista
									</option>
									
									<option 
										<?php if($paciente['espec']['VAA']=='SNE'){echo " selected='selected' ";} ?> 
										value="SNE">
										SNE			
									</option>
									
									<option 
										<?php if($paciente['espec']['VAA']=='PEG'){echo " selected='selected' ";} ?> 
										value="PEG">
										PEG			
									</option>
									
								</select>
								<br>
							
							</div>
							
							<div class="col-md-12 col-xs-12">
							
								<!-- TQT -->
								<label class="control-label" for="inputTQT"> Traqueostomia </label>
								<select class="form-control" id="inputTQT" name="inputTQT">
									
									<option value=""> Qual tipo de traqueostomia?		</option>
									
									<option 
										<?php if($paciente['espec']['TQT']=='N/A'){echo " selected='selected' ";} ?> 
										value="N/A">							
										Nenhum							
									</option>
									
									<option 
										<?php if($paciente['espec']['TQT']=='Plástica com Cuff insuflado'){echo " selected='selected' ";} ?> 
										value="Plástica com Cuff insuflado">
										Plástica com Cuff insuflado	
									</option>
									
									<option 
										<?php if($paciente['espec']['TQT']=='Plástica com Cuff desinsuflado'){echo " selected='selected' ";} ?> 
										value="Plástica com Cuff desinsuflado">	
										Plástica com Cuff desinsuflado	
									</option>
									
									<option 
										<?php if($paciente['espec']['TQT']=='Metálica ocluida'){echo " selected='selected' ";} ?> 
										value="Metálica ocluida">
										Metálica ocluida
									</option>
									
									<option 
										<?php if($paciente['espec']['TQT']=='Metálica desocluida'){echo " selected='selected' ";} ?> 
										value="Metálica desocluida">
										Metálica desocluida
									</option>
									
								</select>
								<br>
							
							</div>
							
							
							<?php
								
							if( $servico['geral']['fon'] == FALSE ){
								
							?>
							
							<div class="col-md-12 col-xs-12">
								<h3> Fonoaudiologia </h3>
							</div>
							
							<div class="col-md-12 col-xs-12" style="display:none">
								
								<!-- ADAPTAÇÃO DE DIETA -->
								<label class="control-label" for="inputDieta"> Consistência de dieta </label>
								<select class="form-control" id="inputDieta" name="inputDieta">
									
									<option>Qual tipo de dieta?		</option>
									
									<option 
										<?php if($paciente['espec']['dieta']=='Pastosa heterogênea'){echo " selected='selected' ";} ?> 
										value="Pastosa heterogênea"> 
										Pastosa heterogênea 
									</option>
									
									<option 
										<?php if($paciente['espec']['dieta']=='Pastosa homogênea'){echo " selected='selected' ";} ?> 
										value="Pastosa homogênea"> 
										Pastosa homogênea 
									</option>
									
									<option 
										<?php if($paciente['espec']['dieta']=='Sólidos secos'){echo " selected='selected' ";} ?> 
										value="Sólidos secos"> 
										Sólidos secos 
									</option>
									
									<option 
										<?php if($paciente['espec']['dieta']=='Semi-sólidos humidecidos'){echo " selected='selected' ";} ?> 
										value="Semi-sólidos humidecidos"> 
										Semi-sólidos humidecidos 
									</option>
									
								</select>
								<br>
							
							</div>
							
							<div class="col-md-12 col-xs-12" style="display:none">
								
								<!-- ADAPTAÇÃO DE LIQUIDOS -->
								<label class="control-label" for="inputLiquido"> Consistência de líquidos </label>
								<select class="form-control" id="inputLiquido" name="inputLiquido">
									
									<option> Qual a conscistência do líquido? </option>
									
									<option 
										<?php if($paciente['espec']['liquido']=='Líquidos finos '){echo " selected='selected' ";} ?> 
										value="Líquidos finos ">
										Líquidos finos 
									</option>
									
									<option
										<?php if($paciente['espec']['liquido']=='Néctar '){echo " selected='selected' ";} ?> 
										value="Néctar ">
										Néctar 
									</option>
									
									<option
										<?php if($paciente['espec']['liquido']=='Mel'){echo " selected='selected' ";} ?> 
										value="Mel">
										Mel 
									</option>
									
									<option
										<?php if($paciente['espec']['liquido']=='Pudim'){echo " selected='selected' ";} ?> 
										value="Pudim">
										Pudim 
									</option>
									
								</select>
							
							</div>
							
							<?php } ?>
							
							
						</div>
						
						<input type="hidden" value="<?php echo $usuario['geral']['equipe']; ?>" name="inputEquipe" id="inputEquipe">
						<input type="hidden" value="<?php echo $paciente['geral']['id']; ?>" name="inputIDPaciente" id="inputIDPaciente">
						
					</div>
					
					
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
				<button type="submit" class="btn btn-primary">Salvar</button>
			</div>
			
			</form>
			
			
		</div>
	</div>
</div>


	<script>
		
		function hide(){
			
			// DIETAS
			var dieta = document.getElementById('inputDieta');
			dieta.style.display = 'none';
			var dietaAtual = document.getElementById('dietaAtual');
			dietaAtual.style.display = 'block';
			
			// LIQUIDOS
			var liquido = document.getElementById('inputLiquido');
			liquido.style.display = 'none';
			var liquidoAtual = document.getElementById('liquidoAtual');
			liquidoAtual.style.display = 'block';
			
			// COMENTARIO
			var comentario = document.getElementById('inputComentario');
			comentario.style.display = 'none';
			var comentarioAtual = document.getElementById('comentarioAtual');
			comentarioAtual.style.display = 'block';
			
		}

		function show(){
			
			// DIETAS
			var dieta = document.getElementById('inputDieta');
			dieta.style.display = 'block';
			var dietaAtual = document.getElementById('dietaAtual');
			dietaAtual.style.display = 'none';
			
			// LIQUIDOS
			var liquido = document.getElementById('inputLiquido');
			liquido.style.display = 'block';
			var liquidoAtual = document.getElementById('liquidoAtual');
			liquidoAtual.style.display = 'none';
			
			// COMENTARIO
			var comentario = document.getElementById('inputComentario');
			comentario.style.display = 'block';
			var comentarioAtual = document.getElementById('comentarioAtual');
			comentarioAtual.style.display = 'none';
			
		}
		
		function selecionaConduta(select){
			
			if(select.value != 'Mantida'){
				show();
			}else{
				hide();
			}
			
		}
		
	</script>

	
<?php
	
}

?>