﻿<?php
	
// EXIBE OS DADOS DE PERFIL DO PACIENTE
function paciente_perfil($mysqli, $paciente, $usuario){
	
	$query_2 		= "SELECT * FROM ".$_SESSION['user_Servico']."_terapia WHERE id_paciente='".$paciente['geral']['id']."' ORDER BY data DESC LIMIT 4";
	$mysql_query_2	= mysqli_query($mysqli, $query_2); 
	
?>
	
	
	<div class="row">
		
		<!-- ALERTAS -->
		<div class="col-md-12 col-sm-12 col-xs-12 controls">
			<br>
			
			<?php
				
			$query = "	SELECT * FROM 
							".$_SESSION['user_Servico']."_risco 
						WHERE
							id_paciente='".$paciente['geral']['id']."'
						ORDER BY 
							id DESC 
						";
			
			$i = 0;
			
			foreach($mysqli->query($query) as $risco) {  
			
				if($i == 0){
					echo"
						<div class='alert alert-danger alert-dismissible fade in col-sm-3' role='alert'>
							<h4>".$risco['risco']."</h4>
							<h5>".equipe($risco['equipe'],2)."</h5>
						</div>
					";
				}else{
					echo"
						<div class='alert alert-danger alert-dismissible fade in col-sm-3 col-sm-offset-1' role='alert'>
							<h4>".$risco['risco']."</h4>
							<h5>".equipe($risco['equipe'],2)."</h5>
						</div>
					";
				}
				
				$i++;
				
			} 
			
			?>
			
		</div>
		
		<!-- DADOS PRINCIPAIS -->
		<div class="col-md-8 col-sm-12 col-xs-12 controls">
		
			<div class="col-sm-6 controls">
			
				<br>
				<span class="title"><b>Diagnóstico</b></span>
				<p><?php echo diagnostico($paciente['geral']['diagnostico_1'], $paciente['geral']['diagnostico_2'], $paciente['geral']['diagnostico_3']); ?></p>
				
				<span class="title"><b>Sexo</b></span>
				<p><?php echo $paciente['geral']['sexo']; ?></p>
				
				<span class="title"><b>Última conduta</b></span>
				<?php
				
				$query = "	SELECT * FROM 
								".$_SESSION['user_Servico']."_terapia 
							WHERE
								id_paciente='".$paciente['geral']['id']."' AND 
								conduta!='Mantida' 
							ORDER BY 
								data DESC 
							LIMIT 
								1
							";
				$i = 0;
				foreach($mysqli->query($query) as $terapia) {  
				
					echo "<p>".$terapia['conduta']."<br> <i>".equipe($terapia['equipe'],2)."</i></p>";
					$i++;
				} 
				
				if($i==0){
					echo "<h5>Nenhuma conduta realizada</h5>";
				}
				
				?>
				
			</div>
			<div class="col-sm-6 controls">
			
				<br>
				<span class="title"><b>Responsável</b></span>
				<p><?php echo $paciente['geral']['responsavel']." - ".$paciente['geral']['relacao']; ?></p>
				
				<span class="title"><b>Cuidador</b></span>
				<p><?php echo $paciente['geral']['cuidador']; ?></p>
				
				<span class="title"><b>Contato</b></span>
				<p><?php echo $paciente['contato']['nome_1']; ?> &nbsp; <i><?php echo $paciente['contato']['telefone_1']; ?></i></p>
				
			</div>
			
		</div>
		
		<!-- ESCALAS -->
		<div class="col-md-4 col-sm-6 col-xs-12 controls">
			
			<div class="row">
				<div class="col-sm-6 controls">
					
					ASHA NOMS ( <?php echo $paciente['escala']['ASHA_NOMS']; ?> / 7 )
					<br>
					<span class="chart" data-percent="<?php echo 100 * $paciente['escala']['ASHA_NOMS'] / 7; ?>">
						<span class="percent"></span>
						<canvas height="95" width="95"></canvas>
					</span>
					
				</div>
				<div class="col-sm-6 controls">
					
					BRADEN ( <?php echo $paciente['escala']['BRADEN']; ?> / 18 )
					<br>
					<span class="chart" data-percent="<?php echo 100 * $paciente['escala']['BRADEN'] / 18; ?>">
						<span class="percent"></span>
						<canvas height="95" width="95"></canvas>
					</span>
					
				</div>
			</div>
		</div>
		
		
		<!-- DIVISOR -->
		<div class="col-sm-12 controls">
			<hr/>
		</div>
		
		
		<!-- ESPECIFICIDADES -->
		<div class="col-md-4 col-md-offset-1 col-sm-5 col-xs-12">
			<div class="x_panel ui-ribbon-container ">
				
				<div class="x_title">
					<h2>Especificidades</h2>
					<div class="clearfix"></div>
				</div>
				
				<div class="x_content">
					
					<?php
					
					if($paciente['espec']['VAA'] != NULL){
						echo '
							<span class="title"><b>Via Alternativa de Alimentação</b></span>
							<p>'.$paciente['espec']['VAA'].'</p>
						';
					}
					
					if($paciente['espec']['TQT'] != NULL){
						echo '
							<span class="title"><b>Traqueostomia</b></span>
							<p>'.$paciente['espec']['TQT'].'</p>
						';
					}
					
					if($paciente['espec']['dieta'] != NULL){
						echo '
							<span class="title"><b>Dieta liberada</b></span>
							<p>'.$paciente['espec']['dieta'].'</p>
						';
					}
					
					if($paciente['espec']['liquido'] != NULL){
						echo '
							<span class="title"><b>Consistência do líquido</b></span>
							<p>'.$paciente['espec']['liquido'].'</p>
						';
					}
					
					?>
					
				</div>
				
				
				<!-- EDITAR A ESPECIFICIDADE DO PACIENTE -->
				<?php if( acesso_editar_espec($mysqli,$usuario,$paciente) == TRUE ){ ?>
				
					<div class="x_content">
						<a href="" class="btn btn-primary" data-toggle	= "modal" data-target	= ".modal-espec"> <i class="fa fa-pencil"></i> &nbsp; Editar &nbsp; </a>
						<div class="clearfix"></div>
					</div>
				
				<?php } ?>
				
				
			</div>
		</div>
		
		<div class="col-md-6 col-sm-7 col-xs-12">
			<div class="x_panel ui-ribbon-container ">
				
				<div class="x_title">
					<h2>Últimas condutas</h2>
					<div class="clearfix"></div>
				</div>
				
				<div class="x_content">
					
					<table class="table table-hover">
					<tbody>
							
							<?php
							
							$query = "	SELECT * FROM 
											".$_SESSION['user_Servico']."_terapia 
										WHERE
											id_paciente='".$paciente['geral']['id']."' AND 
											conduta!='Mantida' 
										ORDER BY 
											data DESC 
										LIMIT 
											4
										";
							
							$i = 1;
							foreach($mysqli->query($query) as $terapia) {
								
								// FORMATA A DATA
								$d = new DateFormatter($terapia['data']);
								$tempo = $d->formattedInterval();
								
								/*
								echo "
									<tr>
										<td style='border:0px; width: 25%'><b><small>".equipe($terapia['equipe'],2)."</small></b></td>
										<td style='border:0px'><small>".$terapia['conduta']."</small></td>
										<td class='text-center' style='border:0px'><i><small>".$tempo."</small></i></td>
									</tr>
								";
								*/
								if($i==1){
									
									echo "
										<tr>
										<td style='border-top:0px'>
											<small>".equipe($terapia['equipe'],2)."<i class='pull-right'>".$tempo."</small></i>
												<br>
											".$terapia['conduta']."
										</td>
										</tr>
									";
								
								}else{
									
									echo "
										<tr>
										<td>
											<small>".equipe($terapia['equipe'],2)."<i class='pull-right'>".$tempo."</small></i>
												<br>
											".$terapia['conduta']."
										</td>
										</tr>
									";
								
								}
								
								$i++;
								
							} 
							
							?>
							
					</tbody>
					</table>
					
					<?php
					
						
						if($i==1){
							
							echo "<h3>Nenhuma conduta realizada</h3>";
						
						}
						
					?>
					
				</div>
				
			</div>
		</div>
		
		
		<!-- DIVISOR -->
		<div class="col-sm-12 controls">
			<hr/>
		</div>
		
		
		<div class="col-md-10 col-xs-12 col-md-offset-1">
			<div class="x_panel ui-ribbon-container ">
				
				<div class="x_title">
					<h2>Contato</h2>
					<div class="clearfix"></div>
				</div>
				
				<div class="x_content">
				
					<div class="col-sm-4 col-xs-12 controls">
						
						<span class="title"><b>Endereço</b></span>
						<p><?php echo $paciente['contato']['endereco']." - ".$paciente['contato']['bairro'].", ".$paciente['contato']['cidade']; ?></p>
						
						<span class="title"><b>Ponto de referência</b></span>
						<p>
							<?php 
								if($paciente['contato']['referencia']){
									echo $paciente['contato']['referencia']; 
								}else{
									echo "-";
								}
							?>
						</p>
						
						<span class="title"><b>Kilometragem</b></span>
						<p><?php echo $paciente['contato']['kilometragem']." km"; ?></p>
						
					</div>
					
					<div class="col-sm-3 col-xs-12 controls">
					
						<span class="title"><b>Telefone 1</b></span>
						<p><?php echo $paciente['contato']['telefone_1']." - ".$paciente['contato']['nome_1']; ?></p>
						
						<span class="title"><b>Telefone 2</b></span>
						<p><?php echo $paciente['contato']['telefone_2']." - ".$paciente['contato']['nome_2']; ?></p>
						
						<span class="title"><b>Telefone 3</b></span>
						<p><?php echo $paciente['contato']['telefone_3']." - ".$paciente['contato']['nome_3']; ?></p>
						
					</div>
					
					<div class="col-sm-5 col-xs-12 controls">
					
						<h4>Mapa</h4>
						<div id="map" style="height:300px; width:100%"></div>
						<br>
						<a style="cursor: pointer;" onclick="myNavFunc()" class="btn btn-primary hidden-sm hidden-md hidden-lg">Navegar</a>
						
						<!-- GOOGLE MAPS -->
						<script>
							function initMap() {
								var map = new google.maps.Map(document.getElementById('map'), {
									center: {lat: <?php echo $paciente['contato']['lat']; ?>, lng: <?php echo $paciente['contato']['lng']; ?>},
									zoom: 15,
									scrollwheel: false
								});
								
								var myLatlng = new google.maps.LatLng(<?php echo $paciente['contato']['lat']; ?>,<?php echo $paciente['contato']['lng']; ?>);
								
								var marker = new google.maps.Marker({
									position: myLatlng, 
									map: map
								}); 
								
							}
							
							function myNavFunc(){
								// If it's an iPhone..
								if( (navigator.platform.indexOf("iPhone") != -1) 
									|| (navigator.platform.indexOf("iPod") != -1)
									|| (navigator.platform.indexOf("iPad") != -1))
									 window.open("maps://maps.google.com/maps?daddr=<?php echo $paciente['contato']['lat']; ?>,<?php echo $paciente['contato']['lng']; ?>&amp;ll=");
								else
									 window.open("http://maps.google.com/maps?daddr=<?php echo $paciente['contato']['lat']; ?>,<?php echo $paciente['contato']['lng']; ?>&amp;ll=");
							}

						</script>
						
						
						<!-- [END] GOOGLE MAPS -->
						
					</div>
				</div>
			</div>
		</div>
		
	</div>
	
	
	
	<script>
        $(function () {
            $('.chart').easyPieChart({
                easing: 'easeOutBounce',
                lineWidth: '6',
                barColor: '#75BCDD',
                onStep: function (from, to, percent) {
                    $(this.el).find('.percent').text(Math.round(percent));
                }
            });
            var chart = window.chart = $('.chart').data('easyPieChart');
            $('.js_update').on('click', function () {
                chart.update(Math.random() * 200 - 100);
            });

            //hover and retain popover when on popover content
            var originalLeave = $.fn.popover.Constructor.prototype.leave;
            $.fn.popover.Constructor.prototype.leave = function (obj) {
                var self = obj instanceof this.constructor ?
                    obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type)
                var container, timeout;

                originalLeave.call(this, obj);

                if (obj.currentTarget) {
                    container = $(obj.currentTarget).siblings('.popover')
                    timeout = self.timeout;
                    container.one('mouseenter', function () {
                        //We entered the actual popover – call off the dogs
                        clearTimeout(timeout);
                        //Let's monitor popover content instead
                        container.one('mouseleave', function () {
                            $.fn.popover.Constructor.prototype.leave.call(self, self);
                        });
                    })
                }
            };
            $('body').popover({
                selector: '[data-popover]',
                trigger: 'click hover',
                delay: {
                    show: 50,
                    hide: 400
                }
            });

        });
    </script>
<?php
}
?>