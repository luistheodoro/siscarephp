﻿<?php
// MODAL - CRIAR ATENDIMENTO
function editar_responsavel($mysqli,$paciente,$usuario){
?>
	
	
	<!-- EDITAR RESPONSAVEL -->
	<div class="modal fade modal-responsavel" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-md">
			<div class="modal-content">
				
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Editar responsável</h4>
				</div>
				
				
				<div class="modal-body">
				<form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/geral/responsavel/editar.php">
						
						<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 controls">
							
							<div class="row">
								
								
								<div class="col-md-12 col-xs-12">
									<h3> <?php echo equipe($usuario['geral']['equipe'],2); ?> </h3>
								</div>
								
								
								<div class="col-md-12 col-xs-12">
								
									<!-- RESPONSÁVEL -->
									<label class="control-label" for="inputResponsavel"> Escolher responsável </label>
									<select class="form-control" id="inputResponsavel" name="inputResponsavel" required>
										
										<?php
											
											if($usuario['acesso']['editar_responsavel'] == 3){
											
												// SELECIONA TODOS OS COLABORADORES DESTA EQUIPE
												$query = " SELECT * FROM ".$_SESSION['user_Servico']."_usuarios WHERE equipe = '".$usuario['geral']['equipe']."' ";
												
												foreach($mysqli->query($query) as $responsavel) {  
													
													if($paciente[$usuario['geral']['equipe']]['responsavel'] == $responsavel['id_usuario']){
														
														echo "
															<option 
																selected='selected'
																value='".$responsavel['id_usuario']."'>
																".$responsavel['nome']."
															</option>
														";
													
													}else{
														
														echo "
															<option 
																value='".$responsavel['id_usuario']."'>
																".$responsavel['nome']."
															</option>
														";
														
													}
													
												}
												
											
											}else if($usuario['acesso']['editar_responsavel'] == 2){
												
												if($paciente[$usuario['geral']['equipe']]['responsavel'] == $usuario['geral']['id_usuario']){
													
													echo "
														<option 
															selected='selected'
															value='".$usuario['geral']['id_usuario']."'>
															".$usuario['geral']['nome']."
														</option>
													";
												
												}else{
													
													echo "
														<option 
															value='".$usuario['geral']['id_usuario']."'>
															".$usuario['geral']['nome']."
														</option>
													";
													
												}
												
												// SELECIONA TODOS OS COLABORADORES DESTA EQUIPE QUE ESTAO ABAIXO DESTE USUARIO
												$query = " SELECT * FROM ".$_SESSION['user_Servico']."_usuarios WHERE equipe = '".$usuario['geral']['equipe']."' AND id_superior = '".$usuario['geral']['id_usuario']."' ";
												
												foreach($mysqli->query($query) as $responsavel) {  
													
													if($paciente['fon']['responsavel'] == $responsavel['id_usuario']){
														
														echo "
															<option 
																selected='selected'
																value='".$responsavel['id_usuario']."'>
																".$responsavel['nome']."
															</option>
														";
													
													}else{
														
														echo "
															<option 
																value='".$responsavel['id_usuario']."'>
																".$responsavel['nome']."
															</option>
														";
														
													}
													
												}
												
												
											}
										
										
										?>
										
									</select>
									<br>
								
								</div>
								
								
							</div>
							
							<input type="hidden" value="<?php echo $usuario['geral']['equipe']; ?>" name="inputEquipe" id="inputEquipe">
							<input type="hidden" value="<?php echo $paciente['geral']['id']; ?>" name="inputIDPaciente" id="inputIDPaciente">
							
						</div>
						
						
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
					<button type="submit" class="btn btn-primary">Salvar</button>
				</div>
				
				</form>
				
				
			</div>
		</div>
	</div>

	
<?php
	
}

?>