﻿<?php

	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("../../config/db_connect.php");
	include_once("../../config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
	
?>


<div class="modal-dialog">
    <div class="modal-content modal-md">
	
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalhes do atendimento</h4>
        </div>
		
        <div class="modal-body">

			<?php
				
				$id_terapia 	= isset($_GET['id']) ? $_GET['id'] : header("Location: ../../../pacientes.php") ;
				$id_terapia 	= preg_replace("/[^0-9]+/", "", $id_terapia);
					
				// BUSCA AS ULTIMAS CONDUTAS
				$query = "SELECT * FROM ".$_SESSION['user_Servico']."_terapia WHERE id = '".$id_terapia."' LIMIT 1";
				$mysql_query = mysqli_query($mysqli,$query);
				$terapia = mysqli_fetch_assoc($mysql_query);
				
				
				// FORMATA A DATA
				$d = new DateFormatter($terapia['data']);
				$tempo = $d->formattedInterval();
				
				// PROCURA O NOME DA FONOAUDIOLOGA RESPONSAVEL
				$query_resp = "SELECT nome FROM ".$_SESSION['user_Servico']."_usuarios WHERE id_usuario='".$terapia['id_usuario']."' ";
				$mysql_query_resp = mysqli_query($mysqli,$query_resp);
				$nome_responsavel = mysqli_fetch_assoc($mysql_query_resp);
				
				
				// IMPRIME A TERAPIA
				echo "
					<div class='block'>
						<div class='block_content'>
							<h2 class='title'>
								<a>".$terapia['terapia']."</a>
							</h2>
							<div class='byline'>
								<span>".$tempo."</span> por <a href='perfil.php?id=".$terapia['id_usuario']."'>".$nome_responsavel['nome']."</a>
							</div>
							<br>
							<p class='excerpt'>
								Terapias adicionais: ".$terapia['terapia_2']."<br><br>".$terapia['evolucao']." <br>
							</p>
						</div>
					</div>
					
					<div class='block'>
						<div class='block_content'>
							<h2 class='title'>
								<br><a>Conduta: <small>".$terapia['conduta']."</small></a>
							</h2>
							<br>
							<p class='excerpt'>
								".$terapia['comentario']." <br>
							</p>
						</div>
					</div>
					
				";
				
				
			?>

        </div>
		
        <div class="modal-footer">
            <a href="#" class="btn btn-white" data-dismiss="modal">Fechar</a>
        </div>
		
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

<?php

}else{
	
	header("Location: login.php");
	
}

?>



