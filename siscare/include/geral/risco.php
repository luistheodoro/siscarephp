﻿<?php
// MODAL - CRIAR ATENDIMENTO
function risco($mysqli,$paciente,$usuario){
?>

<!-- MODAL NOVO ATENDIMENTO -> FONOAUDIOLOGIA -->
<div class="modal fade modal-risco" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">

			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="myModalLabel">Gerenciar Riscos</h4>
			</div>
			
			
			<div class="modal-body">
				
				<form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/geral/risco/criar.php">
					
					<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 controls">
						
						<div class="row">
							
							<div class="col-md-12 col-xs-12">
								<h3> Adicionar risco </h3>
							</div>
							
							<div class="col-md-12 col-xs-12">
								<label class="control-label" for="inputNovoRisco"> Risco </label>
								<select class="form-control" id="inputNovoRisco" name="inputNovoRisco" required>
									
									<option>Excolher o risco</option>
									<option value='Risco de broncoaspiração'>Risco de broncoaspiração</option>
									
								</select>
								
								
								<br>
							</div>
							
							<div class="col-md-12 col-xs-12">
								<input type="hidden" value="<?php echo $usuario['geral']['equipe']; ?>" name="inputEquipe" id="inputEquipe">
								<input type="hidden" value="<?php echo $paciente['geral']['id']; ?>" name="inputIDPaciente" id="inputIDPaciente">
							</div>
							
							<div class="col-md-12 col-xs-12 controls">
								<button type="submit" class="btn btn-primary pull-right">Adicionar</button>
							</div>
						</div>	
						
					</div>
					
				</form>
			
			</div>
			
			<div class="modal-body">
				
				<form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/geral/risco/excluir.php">
					
					<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 controls">
						
						<div class="row">
							
							<div class="col-md-12 col-xs-12">
								<h3> Excluir risco </h3>
							</div>
							
							<div class="col-md-12 col-xs-12">
								<label class="control-label" for="inputExcluirRisco"> Risco </label>
								
								<select class="form-control" id="inputExcluirRisco" name="inputExcluirRisco">
									
									<option>Excolher o risco</option>
									
									<?php
				
										$query = "	SELECT * FROM 
														".$_SESSION['user_Servico']."_risco 
													WHERE
														id_paciente='".$paciente['geral']['id']."' AND
														equipe = '".$usuario['geral']['equipe']."'
													ORDER BY 
														risco
													";
										
										foreach($mysqli->query($query) as $risco) {  
										
											echo "<option value='".$risco['id']."'>".$risco['risco']."</option>";
											
										} 
									
									?>
									
								</select>
								<br>
							</div>
							
							<div class="col-md-12 col-xs-12">
								<input type="hidden" value="<?php echo $paciente['geral']['id']; ?>" name="inputIDPaciente" id="inputIDPaciente">
							</div>
							
							<div class="col-md-12 col-xs-12 controls">
								<button type="submit" class="btn btn-primary pull-right">Excluir</button>
							</div>
							
						</div>	
						
					</div>
					
				</form>
			
			</div>
			
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
			</div>
			
			
		</div>
	</div>
</div>

	
<?php
	
}

?>