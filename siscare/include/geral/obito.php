﻿<?php
// MODAL - ALTA FONOAUDIOLOGIA
function obito($mysqli,$paciente){
?>

<!-- MODAL ALTA -> FONOAUDIOLOGIA -->
<div class="modal fade modal-obito" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		
		<div class="modal-content">

			<form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/geral/obito/obito.php">
				
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel">Óbito</h4>
				</div>
			
				<div class="modal-body">
				
					<h3> Confirma o óbito do paciente <?php echo $paciente['geral']['nome']; ?>? </h3>
					
					<input type="hidden" value="fon" name="inputEquipe" id="inputEquipe">
					<input type="hidden" value="obito" name="inputAlta" id="inputAlta">
					<input type="hidden" value="<?php echo $paciente['geral']['id']; ?>" name="inputIDPaciente" id="inputIDPaciente">
					
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-primary">Confirmar</button>
				</div>
			
			</form>
			
			
		</div>
	</div>
</div>


<?php
	
}

?>