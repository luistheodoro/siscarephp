﻿<?php

	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "indicadores";
	
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("config/db_connect.php");
	include_once("config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("functions/acesso/usuario.php");
		include_once("functions/erro/erro.php");
		include_once("functions/count/contar.php");
		
		
		/* SQL */
		// Busca os dados do servico
		include_once("sql/servico/buscar.php");
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		// Busca os dados de paciente
		include_once("sql/paciente/buscar.php");
		// Busca os dados de objetivos
		include_once("sql/objetivo/buscar.php");
		
		/* SUPORTE INDICADORES */
		// Busca indicadores de objetivos
		include_once("functions/count/indicadores_objetivo.php");
		// Busca indicadores da equipe
		include_once("functions/count/indicadores_equipe.php");
		// Busca indicadores de espec
		include_once("functions/count/indicadores_espec.php");
		
		
		// BUSCA DADOS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA TIPO DO PERFIL
		$tipo_perfil = isset($_GET['t']) ? $_GET['t'] : "usuario";
		$tipo_perfil = preg_replace("/[^a-zA-Z]+/", "", $tipo_perfil);
		
		// RECUPERA ID DO PERFIL
		$id_perfil = isset($_GET['id']) ? $_GET['id'] : $usuario['geral']['id_usuario'];
		
		if($tipo_perfil!="usuario"){
			
			$id_perfil 		= preg_replace("/[^a-zA-Z]+/", "", $id_perfil);
			$nome_perfil 	= equipe($id_perfil,2);
			$equipe		 	= $id_perfil;
			
		}else{
			
			// BUSCA DADOS DO PERFIL
			$perfil = busca_usuario($mysqli,$id_perfil);
			
			$id_perfil 		= preg_replace("/[^0-9]+/", "", $id_perfil);
			$nome_perfil 	= $perfil['geral']['nome'];
			$equipe			= $perfil['geral']['equipe'];
			
			if( acesso_ver_indicadores($mysqli,$usuario,$perfil) == FALSE ){
				// REDIRECIONA PARA PAGINA DO PACIENTE
				header("Location: indicadores.php?t=usuario&id=".$usuario['geral']['id_usuario']);
			}
			
		}
		
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <title>SiSaN | Equipes</title>
	
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
	<link href="assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
	
    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
	

    
</head>


<body class="nav-md">

    <div class="container body">
		
        <div class="main_container" style="background:#3C3C3C">
			
			<!-- SIDEBAR MENU -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- TOPBAR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- PAGE CONTENT -->
            <div class="right_col" role="main">

                <!-- top tiles -->
                <div class="row">
					<div class="page-title">
					
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Indicadores <small><?php echo $nome_perfil; ?></small></h3>
                        </div>
						
                        <div class="pull-right hidden-xs">
								
								<i class="fa fa-home"></i>&nbsp;<a href="home.php">Home</a>&nbsp;&nbsp;
								
								<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
									<a href="indicadores.php">Indicadores</a>&nbsp;&nbsp;
									
						</div>
						
                    </div>
                </div>
                <!-- /top tiles -->
				
				<br>
				
				<div class="row">
					<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
						<div class="x_panel">
							
							<div class="x_content">

								
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									
									<div class="col-md-12">
									
										<!-- TABELAS -->
										<div class="" role="tabpanel" data-example-id="togglable-tabs">
											
											<!-- ABAS PARA SELEÇÃO -->
											<ul id="myTab" class="nav nav-tabs" role="tablist">
												
												<li role="presentation" class="active">
													<a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Objetivos</a>
												</li>
												<li role="presentation" class="">
													<a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Serviço</a>
												</li>
												<li role="presentation" class="hidden-xs">
													<a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Pacientes</a>
												</li>
												<li role="presentation" class="pull-right">
													<select class="form-control" onChange="location = this.value;">
														<option value="indicadores.php?t=equipe&id=<?php echo $usuario['geral']['equipe']; ?>">
															<?php echo equipe($usuario['geral']['equipe'],2); ?>
														</option>
														
														<?php
															
															$query = "SELECT * FROM ".$_SESSION['user_Servico']."_usuarios WHERE equipe='".$usuario['geral']['equipe']."' ";
															$mysql_query = mysqli_query($mysqli,$query);
															
															while($select = mysqli_fetch_array($mysql_query)){
																
																if($id_perfil == $select['id_usuario']){
																	
																	echo '
																		<option selected value="indicadores.php?t=usuario&id='.$select['id_usuario'].'">
																			'.$select['nome'].'
																		</option>
																	';																	
																	
																}else{
																	
																	echo '
																		<option value="indicadores.php?t=usuario&id='.$select['id_usuario'].'">
																			'.$select['nome'].'
																		</option>
																	';
																	
																}
																
															}
															
														?>
														
													</select>
												</li>
												
											</ul>
											
											<!-- CONTEÚDO DAS ABAS -->
											<div id="myTabContent" class="tab-content">
												
												<!-- OBJETIVOS -->
												<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
													
													<?php 
													
														// Numero de objetivos atrasados
														$num_objetivos_atrasados_equipe 	= num_objetivos_atrasados($mysqli,$tipo_perfil,$id_perfil,FALSE,FALSE);
														// Numero de objetivos no prazo
														$num_objetivos_no_prazo_equipe		= num_objetivos_no_prazo($mysqli,$tipo_perfil,$id_perfil,FALSE,FALSE);
														// Total de objetivos
														$total_objetivos_concluidos 		= $num_objetivos_atrasados_equipe + $num_objetivos_no_prazo_equipe;
														// Atraso nos objetivos
														$atraso_nos_objetivos				= diff_objetivo_planejado_e_real($mysqli,$tipo_perfil,$id_perfil,FALSE,FALSE);
														// Média de duração prevista para objetivos
														$duracao_prevista_objetivo_equipe 	= duracao_prevista_objetivo($mysqli,$tipo_perfil,$id_perfil,FALSE,FALSE);
														// Média de duração real dos objetivos
														$duracao_real_objetivo_equipe 		= duracao_real_objetivo($mysqli,$tipo_perfil,$id_perfil,FALSE,FALSE);
														
														
													?>
													
													<br><br>
													<div class="col-md-4 col-sm-6 col-xs-12 text-left">
													
														<h4> Objetivos concluídos no prazo</h4>
														<div class="progress">
															
															<?php
																$per = round(100 * $num_objetivos_no_prazo_equipe / $total_objetivos_concluidos,1); 
															?>
															
															<div class="progress-bar progress-bar-info" data-transitiongoal="<?php echo $per; ?>" style="width: <?php echo $per; ?>%;" aria-valuenow="<?php echo $per; ?>">
																<?php echo $per."%"; ?>
															</div>
															
														</div>
													
														<h4> Objetivos concluídos fora do prazo</h4>
														<div class="progress">
															
															<?php
																$per = round(100 * $num_objetivos_atrasados_equipe / $total_objetivos_concluidos,1); 
															?>
															
															<div class="progress-bar progress-bar-info" data-transitiongoal="<?php echo $per; ?>" style="width: <?php echo $per; ?>%;" aria-valuenow="<?php echo $per; ?>">
																<?php echo $per."%"; ?>
															</div>
															
														</div>
														
														<br>
														
														<p>Os objetivos são planejados para durar em média</p>
														<h3><?php echo $duracao_prevista_objetivo_equipe." dias"; ?></h3>
														<br>
														
														<p>Os objetivos são finalizados em uma média de</p>
														<h3><?php echo $duracao_real_objetivo_equipe." dias"; ?></h3>
														<br>
														
														<?php
															
															if ($atraso_nos_objetivos > 0){
																echo"
																	<p>Os objetivos tem uma média de atraso de </p>
																	<h3>".$atraso_nos_objetivos." dias</h3>
																	<br>
																";
															}else{
																echo"
																	<p>Os objetivos terminam antes do prazo em média </p>
																	<h3>".(-1)*$atraso_nos_objetivos." dias</h3>
																	<br>
																";
															}
															
														?>
														
														<br><br>
													</div>	
													
													<div class="col-md-8 col-sm-8 col-xs-12 text-left">
														<h4> Objetivos - Visão geral </h4>
														<div id="objetivos_geral" style="width:100%; height:200px;"></div>
													</div>
													
													<div class="col-md-8 col-sm-8 col-xs-12 text-left">
														<h4> Cumprimento dos Objetivos </h4>
														<div id="objetivos_atrasados" style="width:100%; height:200px;"></div>
													</div>
													
												</div>
												<!-- [FIM] OBJETIVOS -->
												
												
												<!-- PACIENTES -->
												<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
													
													<?php 
													
														// Duração dos atendimentos
														$duracao_atendimentos	= duracao_atendimentos($mysqli,$tipo_perfil,$equipe,$id_perfil,FALSE,FALSE);
														// Tipos de alta
														$tipos_alta				= contar_item_equipe($mysqli,'alta',$tipo_perfil,$equipe,$id_perfil,FALSE,FALSE);
														// Dietas iniciais
														$dietas_iniciais		= contar_item_equipe($mysqli,'dieta_inicial',$tipo_perfil,$equipe,$id_perfil,FALSE,FALSE);
														// Liquidos iniciais
														$liquidos_iniciais		= contar_item_equipe($mysqli,'liquido_inicial',$tipo_perfil,$equipe,$id_perfil,FALSE,FALSE);
														
														// Tempo para retirada de SNE
														$tempo_SNE_Oral			= duracao_conduta($mysqli,"VAA",$tipo_perfil,$id_perfil,FALSE,FALSE,"SNE","Via oral");
														// Tempo para retirada de PEG
														$tempo_PEG_Oral			= duracao_conduta($mysqli,"VAA",$tipo_perfil,$id_perfil,FALSE,FALSE,"PEG","Via oral");
														
														// Primeira conduta
														$primeira_conduta		= contar_primeira_conduta_equipe($mysqli,$tipo_perfil,$equipe,$id_perfil,FALSE,FALSE);
														
														$media_atendimentos_paciente	= atendimento_por_paciente($mysqli,$equipe,FALSE,FALSE);
														$media_atendimentos_colaborador = atendimento_por_colaborador($mysqli,$equipe,FALSE,FALSE);
													?>
													
													<br><br>
													<div class="col-md-4 col-sm-6 col-xs-12 text-left">
														
														<p>Cada paciente recebe em média</p>
														<h3><?php echo $media_atendimentos_paciente." atendimento(s)"; ?></h3>
														<br>
														
														<p>Cada colaborador realiza em média</p>
														<h3><?php echo $media_atendimentos_colaborador." atendimentos"; ?></h3>
														<br>
														
														<p>Os atendimentos duram em média</p>
														<h3><?php echo $duracao_atendimentos." dias"; ?></h3>
														<br>
														
														<h4> Motivos do fim do acompanhamento</h4>
														
														<?php
														
															for ($i=1;$i<sizeof($tipos_alta);$i++){
																
														?>
															
															<span><?php echo $tipos_alta[$i][1]; ?></span>
															<div class="progress">
																
																<?php
																	$per = round(100 * $tipos_alta[$i][2] / $tipos_alta[sizeof($tipos_alta)][2]); 
																?>
																
																<div class="progress-bar progress-bar-info" data-transitiongoal="<?php echo $per; ?>" style="width: <?php echo $per; ?>%;" aria-valuenow="<?php echo $per; ?>">
																	<?php echo $per."%"; ?>
																</div>
																
															</div>
														
														<?php
															}
														?>
														
													
													</div>
													
													<div class="col-md-8 col-sm-6 col-xs-12 text-left">
														
														<div class="col-md-6 col-sm-6 col-xs-12 text-center">
															<h4> Dieta inicial </h4>
															<div id="dieta_inicial" style="width:100%; height:200px;"></div>
														</div>
														
														<div class="col-md-6 col-sm-6 col-xs-12 text-center">
															<h4> Liquido inicial </h4>
															<div id="liquido_inicial" style="width:100%; height:200px;"></div>
														</div>
														
														<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
															<br><br>
															<h4> Retirada de VAA </h4>
															<div id="retirada_VAA" style="width:100%; height:200px;"></div>
														</div>
														
														<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
															<br><br>
															<h4> Primeira Conduta </h4>
															<div id="primeira_conduta" style="width:100%; height:200px;"></div>
														</div>
														
													</div>
														
													<div class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12 text-center">
														<br><br>
														<h4> Indicadores mensais de <?php echo date("Y"); ?> </h4>
														
														<table class="table table-condensed">
														
															<tr>
																<th>Indicador</th>
																<th>Jan</th>
																<th>Fev</th>
																<th>Mar</th>
																<th>Abr</th>
																<th>Mai</th>
																<th>Jun</th>
																<th>Jul</th>
																<th>Ago</th>
																<th>Set</th>
																<th>Out</th>
																<th>Nov</th>
																<th>Dez</th>
															</tr>
														<?php 
														
															// Atendimentos por colaborador
															echo "<tr>";
															echo "<td>Atendimentos por colaborador</td>";
															
															for ($i=1;$i<=12;$i++){
																
																
																
																$inicio = date("Y-".$i."-01");
																$fim	= date("Y-m-t", strtotime($inicio));
				
																echo "<td>". atendimento_por_colaborador($mysqli,$equipe,$inicio,$fim) ."</td>"; 
																
															}
															
															echo "</tr>";
															
															
															// Atendimentos por paciente
															echo "<tr>";
															echo "<td>Atendimentos por paciente</td>";
															
															for ($i=1;$i<=12;$i++){
																
																
																
																$inicio = date("Y-".$i."-01");
																$fim	= date("Y-m-t", strtotime($inicio));
				
																echo "<td>". atendimento_por_paciente($mysqli,$equipe,$inicio,$fim) ."</td>"; 
																
															}
															
															echo "</tr>";
															
															
															// Passientes com VAA
															echo "<tr>";
															echo "<td>Pacientes com VAA</td>";
															
															for ($i=1;$i<=12;$i++){
																
																
																
																$inicio = date("Y-".$i."-01");
																$fim	= date("Y-m-t", strtotime($inicio));
				
																echo "<td>". pacientes_com_VAA($mysqli,$equipe,$inicio,$fim) ."</td>"; 
																
															}
															
															echo "</tr>";
															
															
															// Taxa de alta
															echo "<tr>";
															echo "<td>Taxa de alta</td>";
															
															for ($i=1;$i<=12;$i++){
																
																
																
																$inicio = date("Y-".$i."-01");
																$fim	= date("Y-m-t", strtotime($inicio));
				
																echo "<td>". taxa_alta($mysqli,$equipe,$inicio,$fim) ."</td>"; 
																
															}
															
															echo "</tr>";
															
														?>
														</table>
														
													</div>
												
												</div>
												<!-- [FIM] PACIENTES -->
												
												
												<div role="tabpanel" class="tab-pane fade hidden-xs" id="tab_content3" aria-labelledby="profile-tab">
													
													<br><br>
													<div class="col-lg-8 col-md-8 col-sm-12 col-lg-offset-2 col-md-offset-2">
														<div class="alert alert-warning alert-dismissible fade in" role="alert">
															<strong>Holy guacamole!</strong> Essa parte não esta pronta ainda!
														</div>
													</div>
													
												</div>
											</div>
										</div>
										
									</div>
										
								</div>
							</div>
						</div>
					</div>
				</div>
				
                <br />
				

                <!-- FOOTER-->
				<?php include_once("include/layout/footer.php"); ?>
				
				
            </div>
            <!-- /page content -->
		
        </div>
		
    </div>
	
	
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/nicescroll/jquery.nicescroll.min.js"></script>

    <script src="assets/js/custom.js"></script>

    
	<!-- image cropping -->
    <script src="assets/js/cropping/cropper.min.js"></script>
    <script src="assets/js/cropping/main.js"></script>
	
	
	<!-- PAGINATION -->
    <script src="assets/js/pagination.js"></script>
	
	
	<!-- GRÁFICO -->
	<script src="assets/js/moris/raphael-min.js"></script>
    <script src="assets/js/moris/morris.js"></script>
	
		<?php
			
			
			
			// DADOS PARA GRAFICO DE BARRAS - OBJETIVOS POR MES
			for ($i=0;$i<=12;$i++){
				
				$inicio = date("Y-".$i."-01");
				$fim	= date("Y-m-t", strtotime($inicio));
				
				$data_objetivo[1][$i] = num_objetivos_finalizados($mysqli,$tipo_perfil,$id_perfil,$inicio,$fim);
				$data_objetivo[2][$i] = num_objetivos_previstos($mysqli,$tipo_perfil,$id_perfil,$inicio,$fim);
				
			}
			
			
			// DADOS PARA GRAFICO DE BARRAS - ATRASOS POR MES
			for ($i=0;$i<=12;$i++){
				
				$inicio = date("Y-".$i."-01");
				$fim	= date("Y-m-t", strtotime($inicio));
				
				$data_atraso[1][$i] = num_objetivos_atrasados($mysqli,$tipo_perfil,$id_perfil,$inicio,$fim);
				$data_atraso[2][$i] = num_objetivos_no_prazo($mysqli,$tipo_perfil,$id_perfil,$inicio,$fim);
				
			}
			
			
			// DADOS PARA RETIRADA DE VAA
			$tempo_retirada_VAA[1] = 0;
			$tempo_retirada_VAA[2] = 0;
			$tempo_retirada_VAA[3] = 0;
			
			for($i=1;$i<=sizeof($tempo_SNE_Oral);$i++){
				
				// 1 à 2 semanas
				if($tempo_SNE_Oral[$i] <= 14){
					$tempo_retirada_VAA[1] = $tempo_retirada_VAA[1] + 1;
				}
				
				// 2 à 4 semanas
				if($tempo_SNE_Oral[$i] > 14 AND $tempo_SNE_Oral[$i] <= 28){
					$tempo_retirada_VAA[2] = $tempo_retirada_VAA[2] + 1;
				}
				
				// mais de 4 semanas
				if($tempo_SNE_Oral[$i] > 28){
					$tempo_retirada_VAA[3] = $tempo_retirada_VAA[3] + 1;
				}
				
			}
			
			for($i=1;$i<=sizeof($tempo_PEG_Oral);$i++){
				
				// 1 à 2 semanas
				if($tempo_PEG_Oral[$i] <= 14){
					$tempo_retirada_VAA[1] = $tempo_retirada_VAA[1] + 1;
				}
				
				// 2 à 4 semanas
				if($tempo_PEG_Oral[$i] > 14 AND $tempo_PEG_Oral[$i] <= 28){
					$tempo_retirada_VAA[2] = $tempo_retirada_VAA[2] + 1;
				}
				
				// mais de 4 semanas
				if($tempo_PEG_Oral[$i] > 28){
					$tempo_retirada_VAA[3] = $tempo_retirada_VAA[3] + 1;
				}
				
			}
			
			
		?>
		
	<script>
		
		$(function () {
			
            var objetivos = [
                {
                    "period": "Jan",
                    "Finalizados": <?php echo $data_objetivo[1][1]; ?>,
					"Previstos": <?php echo $data_objetivo[2][1]; ?>
                },
                {
                    "period": "Fev",
                    "Finalizados": <?php echo $data_objetivo[1][2]; ?>,
					"Previstos": <?php echo $data_objetivo[2][2]; ?>
                },
                {
                    "period": "Mar",
                    "Finalizados": <?php echo $data_objetivo[1][3]; ?>,
					"Previstos": <?php echo $data_objetivo[2][3]; ?>
                },
                {
                    "period": "Abr",
                    "Finalizados": <?php echo $data_objetivo[1][4]; ?>,
					"Previstos": <?php echo $data_objetivo[2][4]; ?>
                },
                {
                    "period": "Mai",
                    "Finalizados": <?php echo $data_objetivo[1][5]; ?>,
					"Previstos": <?php echo $data_objetivo[2][5]; ?>
                },
                {
                    "period": "Jun",
                    "Finalizados": <?php echo $data_objetivo[1][6]; ?>,
					"Previstos": <?php echo $data_objetivo[2][6]; ?>
                },
                {
                    "period": "Jul",
                    "Finalizados": <?php echo $data_objetivo[1][7]; ?>,
					"Previstos": <?php echo $data_objetivo[2][7]; ?>
                },
                {
                    "period": "Ago",
                    "Finalizados": <?php echo $data_objetivo[1][8]; ?>,
					"Previstos": <?php echo $data_objetivo[2][8]; ?>
                },
                {
                    "period": "Set",
                    "Finalizados": <?php echo $data_objetivo[1][9]; ?>,
					"Previstos": <?php echo $data_objetivo[2][9]; ?>
                },
                {
                    "period": "Out",
                    "Finalizados": <?php echo $data_objetivo[1][10]; ?>,
					"Previstos": <?php echo $data_objetivo[2][10]; ?>
                },
                {
                    "period": "Nov",
                    "Finalizados": <?php echo $data_objetivo[1][11]; ?>,
					"Previstos": <?php echo $data_objetivo[2][11]; ?>
                },
                {
                    "period": "Dez",
                    "Finalizados": <?php echo $data_objetivo[1][12]; ?>,
					"Previstos": <?php echo $data_objetivo[2][12]; ?>
                }
			];
			
			var atrasos = [
                {
                    "period": "Jan",
                    "Finalizados": <?php echo $data_atraso[1][1]; ?>,
					"Previstos": <?php echo $data_atraso[2][1]; ?>
                },
                {
                    "period": "Fev",
                    "Finalizados": <?php echo $data_atraso[1][2]; ?>,
					"Previstos": <?php echo $data_atraso[2][2]; ?>
                },
                {
                    "period": "Mar",
                    "Finalizados": <?php echo $data_atraso[1][3]; ?>,
					"Previstos": <?php echo $data_atraso[2][3]; ?>
                },
                {
                    "period": "Abr",
                    "Finalizados": <?php echo $data_atraso[1][4]; ?>,
					"Previstos": <?php echo $data_atraso[2][4]; ?>
                },
                {
                    "period": "Mai",
                    "Finalizados": <?php echo $data_atraso[1][5]; ?>,
					"Previstos": <?php echo $data_atraso[2][5]; ?>
                },
                {
                    "period": "Jun",
                    "Finalizados": <?php echo $data_atraso[1][6]; ?>,
					"Previstos": <?php echo $data_atraso[2][6]; ?>
                },
                {
                    "period": "Jul",
                    "Finalizados": <?php echo $data_atraso[1][7]; ?>,
					"Previstos": <?php echo $data_atraso[2][7]; ?>
                },
                {
                    "period": "Ago",
                    "Finalizados": <?php echo $data_atraso[1][8]; ?>,
					"Previstos": <?php echo $data_atraso[2][8]; ?>
                },
                {
                    "period": "Set",
                    "Finalizados": <?php echo $data_atraso[1][9]; ?>,
					"Previstos": <?php echo $data_atraso[2][9]; ?>
                },
                {
                    "period": "Out",
                    "Finalizados": <?php echo $data_atraso[1][10]; ?>,
					"Previstos": <?php echo $data_atraso[2][10]; ?>
                },
                {
                    "period": "Nov",
                    "Finalizados": <?php echo $data_atraso[1][11]; ?>,
					"Previstos": <?php echo $data_atraso[2][11]; ?>
                },
                {
                    "period": "Dez",
                    "Finalizados": <?php echo $data_atraso[1][12]; ?>,
					"Previstos": <?php echo $data_atraso[2][12]; ?>
                }
			];
			
			var retirada_VAA = [
                {
                    "period": "Até 2 semanas",
                    "Finalizados": <?php echo $tempo_retirada_VAA[1]; ?>
                },
                {
                    "period": "2 à 4 semanas",
                    "Finalizados": <?php echo $tempo_retirada_VAA[2]; ?>
                },
                {
                    "period": "Mais do que 4 semanas",
                    "Finalizados": <?php echo $tempo_retirada_VAA[3]; ?>
                }
			];
			
			
			// GRAFICO DE OBJETIVOS
            var graph_objetivos = Morris.Bar({
                element: 'objetivos_geral',
                data: objetivos,
                xkey: 'period',
				xLabelMargin: 10,
				behaveLikeLine: true,
				resize: true,
                hideHover: 'auto',
                barColors: ['#286090', '#34495E', '#ACADAC', '#3498DB'],
                ykeys: ['Previstos', 'Finalizados'],
				labels: ['Previstos', 'Finalizados']
            });
			
			$('ul.nav a').on('shown.bs.tab', function (e) {
				graph_objetivos.redraw();
			});
			
			
			// GRAFICO DE ATRASOS
			var graph_atraso = Morris.Bar({
                element: 'objetivos_atrasados',
                data: atrasos,
                xkey: 'period',
				xLabelMargin: 10,
				behaveLikeLine: true,
				resize: true,
                hideHover: 'auto',
                barColors: ['#286090', '#34495E', '#ACADAC', '#3498DB'],
                ykeys: ['Previstos', 'Finalizados'],
				labels: ['No Prazo', 'Atrasados']
            });
			
			$('ul.nav a').on('shown.bs.tab', function (e) {
				graph_atraso.redraw();
			});
			
			
			// GRAFICO DE RETIRADA DE VAA
			var graph_retirada_VAA = Morris.Bar({
                element: 'retirada_VAA',
                data: retirada_VAA,
                xkey: 'period',
				xLabelMargin: 10,
				behaveLikeLine: true,
				resize: true,
                hideHover: 'auto',
                barColors: ['#286090', '#34495E', '#ACADAC', '#3498DB'],
                ykeys: ['Finalizados']
            });
			
			$('ul.nav a').on('shown.bs.tab', function (e) {
				graph_retirada_VAA.redraw();
			});
			
			
			
			
        });
		
	</script>
	
	<script>
		
		// GRAFICO DE DIETA INICIAL
		var graph_dieta_inicial = Morris.Donut({
			element: 'dieta_inicial',
			data: [
			
			<?php
				
				$total = $dietas_iniciais[sizeof($dietas_iniciais)][2];
				
				for ($j=1;$j<=sizeof($dietas_iniciais)-1;$j++){
					
					$per = round( 100 * $dietas_iniciais[$j][2] / $total);
					echo "{label: ' ".$dietas_iniciais[$j][1]." ', value: ".$per." }";
					
					if( $j < sizeof($dietas_iniciais)-1 ){
						echo ",";
					}
					
				}
			?>
			
			],
			colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
			formatter: function (y) {
				return y + "%"
			}
		});
		
		$('ul.nav a').on('shown.bs.tab', function (e) {
			graph_dieta_inicial.redraw();
		});
		
		
		// GRAFICO DE LIQUIDO INICIAL
		var graph_liquido_inicial = Morris.Donut({
			element: 'liquido_inicial',
			data: [
			
			<?php
				
				$total = $liquidos_iniciais[sizeof($liquidos_iniciais)][2];
				
				for ($j=1;$j<=sizeof($liquidos_iniciais)-1;$j++){
					
					$per = round(100 * $liquidos_iniciais[$j][2] / $total);
					echo "{label: ' ".$liquidos_iniciais[$j][1]." ', value: ".$per." }";
					
					if( $j < sizeof($liquidos_iniciais)-1 ){
						echo ",";
					}
					
				}
			?>
			
			],
			colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
			formatter: function (y) {
				return y + "%"
			}
		});
		
		$('ul.nav a').on('shown.bs.tab', function (e) {
			graph_liquido_inicial.redraw();
		});
		
		
		// GRAFICO DE PRIMEIRA CONDUTA
		var graph_primeira_conduta = Morris.Donut({
			element: 'primeira_conduta',
			data: [
			
			<?php
				
				$total = $primeira_conduta[sizeof($primeira_conduta)][2];
				
				for ($j=1;$j<=sizeof($primeira_conduta)-1;$j++){
					
					$per = round( 100 * $primeira_conduta[$j][2] / $total);
					echo "{label: ' ".$primeira_conduta[$j][1]." ', value: ".$per." }";
					
					if( $j < sizeof($primeira_conduta)-1 ){
						echo ",";
					}
					
				}
			?>
			
			],
			colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
			formatter: function (y) {
				return y + "%"
			}
		});
		
		$('ul.nav a').on('shown.bs.tab', function (e) {
			graph_primeira_conduta.redraw();
		});
		
		
		
    </script>
	
	
    
    <!-- /footer content -->
	
</body>

</html>

<?php

}else{
	
	header("Location: login.php");
	
}

?>