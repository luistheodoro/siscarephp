﻿<?php
	
	
	function msg_erro($erro){
		
		$texto = FALSE;
		
		switch($erro){
			
			/* ERRO DE CADASTRO */
			
			// Paciente
			case 101:
				$texto = "Falha ao cadastrar a terapia. (Erro: 101)";
				break;
				
			case 102:
				$texto = "Falha ao cadastrar a avaliação. (Erro: 102)";
				break;
			
			case 103:
				$texto = "Falha para gerenciar especificidade. (Erro: 103)";
				break;
				
			case 104:
				$texto = "Falha para alterar a frequência de atendimento. (Erro: 104)";
				break;
			
			case 1051:
				$texto = "Falha ao cadastrar dados gerais do paciente. (Erro: 1051)";
				break;
			
			case 1052:
				$texto = "Falha ao cadastrar dados de contato do paciente. (Erro: 1052)";
				break;
				
			case 1061:
				$texto = "Falha ao editar dados gerais do paciente. (Erro: 1061)";
				break;
					
			case 1062:
				$texto = "Falha ao editar dados de contato do paciente. (Erro: 1062)";
				break;
					
			// Objetivo
			case 111:
				$texto = "Falha para criar objetivo. (Erro: 111)";
				break;
				
			case 112:
				$texto = "Falha para editar objetivo. (Erro: 112)";
				break;
			
			case 113:
				$texto = "Falha para concluir objetivo. (Erro: 113)";
				break;
			
			// Usuario
			case 121:
				$texto = "Falha para criar usuario. (Erro 121)";
				break;
				
			case 122:
				$texto = "Usuário / E-mail já existe. (Erro 122)";
				break;
				
			case 123:
				$texto = "Senha inválida. (Erro 123)";
				break;
				
			case 124:
				$texto = "E-mail inválido. (Erro 124)";
				break;
				
			
			/* ERRO DE ACESSO */
			
			// Paciente
			case 201:
				$texto = "Você não possui acesso para criar uma terapia para esse paciente. (Erro: 201)";
				break;
				
			case 202:
				$texto = "Você não possui acesso para criar uma avaliação para esse paciente. (Erro: 202)";
				break;
			
			case 203:
				$texto = "Você não possui acesso para cadastrar um novo paciente. (Erro: 203)";
				break;
				
			case 204:
				$texto = "Você não possui acesso para editar os dados desse paciente. (Erro: 204)";
				break;
			
			// Geral
			case 211:
				$texto = "Você não possui acesso para gerenciar os riscos desse paciente. (Erro: 211)";
				break;
				
			case 212:
				$texto = "Você não possui acesso para editar a frequência de atendimentos desse paciente. (Erro: 212)";
				break;
			
			case 213:
				$texto = "Você não possui acesso para editar as especificações desse paciente. (Erro: 213)";
				break;
			
			case 214:
				$texto = "Você não possui acesso para editar o responsável por esse paciente. (Erro: 214)";
				break;
			
			// Objetivo
			case 221:
				$texto = "Você não possui acesso para criar um objetivo para esse paciente. (Erro: 221)";
				break;
				
			case 222:
				$texto = "Você não possui acesso para editar o objetivo desse paciente. (Erro: 222)";
				break;
				
			case 223:
				$texto = "Você não possui acesso para concluir o objetivo desse paciente. (Erro: 223)";
				break;
				
		}
		
		return $texto;
		
	}
	
	
?>