﻿<?php
	
	$ano_passado	= date("Y") - 1;
	$ano_atual		= date("Y");
	
	// DADOS PARA GRAFICO DE BARRAS - NUMERO TOTAL DE ATENDIMENTOS POR MES
	
	$min[1]['mes'] 			= 9999999999;
	$min[1]['atendimento'] 	= 9999999999;
	
	$max[1]['mes'] 			= 0;
	$max[1]['atendimento'] 	= 0;
	
	$data_1 = "data: [";
	
	for ($i=1;$i<=12;$i++){
		
		$periodo['inicio'] = date($ano_passado.'-'.$i.'-01');
		$periodo['fim'] = date($ano_passado.'-'.$i.'-t');
		$total = count_atendimento_equipe($mysqli,$periodo,$equipe);
		
		if($total < 1){
			$data_1 .= 0;
		}else{
			$data_1 .= $total;
		}
		
		if($i<12){
			$data_1 .= ", ";
		}
		
		if( $total < $min[1]['atendimento'] ){
			$min[1]['mes'] 			= $i;
			$min[1]['atendimento'] 	= $total;
		}
		
		if( $total > $max[1]['atendimento'] ){
			$max[1]['mes'] 			= $i;
			$max[1]['atendimento'] 	= $total;
		}
		
	}
	
	$data_1 .= "],";
	
	
	// DADOS PARA GRAFICO DE BARRAS - NUMERO TOTAL DE ATENDIMENTOS POR MES
	
	$min[2]['mes'] 			= 9999999999;
	$min[2]['atendimento'] 	= 9999999999;
	
	$max[2]['mes'] 			= 0;
	$max[2]['atendimento'] 	= 0;
	
	$data_2 = "data: [";
	
	for ($i=1;$i<=12;$i++){
		
		$periodo['inicio'] = date($ano_atual.'-'.$i.'-01');
		$periodo['fim'] = date($ano_atual.'-'.$i.'-t');
		$total = count_atendimento_equipe($mysqli,$periodo,$equipe);
		
		if($total < 1){
			$data_2 .= 0;
		}else{
			$data_2 .= $total;
		}
		
		if($i<12){
			$data_2 .= ", ";
		}
		
		if( $total < $min[2]['atendimento'] ){
			$min[2]['mes'] 			= $i;
			$min[2]['atendimento'] 	= $total;
		}
		
		if( $total > $max[2]['atendimento'] ){
			$max[2]['mes'] 			= $i;
			$max[2]['atendimento'] 	= $total;
		}
		
	}
	
	$data_2 .= "],";
	
?>
	
<script>
	
	var myChart9 = echarts.init(document.getElementById('mainb'), theme);
	myChart9.setOption({
		
		title: {
			text: 'Atendimentos',
			subtext: '<?php echo equipe($equipe,2); ?>'
		},
		//theme : theme,
		tooltip: {
			trigger: 'axis'
		},
		
		legend: {
			data: ['<?php echo $ano_passado; ?>', '<?php echo $ano_atual; ?>']
		},
		
		toolbox: {
			show: false
		},
		
		calculable: false,
		
		toolbox: {
			show: true,
			feature: {
				magicType: {
					show: true,
					type: ['line', 'bar', 'stack', 'tiled']
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		
		xAxis: [
			{
				type: 'category',
				data: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
			}
		],
		
		yAxis: [
			{
				type: 'value'
			}
		],
		
		series: [
			{
				name: '<?php echo $ano_passado; ?>',
				type: 'bar',
				<?php echo $data_1; ?>
				markPoint: {
					data: [
						{
							type: 'max',
							name: '???'
						},
						{
							type: 'min',
							name: '???'
						}
				]
				},
				markLine: {
					data: [
						{
							type: 'average',
							name: '???'
						}
				]
				}
			},
			{
				name: '<?php echo $ano_atual; ?>',
				type: 'bar',
				<?php echo $data_2; ?>
				markPoint: {
					data: [
						{
							type: 'max',
							name: '???'
						},
						{
							type: 'min',
							name: '???'
						}
				]
				},
				markLine: {
					data: [
						{
							type: 'average',
							name: '???'
						}
				]
				}
			}
		]
	});
	
	
</script>