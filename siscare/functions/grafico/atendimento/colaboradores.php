﻿<?php
	
	// DADOS PARA GRAFICO DE LINHAS - NUMERO DE ATENDIMENTOS POR MES POR COLABORADOR
	
	$j = 0;
	
	// Valores
	$query = "SELECT * FROM ".$_SESSION['user_Servico']."_usuarios WHERE equipe = '".$equipe."' ";
	foreach($mysqli->query($query) as $colaborador){
		
		$j = $j + 1;
		
		$chart[$j]['nome'] = $colaborador['nome'];
		
		for ($i=1;$i<=12;$i++){
			
			$periodo['inicio'] = date('Y-'.$i.'-01');
			$periodo['fim'] = date('Y-'.$i.'-t');
			$total = count_atendimento_usuario($mysqli,$periodo,$colaborador['id_usuario']);
			
			if($total < 1){
				$chart[$j][$i] = 0;
			}else{
				$chart[$j][$i] = $total;
			}
			
			
		}
		
		
		
	}
	
	$num_colaborador = $j;
	
	
	
	// ---------- DADOS PARA OS GRAFICOS --------- //
	
	// Legenda
	$legenda = "legend: { data: [";
	for ( $i=1 ; $i<=$num_colaborador ; $i++ ){
		
		if($i==1){
			$legenda .= "'".$chart[$i]['nome']."'";
		}else{
			$legenda .= ", '".$chart[$i]['nome']."'";
		}
		
	}
	$legenda .= "]},";
	
	
	// Series
	$serie = "series: [";
	for ( $i=1 ; $i<=$num_colaborador ; $i++ ){
			
			
			$jan = $chart[$i][1];
			$fev = $chart[$i][2];
			$mar = $chart[$i][3];
			$abr = $chart[$i][4];
			$mai = $chart[$i][5];
			$jun = $chart[$i][6];
			$jul = $chart[$i][7];
			$ago = $chart[$i][8];
			$set = $chart[$i][9];
			$out = $chart[$i][10];
			$nov = $chart[$i][11];
			$dez = $chart[$i][12];
		
		if($i==1){
			
			$serie .= "
		
				{
					name: '".$chart[$i]['nome']."',
					type: 'line',
					smooth: true,
					itemStyle: {
						normal: {
							areaStyle: {
								type: 'default'
							}
						}
					},
					data: [".$jan.", ".$fev.", ".$mar.", ".$abr.", ".$mai.", ".$jun.", ".$jul.", ".$ago.", ".$set.", ".$out.", ".$nov.", ".$dez."]
				}
				
			";
			
		}else{
			
			$serie .= "
		
				, {
					name: '".$chart[$i]['nome']."',
					type: 'line',
					smooth: true,
					itemStyle: {
						normal: {
							areaStyle: {
								type: 'default'
							}
						}
					},
					data: [".$jan.", ".$fev.", ".$mar.", ".$abr.", ".$mai.", ".$jun.", ".$jul.", ".$ago.", ".$set.", ".$out.", ".$nov.", ".$dez."]
				}
				
			";
			
		}
		
	}
	
	$serie .= "]";
	
?>



<script>

	// GRAFICO DE LINHAS - NUMERO DE ATENDIMENTOS POR MES POR COLABORADOR
	var myChart = echarts.init(document.getElementById('echart_line'), theme);
	myChart.setOption({
		
		title: {
			text: 'Atendimentos',
			subtext: 'Colaboradores'
		},
		
		tooltip: {
			trigger: 'axis'
		},
		
		<?php echo $legenda; ?>
		
		toolbox: {
			show: true,
			feature: {
				magicType: {
					show: true,
					type: ['line', 'bar', 'stack', 'tiled']
				},
				restore: {
					show: true
				},
				saveAsImage: {
					show: true
				}
			}
		},
		
		calculable: true,
		
		xAxis: [
			{
				type: 'category',
				boundaryGap: false,
				data: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
			}
		],
		
		yAxis: [
			{
				type: 'value'
			}
		],
		
		<?php echo $serie; ?>
		
	});
	
</script>