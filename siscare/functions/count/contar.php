<?php

function count_atendimento_usuario($mysqli,$periodo,$id_usuario){

	$query = "SELECT COUNT(id) as total FROM ".$_SESSION['user_Servico']."_terapia WHERE id_usuario = '".$id_usuario."' AND data BETWEEN '".$periodo['inicio']."' AND '".$periodo['fim']."' ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);

	return $total['total'];

}


function count_atendimento_equipe($mysqli,$periodo,$equipe){

	$query = "SELECT COUNT(id) as total FROM ".$_SESSION['user_Servico']."_terapia WHERE equipe = '".$equipe."' AND data BETWEEN '".$periodo['inicio']."' AND '".$periodo['fim']."' ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);

	return $total['total'];

}


function count_pacientes_usuario($mysqli,$equipe,$id_usuario){

	$tabela = $_SESSION['user_Servico']."_".$equipe;

	$query = "SELECT COUNT(id) as total FROM ".$tabela." WHERE responsavel = '".$id_usuario."' AND data_termino IS NULL ";
	$mysql_query = mysqli_query($mysqli,$query);
	
	if(gettype($mysql_query) == 'boolean'){
		$total['total'] = 0;
	}else{
		$total = mysqli_fetch_assoc($mysql_query);
	}
	
	return $total['total'];

}


function count_pacientes_equipe($mysqli,$equipe){

	$tabela = $_SESSION['user_Servico']."_".$equipe;

	$query = "SELECT COUNT(id) as total FROM ".$tabela." WHERE data_termino IS NULL ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);

	return $total['total'];

}


function count_objetivos_usuario($mysqli,$id_usuario){

	$tabela = $_SESSION['user_Servico']."_objetivo";

	$query = "SELECT COUNT(id) as total FROM ".$tabela." WHERE id_responsavel = '".$id_usuario."' AND fim_objetivo IS NULL ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);

	return $total['total'];

}

?>