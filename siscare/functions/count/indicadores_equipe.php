﻿<?php



/* ---------- INDICADORES POR USUARIO ---------- */


// CALCULA O TEMPO MÉDIO DE ATENDIMENTO
function duracao_atendimentos($mysqli,$tipo,$equipe,$id,$inicio,$fim){
	
	// CONDIÇÃO DE USUARIO OU EQUIPE
	if($tipo=="usuario"){
		$cond_1 = " responsavel = '".$id."' ";
	}
	if($tipo=="equipe"){
		$cond_1 = " 1=1 ";
	}
	
	// CONDIÇÃO DO PERIODO A PESQUISAR
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_2 = " data_inicio >= '".$inicio."' AND data_termino <= '".$fim."' ";
	}else{
		$cond_2 = " 1=1 ";
	}
	
	
	// Contadores
	$i = 0;
	$dias = 0;
	
	$tabela = $_SESSION['user_Servico']."_".$equipe;

	$query = "SELECT DATEDIFF(data_termino,data_inicio) AS DiffDate FROM ".$tabela." WHERE ".$cond_1." AND ".$cond_2." AND data_termino IS NOT NULL ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);

	if( mysqli_num_rows($mysql_query)>0 ){
		
		while($total = mysqli_fetch_array($mysql_query)){
			
			$i++;
			$dias = $dias + $total['DiffDate'];
			
		}
		
		return round($dias / $i,1);
		
	}else{
		return FALSE;
	}

}


// CONTAR ITEM
function contar_item_equipe($mysqli,$item,$tipo,$equipe,$id,$inicio,$fim){
	
	// CONDIÇÃO DE USUARIO OU EQUIPE
	if($tipo=="usuario"){
		$cond_1 = " responsavel = '".$id."' ";
	}
	if($tipo=="equipe"){
		$cond_1 = " 1=1 ";
	}
	
	// CONDIÇÃO DO PERIODO A PESQUISAR
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_2 = " data_termino >= '".$inicio."' AND data_termino <= '".$fim."' ";
	}else{
		$cond_2 = " 1=1 ";
	}
	
	$i = 1;
	$total = 0;
	
	$tabela = $_SESSION['user_Servico']."_".$equipe;
	
	$query = " 	SELECT 
					".$item.",
					COUNT(".$item.") as num
				FROM 
					".$tabela."
				WHERE 
					".$cond_1." AND
					".$cond_2." AND
					data_termino IS NOT NULL
				GROUP BY 
					".$item." ";
					
	
	foreach($mysqli->query($query) as $row) {  
	
		$coluna[$i][1] = $row[$item];
		$coluna[$i][2] = $row['num'];
		
		$total = $total + $row['num'];
		
		$i++;
		
	}
	
	$coluna[$i][1] = "total";
	$coluna[$i][2] = $total;

	return $coluna;

}


// PRIMEIRA CONDUTA
function contar_primeira_conduta_equipe($mysqli,$tipo,$equipe,$id,$inicio,$fim){
	
	// CONDIÇÃO DE USUARIO OU EQUIPE
	if($tipo=="usuario"){
		$cond_1 = " id_usuario = '".$id."' ";
	}
	if($tipo=="equipe"){
		$cond_1 = " 1=1 ";
	}
	
	// CONDIÇÃO DO PERIODO A PESQUISAR
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_2 = " data_termino >= '".$inicio."' AND data_termino <= '".$fim."' ";
	}else{
		$cond_2 = " 1=1 ";
	}
	
	$i = 1;
	$total = 0;
	
	$tabela = $_SESSION['user_Servico']."_terapia";
	
	
	$query_1 = "SELECT
					DISTINCT (id_paciente),
					conduta
				FROM
					".$tabela."
				WHERE
					".$cond_1." AND
					".$cond_2." AND
					equipe = '".$equipe."'
				ORDER BY
					data
				";
					
	$query_2 = "SELECT 
					conduta,
					COUNT(conduta) as num
				FROM
					(".$query_1.") as query_1
				GROUP BY 
					conduta";
					

	foreach($mysqli->query($query_2) as $row) {  
	
		$coluna[$i][1] = $row['conduta'];
		$coluna[$i][2] = $row['num'];
		
		$total = $total + $row['num'];
		
		$i++;
		
	}
	
	$coluna[$i][1] = "total";
	$coluna[$i][2] = $total;

	return $coluna;

}


function atendimento_por_paciente($mysqli,$equipe,$inicio,$fim){
	
	// CONDIÇÃO DO PERIODO A PESQUISAR NA TABELA DE PACIENTES DA EQUIPE
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_1 = " data_inicio <= '".$fim."' AND (data_termino >= '".$inicio."' OR data_termino IS NULL) ";
	}else{
		$cond_1 = " 1=1 ";
	}
	
	$tabela_1 = $_SESSION['user_Servico']."_".$equipe;
	
	// CONTAR NUMERO DE PACIENTES EM ATENDIMENTO NO MES
	$query_1 		= "SELECT COUNT(DISTINCT id_paciente) AS qnt FROM ".$tabela_1." WHERE ".$cond_1." ";
	$mysql_query_1 	= mysqli_query($mysqli,$query_1);
	$qnt_paciente 	= mysqli_fetch_assoc($mysql_query_1);
	
	
	// CONDIÇÃO DO PERIODO A PESQUISAR NA TABELA DE ATENDIMENTOS
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_2 = " data >= '".$inicio."' AND data <= '".$fim."' ";
	}else{
		$cond_2 = " 1=1 ";
	}
	
	$tabela_2 = $_SESSION['user_Servico']."_terapia";
	
	// CONTAR NUMERO DE TERAPIAS NO MES
	$query_2 		= "SELECT COUNT(id) AS qnt FROM ".$tabela_2." WHERE ".$cond_2." AND equipe='".$equipe."' ";
	$mysql_query_2 	= mysqli_query($mysqli,$query_2);
	$qnt_terapia 	= mysqli_fetch_assoc($mysql_query_2);
	
	
	// CALCULA O INDICADOR
	if($qnt_paciente['qnt'] > 0){
		$indicador = round($qnt_terapia['qnt'] / $qnt_paciente['qnt'],1);
	}else{
		$indicador = 0;
	}
	
	return $indicador;
	
}

function atendimento_por_colaborador($mysqli,$equipe,$inicio,$fim){
	
	// CONDIÇÃO DO PERIODO A PESQUISAR NA TABELA DE PACIENTES DA EQUIPE
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_1 = " data >= '".$inicio."' AND data <= '".$fim."' ";
	}else{
		$cond_1 = " 1=1 ";
	}
	
	$tabela_1 = $_SESSION['user_Servico']."_terapia";
	
	// CONTAR NUMERO DE COLABORADORES QUE REALIZARAM ATENDIMENTO NO PERIODO
	$query_1 			= "SELECT COUNT(DISTINCT id_usuario) AS qnt FROM ".$tabela_1." WHERE ".$cond_1." AND equipe='".$equipe."' ";
	$mysql_query_1 		= mysqli_query($mysqli,$query_1);
	$qnt_colaborador 	= mysqli_fetch_assoc($mysql_query_1);
	
	
	// CONDIÇÃO DO PERIODO A PESQUISAR NA TABELA DE ATENDIMENTOS
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_2 = " data >= '".$inicio."' AND data <= '".$fim."' ";
	}else{
		$cond_2 = " 1=1 ";
	}
	
	$tabela_2 = $_SESSION['user_Servico']."_terapia";
	
	// CONTAR NUMERO DE TERAPIAS NO MES
	$query_2 		= "SELECT COUNT(id) AS qnt FROM ".$tabela_2." WHERE ".$cond_2." AND equipe='".$equipe."' ";
	$mysql_query_2 	= mysqli_query($mysqli,$query_2);
	$qnt_terapia 	= mysqli_fetch_assoc($mysql_query_2);
	
	
	// CALCULA O INDICADOR
	if($qnt_colaborador['qnt'] > 0){
		$indicador = round($qnt_terapia['qnt'] / $qnt_colaborador['qnt'],1);
	}else{
		$indicador = 0;
	}
	
	return $indicador;
	
}


function pacientes_com_VAA($mysqli,$equipe,$inicio,$fim){
	
	/*
	PEGO A DATA DO PRIMEIRO 'SNE'
	PEGO A DATA DO PRIMEIRO 'VIA ORAL' QUE SEJA > DATA 'SNE'
	CONTABILIZA
	*/
	
	$j = 0;
	$k = 0;
	
	// CONDIÇÃO DO PERIODO A PESQUISAR NA TABELA DE PACIENTES DA EQUIPE
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_1 = " data_inicio <= '".$fim."' AND (data_termino >= '".$inicio."' OR data_termino IS NULL) ";
	}else{
		$cond_1 = " 1=1 ";
	}
	
	$tabela_1 = $_SESSION['user_Servico']."_".$equipe;
	
	// BUSCAR PACIENTES EM ATENDIMENTO NO PERIODO
	$query_1 		= "SELECT DISTINCT id_paciente FROM ".$tabela_1." WHERE ".$cond_1." ";
	$mysql_query_1 	= mysqli_query($mysqli,$query_1);
	
	while ($paciente = mysqli_fetch_array($mysql_query_1)){
		
		$j++;
		
		// CONDIÇÃO DO PERIODO A PESQUISAR NA TABELA DE ESPEC DO PACIENTE
		if($inicio!=FALSE AND $fim!=FALSE){
			$cond_2 = " modificado_em <= '".$fim."' ";
		}else{
			$cond_2 = " 1=1 ";
		}
		
		// CONDIÇÃO COM VAA
		$cond_3 = " (VAA='SNE' OR VAA='PEG') ";
		
		$tabela_2 = $_SESSION['user_Servico']."_espec";
		
		$query_2 		= "SELECT modificado_em FROM ".$tabela_2." WHERE id_paciente='".$paciente['id_paciente']."' AND ".$cond_2." AND ".$cond_3." ORDER BY modificado_em DESC LIMIT 1";
		$mysql_query_2 	= mysqli_query($mysqli,$query_2);
		
		if( mysqli_num_rows($mysql_query_2)>0 ){
			
			$VAA = mysqli_fetch_assoc($mysql_query_2);
			
			// CONDIÇÃO SEM VAA
			$cond_4 = " VAA!='SNE' AND VAA!='PEG' ";
			$cond_5 = " modificado_em >= '".$VAA['modificado_em']."'";
			
			$query_3 = "SELECT * FROM ".$tabela_2." WHERE id_paciente='".$paciente['id_paciente']."' AND ".$cond_2." AND ".$cond_4." AND ".$cond_5." ORDER BY modificado_em DESC LIMIT 1";
			$mysql_query_3 	= mysqli_query($mysqli,$query_3);
			
			if( mysqli_num_rows($mysql_query_3)==0 ){
				$k++;
			}
		
		}
		
		
	}
	
	if($j > 0){
		$i = round(100 * $k / $j,1);
	}else{
		$i = 0;
	}
	
	
	return $i."%";
	
}


function taxa_alta($mysqli,$equipe,$inicio,$fim){
	
	// CONDIÇÃO DO PERIODO A PESQUISAR NA TABELA DE PACIENTES DA EQUIPE
	$cond_1 = " data_inicio <= '".$fim."' AND (data_termino >= '".$inicio."' OR data_termino IS NULL) ";
	
	// CONDIÇÃO DO PERIODO A PESQUISAR NA TABELA DE PACIENTES DA EQUIPE
	$cond_2 = " data_termino <= '".$fim."' AND data_termino >= '".$inicio."' ";
	
	$tabela_1 = $_SESSION['user_Servico']."_".$equipe;
	
	$query_1 = "SELECT COUNT(DISTINCT id_paciente) AS qnt FROM ".$tabela_1." WHERE ".$cond_1." ";
	$mysql_query_1 = mysqli_query($mysqli,$query_1);
	$total = mysqli_fetch_assoc($mysql_query_1);
	
	$query_2 = "SELECT COUNT(DISTINCT id_paciente) AS qnt FROM ".$tabela_1." WHERE ".$cond_2." ";
	$mysql_query_2 = mysqli_query($mysqli,$query_2);
	$alta = mysqli_fetch_assoc($mysql_query_2);
	
	if($total['qnt']>0){
		return round( 100 * $alta['qnt'] / $total['qnt'],1)."%";
	}else{
		return "0%";
	}
	
	
}


function taxa_gerenciamento($mysqli,$equipe,$inicio,$fim){
	
	// CONDIÇÃO DO PERIODO A PESQUISAR NA TABELA DE PACIENTES DA EQUIPE
	$cond_1 = " data_inicio <= '".$fim."' AND (data_termino >= '".$inicio."' OR data_termino IS NULL) ";
	
	// CONDIÇÃO DO PERIODO A PESQUISAR NA TABELA DE PACIENTES DA EQUIPE
	$cond_2 = " data_termino <= '".$fim."' AND data_termino >= '".$inicio."' ";
	
	$tabela_1 = $_SESSION['user_Servico']."_".$equipe;
	
	$query_1 = "SELECT COUNT(DISTINCT id_paciente) AS qnt FROM ".$tabela_1." WHERE ".$cond_1." ";
	$mysql_query_1 = mysqli_query($mysqli,$query_1);
	$total = mysqli_fetch_assoc($mysql_query_1);
	
	$query_2 = "SELECT COUNT(DISTINCT id_paciente) AS qnt FROM ".$tabela_1." WHERE ".$cond_2." ";
	$mysql_query_2 = mysqli_query($mysqli,$query_2);
	$alta = mysqli_fetch_assoc($mysql_query_2);
	
	if($total['qnt']>0){
		return round( 100 * $alta['qnt'] / $total['qnt'],1)."%";
	}else{
		return "0%";
	}
	
	
}



?>





















