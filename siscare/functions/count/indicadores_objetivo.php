﻿<?php



// CONTA O NUMERO DE OBJETIVOS FINALIZADOS
function num_objetivos_finalizados($mysqli,$tipo,$id,$inicio,$fim){
	
	
	// CONDIÇÃO DE USUARIO OU EQUIPE
	if($tipo=="usuario"){
		$cond_1 = " id_responsavel = '".$id."' ";
	}
	if($tipo=="equipe"){
		$cond_1 = " equipe = '".$id."' ";
	}
	
	// CONDIÇÃO DO PERIODO A PESQUISAR
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_2 = " fim_objetivo >= '".$inicio."' AND fim_objetivo <= '".$fim."' ";
	}else{
		$cond_2 = " 1=1 ";
	}
	
	
	$tabela = $_SESSION['user_Servico']."_objetivo";

	$query = "SELECT COUNT(id) as total FROM ".$tabela." WHERE ".$cond_1." AND ".$cond_2." AND fim_objetivo IS NOT NULL ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);

	return $total['total'];

}


// CONTA O NUMERO DE OBJETIVOS PREVISTOS
function num_objetivos_previstos($mysqli,$tipo,$id,$inicio,$fim){
	
	
	// CONDIÇÃO DE USUARIO OU EQUIPE
	if($tipo=="usuario"){
		$cond_1 = " id_responsavel = '".$id."' ";
	}
	if($tipo=="equipe"){
		$cond_1 = " equipe = '".$id."' ";
	}
	
	// CONDIÇÃO DO PERIODO A PESQUISAR
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_2 = " previsao_objetivo >= '".$inicio."' AND previsao_objetivo <= '".$fim."' ";
	}else{
		$cond_2 = " 1=1 ";
	}
	
	
	$tabela = $_SESSION['user_Servico']."_objetivo";

	$query = "SELECT COUNT(id) as total FROM ".$tabela." WHERE ".$cond_1." AND ".$cond_2." ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);

	return $total['total'];

}


// CONTA O NUMERO DE OBJETIVOS FINALIZADOS COM ATRASO
function num_objetivos_atrasados($mysqli,$tipo,$id,$inicio,$fim){
	
	// CONDIÇÃO DE USUARIO OU EQUIPE
	if($tipo=="usuario"){
		$cond_1 = " id_responsavel = '".$id."' ";
	}
	if($tipo=="equipe"){
		$cond_1 = " equipe = '".$id."' ";
	}
	
	// CONDIÇÃO DO PERIODO A PESQUISAR
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_2 = " fim_objetivo >= '".$inicio."' AND fim_objetivo <= '".$fim."' ";
	}else{
		$cond_2 = " 1=1 ";
	}
	
	// CONDIÇÃO DE ATRASADO
	$cond_3 = "fim_objetivo > previsao_objetivo";
	
	
	
	$tabela = $_SESSION['user_Servico']."_objetivo";

	$query = "SELECT COUNT(id) as total FROM ".$tabela." WHERE ".$cond_1." AND ".$cond_2." AND ".$cond_3." AND fim_objetivo IS NOT NULL ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);

	return $total['total'];

}


// CONTA O NUMERO DE OBJETIVOS FINALIZADOS NO PRAZO
function num_objetivos_no_prazo($mysqli,$tipo,$id,$inicio,$fim){
	
	// CONDIÇÃO DE USUARIO OU EQUIPE
	if($tipo=="usuario"){
		$cond_1 = " id_responsavel = '".$id."' ";
	}
	if($tipo=="equipe"){
		$cond_1 = " equipe = '".$id."' ";
	}
	
	// CONDIÇÃO DO PERIODO A PESQUISAR
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_2 = " fim_objetivo >= '".$inicio."' AND fim_objetivo <= '".$fim."' ";
	}else{
		$cond_2 = " 1=1 ";
	}
	
	// CONDIÇÃO DE ATRASADO
	$cond_3 = "fim_objetivo <= previsao_objetivo";
	
	
	
	$tabela = $_SESSION['user_Servico']."_objetivo";

	$query = "SELECT COUNT(id) as total FROM ".$tabela." WHERE ".$cond_1." AND ".$cond_2." AND ".$cond_3." AND fim_objetivo IS NOT NULL ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);

	return $total['total'];

}


// CONTA A MEDIA DE DIAS DE ATRASO DOS OBJETIVOS FINALIZADOS
function diff_objetivo_planejado_e_real($mysqli,$tipo,$id,$inicio,$fim){

	// CONDIÇÃO DE USUARIO OU EQUIPE
	if($tipo=="usuario"){
		$cond_1 = " id_responsavel = '".$id."' ";
	}
	if($tipo=="equipe"){
		$cond_1 = " equipe = '".$id."' ";
	}
	
	// CONDIÇÃO DO PERIODO A PESQUISAR
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_2 = " fim_objetivo >= '".$inicio."' AND fim_objetivo <= '".$fim."' ";
	}else{
		$cond_2 = " 1=1 ";
	}
	
	
	// Contadores
	$i = 0;
	$dias = 0;
	
	$tabela = $_SESSION['user_Servico']."_objetivo";
	
	$query = "SELECT DATEDIFF(fim_objetivo,previsao_objetivo) AS DiffDate FROM ".$tabela." WHERE ".$cond_1." AND ".$cond_2." AND fim_objetivo IS NOT NULL ";
	$mysql_query = mysqli_query($mysqli,$query);
	
	if( mysqli_num_rows($mysql_query)>0 ){
		
		while($total = mysqli_fetch_array($mysql_query)){
			
			$i++;
			$dias = $dias + $total['DiffDate'];
			
		}
		
		return round($dias / $i,1);
		
	}else{
		return FALSE;
	}

}


// CONTA O TEMPO PLANEJADO PARA COMPLETAR OS OBJETIVOS
function duracao_prevista_objetivo($mysqli,$tipo,$id,$inicio,$fim){
	
	// CONDIÇÃO DE USUARIO OU EQUIPE
	if($tipo=="usuario"){
		$cond_1 = " id_responsavel = '".$id."' ";
	}
	if($tipo=="equipe"){
		$cond_1 = " equipe = '".$id."' ";
	}
	
	// CONDIÇÃO DO PERIODO A PESQUISAR
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_2 = " previsao_objetivo >= '".$inicio."' AND previsao_objetivo <= '".$fim."' ";
	}else{
		$cond_2 = " 1=1 ";
	}
	
	
	// Contadores
	$i = 0;
	$dias = 0;
	
	$tabela = $_SESSION['user_Servico']."_objetivo";
	
	$query = "SELECT DATEDIFF(previsao_objetivo,inicio_objetivo) AS DiffDate FROM ".$tabela." WHERE ".$cond_1." AND ".$cond_2." ";
	$mysql_query = mysqli_query($mysqli,$query);
	
	if( mysqli_num_rows($mysql_query)>0 ){
		
		while($total = mysqli_fetch_array($mysql_query)){
			
			$i++;
			$dias = $dias + $total['DiffDate'];
			
		}
		
		return round($dias / $i,1);
		
	}else{
		return FALSE;
	}

}


// CONTA O TEMPO REAL PARA COMPLETAR OS OBJETIVOS
function duracao_real_objetivo($mysqli,$tipo,$id,$inicio,$fim){
	
	// CONDIÇÃO DE USUARIO OU EQUIPE
	if($tipo=="usuario"){
		$cond_1 = " id_responsavel = '".$id."' ";
	}
	if($tipo=="equipe"){
		$cond_1 = " equipe = '".$id."' ";
	}
	
	// CONDIÇÃO DO PERIODO A PESQUISAR
	if($inicio!=FALSE AND $fim!=FALSE){
		$cond_2 = " fim_objetivo >= '".$inicio."' AND fim_objetivo <= '".$fim."' ";
	}else{
		$cond_2 = " 1=1 ";
	}
	
	
	// Contadores
	$i = 0;
	$dias = 0;
	
	$tabela = $_SESSION['user_Servico']."_objetivo";
	
	$query = "SELECT DATEDIFF(fim_objetivo,inicio_objetivo) AS DiffDate FROM ".$tabela." WHERE ".$cond_1." AND ".$cond_2." AND fim_objetivo IS NOT NULL ";
	$mysql_query = mysqli_query($mysqli,$query);
	
	if( mysqli_num_rows($mysql_query)>0 ){
		
		while($total = mysqli_fetch_array($mysql_query)){
			
			$i++;
			$dias = $dias + $total['DiffDate'];
			
		}
		
		return round($dias / $i,1);
		
	}else{
		return FALSE;
	}

}



?>





















