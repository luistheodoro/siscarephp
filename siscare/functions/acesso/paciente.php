﻿<?php


/* ------------------ PACIENTE ------------------ */

	
	/* FUNÇÃO VERIFICADA */
	function acesso_criar_terapia($mysqli,$usuario,$paciente){
		
		$equipe = $usuario['geral']['equipe'];
		$acesso	= 'criar_terapia';
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
		
			// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELA EQUIPE
			if($paciente['geral'][$equipe] == 1){
			
				// VERIFICA ACESSO DO USUARIO NA EQUIPE
				if($usuario['acesso'][$acesso] == 3){
				
					return TRUE;
				
				}else{
					
					// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELO USUARIO
					if($paciente[$equipe]['responsavel'] == $usuario['geral']['id_usuario']){
						
						// VERIFICA ACESSO DO USUARIO NO PACIENTE QUE ELE ATENDE
						if($usuario['acesso'][$acesso] == 1 OR $usuario['acesso'][$acesso] == 2){
							
							return TRUE;
							
						}else{
						
							return FALSE;
						
						}
						
					}else{
						
						// BUSCA DADOS DO RESPONSAVEL PELO PACIENTE
						$id_responsavel = $paciente[$equipe]['responsavel'];
						$responsavel = busca_usuario_geral($mysqli,$id_responsavel);
						
						// VERIFICA SE RESPONSAVEL PELO PACIENTE POSSUI UM SUPERIOR
						if( $responsavel['id_superior'] > 0 ){
						
							// VERIFICA ACESSO DO USUARIO NO PACIENTE ATENDIDO POR ALGUEM ABAIXO ELE
							if( 
								$responsavel['id_superior'] == $usuario['geral']['id_usuario'] AND 
								$usuario['acesso'][$acesso] == 2 
							){
								
								return TRUE;
								
							}else{
								
								return FALSE;
								
							}
						
						}else{
						
							return FALSE;
						
						}
						
						
					}
					
					
				
				}
				
				
				
			}else{
				
				return FALSE;
				
			}
			
			
		}
	
	}
	
	
	/* FUNÇÃO VERIFICADA */
	function acesso_criar_objetivo_1($mysqli,$usuario){
		
		$texto = "criar_objetivo_".$usuario['geral']['equipe'];
		if( 
			$usuario['acesso']['criar_objetivo'] > 0
		){
			
			return TRUE;
			
		}else{
			
			return FALSE;
			
		}
		
	}
	
	
	/* FUNÇÃO VERIFICADA */
	function acesso_criar_objetivo_2($mysqli,$usuario,$paciente){
		
		$equipe = $usuario['geral']['equipe'];
		$acesso	= 'editar_objetivo';
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
		
			// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELA EQUIPE
			if($paciente['geral'][$equipe] == 1){
				
				// VERIFICA SE PACIENTE POSSUI ALGUM OBJETIVO
				$query 	= "	SELECT 
						COUNT(*) as total
					FROM 
						".$_SESSION['user_Servico']."_objetivo 
					WHERE
						id_paciente = '".$paciente['geral']['id']."' AND
						equipe = '".$equipe."' AND
						progresso < '100'
					";
		
				$mysql_query = mysqli_query($mysqli,$query) or exit(mysql_error());
				$total = mysqli_fetch_assoc($mysql_query);
				
				
				// VERIFICA SE PACIENTE JA POSSUI OBJETIVO
				if( $total['total'] == 0 ){
					
					// VERIFICA ACESSO DO USUARIO NA EQUIPE
					if($usuario['acesso'][$acesso] == 3){
					
						return TRUE;
					
					}else{
						
						// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELO USUARIO
						if($paciente[$equipe]['responsavel'] == $usuario['geral']['id_usuario']){
							
							// VERIFICA ACESSO DO USUARIO NO PACIENTE QUE ELE ATENDE
							if($usuario['acesso'][$acesso] == 1 OR $usuario['acesso'][$acesso] == 2){
								
								return TRUE;
								
							}else{
							
								return FALSE;
							
							}
							
						}else{
							
							// BUSCA DADOS DO RESPONSAVEL PELO PACIENTE
							$id_responsavel = $paciente[$equipe]['responsavel'];
							$responsavel = busca_usuario_geral($mysqli,$id_responsavel);
							
							// VERIFICA SE RESPONSAVEL PELO PACIENTE POSSUI UM SUPERIOR
							if( $responsavel['id_superior'] > 0 ){
							
								// VERIFICA ACESSO DO USUARIO NO PACIENTE ATENDIDO POR ALGUEM ABAIXO ELE
								if( 
									$responsavel['id_superior'] == $usuario['geral']['id_usuario'] AND 
									$usuario['acesso'][$acesso] == 2 
								){
									
									return TRUE;
									
								}else{
									
									return FALSE;
									
								}
							
							}else{
							
								return FALSE;
							
							}
							
							
						}
						
						
					
					}
				
				}else{
				
					return FALSE;
				
				}
				
			}else{
				
				return FALSE;
				
			}
			
			
		}
	
	}
	
	
	/* FUNÇÃO VERIFICADA */
	function acesso_editar_objetivo($mysqli,$usuario,$paciente){
		
		$equipe = $usuario['geral']['equipe'];
		$acesso	= 'editar_objetivo';
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
		
			// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELA EQUIPE
			if($paciente['geral'][$equipe] == 1){
				
				// VERIFICA SE PACIENTE POSSUI ALGUM OBJETIVO
				$query 	= "	SELECT 
						COUNT(*) as total
					FROM 
						".$_SESSION['user_Servico']."_objetivo 
					WHERE
						id_paciente = '".$paciente['geral']['id']."' AND
						equipe = '".$equipe."' AND
						progresso < '100'
					";
		
				$mysql_query = mysqli_query($mysqli,$query) or exit(mysql_error());
				$total = mysqli_fetch_assoc($mysql_query);
				
				
				// VERIFICA SE PACIENTE JA POSSUI OBJETIVO
				if( $total['total'] == 1 ){
					
					// VERIFICA ACESSO DO USUARIO NA EQUIPE
					if($usuario['acesso'][$acesso] == 3){
					
						return TRUE;
					
					}else{
						
						// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELO USUARIO
						if($paciente[$equipe]['responsavel'] == $usuario['geral']['id_usuario']){
							
							// VERIFICA ACESSO DO USUARIO NO PACIENTE QUE ELE ATENDE
							if($usuario['acesso'][$acesso] == 1 OR $usuario['acesso'][$acesso] == 2){
								
								return TRUE;
								
							}else{
							
								return FALSE;
							
							}
							
						}else{
							
							// BUSCA DADOS DO RESPONSAVEL PELO PACIENTE
							$id_responsavel = $paciente[$equipe]['responsavel'];
							$responsavel = busca_usuario_geral($mysqli,$id_responsavel);
							
							// VERIFICA SE RESPONSAVEL PELO PACIENTE POSSUI UM SUPERIOR
							if( $responsavel['id_superior'] > 0 ){
							
								// VERIFICA ACESSO DO USUARIO NO PACIENTE ATENDIDO POR ALGUEM ABAIXO ELE
								if( 
									$responsavel['id_superior'] == $usuario['geral']['id_usuario'] AND 
									$usuario['acesso'][$acesso] == 2 
								){
									
									return TRUE;
									
								}else{
									
									return FALSE;
									
								}
							
							}else{
							
								return FALSE;
							
							}
							
							
						}
						
						
					
					}
				
				}else{
				
					return FALSE;
				
				}
				
			}else{
				
				return FALSE;
				
			}
			
			
		}
	
	}
	
	
	/* FUNÇÃO VERIFICADA */
	function acesso_concluir_objetivo($mysqli,$usuario,$paciente){
		
		$equipe = $usuario['geral']['equipe'];
		$acesso	= 'concluir_objetivo';
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
		
			// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELA EQUIPE
			if($paciente['geral'][$equipe] == 1){
				
				// VERIFICA SE PACIENTE POSSUI ALGUM OBJETIVO
				$query 	= "	SELECT 
						COUNT(*) as total
					FROM 
						".$_SESSION['user_Servico']."_objetivo 
					WHERE
						id_paciente = '".$paciente['geral']['id']."' AND
						equipe = '".$equipe."' AND
						progresso < '100'
					";
		
				$mysql_query = mysqli_query($mysqli,$query) or exit(mysql_error());
				$total = mysqli_fetch_assoc($mysql_query);
				
				
				// VERIFICA SE PACIENTE JA POSSUI OBJETIVO
				if( $total['total'] == 1 ){
					
					// VERIFICA ACESSO DO USUARIO NA EQUIPE
					if($usuario['acesso'][$acesso] == 3){
					
						return TRUE;
					
					}else{
						
						// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELO USUARIO
						if($paciente[$equipe]['responsavel'] == $usuario['geral']['id_usuario']){
							
							// VERIFICA ACESSO DO USUARIO NO PACIENTE QUE ELE ATENDE
							if($usuario['acesso'][$acesso] == 1 OR $usuario['acesso'][$acesso] == 2){
								
								return TRUE;
								
							}else{
							
								return FALSE;
							
							}
							
						}else{
							
							// BUSCA DADOS DO RESPONSAVEL PELO PACIENTE
							$id_responsavel = $paciente[$equipe]['responsavel'];
							$responsavel = busca_usuario_geral($mysqli,$id_responsavel);
							
							// VERIFICA SE RESPONSAVEL PELO PACIENTE POSSUI UM SUPERIOR
							if( $responsavel['id_superior'] > 0 ){
							
								// VERIFICA ACESSO DO USUARIO NO PACIENTE ATENDIDO POR ALGUEM ABAIXO ELE
								if( 
									$responsavel['id_superior'] == $usuario['geral']['id_usuario'] AND 
									$usuario['acesso'][$acesso] == 2 
								){
									
									return TRUE;
									
								}else{
									
									return FALSE;
									
								}
							
							}else{
							
								return FALSE;
							
							}
							
							
						}
						
						
					
					}
				
				}else{
				
					return FALSE;
				
				}
				
			}else{
				
				return FALSE;
				
			}
			
			
		}
	
	}
	
	
	/* FUNÇÃO VERIFICADA */
	function acesso_gerenciar_risco($mysqli,$usuario,$paciente){
		
		$equipe = $usuario['geral']['equipe'];
		$acesso	= 'gerenciar_risco';
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
		
			// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELA EQUIPE
			if($paciente['geral'][$equipe] == 1){
			
				// VERIFICA ACESSO DO USUARIO NA EQUIPE
				if($usuario['acesso'][$acesso] == 3){
				
					return TRUE;
				
				}else{
					
					// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELO USUARIO
					if($paciente[$equipe]['responsavel'] == $usuario['geral']['id_usuario']){
						
						// VERIFICA ACESSO DO USUARIO NO PACIENTE QUE ELE ATENDE
						if($usuario['acesso'][$acesso] == 1 OR $usuario['acesso'][$acesso] == 2){
							
							return TRUE;
							
						}else{
						
							return FALSE;
						
						}
						
					}else{
						
						// BUSCA DADOS DO RESPONSAVEL PELO PACIENTE
						$id_responsavel = $paciente[$equipe]['responsavel'];
						$responsavel = busca_usuario_geral($mysqli,$id_responsavel);
						
						// VERIFICA SE RESPONSAVEL PELO PACIENTE POSSUI UM SUPERIOR
						if( $responsavel['id_superior'] > 0 ){
						
							// VERIFICA ACESSO DO USUARIO NO PACIENTE ATENDIDO POR ALGUEM ABAIXO ELE
							if( 
								$responsavel['id_superior'] == $usuario['geral']['id_usuario'] AND 
								$usuario['acesso'][$acesso] == 2 
							){
								
								return TRUE;
								
							}else{
								
								return FALSE;
								
							}
						
						}else{
						
							return FALSE;
						
						}
						
						
					}
					
					
				
				}
				
				
				
			}else{
				
				return FALSE;
				
			}
			
			
		}
	
	}

	
	/* FUNÇÃO VERIFICADA */
	function acesso_editar_frequencia($mysqli,$usuario,$paciente){
		
		$equipe = $usuario['geral']['equipe'];
		$acesso	= 'editar_frequencia';
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
		
			// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELA EQUIPE
			if($paciente['geral'][$equipe] == 1){
			
				// VERIFICA ACESSO DO USUARIO NA EQUIPE
				if($usuario['acesso'][$acesso] == 3){
				
					return TRUE;
				
				}else{
					
					// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELO USUARIO
					if($paciente[$equipe]['responsavel'] == $usuario['geral']['id_usuario']){
						
						// VERIFICA ACESSO DO USUARIO NO PACIENTE QUE ELE ATENDE
						if($usuario['acesso'][$acesso] == 1 OR $usuario['acesso'][$acesso] == 2){
							
							return TRUE;
							
						}else{
						
							return FALSE;
						
						}
						
					}else{
						
						// BUSCA DADOS DO RESPONSAVEL PELO PACIENTE
						$id_responsavel = $paciente[$equipe]['responsavel'];
						$responsavel = busca_usuario_geral($mysqli,$id_responsavel);
						
						// VERIFICA SE RESPONSAVEL PELO PACIENTE POSSUI UM SUPERIOR
						if( $responsavel['id_superior'] > 0 ){
						
							// VERIFICA ACESSO DO USUARIO NO PACIENTE ATENDIDO POR ALGUEM ABAIXO ELE
							if( 
								$responsavel['id_superior'] == $usuario['geral']['id_usuario'] AND 
								$usuario['acesso'][$acesso] == 2 
							){
								
								return TRUE;
								
							}else{
								
								return FALSE;
								
							}
						
						}else{
						
							return FALSE;
						
						}
						
						
					}
					
					
				
				}
				
				
				
			}else{
				
				return FALSE;
				
			}
			
			
		}
	
	}

	
	/* FUNÇÃO VERIFICADA */
	function acesso_editar_responsavel($mysqli,$usuario,$paciente){
		
		$equipe = $usuario['geral']['equipe'];
		$acesso	= 'editar_responsavel';
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
		
			// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELA EQUIPE
			if($paciente['geral'][$equipe] == 1){
			
				// VERIFICA ACESSO DO USUARIO NA EQUIPE
				if($usuario['acesso'][$acesso] == 3){
				
					return TRUE;
				
				}else{
					
					// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELO USUARIO
					if($paciente[$equipe]['responsavel'] == $usuario['geral']['id_usuario']){
						
						// VERIFICA ACESSO DO USUARIO NO PACIENTE QUE ELE ATENDE
						if($usuario['acesso'][$acesso] == 1 OR $usuario['acesso'][$acesso] == 2){
							
							return TRUE;
							
						}else{
						
							return FALSE;
						
						}
						
					}else{
						
						// BUSCA DADOS DO RESPONSAVEL PELO PACIENTE
						$id_responsavel = $paciente[$equipe]['responsavel'];
						$responsavel = busca_usuario_geral($mysqli,$id_responsavel);
						
						// VERIFICA SE RESPONSAVEL PELO PACIENTE POSSUI UM SUPERIOR
						if( $responsavel['id_superior'] > 0 ){
						
							// VERIFICA ACESSO DO USUARIO NO PACIENTE ATENDIDO POR ALGUEM ABAIXO ELE
							if( 
								$responsavel['id_superior'] == $usuario['geral']['id_usuario'] AND 
								$usuario['acesso'][$acesso] == 2 
							){
								
								return TRUE;
								
							}else{
								
								return FALSE;
								
							}
						
						}else{
						
							return FALSE;
						
						}
						
						
					}
					
					
				
				}
				
				
				
			}else{
				
				return FALSE;
				
			}
			
			
		}
	
	}
	
	
	/* FUNÇÃO VERIFICADA */
	function acesso_editar_espec($mysqli,$usuario,$paciente){
		
		$equipe = $usuario['geral']['equipe'];
		$acesso	= 'editar_espec';
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
		
			// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELA EQUIPE
			if($paciente['geral'][$equipe] == 1){
			
				// VERIFICA ACESSO DO USUARIO NA EQUIPE
				if($usuario['acesso'][$acesso] == 3){
				
					return TRUE;
				
				}else{
					
					// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELO USUARIO
					if($paciente[$equipe]['responsavel'] == $usuario['geral']['id_usuario']){
						
						// VERIFICA ACESSO DO USUARIO NO PACIENTE QUE ELE ATENDE
						if($usuario['acesso'][$acesso] == 1 OR $usuario['acesso'][$acesso] == 2){
							
							return TRUE;
							
						}else{
						
							return FALSE;
						
						}
						
					}else{
						
						// BUSCA DADOS DO RESPONSAVEL PELO PACIENTE
						$id_responsavel = $paciente[$equipe]['responsavel'];
						$responsavel = busca_usuario_geral($mysqli,$id_responsavel);
						
						// VERIFICA SE RESPONSAVEL PELO PACIENTE POSSUI UM SUPERIOR
						if( $responsavel['id_superior'] > 0 ){
						
							// VERIFICA ACESSO DO USUARIO NO PACIENTE ATENDIDO POR ALGUEM ABAIXO ELE
							if( 
								$responsavel['id_superior'] == $usuario['geral']['id_usuario'] AND 
								$usuario['acesso'][$acesso] == 2 
							){
								
								return TRUE;
								
							}else{
								
								return FALSE;
								
							}
						
						}else{
						
							return FALSE;
						
						}
						
						
					}
					
					
				
				}
				
				
				
			}else{
				
				return FALSE;
				
			}
			
			
		}
	
	}


	/* FUNÇÃO VERIFICADA */
	function acesso_cadastrar_paciente($usuario){
		
		if( $usuario['acesso']['cadastrar_paciente'] == 4 ){
			
			return TRUE;
			
		}else{
			
			return FALSE;
			
		}
		
	}
	
	
	/* FUNÇÃO COMPLETA */
	function acesso_paciente($mysqli,$acesso,$paciente,$usuario){
		
		$equipe = $usuario['geral']['equipe'];
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
		
			// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELA EQUIPE
			if($paciente['geral'][$equipe] == 1){
			
				// VERIFICA ACESSO DO USUARIO NA EQUIPE
				if($usuario['acesso'][$acesso] == 3){
				
					return TRUE;
				
				}else{
					
					// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELO USUARIO
					if($paciente[$equipe]['responsavel'] == $usuario['geral']['id_usuario']){
						
						// VERIFICA ACESSO DO USUARIO NO PACIENTE QUE ELE ATENDE
						if($usuario['acesso'][$acesso] == 1 OR $usuario['acesso'][$acesso] == 2){
							
							return TRUE;
							
						}else{
						
							return FALSE;
						
						}
						
					}else{
						
						// BUSCA DADOS DO RESPONSAVEL PELO PACIENTE
						$id_responsavel = $paciente[$equipe]['responsavel'];
						$responsavel = busca_usuario_geral($mysqli,$id_responsavel);
						
						// VERIFICA SE RESPONSAVEL PELO PACIENTE POSSUI UM SUPERIOR
						if( $responsavel['id_superior'] > 0 ){
						
							// VERIFICA ACESSO DO USUARIO NO PACIENTE ATENDIDO POR ALGUEM ABAIXO ELE
							if( 
								$responsavel['id_superior'] == $usuario['geral']['id_usuario'] AND 
								$usuario['acesso'][$acesso] == 2 
							){
								
								return TRUE;
								
							}else{
								
								return FALSE;
								
							}
						
						}else{
						
							return FALSE;
						
						}
						
						
					}
					
					
				
				}
				
				
				
			}else{
				
				return FALSE;
				
			}
			
			
		}
	
	}
	
	
	/* FUNÇÃO VERIFICADA */
	function acesso_editar_perfil_paciente($mysqli,$usuario,$paciente){
		
		$equipe = $usuario['geral']['equipe'];
		$acesso	= 'editar_paciente';
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
		
			// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELA EQUIPE
			if($paciente['geral'][$equipe] == 1){
			
				// VERIFICA ACESSO DO USUARIO NA EQUIPE
				if($usuario['acesso'][$acesso] == 3){
				
					return TRUE;
				
				}else{
					
					// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELO USUARIO
					if($paciente[$equipe]['responsavel'] == $usuario['geral']['id_usuario']){
						
						// VERIFICA ACESSO DO USUARIO NO PACIENTE QUE ELE ATENDE
						if($usuario['acesso'][$acesso] == 1 OR $usuario['acesso'][$acesso] == 2){
							
							return TRUE;
							
						}else{
						
							return FALSE;
						
						}
						
					}else{
						
						// BUSCA DADOS DO RESPONSAVEL PELO PACIENTE
						$id_responsavel = $paciente[$equipe]['responsavel'];
						$responsavel = busca_usuario_geral($mysqli,$id_responsavel);
						
						// VERIFICA SE RESPONSAVEL PELO PACIENTE POSSUI UM SUPERIOR
						if( $responsavel['id_superior'] > 0 ){
						
							// VERIFICA ACESSO DO USUARIO NO PACIENTE ATENDIDO POR ALGUEM ABAIXO ELE
							if( 
								$responsavel['id_superior'] == $usuario['geral']['id_usuario'] AND 
								$usuario['acesso'][$acesso] == 2 
							){
								
								return TRUE;
								
							}else{
								
								return FALSE;
								
							}
						
						}else{
						
							return FALSE;
						
						}
						
						
					}
					
					
				
				}
				
				
				
			}else{
				
				return FALSE;
				
			}
			
			
		}
		
	}
	
	
	/* FUNÇÃO VERIFICADA */
	function acesso_ver_perfil_paciente($mysqli,$usuario,$paciente){
		
		$equipe = $usuario['geral']['equipe'];
		$acesso	= 'ver_paciente';
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
		
			// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELA EQUIPE
			if($paciente['geral'][$equipe] == 1){
			
				// VERIFICA ACESSO DO USUARIO NA EQUIPE
				if($usuario['acesso'][$acesso] == 3){
				
					return TRUE;
				
				}else{
					
					// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELO USUARIO
					if($paciente[$equipe]['responsavel'] == $usuario['geral']['id_usuario']){
						
						// VERIFICA ACESSO DO USUARIO NO PACIENTE QUE ELE ATENDE
						if($usuario['acesso'][$acesso] == 1 OR $usuario['acesso'][$acesso] == 2){
							
							return TRUE;
							
						}else{
						
							return FALSE;
						
						}
						
					}else{
						
						// BUSCA DADOS DO RESPONSAVEL PELO PACIENTE
						$id_responsavel = $paciente[$equipe]['responsavel'];
						$responsavel = busca_usuario_geral($mysqli,$id_responsavel);
						
						// VERIFICA SE RESPONSAVEL PELO PACIENTE POSSUI UM SUPERIOR
						if( $responsavel['id_superior'] > 0 ){
						
							// VERIFICA ACESSO DO USUARIO NO PACIENTE ATENDIDO POR ALGUEM ABAIXO ELE
							if( 
								$responsavel['id_superior'] == $usuario['geral']['id_usuario'] AND 
								$usuario['acesso'][$acesso] == 2 
							){
								
								return TRUE;
								
							}else{
								
								return FALSE;
								
							}
						
						}else{
						
							return FALSE;
						
						}
						
						
					}
					
					
				
				}
				
				
				
			}else{
				
				return FALSE;
				
			}
			
			
		}
		
	}
	
	
	/* FUNÇÃO VERIFICADA */
	function acesso_criar_avaliacao($mysqli,$usuario,$paciente){
		
		$equipe = $usuario['geral']['equipe'];
		
		// VERIFICA SE PACIENTE ESTA EM TRATAMENTO PELA EQUIPE
		if( $paciente['geral'][$equipe] == FALSE ){
			
			// VERIFICA SE USUARIO PODE AVALIAR O PACIENTE
			if( $usuario['acesso']['avaliar_paciente'] == 4 ){
				
				return TRUE;
				
			}else{
			
				return FALSE;
			
			}
			
		}else{
			
			return FALSE;
			
		}
		
	}
	
	
	/* FUNÇÃO VERIFICADA */
	function acesso_alta($mysqli,$paciente,$usuario){
		
		$equipe = $usuario['geral']['equipe'];
		$acesso = 'alta';
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
		
			// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELA EQUIPE
			if($paciente['geral'][$equipe] == 1){
			
				// VERIFICA ACESSO DO USUARIO NA EQUIPE
				if($usuario['acesso'][$acesso] == 3){
				
					return TRUE;
				
				}else{
					
					// VERIFICA SE PACIENTE ESTA EM ATENDIMENTO PELO USUARIO
					if($paciente[$equipe]['responsavel'] == $usuario['geral']['id_usuario']){
						
						// VERIFICA ACESSO DO USUARIO NO PACIENTE QUE ELE ATENDE
						if($usuario['acesso'][$acesso] == 1 OR $usuario['acesso'][$acesso] == 2){
							
							return TRUE;
							
						}else{
						
							return FALSE;
						
						}
						
					}else{
						
						// BUSCA DADOS DO RESPONSAVEL PELO PACIENTE
						$id_responsavel = $paciente[$equipe]['responsavel'];
						$responsavel = busca_usuario_geral($mysqli,$id_responsavel);
						
						// VERIFICA SE RESPONSAVEL PELO PACIENTE POSSUI UM SUPERIOR
						if( $responsavel['id_superior'] > 0 ){
						
							// VERIFICA ACESSO DO USUARIO NO PACIENTE ATENDIDO POR ALGUEM ABAIXO ELE
							if( 
								$responsavel['id_superior'] == $usuario['geral']['id_usuario'] AND 
								$usuario['acesso'][$acesso] == 2 
							){
								
								return TRUE;
								
							}else{
								
								return FALSE;
								
							}
						
						}else{
						
							return FALSE;
						
						}
						
						
					}
					
					
				
				}
				
				
				
			}else{
				
				return FALSE;
				
			}
			
			
		}
	
	}
	
	
	function acesso_obito($mysqli,$paciente,$usuario){
		
		$equipe = $usuario['geral']['equipe'];
		$acesso = 'alta';
		
		// VERIFICA ACESSO DO USUARIO EM TUDO
		if( $usuario['acesso'][$acesso] == 4 ){
		
			return TRUE;
		
		}else{
			
			// VERIFICA ACESSO DO USUARIO NA EQUIPE
			if($usuario['acesso'][$acesso] >= 1){
			
				return TRUE;
			
			}else{
				
				return FALSE;
			
			}
			
			
		}
	
	}


?>