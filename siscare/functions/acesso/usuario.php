﻿<?php
	
	
	function acesso_cadastrar_perfil($mysqli,$usuario,$equipe){
		
		// VERIFICA SE USUARIO TEM ACESSO PARA CADASTRAR USUARIO NA SUA EQUIPE
		if( $usuario['acesso']['cadastrar_perfil'] == 3 AND $usuario['geral']['equipe'] == $equipe ){
		
			return TRUE;
			
		}else{
			
			// VERIFICA SE USUARIO PODE CRIAR PERFIL DE USUARIOS PARA TODAS AS EQUIPES
			if( $usuario['acesso']['cadastrar_perfil'] == 4 ){
				
				return TRUE;
				
			}else{
				
				return FALSE;
				
			}
			
		}
		
	}

	
	function acesso_ver_perfil($mysqli,$usuario,$perfil){
		
		// VERIFICA SE USUARIO PODE VER PERFIL DE TODAS EQUIPES
		if( $usuario['acesso']['ver_perfil'] == 4 ){
		
			return TRUE;
			
		}else{
			
			// VERIFICA SE USUARIO PODE VER PERFIL DESTA EQUIPE
			if( $perfil['geral']['equipe'] == $usuario['geral']['equipe'] AND $usuario['acesso']['ver_perfil'] == 3 ){
				
				return TRUE;
				
			}else{
				
				// VERIFICA SE O PERFIL É DO USUARIO
				if( $perfil['geral']['id_usuario'] == $usuario['geral']['id_usuario'] ){
					
					return TRUE;
					
				}else{
					
					// VERIFICA SE ESSE PERFIL POSSUI UM SUPERIOR
					if( $perfil['geral']['id_superior'] > 0 ){
						
						// VERIFICA SE USUARIO É SUPERIOR DESSE PERFIL E PODE VER ESSE PERFIL
						if( 
							$perfil['geral']['id_superior'] == $usuario['geral']['id_usuario'] AND 
							$usuario['acesso']['ver_perfil'] == 2
						){
							
							return TRUE;
							
						}else{
							
							return FALSE;
							
						}
						
					}else{
						
						return FALSE;
						
					}
					
				}
				
			}
			
		}
		
	}

	
	function acesso_editar_perfil($mysqli,$usuario,$perfil){
		
		
		// VERIFICA SE USUARIO PODE EDITAR PERFIL DE TODAS EQUIPES
		if( $usuario['acesso']['editar_perfil'] == 4 ){
		
			return TRUE;
			
		}else{
			
			// VERIFICA SE USUARIO PODE EDITAR PERFIL DESTA EQUIPE
			if( $perfil['geral']['equipe'] == $usuario['geral']['equipe'] AND $usuario['acesso']['editar_perfil'] == 3 ){
				
				return TRUE;
				
			}else{
				
				// VERIFICA SE O PERFIL É DO USUARIO
				if( $perfil['geral']['id_usuario'] == $usuario['geral']['id_usuario'] ){
					
					return TRUE;
					
				}else{
					
					// VERIFICA SE ESSE PERFIL POSSUI UM SUPERIOR
					if( $perfil['geral']['id_superior'] > 0 ){
						
						
						// VERIFICA SE USUARIO É SUPERIOR DESSE PERFIL E PODE EDITAR ESSE PERFIL
						if( 
							$perfil['geral']['id_superior'] == $usuario['geral']['id_usuario'] AND 
							$usuario['acesso']['editar_perfil'] == 2
						){
							
							return TRUE;
							
						}else{
							
							return FALSE;
							
						}
						
					}else{
						
						return FALSE;
						
					}
					
				}
				
			}
			
		}
		
	}


	function acesso_ver_indicadores($mysqli,$usuario,$perfil){
		
		// VERIFICA SE USUARIO PODE VER PERFIL DE TODAS EQUIPES
		if( $usuario['acesso']['ver_indicadores'] == 4 ){
		
			return TRUE;
			
		}else{
			
			// VERIFICA SE USUARIO PODE VER PERFIL DESTA EQUIPE
			if( $perfil['geral']['equipe'] == $usuario['geral']['equipe'] AND $usuario['acesso']['ver_indicadores'] == 3 ){
				
				return TRUE;
				
			}else{
				
				// VERIFICA SE O PERFIL É DO USUARIO
				if( $perfil['geral']['id_usuario'] == $usuario['geral']['id_usuario'] ){
					
					return TRUE;
					
				}else{
					
					// VERIFICA SE ESSE PERFIL POSSUI UM SUPERIOR
					if( $perfil['geral']['id_superior'] > 0 ){
						
						// VERIFICA SE USUARIO É SUPERIOR DESSE PERFIL E PODE VER ESSE PERFIL
						if( 
							$perfil['geral']['id_superior'] == $usuario['geral']['id_usuario'] AND 
							$usuario['acesso']['ver_indicadores'] == 2
						){
							
							return TRUE;
							
						}else{
							
							return FALSE;
							
						}
						
					}else{
						
						return FALSE;
						
					}
					
				}
				
			}
			
		}
		
	}

?>