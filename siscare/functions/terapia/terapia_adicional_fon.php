﻿<?php

	function terapia_adicional_fon($i){
		
		switch ($i){
			case 1:
				$adicional = 'Estimulo de deglutição de saliva';
				break;
			case 2:
				$adicional = 'Treino via oral';
				break;
			case 3:
				$adicional = 'Exercício para deglutição';
				break;
			case 4:
				$adicional = 'Exercício para voz';
				break;
			case 5:
				$adicional = 'Exercício para motricidade';
				break;
			case 6:
				$adicional = 'Exercício para linguagem';
				break;
			case 7:
				$adicional = 'Acompanhamento de refeição';
				break;
			case 8:
				$adicional = 'Orientação';
				break;
		}
		
		return $adicional;
		
	}
	
?>