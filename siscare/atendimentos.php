﻿<?php
	
	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "atendimentos";
	
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("config/db_connect.php");
	include_once("config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verifica��o de acessos
		include_once("functions/acesso/usuario.php");
		include_once("functions/erro/erro.php");
		include_once("functions/count/contar.php");
		
		
		/* SQL */
		// Busca os dados do servico
		include_once("sql/servico/buscar.php");
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		// Busca os dados de paciente
		include_once("sql/paciente/buscar.php");
		// Busca os dados de objetivos
		include_once("sql/objetivo/buscar.php");
		
		
		// BUSCA DADOS GERAIS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA EQUIPE
		$equipe = isset($_GET['id']) ? $_GET['id'] : $usuario['geral']['equipe'];
		$equipe = preg_replace("/[^a-z]+/", "", $equipe);
		
		
		if($usuario['acesso']['ver_atendimentos'] < 3 ){
			header("Location: home.php");
		}
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SiSaN | Atendimentos</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="assets/css/icheck/flat/green.css" rel="stylesheet" />
	
	<link href="assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
    
</head>


<body class="nav-md">

    <div class="container body">
		
        <div class="main_container" style="background:#3C3C3C">
			
			<!-- MENU DA ESQUERDA -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- BARA SUPERIOR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- CONTEÚDO DA PAGINA -->
            <div class="right_col" role="main">

                <!-- TITULO -->
                <div class="row">
					<div class="page-title">
						
						<!-- NOME DA PAGINA -->
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Atendimentos</h3>
                        </div>
						
						<!-- CAMINHO DA PAGINA -->
                        <div class="pull-right hidden-xs">
							
							<i class="fa fa-home"></i>&nbsp;<a href="home.php">Home</a>&nbsp;&nbsp;
							<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							Atendimentos&nbsp;&nbsp;
							
						</div>
						
                    </div>
                </div>
				
				
				<br>
				<div class="row">
				
					<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12 col-lg-offset-1">
						<div class="x_panel">
							
							<div class="x_content">

								<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 profile_left">
									
									<!-- DADOS GERAIS -->
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 controls">
										
										<ul class="list-unstyled user_data">
											
											<!-- ATENDIMENTOS DO MES ATUAL -->
											<li>
												
												<h4>
													<span class="count_top">
														<i class="fa fa-medkit"></i>
														&nbsp; Atendimentos no mês
													</span>
												</h4>
												
												<?php
												
												// DETERMINA O PERIODO A SER PESQUISADO - MES ATUAL
												$periodo['inicio'] = date('Y-m-01');
												$periodo['fim'] = date('Y-m-t');
												
												// BUSCA OS USUARIOS DA EQUIPE
												$query = "SELECT * FROM ".$_SESSION['user_Servico']."_usuarios WHERE equipe = '".$equipe."' ";
												foreach($mysqli->query($query) as $colaborador){
													
													// CONTA O NUMERO DE ATENDIMENTOS POR USUARIO
													$atendimentos = count_atendimento_usuario($mysqli,$periodo,$colaborador['id_usuario']);
													echo "<h5> <div class='count'>".$colaborador['nome'].": ".$atendimentos."</div> </h5>";
													
												}
												
												// CONTA O NUMERO TOTAL DE ATENDIMENTOS DA EQUIPE
												$atendimentos = count_atendimento_equipe($mysqli,$periodo,$equipe);
												echo "<h4> <div class='count'>Total: ".$atendimentos."</div> </h4>";
												
												?>
												
											</li>
											
											<!-- ATENDIMENTOS DO MES PASSADO -->
											<li class="">
												
												<br>
												
												<h4> 
													<span class="count_top">
														<i class="fa fa-medkit"></i>
														&nbsp; Atendimentos de <?php echo ajusta_mes(date("m") - 1); ?>
													</span>
												</h4>
												
												<?php
												
												// DETERMINA O PERIODO A SER PESQUISADO - MES PASSADO
												$periodo['inicio'] = date('Y-m-d 00:00:00', strtotime('first day of previous month'));
												$periodo['fim'] = date('Y-m-d 23:59:59', strtotime('last day of previous month'));
												
												
												// BUSCA OS USUARIOS DA EQUIPE
												$query = "SELECT * FROM ".$_SESSION['user_Servico']."_usuarios WHERE equipe = '".$equipe."' ";
												foreach($mysqli->query($query) as $colaborador){
													
													// CONTA O NUMERO DE ATENDIMENTOS POR USUARIO
													$atendimentos = count_atendimento_usuario($mysqli,$periodo,$colaborador['id_usuario']);
													echo "<h5> <div class='count'>".$colaborador['nome'].": ".$atendimentos."</div> </h5>";
													
												}
												
												// CONTA O NUMERO TOTAL DE ATENDIMENTOS DA EQUIPE
												$atendimentos = count_atendimento_equipe($mysqli,$periodo,$equipe);
												echo "<h4> <div class='count'>Total: ".$atendimentos."</div> </h4>";
												
												?>
												
											</li>
											
											<br>
											
											<!-- PACIENTES EM ATENDIMENTO -->
											<li>
												
												<h4>
													<span class="count_top">
														<i class="fa fa-wheelchair"></i>
														&nbsp; Pacientes
													</span>
												</h4>
												
												<?php
												
												// BUSCA OS USUARIOS DA EQUIPE
												$query = "SELECT * FROM ".$_SESSION['user_Servico']."_usuarios WHERE equipe = '".$equipe."' ";
												foreach($mysqli->query($query) as $colaborador){
													
													// CONTA O NUMERO DE PACIENTES POR USUARIO
													$atendimentos = count_pacientes_usuario($mysqli,$equipe,$colaborador['id_usuario']);
													echo "<h5> <div class='count'>".$colaborador['nome'].": ".$atendimentos."</div> </h5>";
													
												}
												
												// CONTA O NUMERO TOTAL DE PACIENTES POR USUARIO
												$pacientes = count_pacientes_equipe($mysqli,$equipe);
												echo "<h4> <div class='count'>Total: ".$pacientes."</div> </h4>";
												
												?>
												
											</li>
											
										</ul>
										
									</div>
									
								
								</div>
								
								
								<div class="col-lg-9 col-md-9 col-sm-9 hidden-xs">
									
									<!-- GRÁFICOS -->
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="x_panel">
											
											<div class="x_content">

												<div id="mainb" style="height:350px;"></div>

											</div>
											
										</div>
									</div>
									
									<div class="col-lg-12 col-md-12 col-sm-12">
										<div class="x_panel">
											
											<div class="x_content">

												<div id="echart_line" style="height:350px;"></div>

											</div>
											
										</div>
									</div>
									<!-- [FIM] GRÁFICOS -->
									
								</div>
							
								<div class="col-lg-9 col-md-9 col-sm-12 hidden-xs col-lg-offset-3 col-md-offset-3">
									
									
									<div class="col-sm-12 col-md-12 col-lg-12">
									
										<!-- TABELAS -->
										<div class="" role="tabpanel" data-example-id="togglable-tabs">
											
											<!-- ABAS PARA SELEÇÃO -->
											<ul id="myTab" class="nav nav-tabs" role="tablist">
												
												<!-- ABA RESUMIDA -->
												<li role="presentation" class="active">
													<a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Resumo</a>
												</li>
												
												<!-- ABA DE DETALHES -->
												<li role="presentation" class="">
													<a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Detalhes</a>
												</li>
												
											</ul>
											
											<!-- CONTEÚDO DAS ABAS -->
											<div id="myTabContent" class="tab-content">
												
												<!-- ATIVIDADES RECENTES -->
												<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
													
													<div class="row">

														<div class="col-sm-12 col-md-12 col-lg-12">
															<div class="x_panel">
																
																<div class="x_content">
																	
																	<table id="tabela_resumo" class="table table-striped responsive-utilities jambo_table">
																		
																		<thead>
																			<tr class="headings">
																				<th>id				</th>
																				<th>Periodo 		</th>
																				<th>Colaborador 	</th>
																				<th>Atendimentos 	</th>
																				<th>Valor 			</th>
																			</tr>
																		</thead>

																		<tbody>
																			
																		<?php
																			
																			$j = 0;
																			
																			for ($i=0;$i<6;$i++){
																				
																				// BUSCA O MES ATUAL
																				$mes = date('m');
																				$ano = date('Y');
																				
																				// DESLOCA O MES ATUAL CONFORME $i E CORRIGE ANO
																				if($mes - $i <= 0){
																					$mes = 12 + $mes - $i;
																					$ano = $ano - 1;
																				}else{
																					$mes = $mes - $i;
																				}
																				
																				// DETERMINA OS PERIODOS DE BUSCA
																				$periodo['inicio'] = date($ano.'-'.$mes.'-01');
																				$periodo['fim'] = date($ano.'-'.$mes.'-t');
																				
																				// BUSCA OS COLABORADORES DESSA EQUIPE
																				$query = "SELECT * FROM ".$_SESSION['user_Servico']."_usuarios WHERE equipe = '".$equipe."' ORDER BY nome";
																				foreach($mysqli->query($query) as $colaborador){
																					
																					// SOMA TODOS OS ATENDIMENTOS DO COLABORADOR NO PERIODO
																					$total = count_atendimento_usuario($mysqli,$periodo,$colaborador['id_usuario']);
																					$valor = "R$ ".number_format($total * $colaborador['valor_terapia'], 2, ',', ' ');
																					
																					$data = ajusta_mes_2($mes)."/".$ano;
																					
																					$j++;
																					
																					if($total > 0){
																						echo "
																							<tr class='even pointer'>
																								<td class=' '>
																									".$j."
																								</td>
																								<td class='date'>
																									".$data."
																								</td>
																								<td class=''>
																									".$colaborador['nome']."
																								</td>
																								<td class='text-center'>
																									".$total."
																								</td>
																								<td class=''>
																									".$valor."
																								</td>
																							</tr>
																						";
																					}
																					
																				}
																			}
																			
																		?>
																			
																		</tbody>

																	</table>
																</div>
																
															</div>
														</div>

														<br />
														<br />
														<br />

													</div>

												</div>
												<!-- [FIM] ATIVIDADES RECENTES -->
												
												
												<!-- PACIENTES -->
												<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
													
													<!-- DETALHES DOS ATENDIMENTOS -->
													<div class="row">

														<div class="col-sm-12 col-md-12 col-lg-12">
															<div class="x_panel">
																
																<div class="x_content">
																
																	<table id="tabela_detalhe" class="table table-striped responsive-utilities jambo_table">
																		
																		<thead>
																			<tr class="headings">
																				<th>id			</th>
																				<th>Data 		</th>
																				<th>Colaborador </th>
																				<th>Paciente	</th>
																				<th>Valor		</th>
																			</tr>
																		</thead>

																		<tbody>
																			
																		<?php
																			
																			$j = 0;
																			
																			// BUSCA OS COLABORADORES DESSA EQUIPE
																			$query = "SELECT * FROM ".$_SESSION['user_Servico']."_usuarios WHERE equipe = '".$equipe."' ORDER BY nome";
																			foreach($mysqli->query($query) as $colaborador){
																				
																				$query_2 = "SELECT
																								".$_SESSION['user_Servico']."_terapia.data as data,
																								".$_SESSION['user_Servico']."_paciente_geral.nome as nome,
																								".$_SESSION['user_Servico']."_usuarios.nome as colaborador,
																								".$_SESSION['user_Servico']."_usuarios.valor_terapia as valor_terapia
																							FROM 
																								".$_SESSION['user_Servico']."_terapia 
																							JOIN 
																								".$_SESSION['user_Servico']."_paciente_geral 
																							ON 
																								".$_SESSION['user_Servico']."_terapia.id_paciente = ".$_SESSION['user_Servico']."_paciente_geral.id
																							JOIN 
																								".$_SESSION['user_Servico']."_usuarios 
																							ON
																								".$_SESSION['user_Servico']."_terapia.id_usuario = ".$_SESSION['user_Servico']."_usuarios.id_usuario
																							WHERE 
																								".$_SESSION['user_Servico']."_terapia.id_usuario = '".$colaborador['id_usuario']."' 
																							ORDER BY 
																								YEAR( data ),
																								MONTH( data ),
																								colaborador
																							";
																				$mysql_query_2 = mysqli_query($mysqli,$query_2);
																				
																				while($tabela = mysqli_fetch_array($mysql_query_2)){
																					
																					$j++;
																					
																					$phpdate = strtotime($tabela['data']);
																					$data = date( 'd/m/Y', $phpdate );
																					
																					echo "
																						<tr class='even pointer'>
																							<td class=' '>
																								".$j."
																							</td>
																							<td class='date'>
																								".$data."
																							</td>
																							<td class=''>
																								".$tabela['colaborador']."
																							</td>
																							<td class=''>
																								".$tabela['nome']."
																							</td>
																							<td class=''>
																								R$ ".number_format($tabela['valor_terapia'], 2, ',', ' ')."
																							</td>
																						</tr>
																					";
																					
																				}
																				
																				
																			}
																			
																		?>
																			
																		</tbody>

																	</table>
																</div>
																
															</div>
														</div>

														<br />
														<br />
														<br />

													</div>
													
												</div>
												<!-- [FIM] PACIENTES -->
												
											</div>
										</div>
										
									</div>
									
								</div>
							</div>
						</div>
						
					</div>
					
				</div>
				
                <br>
				
				
                <!-- FOOTER-->
				<?php include_once("include/layout/footer.php"); ?>
				
				
            </div>
		
        </div>
		
    </div>
	
	
    <script src="assets/js/bootstrap.min.js"></script>
	
    <!-- bootstrap progress js -->
    <script src="assets/js/custom.js"></script>
	
	
	<!-- GRÁFICO -->
    <script src="assets/js/echart/echarts-all.js"></script>
    <script src="assets/js/echart/green.js"></script>
	
	
	<?php include("functions/grafico/atendimento/equipe.php"); ?>
	<?php include("functions/grafico/atendimento/colaboradores.php"); ?>
    
	
	<!-- TABELA DINÂMICA -->
	<script src="//code.jquery.com/jquery-1.12.0.min.js "></script>
	<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js "></script>
	<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" rel="stylesheet" />
	
	
	<link href="https://cdn.datatables.net/buttons/1.1.1/css/buttons.dataTables.min.css" rel="stylesheet" />
	
	<script src="https://cdn.datatables.net/buttons/1.1.1/js/dataTables.buttons.min.js "></script>
	<script src="//cdn.datatables.net/buttons/1.1.1/js/buttons.flash.min.js "></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js "></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js "></script>
	<script src="//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js "></script>
	<script src="//cdn.datatables.net/buttons/1.1.1/js/buttons.html5.min.js "></script>
	<script src="//cdn.datatables.net/buttons/1.1.1/js/buttons.print.min.js "></script>
	
	<script>
	
		$(document).ready(function() {

			$('#tabela_resumo').DataTable( {
				
				"paging":   true,
				"bLengthChange": false,
				"pageLength": 20,
				"ordering": true,
				"info":     true,
				"sPaginationType": "full_numbers",
				"order": [[ 0, "asc" ]],
				"columnDefs": [ { "targets": 1, "orderable": false } ],
				stateSave: false,
				
				"language": {
					"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
				},
				
				dom: 'B<"clear">lfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf'
				]
				
			} );
			
			
			
			$('#tabela_detalhe').DataTable( {
				
				"paging":   true,
				"bLengthChange": false,
				"pageLength": 20,
				"ordering": true,
				"info":     true,
				"sPaginationType": "full_numbers",
				"order": [[ 0, "asc" ]],
				"columnDefs": [ { "targets": 1, "orderable": false } ],
				stateSave: false,
				
				"language": {
					"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
				},
				
				dom: 'B<"clear">lfrtip',
				buttons: [
					'copy', 'csv', 'excel', 'pdf'
				]
				
			} );

		} );
		
	</script>
	
</body>

</html>

<?php

}else{
	
	header("Location: login.php");
	
}

?>