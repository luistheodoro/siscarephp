<?php
	
	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "pacientes";
	
	
	// CONECTAR BANCO DE DADOS E ADICIONAR ARQUIVO DE FUNÃ‡Ã•ES 
	include_once 'config/db_connect.php';
	include_once 'config/functions.php';
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("functions/acesso/paciente.php");
		include_once("functions/erro/erro.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		
		
		// BUSCA DADOS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// VERIFICA ACESSO PARA CADASTRAR PACIENTE
		if($usuario['acesso']['cadastrar_paciente'] == 4){
			$criar = TRUE;
		}else{
			$criar = FALSE;
			header("Location: pacientes.php");
		}
		
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	
    <title>SiSaN | Cadastrar paciente</title>
	
	
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/maps/jquery-jvectormap-2.0.1.css" />
    <link href="assets/css/icheck/flat/green.css" rel="stylesheet" />
	
	
    <script src="assets/js/jquery.min.js"></script>
    
	
	<!-- CARREGAR CSS DE MAPA -->
	<link type="text/css" rel="stylesheet" href="assets/css/maps.css">
	
	
</head>


<body class="nav-md">

    <div class="container body">
		
        <div class="main_container" style="background:#3C3C3C">
			
			<!-- MENU DA ESQUERDA -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- BARA SUPERIOR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- CONTEÚDO DA PAGINA -->
            <div class="right_col" role="main">

                <!-- TITULO -->
                <div class="row">
					<div class="page-title">
						
						<!-- NOME DA PAGINA -->
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Cadastrar Paciente </h3>
                        </div>
						
						<!-- CAMINHO DA PAGINA -->
                        <div class="pull-right hidden-xs">
							
							<i class="fa fa-home"></i>&nbsp;<a href="home.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							<a href="pacientes.php">Pacientes</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							Cadastrar paciente &nbsp;&nbsp;
							
                        </div>
						
                    </div>
                </div>
                
				<br>
                <div class="row">
                   
					<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        <div class="x_panel">
							
							<div class="x_content">
                            <form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/paciente/criar.php">
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 controls">
									
									<h3> Dados pessoais </h3>
									<br>
									
									<!-- NOME -->
									<label class="control-label" for="inputNome"> Nome </label>
									<input class="form-control" id="inputNome" name="inputNome" type="text" required>
									<br>
									
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- NASCIMENTO -->
											<label class="control-label" for="inputNascimento"> Data de nascimento </label>
											<input class="form-control" id="inputNascimento" name="inputNascimento" type="date">
											<br>
											
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- SEXO -->
											<label class="control-label" for="inputSexo"> Sexo </label>
											<select class="form-control" id="inputSexo" name="inputSexo" required>
												<option>Sexo</option>
												<option value="Masculino">Masculino</option>
												<option value="Feminino">Feminino</option>
											</select>
											<br>
											
										</div>
									</div>
									
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										
											<!-- RESPONSÁVEL -->
											<label class="control-label" for="inputResponsavel"> Responsável </label>
											<input class="form-control" id="inputResponsavel" name="inputResponsavel" type="text">
											<br>
										
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										
											<!-- RELAÃ‡ÃƒO DO RESPONSÃ�VEL -->
											<label class="control-label" for="inputRelacao"> Relação </label>
											<input class="form-control" id="inputRelacao" name="inputRelacao" type="text">
											<br>
										
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										
											<!-- CUIDADOR -->
											<label class="control-label" for="inputCuidador"> Cuidador </label>
											<input class="form-control" id="inputCuidador" name="inputCuidador" type="text">
											<br>
										
										</div>
									</div>
									
									<!-- DIAGNÃ“STICO 1 -->
									<label class="control-label" for="inputDiagnostico1"> Diagnóstico 1 </label>
									<input class="form-control" id="inputDiagnostico1" name="inputDiagnostico1" type="text">
									<br>
									
									<!-- DIAGNÃ“STICO 2 -->
									<label class="control-label" for="inputDiagnostico2"> Diagnóstico 2 </label>
									<input class="form-control" id="inputDiagnostico2" name="inputDiagnostico2" type="text">
									<br>
									
									<!-- DIAGNÃ“STICO 3 -->
									<label class="control-label" for="inputDiagnostico3"> Diagnóstico 3 </label>
									<input class="form-control" id="inputDiagnostico3" name="inputDiagnostico3" type="text">
									<br>
									
								</div>
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 controls">
								
									<h3> Contato </h3>
									<br>
									
									<!-- CONTATO 1 -->
									<b>Contato 1</b>
									<br>
										
										<!-- RESPONSÁVEL 1 -->
										<label class="control-label" for="inputNome1"> Nome </label>
										<input class="form-control" id="inputNome1" name="inputNome1" type="text">
										<br>
									
										<!-- TELEFONE 1 -->
										<label class="control-label" for="inputTelefone1"> Telefone</label>
										<input class="form-control" id="inputTelefone1" name="inputTelefone1" type="text">
										<br>
										
											
									
									<b>Contato 2</b>
									<br>
										
										<!-- RESPONSÃ�VEL 2 -->
										<label class="control-label" for="inputNome2"> Nome </label>
										<input class="form-control" id="inputNome2" name="inputNome2" type="text">
										<br>
									
										<!-- TELEFONE 2 -->
										<label class="control-label" for="inputTelefone2"> Telefone</label>
										<input class="form-control" id="inputTelefone2" name="inputTelefone2" type="text">
										<br>
										
									
									<b>Contato 3</b>
									<br>
										
										<!-- RESPONSÃ�VEL 3 -->
										<label class="control-label" for="inputNome3"> Nome </label>
										<input class="form-control" id="inputNome3" name="inputNome3" type="text">
										<br>
									
										<!-- TELEFONE 3 -->
										<label class="control-label" for="inputTelefone3"> Telefone</label>
										<input class="form-control" id="inputTelefone3" name="inputTelefone3" type="text">
										<br>
										
								</div>
								
								
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 controls">
								
									<h3> Endereço </h3>
									<br>
									
									<!-- ENDEREÇO -->
									<input id="pac-input" class="controls2" type="text" placeholder="Endereço"></input>
									<div id="map" style="height:300px; width:100%"></div>
									<br>
								
								</div>
									
								<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12 controls">
								
									<label class="control-label" for="inputEndereco"> Endereço </label>
									<input type="text" id="inputEndereco" name="inputEndereco" class="form-control" />
									<br>
									
									<label class="control-label" for="inputSubject"> Bairro </label>
									<input type="text" id="inputBairro" class="form-control" name="inputBairro" />
									<br>
									
									<label class="control-label" for="inputSubject"> Cidade </label>
									<input type="text" id="inputCidade" name="inputCidade" class="form-control" />
									<br>
									
									<input type="hidden" id="inputCoordenada" name="inputCoordenada" class="form-control" />
									<input type="hidden" id="inputLat" name="inputLat" class="form-control" />
									<input type="hidden" id="inputLng" name="inputLng" class="form-control" />
									
									
									<!-- PONTO DE REFERENCIA -->
									<label class="control-label" for="inputReferencia"> Ponto de referencia </label>
									<input class="form-control" id="inputReferencia" name="inputReferencia" type="text">
									<br>
									
									<!-- KILOMETRAGEM PARA REEMBOLSO -->
									<label class="control-label" for="inputDistancia"> Distância do hospital </label>
									<input class="form-control" id="inputDistancia" name="inputDistancia" type="text">
									<br>
									
									<!-- BOTÃƒO DE ENVIAR -->
									<input type="button" value=" &nbsp; Cadastrar &nbsp; " class="btn btn-primary pull-right" onClick="this.form.submit()">
									
								</div>
							
							</form>
							</div>
							
                        </div>
                    </div>

                </div>
                <br />
				

                <!-- FOOTER-->
                <?php include_once("include/layout/footer.php"); ?>
				
				
            </div>
            <!-- /page content -->

        </div>

    </div>
	
    <script src="assets/js/bootstrap.min.js"></script>
	
    <!-- bootstrap progress js -->
    <script src="assets/js/nicescroll/jquery.nicescroll.min.js"></script>
	
    <script src="assets/js/custom.js"></script>
	
    
	<!-- GOOGLE MAPS -->
	<script>
		function initMap() {
		  var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: -22.9098833, lng: -47.06258120000001},
			zoom: 10
		  });
		  var input = /** @type {!HTMLInputElement} */(
			  document.getElementById('pac-input'));

		  var types = document.getElementById('type-selector');
		  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
		  map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

		  var autocomplete = new google.maps.places.Autocomplete(input);
		  autocomplete.bindTo('bounds', map);

		  var infowindow = new google.maps.InfoWindow();
		  var marker = new google.maps.Marker({
			map: map,
			anchorPoint: new google.maps.Point(0, -29)
		  });

		  autocomplete.addListener('place_changed', function() {
			infowindow.close();
			marker.setVisible(false);
			var place = autocomplete.getPlace();
			if (!place.geometry) {
			  window.alert("Autocomplete's returned place contains no geometry");
			  return;
			}

			// If the place has a geometry, then present it on a map.
			if (place.geometry.viewport) {
			  map.fitBounds(place.geometry.viewport);
			} else {
			  map.setCenter(place.geometry.location);
			  map.setZoom(17);  // Why 17? Because it looks good.
			}
			marker.setIcon(/** @type {google.maps.Icon} */({
			  url: place.icon,
			  size: new google.maps.Size(71, 71),
			  origin: new google.maps.Point(0, 0),
			  anchor: new google.maps.Point(17, 34),
			  scaledSize: new google.maps.Size(35, 35)
			}));
			marker.setPosition(place.geometry.location);
			marker.setVisible(true);

			var endereco = '';
			if (place.address_components) {
			  
			  endereco = [
				(place.address_components[0] && place.address_components[0].short_name || ''),
				(place.address_components[1] && place.address_components[1].short_name || ''),
				(place.address_components[2] && place.address_components[2].short_name || ''),
			  ].join(' ');
			  
			  bairro = [
				(place.address_components[2] && place.address_components[2].short_name || '')
			  ].join(' ');
			  
			  cidade = [
				(place.address_components[3] && place.address_components[3].short_name || '')
			  ].join(' ');
			  
			}
			
			document.getElementById('inputCoordenada').value = place.geometry.location;
			document.getElementById('inputLat').value = place.geometry.location.lat();
			document.getElementById('inputLng').value = place.geometry.location.lng();
			document.getElementById('inputEndereco').value = place.name;
			document.getElementById('inputBairro').value = bairro;
			document.getElementById('inputCidade').value = cidade;
			
			
			infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
			infowindow.open(map, marker);
		  });

		  // Sets a listener on a radio button to change the filter type on Places
		  // Autocomplete.
		  function setupClickListener(id, types) {
			var radioButton = document.getElementById(id);
			radioButton.addEventListener('click', function() {
			  autocomplete.setTypes(types);
			});
		  }

		  setupClickListener('changetype-all', []);
		  setupClickListener('changetype-address', ['address']);
		  setupClickListener('changetype-establishment', ['establishment']);
		  setupClickListener('changetype-geocode', ['geocode']);
		}

    </script>
	
    <!-- GOOGLE MAPS -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOiVaKKO0XJltgm3witPr14h3RnhqiLAY&signed_in=true&libraries=places&callback=initMap"
        async defer>
	</script>
	<!-- [END] GOOGLE MAPS -->
	
	
</body>

</html>

<?php

}else{
	
	header("Location: login.php");
	
}

?>