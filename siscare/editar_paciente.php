<?php
	
	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "pacientes";
	
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("config/db_connect.php");
	include_once("config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("functions/acesso/paciente.php");
		include_once("functions/erro/erro.php");
		
		
		/* SQL */
		// Busca os dados do servico
		include_once("sql/servico/buscar.php");
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		// Busca os dados de paciente
		include_once("sql/paciente/buscar.php");
		
		
		// BUSCA DADOS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// BUSCA O ID DO PACIENTE
		$id_paciente = isset($_GET['id']) ? $_GET['id'] : header("Location: pacientes.php");
		$id_paciente = preg_replace("/[^0-9]+/", "", $id_paciente);
		$paciente 		= busca_paciente($mysqli, $id_paciente);
		
		
		// VERIFICA ACESSO PARA EDITAR O PACIENTE
		if( acesso_editar_perfil_paciente($mysqli,$usuario,$paciente) == FALSE ){
			header("Location: pacientes.php");
		}
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SiSaN | Editar paciente</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">

    <script src="assets/js/jquery.min.js"></script>
	
	<!--Loading maps css-->
	<link type="text/css" rel="stylesheet" href="assets/css/maps.css">
	
</head>


<body class="nav-md">

    <div class="container body">


        <div class="main_container" style="background:#3C3C3C">
			
			<!-- MENU DA ESQUERDA -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- BARA SUPERIOR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- CONTEÃšDO DA PAGINA -->
            <div class="right_col" role="main">

                <!-- TITULO -->
                <div class="row">
					<div class="page-title">
					
						<!-- NOME DA PAGINA -->
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Editar Paciente</h3>
                        </div>
						
						<!-- CAMINHO DA PAGINA -->
                        <div class="pull-right hidden-xs">
						
							<i class="fa fa-home"></i>&nbsp;<a href="home.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							<a href="pacientes.php">Pacientes</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							<a href="paciente.php?id=<?php echo $paciente['geral']['id']; ?>"><?php echo $paciente['geral']['nome']; ?></a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							Editar &nbsp;&nbsp;
							
                        </div>
						
                    </div>
                </div>
				
				
				<br>
                <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                        <div class="dashboard_graph">

                            <form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/paciente/editar.php">
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 controls">
									
									<h3> Dados pessoais </h3>
									
									<!-- NOME -->
									<label class="control-label" for="inputNome"> Nome </label>
									<input class="form-control" id="inputNome" name="inputNome" type="text" placeholder="" required
										value="<?php echo $paciente['geral']['nome']; ?>"
									>
									<br>
									
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
											
											<!-- NASCIMENTO -->
											<label class="control-label" for="inputNascimento"> Data de nascimento </label>
											<input class="form-control" id="inputNascimento" name="inputNascimento" type="date" placeholder=""
												value="<?php echo $paciente['geral']['nascimento']; ?>"
											>
											<br>
											
										</div>
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
											
											<!-- SEXO -->
											<label class="control-label" for="inputSexo"> Sexo </label>
											<select class="form-control" id="inputSexo" name="inputSexo" required>
												<option>Sexo</option>
												<option
													<?php if($paciente['geral']['sexo']=='Masculino'){echo " selected='selected' ";} ?> 
													value="Masculino">
														Masculino
												</option>
												<option
													<?php if($paciente['geral']['sexo']=='Feminino'){echo " selected='selected' ";} ?> 
													value="Feminino">
														Feminino
												</option>
											</select>
											<br>
											
										</div>
									</div>
									
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										
											<!-- RESPONSÃ�VEL -->
											<label class="control-label" for="inputResponsavel"> Responsável </label>
											<input class="form-control" id="inputResponsavel" name="inputResponsavel" type="text" placeholder=""
												value="<?php echo $paciente['geral']['responsavel']; ?>"
											>
											<br>
										
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										
											<!-- RELAÃ‡ÃƒO DO RESPONSÃ�VEL -->
											<label class="control-label" for="inputRelacao"> Relação </label>
											<input class="form-control" id="inputRelacao" name="inputRelacao" type="text" placeholder=""
												value="<?php echo $paciente['geral']['relacao']; ?>"
											>
											<br>
										
										</div>
										<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										
											<!-- CUIDADOR -->
											<label class="control-label" for="inputCuidador"> Cuidador </label>
											<input class="form-control" id="inputCuidador" name="inputCuidador" type="text" placeholder=""
												value="<?php echo $paciente['geral']['cuidador']; ?>"
											>
											<br>
										
										</div>
									</div>
									
									<!-- DIAGNÓSTICO 1 -->
									<label class="control-label" for="inputDiagnostico1"> Diagnóstico 1 </label>
									<input class="form-control" id="inputDiagnostico1" name="inputDiagnostico1" type="text" placeholder="" required
										value="<?php echo $paciente['geral']['diagnostico_1']; ?>"
									>
									<br>
									
									<!-- DIAGNÓSTICO 2 -->
									<label class="control-label" for="inputDiagnostico2"> Diagnóstico 2 </label>
									<input class="form-control" id="inputDiagnostico2" name="inputDiagnostico2" type="text" placeholder=""
										value="<?php echo $paciente['geral']['diagnostico_2']; ?>"
									>
									<br>
									
									<!-- DIAGNÓSTICO 3 -->
									<label class="control-label" for="inputDiagnostico3"> Diagnóstico 3 </label>
									<input class="form-control" id="inputDiagnostico3" name="inputDiagnostico3" type="text" placeholder=""
										value="<?php echo $paciente['geral']['diagnostico_3']; ?>"
									>
									<br>
									
									<h3> Contato </h3>
									
									<!-- CONTATO 1 -->
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- TELEFONE 1 -->
											<label class="control-label" for="inputTelefone1"> Telefone 1 </label>
											<input class="form-control" id="inputTelefone1" name="inputTelefone1" type="text" placeholder=""
												value="<?php echo $paciente['contato']['telefone_1']; ?>"
											>
											<br>
											
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- RESPONSÃ�VEL 1 -->
											<label class="control-label" for="inputNome1"> Nome </label>
											<input class="form-control" id="inputNome1" name="inputNome1" type="text" placeholder=""
												value="<?php echo $paciente['contato']['nome_1']; ?>"
											>
											<br>
											
										</div>
									</div>
									
									<!-- CONTATO 2 -->
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- TELEFONE 2 -->
											<label class="control-label" for="inputTelefone2"> Telefone 2 </label>
											<input class="form-control" id="inputTelefone2" name="inputTelefone2" type="text" placeholder=""
												value="<?php echo $paciente['contato']['telefone_2']; ?>"
											>
											<br>
											
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- RESPONSÃ�VEL 2 -->
											<label class="control-label" for="inputNome2"> Nome </label>
											<input class="form-control" id="inputNome2" name="inputNome2" type="text" placeholder=""
												value="<?php echo $paciente['contato']['nome_2']; ?>"
											>
											<br>
											
										</div>
									</div>
									
									<!-- CONTATO 3 -->
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- TELEFONE 3 -->
											<label class="control-label" for="inputTelefone3"> Telefone 3 </label>
											<input class="form-control" id="inputTelefone3" name="inputTelefone3" type="text" placeholder=""
												value="<?php echo $paciente['contato']['telefone_3']; ?>"
											>
											<br>
											
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- RESPONSÃ�VEL 3 -->
											<label class="control-label" for="inputNome3"> Nome </label>
											<input class="form-control" id="inputNome3" name="inputNome3" type="text" placeholder=""
												value="<?php echo $paciente['contato']['nome_3']; ?>"
											>
											<br>
											
										</div>
									</div>
									
								</div>
								
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 controls">
								
									<h3> Endereço </h3>
									<br>
									
									<!-- ENDEREÇO -->
									<input id="pac-input" class="controls2" type="text" placeholder="Endereço"></input>
									<div id="map" style="height:300px; width:100%"></div>
									<br>
									
									<label class="control-label" for="inputEndereco"> Endereço </label>
									<input type="text" id="inputEndereco" name="inputEndereco" class="form-control"
										value="<?php echo $paciente['contato']['endereco']; ?>"
									>
									<br>
									
									<label class="control-label" for="inputSubject"> Bairro </label>
									<input type="text" id="inputBairro" class="form-control" name="inputBairro"
										value="<?php echo $paciente['contato']['bairro']; ?>"
									>
									<br>
									
									<label class="control-label" for="inputSubject"> Cidade </label>
									<input type="text" id="inputCidade" name="inputCidade" class="form-control"
										value="<?php echo $paciente['contato']['cidade']; ?>"
									>
									<br>
									
									<input type="hidden" id="inputCoordenada" name="inputCoordenada" class="form-control"
										value="<?php echo $paciente['contato']['coordenada']; ?>"
									>
									<input type="hidden" id="inputLat" name="inputLat" class="form-control"
										value="<?php echo $paciente['contato']['lat']; ?>"
									>
									<input type="hidden" id="inputLng" name="inputLng" class="form-control"
										value="<?php echo $paciente['contato']['lng']; ?>"
									>
									
									
									<!-- PONTO DE REFERENCIA -->
									<label class="control-label" for="inputReferencia"> Ponto de referencia </label>
									<input class="form-control" id="inputReferencia" name="inputReferencia" type="text" placeholder=""
										value="<?php echo $paciente['contato']['referencia']; ?>"
									>
									<br>
									
									<!-- KILOMETRAGEM PARA REEMBOLSO -->
									<label class="control-label" for="inputDistancia"> Distância do hospital (km) </label>
									<input class="form-control" id="inputDistancia" name="inputDistancia" type="number" required min="0" step="any"
										value="<?php echo $paciente['contato']['kilometragem']; ?>"
									>
									<br>
									
									<input type='hidden' value='<?php echo $paciente['geral']['id']; ?>' name='inputID' id='inputID'>
									
									<!-- BOTÃƒO DE ENVIAR -->
									<input type="submit" value=" &nbsp; Salvar &nbsp; " class="btn btn-primary pull-right">
									
								</div>
							
							</form>
							
                        </div>
                    </div>

                </div>
                <br />
				
                <!-- FOOTER-->
                <?php include_once("include/layout/footer.php"); ?>
				
            </div>

        </div>

    </div>
	
	
    <script src="assets/js/bootstrap.min.js"></script>

   
    <!-- bootstrap progress js -->
    <script src="assets/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/nicescroll/jquery.nicescroll.min.js"></script>
   
    <script src="assets/js/custom.js"></script>
	
	<!-- GOOGLE MAPS -->
	<script>
		
		function initMap() {
			
			// INICIAR MAPA COM A COORDENADA ATUAL
			var map = new google.maps.Map(document.getElementById('map'), {
				center: {lat: <?php echo $paciente['contato']['lat']; ?>, lng: <?php echo $paciente['contato']['lng']; ?>},
				zoom: 15
			});
			
			var myLatlng = new google.maps.LatLng(<?php echo $paciente['contato']['lat']; ?>,<?php echo $paciente['contato']['lng']; ?>);
			
			var marker = new google.maps.Marker({
				position: myLatlng, 
				map: map
			}); 
			
			
			// ALTERAR A POSIÇÃO DO MARKER QUANDO NOVO ENDEREÃ‡O FOR SELECIONADO
			var input = ( document.getElementById('pac-input') );

			var types = document.getElementById('type-selector');
			map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
			map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

			var autocomplete = new google.maps.places.Autocomplete(input);
			autocomplete.bindTo('bounds', map);

			var infowindow = new google.maps.InfoWindow();
			var marker = new google.maps.Marker({
				map: map,
				anchorPoint: new google.maps.Point(0, -29)
			});
			
			autocomplete.addListener('place_changed', function() {
				
				infowindow.close();
				marker.setVisible(false);
				var place = autocomplete.getPlace();
				if (!place.geometry) {
					window.alert("Autocomplete's returned place contains no geometry");
					return;
				}

				// If the place has a geometry, then present it on a map.
				if (place.geometry.viewport) {
					map.fitBounds(place.geometry.viewport);
				} else {
					map.setCenter(place.geometry.location);
					map.setZoom(17);  // Why 17? Because it looks good.
				}
				
				marker.setIcon(/** @type {google.maps.Icon} */({
					url: place.icon,
					size: new google.maps.Size(71, 71),
					origin: new google.maps.Point(0, 0),
					anchor: new google.maps.Point(17, 34),
					scaledSize: new google.maps.Size(35, 35)
				}));
				
				marker.setPosition(place.geometry.location);
				marker.setVisible(true);

				var endereco = '';
				if (place.address_components) {
				  
					endereco = [
						(place.address_components[0] && place.address_components[0].short_name || ''),
						(place.address_components[1] && place.address_components[1].short_name || ''),
						(place.address_components[2] && place.address_components[2].short_name || ''),
					].join(' ');

					bairro = [
						(place.address_components[2] && place.address_components[2].short_name || '')
					].join(' ');

					cidade = [
						(place.address_components[3] && place.address_components[3].short_name || '')
					].join(' ');
				  
				}

				document.getElementById('inputCoordenada').value = place.geometry.location;
				document.getElementById('inputLat').value = place.geometry.location.lat();
				document.getElementById('inputLng').value = place.geometry.location.lng();
				document.getElementById('inputEndereco').value = place.name;
				document.getElementById('inputBairro').value = bairro;
				document.getElementById('inputCidade').value = cidade;
				
				
				infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
				infowindow.open(map, marker);
			
			});

			// Sets a listener on a radio button to change the filter type on Places
			// Autocomplete.
			function setupClickListener(id, types) {
				
				var radioButton = document.getElementById(id);
				radioButton.addEventListener('click', function() {
					autocomplete.setTypes(types);
				});
				
			}

			// setupClickListener('changetype-all', []);
			// setupClickListener('changetype-address', ['address']);
			// setupClickListener('changetype-establishment', ['establishment']);
			// setupClickListener('changetype-geocode', ['geocode']);
			
		}

    </script>
	
    <!-- GOOGLE MAPS -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOiVaKKO0XJltgm3witPr14h3RnhqiLAY&signed_in=true&libraries=places&callback=initMap"
        async defer>
	</script>
	<!-- [END] GOOGLE MAPS -->
	
	
</body>

</html>

<?php

}else{
	
	header("Location: login.php");
	
}

?>