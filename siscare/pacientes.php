<?php

	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "pacientes";
	
	
	// CONECTAR BANCO DE DADOS E ADICIONAR ARQUIVO DE FUNÃ‡Ã•ES
	include("config/db_connect.php");
	include("config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("functions/acesso/paciente.php");
		include_once("functions/erro/erro.php");
		
		
		/* SQL */
		// Busca os dados do servico
		include_once("sql/servico/buscar.php");
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		// Busca os dados de paciente
		include_once("sql/paciente/buscar.php");
		// Busca os dados de objetivos
		include_once("sql/objetivo/buscar.php");
		
		
		include_once("include/tabela/paciente/meus_pacientes.php");
		include_once("include/tabela/paciente/todos_pacientes.php");
		include_once("include/tabela/paciente/espec.php");
		
		
		// BUSCA DADOS GERAIS DO SERVIÇO
		$servico = busca_servico($mysqli,$_SESSION['user_Servico']);
		
		// BUSCA DADOS GERAIS DO USUARIO
		$usuario   = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SiSaN | Pacientes</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
	<link href="assets/css/icheck/flat/green.css" rel="stylesheet">
	<link href="assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">

</head>


<body class="nav-md">
	
    <div class="container body">
		
        <div class="main_container" style="background:#3C3C3C">
			
			<!-- MENU DA ESQUERDA -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- BARA SUPERIOR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- CONTEÃšDO DA PAGINA -->
            <div class="right_col" role="main">
				
				<!-- TITULO -->
                <div class="row">
					<div class="page-title">
						
						<!-- NOME DA PAGINA -->
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Pacientes</h3>
                        </div>
						
						<!-- CAMINHO DA PAGINA -->
                        <div class="pull-right hidden-xs">
						
							<i class="fa fa-home"></i>&nbsp;<a href="home.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							Pacientes &nbsp;&nbsp;
							
                        </div>
						
                    </div>
                </div>
				
				<br>
                <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        
						<div class="x_panel">
							<div class="x_content">
							
								<div role="tabpanel" data-example-id="togglable-tabs">
									
									<ul class="nav nav-tabs">
										
										<li class="active"><a href='#tab-meus-pacientes' data-toggle='tab'>Meus pacientes</a></li>
										<li><a href='#tab-pacientes' data-toggle='tab'>Todos</a></li>
									
										<?php
										
											
											// ABA ENFERMAGEM
											if( $servico['geral']['enf'] == 1 ){
												echo "<li><a href='#tab-enf' data-toggle='tab'>Enfermagem</a></li>";
												$active = "";
											}
											
											// ABA FISIOTERAPIA MOTORA
											if( $servico['geral']['fim'] == 1 ){
												echo "<li><a href='#tab-fim' data-toggle='tab'>Fisio Motora</a></li>";
												$active = "";
											}
											
											// ABA FISIOTERAPIA RESPIRATORIA
											if( $servico['geral']['fir'] == 1 ){
												echo "<li><a href='#tab-fir' data-toggle='tab'>Fisio Respiratória</a></li>";
												$active = "";
											}
											
											// ABA FONOAUDIOLOGIA
											if( $servico['geral']['fon'] == 1 ){
												echo "<li><a href='#tab-fon' data-toggle='tab'>Fonoaudiologia</a></li>";
												$active = "";
											}
											
											// ABA EQUIPE MÉDICA
											if( $servico['geral']['med'] == 1 ){
												echo "<li><a href='#tab-med' data-toggle='tab'>Médico</a></li>";
												$active = "";
											}
											
											// ABA NUTRIÇÃO
											if( $servico['geral']['nut'] == 1 ){
												echo "<li><a href='#tab-nut' data-toggle='tab'>Nutrição</a></li>";
												$active = "";
											}
											
											
										?>
										
									</ul>
									
									<!-- CONTEUDO DAS ABAS -->
									<div id="generalTabContent" class="tab-content">
										
										<!-- TODOS MEUS PACIENTES -->
										<?php 
											exibir_meus_pacientes($mysqli, $usuario);
										?>
										
										<!-- TODOS PACIENTES -->
										<?php
											exibir_pacientes_perfil($mysqli, $usuario['acesso']);
										?>
										
										<!-- ENFERMAGEM -->
										<?php
										
											// TABELA DE ENFERMAGEM
											if( $servico['geral']['enf'] == 1 ){
												
												$equipe = 'enf';
												$equipe_nome = 'Pacientes de Enfermagem';
												
												exibir_pacientes_especialidades($mysqli,$equipe_nome,$equipe,$usuario['acesso']);
												
											}
											
										?>
										
										<!-- FISIOTERAPIA MOTORA -->
										<?php
										
											// TABELA DE FISIOTERAPIA MOTORA
											if( $servico['geral']['fim'] == 1 ){
												
												$equipe = 'fim';
												$equipe_nome = 'Pacientes da Fisioterapia Motora';
												
												exibir_pacientes_especialidades($mysqli,$equipe_nome,$equipe,$usuario['acesso']);
												
											}
											
										?>
										
										<!-- FISIOTERAPIA RESPIRATORIA -->
										<?php
										
											// TABELA DA FISIOTERAPIA RESPIRATORIA
											if( $servico['geral']['fir'] == 1 ){
												
												$equipe = 'fir';
												$equipe_nome = 'Pacientes da Fisioterapia Respiratória';
												
												exibir_pacientes_especialidades($mysqli,$equipe_nome,$equipe,$usuario['acesso']);
												
											}
											
										?>
										
										<!-- FONOAUDIOLOGIA -->
										<?php
										
											// TABELA DE FONOAUDIOLOGIA
											if( $servico['geral']['fon'] == 1 ){
												
												$equipe = 'fon';
												$equipe_nome = 'Pacientes da Fonoaudiologia';
												
												exibir_pacientes_especialidades($mysqli,$equipe_nome,$equipe,$usuario['acesso']);
												
											}
											
										?>
										
										<!-- EQUIPE MÉDICA -->
										<?php
										
											// TABELA DA EQUIPE MÉDICA
											if( $servico['geral']['med'] == 1 ){
												
												$equipe = 'med';
												$equipe_nome = 'Pacientes da Equipe Médica';
												
												exibir_pacientes_especialidades($mysqli,$equipe_nome,$equipe,$usuario['acesso']);
												
											}
											
										?>
										
										<!-- NUTRIÇÃO -->
										<?php
										
											// TABELA DE NUTRIÇÃO
											if( $servico['geral']['nut'] == 1 ){
												
												$equipe = 'nut';
												$equipe_nome = 'Pacientes da Nutrição';
												
												exibir_pacientes_especialidades($mysqli,$equipe_nome,$equipe,$usuario['acesso']);
												
											}
											
										?>
										
									</div>
									
								</div>

							</div>
						</div>
						
                    </div>

                </div>
                <br />
				

                <!-- FOOTER-->
				<?php include_once("include/layout/footer.php"); ?>
				
				
            </div>
		
        </div>
		
    </div>
	
	
    <script src="assets/js/bootstrap.min.js"></script>
	
	<!-- chart js -->
    <script src="assets/js/chartjs/chart.min.js"></script>
		
    <!-- bootstrap progress js -->
    <script src="assets/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/nicescroll/jquery.nicescroll.min.js"></script>
    
    <script src="assets/js/custom.js"></script>
	
	
	<!-- TABELA DINAMICA -->
	<script src="//code.jquery.com/jquery-1.12.3.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

	<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />
	
	
	<script>
	
		$(document).ready(function() {
			
			$('#tabela_pacientes_fon').DataTable( {
				
				"paging":   		true,
				"bLengthChange": 	true,
				"pageLength": 		20,
				"ordering": 		true,
				"info":     		true,
				"sPaginationType": 	"simple_numbers",
				"order": 			[[ 0, "asc" ]],
				"columnDefs": 		[ { "targets": 1, "orderable": true } ],
				stateSave: 			true,
				
				"dom": '<"left"f><l>rt<"bottom"pi><"clear">',
				
				"language": {
					"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
				},
				
				buttons: [ ]
				
			} );
			
			$('#tabela_pacientes_todos').DataTable( {
				
				"paging":   		true,
				"bLengthChange": 	true,
				"pageLength": 		20,
				"ordering": 		true,
				"info":     		true,
				"sPaginationType": 	"simple_numbers",
				"order": 			[[ 0, "asc" ]],
				"columnDefs": 		[ { "targets": 1, "orderable": true } ],
				stateSave: 			true,
				
				"dom": '<"left"f><l>rt<"bottom"pi><"clear">',
				
				"language": {
					"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
				},
				
				buttons: [ ]
				
			} );
			
			$('#tabela_pacientes_meus').DataTable( {
				
				"paging":   		true,
				"bLengthChange": 	true,
				"pageLength": 		20,
				"ordering": 		true,
				"info":     		true,
				"sPaginationType": 	"simple_numbers",
				"order": 			[[ 0, "asc" ]],
				"columnDefs": 		[ { "targets": 1, "orderable": true } ],
				stateSave: 			true,
				
				"dom": '<"left"f><l>rt<"bottom"pi><"clear">',
				
				"language": {
					"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
				},
				
				buttons: [ ]
				
			} );

		} );
		
	</script>
	
	
</body>

</html>

<?php

}else{
	
	header("Location: login.php");
	
}

?>