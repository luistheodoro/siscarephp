<?php

	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "equipes";
	
	
	// Inclua fun��es e conex�es de banco de dados aqui. Ver 3.1. 
	include_once("config/db_connect.php");
	include_once("config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verifica��o de acessos
		include_once("functions/acesso/usuario.php");
		include_once("functions/erro/erro.php");
		
		
		/* SQL */
		// Busca os dados do servico
		include_once("sql/servico/buscar.php");
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		
		
		include("include/tabela/usuario/equipe.php");


		// BUSCA DADOS GERAIS DO SERVI�O
		$servico = busca_servico($mysqli,$_SESSION['user_Servico']);
		
		// BUSCA DADOS GERAIS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SiSaN | Equipes</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
    
</head>


<body class="nav-md">
	
    <div class="container body">
		
        <div class="main_container" style="background:#3C3C3C">
			
			<!-- MENU DA ESQUERDA -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- BARA SUPERIOR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- CONTEÚDO DA PAGINA -->
            <div class="right_col" role="main">

                <!-- TITULO -->
                <div class="row">
					<div class="page-title">
					
						<!-- NOME DA PAGINA -->
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Equipes</h3>
                        </div>
						
						<!-- CAMINHO DA PAGINA -->
                        <div class="pull-right hidden-xs">
							
							<i class="fa fa-home"></i>&nbsp;<a href="home.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							Equipes &nbsp;&nbsp;
							
                        </div>
						
                    </div>
                </div>
				
				<br>
                <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        
						<div class="x_panel">
							<div class="x_content">
							
								<div role="tabpanel" data-example-id="togglable-tabs">
									
									<ul class="nav nav-tabs">
										
										<?php
										
											$active = " class='active' ";
											
											if( $servico['geral']['enf'] == 1 ){
												echo "<li ".$active."><a href='#tab-enf' data-toggle='tab'>Enfermagem</a></li>";
												$active = "";
											}
											if( $servico['geral']['fim'] == 1 ){
												echo "<li ".$active."><a href='#tab-fim' data-toggle='tab'>Fisio Motora</a></li>";
												$active = "";
											}
											if( $servico['geral']['fir'] == 1 ){
												echo "<li ".$active."><a href='#tab-fir' data-toggle='tab'>Fisio Respiratória</a></li>";
												$active = "";
											}
											if( $servico['geral']['fon'] == 1 ){
												echo "<li ".$active."><a href='#tab-fon' data-toggle='tab'>Fonoaudiologia</a></li>";
												$active = "";
											}
											if( $servico['geral']['med'] == 1 ){
												echo "<li ".$active."><a href='#tab-med' data-toggle='tab'>Médico</a></li>";
												$active = "";
											}
											if( $servico['geral']['nut'] == 1 ){
												echo "<li ".$active."><a href='#tab-nut' data-toggle='tab'>Nutrição</a></li>";
												$active = "";
											}
											
										
										?>
										
									</ul>
									
									<!-- PERFIL -->
									<div id="generalTabContent" class="tab-content">
									
										<?php
										
											
											$active = "active";
											
											// TABELA DE ENFERMAGEM
											if( $servico['geral']['enf'] == 1 ){
												
												$equipe = 'enf';
												$equipe_nome = 'Equipe de Enfermagem';
												
												exibir_equipe($mysqli,$equipe_nome,$equipe,$active,$usuario,$servico);
												
												$active = "";
												
											}
											
											// TABELA DE FISIOTERAPIA MOTORA
											if( $servico['geral']['fim'] == 1 ){
												
												$equipe = 'fim';
												$equipe_nome = 'Equipe de Fisioterapia Motora';
												
												exibir_equipe($mysqli,$equipe_nome,$equipe,$active,$usuario,$servico);
												
												$active = "";
												
											}
											
											// TABELA DE FISIOTERAPIA RESPIRATÓRIA
											if( $servico['geral']['fir'] == 1 ){
												
												$equipe = 'fir';
												$equipe_nome = 'Equipe de Fisioterapia Respiratória';
												
												exibir_equipe($mysqli,$equipe_nome,$equipe,$active,$usuario,$servico);
												
												$active = "";
												
											}
											
											// TABELA DE FONOAUDIOLOGIA
											if( $servico['geral']['fon'] == 1 ){
												
												$equipe = 'fon';
												$equipe_nome = 'Equipe de Fonoaudiologia';
												
												exibir_equipe($mysqli,$equipe_nome,$equipe,$active,$usuario,$servico);
												
												$active = "";
												
											}
											
											// TABELA DE EQUIPE MÉDICA
											if( $servico['geral']['med'] == 1 ){
												
												$equipe = 'med';
												$equipe_nome = 'Equipe Médica';
												
												exibir_equipe($mysqli,$equipe_nome,$equipe,$active,$usuario,$servico);
												
												$active = "";
												
											}
											
											// TABELA DE NUTRICAO
											if( $servico['geral']['nut'] == 1 ){
												
												$equipe = 'nut';
												$equipe_nome = 'Equipe de Nutrição';
												
												exibir_equipe($mysqli,$equipe_nome,$equipe,$active,$usuario,$servico);
												
												$active = "";
												
											}
											
										?>
										
									</div>
									
								</div>

							</div>
						</div>
						
                    </div>

                </div>
                <br />
				

                <!-- FOOTER-->
				<?php include_once("include/layout/footer.php"); ?>
				
				
            </div>
		
        </div>
		
    </div>
	
	
    <script src="assets/js/bootstrap.min.js"></script>
	
    <!-- bootstrap progress js -->
    <script src="assets/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/nicescroll/jquery.nicescroll.min.js"></script>
    
    <script src="assets/js/custom.js"></script>
	
</body>

</html>

<?php

}else{
	
	header("Location: login.php");
	
}

?>