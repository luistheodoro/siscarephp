﻿<?php

	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "config";
	
	// ZERAR MENSAGEM DE ERRO
	$error_msg = "";
	
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("config/db_connect.php");
	include_once("config/functions.php");
	
	
	sec_session_start();  
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("functions/acesso/usuario.php");
		include_once("functions/erro/erro.php");
		
		
		/* SQL */
		// Busca os dados do servico
		include_once("sql/servico/buscar.php");
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		
		
		// BUSCA DADOS DO SERVICO
		$servico = busca_servico($mysqli,$_SESSION['user_Servico2']);
		
		// BUSCA DADOS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	
    <title>SiSaN | Configurações</title>
	
	
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
	
    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
    
</head>


<body class="nav-md">

    <div class="container body">
		
        <div class="main_container" style="background:#3C3C3C">
			
			<!-- SIDEBAR MENU -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- TOPBAR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- PAGE CONTENT -->
            <div class="right_col" role="main">

                <!-- top tiles -->
                <div class="row">
					<div class="page-title">
					
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Configurações</h3>
                        </div>
						
                        <div class="pull-right hidden-xs">
								
								<i class="fa fa-home"></i>&nbsp;<a href="home.php">Home</a>&nbsp;&nbsp;
								
								<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
									Configurações &nbsp;&nbsp;
									
						</div>
						
                    </div>
                </div>
                <!-- /top tiles -->
				
				<br>
				

				<div class="row">
					<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
						<div class="x_panel">
							
							<div class="x_content">

								<div class="col-md-5 col-sm-5 col-xs-12 profile_left">
									
									<h3>Cadastrar novo hospital</h3>
									
									<?php
										if( acesso_editar_perfil($mysqli,$usuario,$usuario) == TRUE ){
									?>
										<form method="post" action="sql/servico/criar.php">
											
											<label class="">Nome do serviço</label>
											<input class="form-control" type="text" name="nome_servico" id="nome_servico"></input>
											
											<br>
											<button type="submit" class="btn btn-primary">Criar</button>
											
										</form>
										
									<?php } ?>
									
									
								</div>
								<div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
									
									
									<div class="row">
									
										<div class="col-md-12">
											
											<h3>Selecionar Hospital</h3>
											<br>
											
											<form method="post" action="sql/servico/alterar_servico.php">
												
												<select class="form-control" name="codigo_servico" id="codigo_servico" onchange="this.form.submit()">
												
													<?php
													
														$query = "SELECT * FROM servicos ORDER BY nome_servico";
														$mysql_query = mysqli_query($mysqli,$query);
														
														while($busca_servico=mysqli_fetch_array($mysql_query)){
															
															if($busca_servico['codigo_servico'] == $servico['geral']['codigo_servico']){
																echo '<option selected value="'.$busca_servico['codigo_servico'].'">'.$busca_servico['codigo_servico'].' - '.$busca_servico['nome_servico'].'</option>';
															}else{
																echo '<option value="'.$busca_servico['codigo_servico'].'">'.$busca_servico['codigo_servico'].' - '.$busca_servico['nome_servico'].'</option>';
															}
															
														}
													
													?>
													
												</select>
												
											</form>
											
											<br>
											<div class="x_panel">
												
												<div class="x_title">
													<h4>Módulos de Serviço - <b><?php echo $servico['geral']['nome_servico']; ?></b></h4>
												</div>
												
												<div class="x_content">
													
													<form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/servico/editar.php">
													
														<?php
										
															if($usuario['acesso']['cadastrar_perfil'] == 4){
																
																if($servico['geral']['enf']==1){
																	echo '<input type="checkbox" name="enf" id="enf" checked> Enfermagem <br>';
																}else{
																	echo '<input type="checkbox" name="enf" id="enf"> Enfermagem <br>';
																}
																
																if($servico['geral']['fon']==1){
																	echo '<input type="checkbox" name="fon" id="fon" checked> Fonoaudiologia <br>';
																}else{
																	echo '<input type="checkbox" name="fon" id="fon"> Fonoaudiologia <br>';
																}
																
																if($servico['geral']['nut']==1){
																	echo '<input type="checkbox" name="nut" id="nut" checked> Nuticionista <br>';
																}else{
																	echo '<input type="checkbox" name="nut" id="nut"> Nuticionista <br>';
																}
																
																if($servico['geral']['fir']==1){
																	echo '<input type="checkbox" name="fir" id="fir" checked> Fisioterapia Respiratória <br>';
																}else{
																	echo '<input type="checkbox" name="fir" id="fir"> Fisioterapia Respiratória <br>';
																}
																
																if($servico['geral']['fim']==1){
																	echo '<input type="checkbox" name="fim" id="fim" checked> Fisioterapia Motora <br>';
																}else{
																	echo '<input type="checkbox" name="fim" id="fim"> Fisioterapia Motora <br>';
																}
																
																if($servico['geral']['med']==1){
																	echo '<input type="checkbox" name="med" id="med" checked> Equipe Médica <br>';
																}else{
																	echo '<input type="checkbox" name="med" id="med"> Equipe Médica <br>';
																}
																
															}
															
														?>
														
														<br>
														<label>Quantidade de usuários</label>
														<input class="form-control" type="text" value="<?php echo $servico['geral']['usuarios']; ?>" name="num_usuarios" id="num_usuarios"></input>
														
												</div>
												
												<div class="x_footer">
												
														<br>
														<input type="hidden" value="<?php echo $servico['geral']['codigo_servico']; ?>" name="codigo_servico" id="codigo_servico">
														<button class="btn btn-primary" type="submit">Salvar</button>
													
													</form>
												</div>
												
											</div>
										</div>
									
									</div>
									
									
									
									
								</div>
							</div>
						</div>
					</div>
				</div>
				
                <br />
				

                <!-- FOOTER-->
				<?php include_once("include/layout/footer.php"); ?>
				
				
            </div>
            <!-- /page content -->
		
        </div>
		
    </div>
	
	
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="assets/js/custom.js"></script>

    
	<!-- image cropping -->
    <script src="assets/js/cropping/cropper.min.js"></script>
    <script src="assets/js/cropping/main.js"></script>
	
	
    <!-- /footer content -->
	
</body>

</html>

<?php

}else{
	
	header("Location: login.php");
	
}

?>