﻿<?php
	
	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "pacientes";
	
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("config/db_connect.php");
	include_once("config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("functions/acesso/paciente.php");
		include_once("functions/erro/erro.php");
		
		
		/* SQL */
		// Busca os dados do servico
		include_once("sql/servico/buscar.php");
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		// Busca os dados de paciente
		include_once("sql/paciente/buscar.php");
		
		
		// BUSCA DADOS DO SERVICO
		$servico = busca_servico($mysqli,$_SESSION['user_Servico']);
		
		// BUSCA DADOS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA O ID E DADOS DO PACIENTE
		$id_paciente 	= isset($_GET['id']) ? $_GET['id'] : header("Location: ../../../pacientes.php") ;
		$id_paciente 	= preg_replace("/[^0-9]+/", "", $id_paciente);
		$paciente 		= busca_paciente($mysqli, $id_paciente);
		
		
		// VERIFICA QUAL ABA DEVE SER MOSTRADA
		$tab = isset($_GET['tab']) ? $_GET['tab'] : '';
		
		// RECUPERA MENSAGENS DE ERRO
		$erro = isset($_GET['e']) ? $_GET['e'] : '';
		$erro_texto = msg_erro($erro);
		
		// RECUPERA MENSAGENS DE SUCESSO
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	
    <title>SiSaN | Pacientes</title>
	
	
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
	
    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/maps/jquery-jvectormap-2.0.1.css">
    <link href="assets/css/icheck/flat/green.css" rel="stylesheet">
	
	
    <script src="assets/js/jquery.min.js"></script>
    
	
	<!-- CARREGAR CSS DE MAPA -->
	<link type="text/css" rel="stylesheet" href="assets/css/maps.css">
	
	
	<!-- JAVASCRIPT DE FORMULARIOS -->
	<script src="include/fon/asha.js"></script>
	<script src="include/fon/inputs.js"></script>
	
	<script src="include/fir/nivelconsciencia.js"></script>


	
</head>


<body class="nav-md">

    <div class="container body">
		
        <div class="main_container" style="background:#3C3C3C">
			
			<!-- MENU DA ESQUERDA -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- BARA SUPERIOR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- CONTEÚDO DA PAGINA -->
            <div class="right_col" role="main">

                <!-- TITULO -->
                <div class="row">
					<div class="page-title">
						
						<!-- NOME DA PAGINA -->
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Pacientes</h3>
                        </div>
						
						<!-- CAMINHO DA PAGINA -->
                        <div class="pull-right hidden-xs">
								<i class="fa fa-home"></i>&nbsp;<a href="home.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
								<a href="pacientes.php">Pacientes</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
								<?php echo $paciente['geral']['nome']; ?>&nbsp;&nbsp;
                        </div>
						
                    </div>
                </div>
				<br>
				
				
				<!-- MENSAGEM DE ERRO -->
				<?php
				
					if($erro_texto!= FALSE){
						echo '<div class="alert alert-danger"><strong>Ops!</strong> '.$erro_texto.'</div>';
					}
					
				?>
				
				
                <div class="row">
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 col-lg-offset-1 col-md-offset-1 col-sm-offset-1">
                        
						<div class="x_panel">
							<div class="x_content">
								
								<!-- NOME, IDADE E PROGNOSTICO DO PACIENTE -->
								<h1> 
									
									<!-- NOME DO PACIENTE -->
									<?php echo $paciente['geral']['nome']; ?>
									
									<small>
										
										<!-- IDADE DO PACIENTE -->
										<i><?php echo idade($paciente['geral']['nascimento']); ?></i>
										
										
											&nbsp; &nbsp;
											<a 	tabindex	= "-1"
												role		= "menuitem" 
												data-toggle	= "modal" 
												data-target	= ".modal-obito"
												style		= "text-decoration: none; cursor: pointer; margin-left:20px;" class="pull-right"
											>
												<small> <i> Óbito </i> <i class="fa fa-times" aria-hidden="true"></i> </small>
											</a>
										
										<!-- EDITAR DADOS DO PERFIL DO PACIENTE -->
										<?php if( acesso_editar_perfil_paciente($mysqli,$usuario,$paciente) == TRUE ){ ?>
											
											
											&nbsp; &nbsp;
											<a 	tabindex	= "-1" 
												role		= "menuitem" 
												href		= "editar_paciente.php?id=<?php echo $paciente['geral']['id']; ?>"
												style="text-decoration: none" class="pull-right"
											>
												<small> <i> Editar </i> <i class="fa fa-pencil"></i> </small>
											</a><br>
											
										<?php } ?>
										
										
										<!-- PROGNOSTICO DO PACIENTE -->
										<?php 
										
										if($paciente['espec']['prognostico'] == "Restrito"){
											
											echo '
												<span class="label label-danger pull-right" style="font-weight:100"> 
													&nbsp; Prognóstico restrito &nbsp; 
												</span>
											';
											
										}
										
										if($paciente['espec']['prognostico'] == "Favorável"){
											
											echo '
												<span class="label label-success pull-right" style="font-weight:100"> 
													&nbsp; Prognóstico favorável &nbsp; 
												</span>
											';

										} 
										
										?>
										
									</small> 
									
								</h1>
								
								<h4>
									
									<!-- CUIDADOS PALIATIVOS -->
									<?php if($paciente['espec']['paliativo'] == TRUE){ ?>
										<span class="label label-warning" style="font-weight:100"> 
											&nbsp; Cuidados Paliativos &nbsp; 
										</span> &nbsp; 
									<?php } ?>
									
									<!-- PACIENTE DEGENERATIVO -->
									<?php if($paciente['espec']['degenerativo'] == TRUE){ ?>
										<span class="label label-warning" style="font-weight:100"> 
											&nbsp; Degenerativo &nbsp; 
										</span> &nbsp;
									<?php } ?>
									
									<!-- CRITERIO DE TERMINALIDADE -->
									<?php if($paciente['espec']['terminalidade'] == TRUE){ ?>
										<span class="label label-warning" style="font-weight:100"> 
											&nbsp; Critério de terminalidade &nbsp; 
										</span> &nbsp;
									<?php } ?>
									
								</h4>
								
								<br>
								
								<div role="tabpanel" data-example-id="togglable-tabs">
									
									<!-- MENU DE ABAS -->
									<ul class="nav nav-tabs">
										
										<li <?php if ($tab==''){ echo "class='active'"; } ?> ><a href="#tab-perfil" data-toggle="tab">Perfil</a></li>
										
										<?php
											
											$ativar = '';
										
											// ABA DE ENFERMAGEM
											if( $paciente['geral']['enf'] == 1 ){
												
												if ($tab=='enf'){ 
													echo "<li class='active'><a href='#tab-enf' data-toggle='tab'>Enfermagem</a></li>";
												}else{
													echo "<li><a href='#tab-enf' data-toggle='tab'>Enfermagem</a></li>";
												}
												
											}
											
											
											// ABA DE FISIOTERAPIA MOTORA
											if( $paciente['geral']['fim'] == 1 ){
												
												if ($tab=='fim'){ 
													echo "<li class='active'><a href='#tab-fim' data-toggle='tab'>Fisio Motora</a></li>";
												}else{
													echo "<li><a href='#tab-fim' data-toggle='tab'>Fisio Motora</a></li>";
												}
												
											}
											
											// ABA DE FISIOTERAPIA RESPIRATORIA
											if( $paciente['geral']['fir'] == 1 ){
												
												if ($tab=='fir'){  
													echo "<li class='active'><a href='#tab-fir' data-toggle='tab'>Fisio Respiratória</a></li>";
												}else{
													echo "<li><a href='#tab-fir' data-toggle='tab'>Fisio Respiratória</a></li>";
												}
												
											}
											
											// ABA DE FONOAUDIOLOGIA
											if( $paciente['geral']['fon'] == 1 ){
												
												if ($tab=='fon'){ 
													echo "<li class='active'><a href='#tab-fon' data-toggle='tab'>Fonoaudiologia</a></li>";
												}else{
													echo "<li><a href='#tab-fon' data-toggle='tab'>Fonoaudiologia</a></li>";
												}
												
											}
											
											// ABA DE EQUIPE MÉDICA
											if( $paciente['geral']['med'] == 1 ){
												
												if ($tab=='med'){ 
													echo "<li class='active'><a href='#tab-med' data-toggle='tab'>Médico</a></li>";
												}else{
													echo "<li><a href='#tab-med' data-toggle='tab'>Médico</a></li>";
												}
												
											}
											
											// ABA DE NUTRIÇÃO
											if( $paciente['geral']['nut'] == 1 ){
												
												if ($tab=='nut'){ 
													echo "<li class='active'><a href='#tab-nut' data-toggle='tab'>Nutrição</a></li>";
												}else{
													echo "<li><a href='#tab-nut' data-toggle='tab'>Nutrição</a></li>";
												}
												
											}
											
										?>
										
										
										<!-- CRIAR NOVA AVALIAÇÃO -->
										<?php if( acesso_criar_avaliacao($mysqli,$usuario,$paciente) == TRUE ){ ?>
											
											<a 	tabindex = "-1" role = "menuitem" data-toggle = "modal" data-target = ".modal-avaliacao-<?php echo $usuario['geral']['equipe']; ?>" style = "cursor: pointer;" class="pull-right label label-primary">
												<h4> 
													&nbsp; Nova Avaliação &nbsp; 
												</h4>
											</a>
											
										<?php } ?>
										
										
										<!-- OPÇÕES EXTRAS -->
										<?php if( $paciente['geral'][$usuario['geral']['equipe']] == TRUE ){ ?>
										
										<li class="dropdown pull-right" role="presentation">
											
											<!-- BOTAO PARA ABRIR O MENU -->
											<a class="dropdown-toggle" id="drop4" role="button" aria-expanded="true" aria-haspopup="true" href="#" data-toggle="dropdown">
												<i class="fa fa-plus-circle fa-2x"></i>
											</a>
											
											<ul class="dropdown-menu animated fadeInDown" id="menu6" role="menu">
												
												
												<!-- NOVA TERAPIA -->
												<?php if( acesso_criar_terapia($mysqli,$usuario,$paciente,$usuario['geral']['equipe']) == TRUE ){ ?>
												
													<li role="presentation">
														<a 	tabindex	= "-1"
															role		= "menuitem" 
															data-toggle	= "modal" 
															data-target	= ".modal-<?php echo $usuario['geral']['equipe']; ?>"
															style		= "cursor: pointer;"
														>
															Nova terapia
														</a>
													</li>
												
												<?php } ?>
												
												
												<!-- SOLICITAR AVALIAÇÃO -->
												<?php /*
												<li role="presentation">
													<a tabindex="-1" role="menuitem" href="#">Solicitar avaliação</a>
												</li>
												
												*/ ?>
												
												
												<!-- GERENCIAR RISCOS -->
												<?php if( acesso_gerenciar_risco($mysqli,$usuario,$paciente) == TRUE ){ ?>
													
													<li role="presentation">
														<a 	tabindex	= "-1"
															role		= "menuitem" 
															data-toggle	= "modal" 
															data-target	= ".modal-risco"
															style		= "cursor: pointer;"
														>
															Gerenciar riscos
														</a>
													</li>
												
												<?php } ?>
												
												
												<!-- ALTA DA ESPECIALIDADE -->
												<?php if($paciente['geral'][$usuario['geral']['equipe']] == 1){ ?>
												
													<li role="presentation">
														<a 	tabindex	= "-1"
															role		= "menuitem" 
															data-toggle	= "modal" 
															data-target	= ".modal-alta-<?php echo $usuario['geral']['equipe']; ?>"
															style		= "cursor: pointer;"
														>
															Alta <?php echo equipe($usuario['geral']['equipe'],4); ?>
														</a>
													</li>
												
												<?php } ?>
												
												
												<!-- SUSPENSÃO DE ACOMPANHAMENTO -->
												<?php if($paciente['geral'][$usuario['geral']['equipe']] == 1){ ?>
												
													<li role="presentation">
														<a 	tabindex	= "-1"
															role		= "menuitem" 
															data-toggle	= "modal" 
															data-target	= ".modal-suspensao-<?php echo $usuario['geral']['equipe']; ?>"
															style		= "cursor: pointer;"
														>
															Suspensão do atendimento
														</a>
													</li>
												
												<?php } ?>
												
												
												<li class="divider" role="presentation"></li>
												
												
												<!-- EDITAR DADOS DO PERFIL DO PACIENTE -->
												<?php if( acesso_editar_perfil_paciente($mysqli,$usuario,$paciente) == TRUE ){ ?>
													
													<li role="presentation">
														<a 	tabindex	= "-1" 
															role		= "menuitem" 
															href		= "editar_paciente.php?id=<?php echo $paciente['geral']['id']; ?>"
														>
															Editar perfil
														</a>
													</li>
													
												<?php } ?>
												
												
												<!-- EDITAR A ESPECIFICIDADE DO PACIENTE -->
												<?php if( acesso_editar_espec($mysqli,$usuario,$paciente) == TRUE ){ ?>
													
													<li role="presentation">
														<a 	tabindex	= "-1"
															role		= "menuitem" 
															data-toggle	= "modal" 
															data-target	= ".modal-espec"
															style		= "cursor: pointer;"
														>
															Editar especificidade
														</a>
													</li>
												
												<?php } ?>
												
												
												<!-- EDITAR FREQUENCIA DE ATENDIMENTO -->
												<?php if( acesso_editar_frequencia($mysqli,$usuario,$paciente,$usuario['geral']['equipe']) == TRUE ){ ?>
												
													<li role="presentation">
														<a 	tabindex	= "-1"
															role		= "menuitem" 
															data-toggle	= "modal" 
															data-target	= ".modal-frequencia"
															style		= "cursor: pointer;"
														>
															Frequência de atendimento
														</a>
													</li>
												
												<?php } ?>
												
												
												<!-- EDITAR RESPONSÁVEL -->
												<?php if( acesso_editar_responsavel($mysqli,$usuario,$paciente) == TRUE ){ ?>
												
													<li role="presentation">
														<a 	tabindex	= "-1"
															role		= "menuitem" 
															data-toggle	= "modal" 
															data-target	= ".modal-responsavel"
															style		= "cursor: pointer;"
														>
															Alterar responsável
														</a>
													</li>
												
												<?php } ?>
												
											</ul>
										</li>
										
										<?php } ?>
										
									</ul>
									
									<!-- CONTEUDO DAS ABAS -->
									<div id="generalTabContent" class="tab-content">
										
										<!-- PERFIL -->
										<div id="tab-perfil" class="tab-pane fade in <?php if ($tab==''){ echo "active"; } ?>">
											<?php 
												
												// INCLUIR ABA DE INFORMAÇÕES
												include_once("include/geral/info.php");
												paciente_perfil($mysqli,$paciente,$usuario); 
											
											?>
										</div>
										
										
										<!-- FONOAUDIOLOGIA -->
										<?php 
											
											// VERIFICA SE PACIENTE ESTA EM TRATAMENTO PELA EQUIPE DE FONOAUDIOLOGIA
											if( $paciente['geral']['fon'] == TRUE ){ 
												
												// ADICIONA A ABA DE INFORMAÇÕES DA FONOAUDIOLOGIA
												include_once("include/fon/info.php");
												paciente_fon($mysqli,$usuario['acesso'],$paciente,$tab);
												
											}
											
										?>
										
										
									</div>
									
								</div>

							</div>
						</div>
						
                    </div>

                </div>
                <br />
				
				<?php 											
					
					/* ----- FONOAUDIOLOGIA ----- */
					
					// MODAL TERAPIA
					if( acesso_criar_terapia($mysqli,$usuario,$paciente,'fon') == TRUE ){
						
						// VERIFICA SE PACIENTE ESTA EM TRATAMENTO PELA EQUIPE
						if( $paciente['geral'][$usuario['geral']['equipe']] == TRUE ){
							include_once("include/fon/terapia.php");
							criar_atendimento_fon($mysqli,$paciente);
						}
						
					}
					
					// MODAL CRIAR NOVA AVALIAÇÃO
					if( acesso_criar_avaliacao($mysqli,$usuario,$paciente) == TRUE ){
					
						// VERIFICA SE PACIENTE ESTA EM TRATAMENTO PELA EQUIPE
						if( $paciente['geral'][$usuario['geral']['equipe']] == FALSE ){
							include_once("include/fon/avaliar.php");
							criar_avaliacao_fon($mysqli,$paciente);
						}
						
						// VERIFICA SE PACIENTE ESTA EM TRATAMENTO PELA EQUIPE
						if( $paciente['geral'][$usuario['geral']['equipe']] == FALSE ){
							include_once("include/fir/avaliar.php");
							criar_avaliacao_fir($mysqli,$paciente);
						}
						
					}
					
					// MODAL EDITAR FREQUENCIA
					if( acesso_editar_frequencia($mysqli,$usuario,$paciente,$usuario['geral']['equipe']) == TRUE ){
						
						include_once("include/geral/frequencia.php");
						editar_frequencia($mysqli,$paciente);
					
					}
					
					// MODAL EDITAR RESPONSAVEL
					if( acesso_editar_responsavel($mysqli,$usuario,$paciente,$usuario['geral']['equipe']) == TRUE ){
						
						include_once("include/geral/responsavel.php");
						editar_responsavel($mysqli,$paciente,$usuario);
					
					}
					
					// MODAL ALTA
					if( acesso_alta($mysqli,$paciente,$usuario) == TRUE ){
						
						include_once("include/fon/alta.php");
						alta_fon($mysqli,$paciente);
					
					}
					
					// MODAL SUSPENSÃO
					if( acesso_alta($mysqli,$paciente,$usuario) == TRUE ){
					
						include_once("include/fon/alta.php");
						suspensao_fon($mysqli,$paciente);
						
					}
					
					
					/* ----- DIVERSOS ----- */
					
					// MODAL ÓBITO
					if( acesso_obito($mysqli,$paciente,$usuario) == TRUE ){
						
						include_once("include/geral/obito.php");
						obito($mysqli,$paciente);
						
					}
					
					// MODAL EDITAR ESPEC
					if( acesso_editar_espec($mysqli,$usuario,$paciente) == TRUE ){
						
						include_once("include/geral/espec.php");
						editar_espec($mysqli,$usuario,$paciente,$servico);
						
					}
					
					// MODAL GERENCIAR RISCO
					if( acesso_gerenciar_risco($mysqli,$usuario,$paciente) == TRUE ){ 
						
						include_once("include/geral/risco.php");
						risco($mysqli,$paciente,$usuario);
						
					}
					
				?>

                <!-- FOOTER-->
				<?php include_once("include/layout/footer.php"); ?>
				
				
            </div>
		
        </div>
		
    </div>
	
	
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- icheck -->
    <script src="assets/js/icheck/icheck.min.js"></script>
    <script src="assets/js/custom.js"></script>
    
    <!-- easypie -->
    <script src="assets/js/easypie/jquery.easypiechart.min.js"></script>
    
	<script>
	
		$('[data-toggle="modal_ver_terapia"]').on('click', function(e) {
			
			// Apaga o modal existente
			$('#modal_ver_terapia').remove();
			
			e.preventDefault();
			
			var $this = $(this)
			  , $remote = $this.data('remote') || $this.attr('href')
			  , $modal = $('<div class="modal" tabindex="-1" role="dialog" aria-hidden="true" id="modal_ver_terapia"></div>');
			
			
			// Creating modal dialog's DOM
			var $dialog = $(
				'<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
				'<div class="modal-dialog modal-sm">' +
				'<div class="modal-content">' +
					'<div class="modal-body text-center">' +
						'<br><i class="fa fa-refresh fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span><br><p>Carregando...</p>' +
					'</div>' +
				'</div></div></div>');
			
			// Opening dialog
			$dialog.modal();
				
			
			// Carrega o conteudo
			$modal.load($remote, function() {
				
				// Closing dialog
				$dialog.modal('hide');
				
				// Cria o modal na pagina
				$('body').append($modal);
				// Exibe o modal
				$modal.modal({backdrop: true, keyboard: true});
				
			});
			
		  }
		);
	
	</script>
    
	<!-- PNotify 
    <script type="text/javascript" src="js/notify/pnotify.core.js"></script>
    <script type="text/javascript" src="js/notify/pnotify.buttons.js"></script>
    <script type="text/javascript" src="js/notify/pnotify.nonblock.js"></script>
	<script src="js/nicescroll/jquery.nicescroll.min.js"></script>
	
    <script type="text/javascript">
        var permanotice, tooltip, _alert;
        $(function () {
            new PNotify({
                title: "PNotify",
                type: "error",
                text: "Welcome. Try hovering over me. You can click things behind me, because I'm non-blocking.",
                nonblock: {
                    nonblock: true
                },
                before_close: function (PNotify) {
                    // You can access the notice's options with this. It is read only.
                    //PNotify.options.text;

                    // You can change the notice's options after the timer like this:
                    PNotify.update({
                        title: PNotify.options.title + " - Enjoy your Stay",
                        before_close: null
                    });
                    PNotify.queueRemove();
                    return false;
                }
            });

        });
    </script>
	-->
	
	
	
	<!-- GOOGLE MAPS -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOiVaKKO0XJltgm3witPr14h3RnhqiLAY&signed_in=true&libraries=places&callback=initMap"
        async defer>
	</script>
	<!-- [END] GOOGLE MAPS -->
	
	
</body>

</html>

<?php

}else{
	
	header("Location: login.php");
	
}

?>