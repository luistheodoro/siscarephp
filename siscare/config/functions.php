<?php
include_once 'psl-config.php';
 
function sec_session_start() {
	
	session_name(md5('seg'.$_SERVER['REMOTE_ADDR'].$_SERVER['HTTP_USER_AGENT']));
	session_cache_expire(10);
	session_start();
	
	ini_set('date.timezone','America/Sao_Paulo');
	
}

function login($UserID, $password, $mysqli) {
    
	
	// Busca senha do usuario no banco de dados basico
	$prep_stmt = "SELECT id, codigo_servico, perfil_acesso, user_id, email, senha, salt FROM usuarios WHERE user_id = ? OR email = ? LIMIT 1";
	$stmt = $mysqli->prepare($prep_stmt);
	
	// Verifica se query esta correta
	if ($stmt) {
		
		// Adiciona as variaveis
		$stmt->bind_param('ss', $UserID, $UserID);
		
		// Verifica se query funcionou
		if($stmt->execute()){
			
			// Armazena o resultado
			$stmt->store_result();
			
			// Verifica se existe algum cadastro no banco
			if ($stmt->num_rows == 1) {
				
				$stmt->bind_result($user['id'], $user['codigo_servico'], $user['perfil_acesso'], $user['user_id'], $user['email'], $user['senha'], $user['salt']);
				$stmt->fetch();
				
				
				$password = hash('sha512', $password . $user['salt']);
				
			}else{
				$error_msg .= '<p class="error">Erro no banco de dados</p>';
				return false;
			}
			
		}else{
			die('Error : ('. $mysqli->errno .') '. $mysqli->error);
			$error_msg .= '<p class="error">Erro no banco de dados</p>';
		}
		
		// Fecha a consulta
		$stmt->close();
		
	}
	
	
	if (checkbrute($user['id'], $mysqli) == true) {
		
		return false;
	}else{
		
		if($user['senha'] == $password){
			
			// A senha está correta!
			
			// Obtém o string usuário-agente do usuário. 
			$user_browser = $_SERVER['HTTP_USER_AGENT'];
			
			// proteção XSS conforme imprimimos este valor
			$user_Codigo 	= preg_replace("/[^0-9]+/", "", $user['id']);
			$_SESSION['user_Codigo'] = $user_Codigo;
			
			// proteção XSS conforme imprimimos este valor
			$user_Servico 	= preg_replace("/[^0-9]+/", "", $user['codigo_servico']);
			$_SESSION['user_Servico'] = $user_Servico;
			$_SESSION['user_Servico2'] = $user_Servico;
			
			// proteção XSS conforme imprimimos este valor 
			$user_Acesso 	= preg_replace("/[^a-zA-Z0-9_\-]+/", "", $user['perfil_acesso']);
			$_SESSION['user_Acesso'] = $user_Acesso;
			
			// proteção XSS conforme imprimimos este valor 
			$user_Email 	= preg_replace("/[^a-zA-Z0-9_\-\@\.]/", "", $user['email']);
			$_SESSION['user_Email'] = $user_Email;
			
			
			$query = "SELECT nome FROM ".$_SESSION['user_Servico']."_usuarios WHERE id_usuario = '".$user_Codigo."' LIMIT 1";
			$mysql_query = mysqli_query($mysqli,$query);
			$usuario = mysqli_fetch_assoc($mysql_query);
			
			// proteção XSS conforme imprimimos este valor 
			$user_Nome 		= preg_replace("/[^a-zA-Z0-9_ ]+/", "", $usuario['nome']);
			$_SESSION['user_Nome'] = $user_Nome;
			
			
			$_SESSION['login_string'] = hash('sha512', $password . $user_browser);
			
			// Login concluído com sucesso.
			return true;
			
		}else{
			
			// A senha não está correta
			
			// Registramos essa tentativa no banco de dados
			$now = time();
			$mysqli = mysqli_query($mysqli, "INSERT INTO login (codigo_usuario, horario) VALUES ( '".$user['id']."', NOW() ) ");
			
			return false;
			
		}
		
	}
	
}

function checkbrute($user_id, $mysqli) {
    // Registra a hora atual 
    $now = time();
 
    // Todas as tentativas de login são contadas dentro do intervalo das últimas 2 horas. 
    $valid_attempts = $now - (2 * 60 * 60);
 
    if ($stmt = $mysqli->prepare("SELECT horario FROM login <code><pre> WHERE codigo_usuario = ? AND horario > '$valid_attempts'")) {
        $stmt->bind_param('i', $user_id);
 
        // Executa a tarefa pré-estabelecida. 
        $stmt->execute();
        $stmt->store_result();
 
        // Se houve mais do que 5 tentativas fracassadas de login 
        if ($stmt->num_rows > 5) {
            return true;
        } else {
            return false;
        }
    }
}

function login_check($mysqli) {
	
	// Verifica se todas as variáveis das sessões foram definidas 
    if (isset($_SESSION['user_Codigo'], $_SESSION['user_Nome'], $_SESSION['login_string'])) {
		
		// Recupera os valores da sessao
        $user_id 		= $_SESSION['user_Codigo'];
        $username 		= $_SESSION['user_Nome'];
        $login_string 	= $_SESSION['login_string'];
 
        // Pega a string do usuário.
        $user_browser = $_SERVER['HTTP_USER_AGENT'];
		
		
		// Busca senha do usuario no banco de dados basico
		$prep_stmt = "SELECT senha FROM usuarios WHERE id = ? LIMIT 1";
		$stmt = $mysqli->prepare($prep_stmt);
		
		// Verifica se query esta correta
		if ($stmt) {
			
			// Adiciona as variaveis
			$stmt->bind_param('s', $user_id);
			
			// Verifica se query funcionou
			if($stmt->execute()){
				
				// Armazena o resultado
				$stmt->store_result();
				
				// Verifica se existe algum cadastro no banco
				if ($stmt->num_rows == 1) {
					
					$stmt->bind_result($password);
					$stmt->fetch();
					$login_check = hash('sha512', $password . $user_browser);
			
					if ($login_check == $login_string) {
						// Logado!!!
						return true;
					} else {
						// Não foi logado 
						// echo "usuário não confere";
						$error_msg .= '<p class="error">Erro no banco de dados</p>';
						return false;
					}
				}else{
					$error_msg .= '<p class="error">Erro no banco de dados</p>';
					return false;
				}
				
			}else{
				die('Error : ('. $mysqli->errno .') '. $mysqli->error);
				$error_msg .= '<p class="error">Erro no banco de dados</p>';
			}
			
			// Fecha a consulta
			$stmt->close();
			
		}
		
    } else {
        // Não foi logado 
		// echo "SESSION nao existe";
		$error_msg .= '<p class="error">Erro no banco de dados</p>';
        return false;
    }
	
}

function esc_url($url) {
 
    if ('' == $url) {
        return $url;
    }
 
    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);
 
    $strip = array('%0d', '%0a', '%0D', '%0A');
    $url = (string) $url;
 
    $count = 1;
    while ($count) {
        $url = str_replace($strip, '', $url, $count);
    }
 
    $url = str_replace(';//', '://', $url);
 
    $url = htmlentities($url);
 
    $url = str_replace('&amp;', '&#038;', $url);
    $url = str_replace("'", '&#039;', $url);
 
    if ($url[0] !== '/') {
        // Estamos interessados somente em links relacionados provenientes de $_SERVER['PHP_SELF']
        return '';
    } else {
        return $url;
    }
}



function ajusta_mes($mes){
		
	switch($mes){
		case 1: 
			$mes = "Janeiro";
			break;
		case 2: 
			$mes = "Fevereiro";
			break;
		case 3: 
			$mes = "Março";
			break;
		case 4: 
			$mes = "Abril";
			break;
		case 5: 
			$mes = "Maio";
			break;
		case 6: 
			$mes = "Junho";
			break;
		case 7: 
			$mes = "Julho";
			break;
		case 8: 
			$mes = "Agosto";
			break;
		case 9: 
			$mes = "Setembro";
			break;
		case 10: 
			$mes = "Outubro";
			break;
		case 11: 
			$mes = "Novembro";
			break;
		case 12: 
			$mes = "Dezembro";
			break;
	}
	
	return $mes;
	
}

function ajusta_mes_2($mes){
		
	switch($mes){
		case 1: 
			$mes = "Jan";
			break;
		case 2: 
			$mes = "Fev";
			break;
		case 3: 
			$mes = "Mar";
			break;
		case 4: 
			$mes = "Abr";
			break;
		case 5: 
			$mes = "Mai";
			break;
		case 6: 
			$mes = "Jun";
			break;
		case 7: 
			$mes = "Jul";
			break;
		case 8: 
			$mes = "Ago";
			break;
		case 9: 
			$mes = "Set";
			break;
		case 10: 
			$mes = "Out";
			break;
		case 11: 
			$mes = "Nov";
			break;
		case 12: 
			$mes = "Dez";
			break;
	}
	
	return $mes;
	
}


function idade($data){
	
	if($data!=NULL){
		
		// Separa em dia, mês e ano
		list($ano, $mes, $dia) = explode('-', $data);
		
		// Descobre que dia é hoje e retorna a unix timestamp
		$hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
		// Descobre a unix timestamp da data de nascimento do fulano
		$nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
		
		// Depois apenas fazemos o cálculo já citado :)
		$idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);
	 
		$idade = $idade." anos";
		
	}else{
		$idade = "";
	}
	
	
	return $idade;
	
}


class DateFormatter extends DateTime{
	
    private $parts = array(
        array(31104000, 'ano', 'anos'),
        array(2592000, 'mês', 'meses'),
        array(86400, 'dia', 'dias'),
        array(3600, 'hora', 'horas'),
        array(60, 'minuto', 'minutos'),
        array(1, 'segundo', 'segundos'),
    );

    public function formattedInterval(DateTime $relativeTo = NULL)
    {
        if (is_null($relativeTo)) {
            $relativeTo = new DateTime();
        }
        $diff       = parent::format('U') - $relativeTo->format('U');
        $past       = FALSE;
        if ($diff < 0) {
            $past   = TRUE;
            $diff  *= -1;
        }
        foreach ($this->parts as $subparts) {
            $n = floor($diff / $subparts[0]);
            if ($n) {
                $output = '%d %s';
                $part   = $subparts[$n > 1 ? 2 : 1];
                if ($past) {
                    $part  .= ' atrás';
                }
                return sprintf($output, $n, $part);
            }
        }
        return 'agora';
    }
}


function limitarTexto($texto, $limite){
	
	$contador = strlen($texto);
	
	if ( $contador >= $limite ) {
		
		$texto = substr($texto, 0, strrpos(substr($texto, 0, $limite), ' ')) . '...';
		return $texto;
		
	}else{
		
		return $texto;
		
	}
	
} 


function diagnostico($d1, $d2, $d3){
	
	$diagnostico = "";
	
	if($d1!=""){
		$diagnostico = $d1;
	}
	
	if($d2!=""){
		if($diagnostico!=""){
			$diagnostico = $diagnostico." / ".$d2;
		}else{
			$diagnostico = $d2;
		}
	}
	
	if($d3!=""){
		if($diagnostico!=""){
			$diagnostico = $diagnostico." / ".$d3;
		}else{
			$diagnostico = $d3;
		}
	}
	
	return $diagnostico;
	
	
} 


function equipe($codigo,$tipo){
	
	if($codigo=='fon'){
		
		switch($tipo){
			case 1:
				return 'Fonoaudiologia';
			case 2:
				return 'Equipe de Fonoaudiologia';
			case 3:
				return 'Fonoaudióloga';
			case 4:
				return 'Fonoaudiológica';
		}
		
	}
	
}
