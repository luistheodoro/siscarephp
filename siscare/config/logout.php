<?php
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include("db_connect.php");
	include("functions.php");
	
	sec_session_start(); 
	
	// ACERTAR QUANDO O LOGIN ESTIVER FUNCIONANDO
	if(login_check($mysqli) == TRUE) {
	
		unset($_SESSION['user_Codigo']);
		unset($_SESSION['user_Servico']);
		unset($_SESSION['user_Acesso']);
		unset($_SESSION['user_Email']);
		unset($_SESSION['user_Nome']);
		unset($_SESSION['login_string']);
		
		session_destroy();
		
		header("Location: ../login.php");
		
		exit;
	
	}else{
		header("Location: ../login.php");
	}
	
?>