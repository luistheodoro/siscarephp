<?php
include_once 'db_connect.php';
include_once 'functions.php';
 
sec_session_start(); // Nossa seguran�a personalizada para iniciar uma sess�o php.
 
if (isset($_POST['UserID'], $_POST['p'])) {

    $userid 	= $_POST['UserID'];
    $password 	= $_POST['p']; // The hashed password.
 
    if (login($userid, $password, $mysqli) == true) {
        // Login com sucesso 
        header('Location: ../home.php');
    } else {
        // Falha de login 
        header('Location: ../login.php?error=1');
    }
} else {
    // As vari�veis POST corretas n�o foram enviadas para esta p�gina. 
    echo 'Invalid Request' ;
}

?>