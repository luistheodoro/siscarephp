﻿<?php
	
	// CONECTAR AO BANCO DE DADOS
	include("db_connect.php");
	
	// RECUPERA O EMAIL DO USUARIO
	$email			= filter_input(INPUT_POST, 'inputEmail'			, FILTER_SANITIZE_EMAIL);
	$email 			= filter_var($email, FILTER_VALIDATE_EMAIL);
	
	// FILTRA A STRING DO EMAIL
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		
		// Email inválido
		header("Location: ../login.php?e=93");
		
	}else{
		
		// VERIFICA SE E-MAIL EXISTE NO BANCO DE DADOS
		$query 		 = " SELECT id FROM usuarios WHERE email = '".$email."' AND recuperar_data < DATE_SUB(NOW(), INTERVAL 5 MINUTE) ";
		$mysql_query = mysqli_query($mysqli,$query);
		$usuario	 = mysqli_fetch_assoc($mysql_query);
		
		if( mysqli_num_rows($mysql_query) == 1 ){
			
			// CRIA O CODIGO PARA RECUPERAÇÃO DE SENHA
			$codigo = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));
			$codigo = hash ( "sha512" , $codigo );
			
			
			// ATUALIZA BANCO DE DADOS
			$query 			= "	UPDATE usuarios SET recuperar = '".$codigo."', recuperar_data = NOW() WHERE email = '".$email."' ";
			$mysql_query 	= mysqli_query($mysqli,$query) or die(mysqli_error($mysqli));
			
			
			// ENVIA E-MAIL PARA USUARIO COM O LINK PARA ALTERAÇÃO DE SENHA
			/*** INÍCIO - DADOS A SEREM ALTERADOS DE ACORDO COM SUAS CONFIGURAÇÕES DE E-MAIL ***/
			
			$enviaFormularioParaNome = "";
			$enviaFormularioParaEmail = $email;
			
			$caixaPostalServidorNome = 'SiSaN | Recuperar senha';
			$caixaPostalServidorEmail = 'no-reply@fonovitta.com.br';
			$caixaPostalServidorSenha = 'SimonGay@*5214';
			
			/*** FIM - DADOS A SEREM ALTERADOS DE ACORDO COM SUAS CONFIGURAÇÕES DE E-MAIL ***/ 
			/* abaixo as veriaveis principais, que devem conter em seu formulario*/
			
			$remetenteNome  = 'SiSaN | Sistema de Saúde nas Nuvens';
			$remetenteEmail = 'no-reply@fonovitta.com.br';
			$assunto  		= 'SiSaN | Recuperar senha';
			
			
			$message = '<html><body>';
			$message .= '<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">';
			$message .= '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">';
			$message .= '<h1 style="font-family: Lato, sans-serif; color:#444; margin-bottom: -0px; ">SiSaN<small><i class="fa fa-cloud"></i></small></h1>';
			$message .= '<div style="border: 1px solid #ccc; text-align: center; ">';
			$message .= '<h3 style="font-family: Lato, sans-serif; color:#222;">Alterar senha</h3>';
			$message .= '<p style="font-family: Lato, sans-serif; color:#222;">Você solicitou a alteração de sua senha no sistema SiSaN. <br> Clique ';
			$link = "http://www.fonovitta.com.br/sisan/recuperar.php?id=".$codigo;
			$message .= '<a href="'.$link.'">aqui</a> para continuar.</p>';
			$message .= '<p style="font-family: Lato, sans-serif; color:#444;"><i>Caso não tenha solicitado a alteração de senha, apenas ignore essa mensagem.</i></p> <br>';
			$message .= '<h2 style="font-family: Lato, sans-serif; color:#444; margin-bottom: -15px; ">SiSaN<small><i class="fa fa-cloud"></i></small></h2>
						 <p style="font-family: Lato, sans-serif; color:#444;"><small> ©2015 Todos direitos reservados. SiSaN - Sistema de Saúde nas Nuvens. Politica de privacidade | Condições de uso </small></p>';
			$message .= '</div>';
			$message .= "</body></html>";
			
			$mensagem = $message;
			
			
			/*********************************** MENSAGEM ************************************/ 
			
			$mensagemConcatenada = $mensagem;
			
			
			/*********************************** A PARTIR DAQUI NAO ALTERAR ************************************/ 
			require_once('../PHPMailer-master/PHPMailerAutoload.php');
			$mail = new PHPMailer();
			 
			$mail->IsSMTP();
			$mail->SMTPAuth  = true;
			$mail->Charset   = 'utf8_decode()';
			$mail->Host  = 'smtp.'.substr(strstr($caixaPostalServidorEmail, '@'), 1);
			$mail->Port  = '587';
			$mail->Username  = $caixaPostalServidorEmail;
			$mail->Password  = $caixaPostalServidorSenha;
			$mail->From  = $caixaPostalServidorEmail;
			$mail->FromName  = utf8_decode($caixaPostalServidorNome);
			$mail->IsHTML(true);
			$mail->Subject  = utf8_decode($assunto);
			$mail->Body  = utf8_decode($mensagemConcatenada);
			
			
			$mail->AddAddress($enviaFormularioParaEmail,utf8_decode($enviaFormularioParaNome));
			
			if(!$mail->Send()){
				// Falha para enviar e-mail
				header("Location: ../login.php?e=93");
			}else{
				// E-mail enviado com sucesso
				header("Location: ../login.php?e=92");
			}
			
			
		}else{
			
			// Email inválido
			header("Location: ../login.php?e=93");
			
		}
		
	}
	

?>