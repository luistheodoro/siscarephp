<?php
	
	// CONECTAR AO BANCO DE DADOS
	include("db_connect.php");
	
	$codigo = $_POST['inputCodigo'];
	$codigo = preg_replace("/[^A-Za-z0-9]+/", "", $codigo);
	
	
	// VERIFICA SE EXISTE O CODIGO NO BANCO DE DADOS
	$query 		 = " SELECT COUNT(*) AS total FROM usuarios WHERE recuperar = '".$codigo."' AND recuperar_data > DATE_SUB(NOW(), INTERVAL 1 DAY) ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total 		 = mysqli_fetch_assoc($mysql_query);
	
	// LIBERA A ALTERA��O DA SENHA
	if( $total['total'] == 1 ){
		
		/* --- CADASTRAR DADOS BASICOS --- */
			
			$senha = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
			if (strlen($senha) != 128) {
				
				// A senha com hash deve ter 128 caracteres.
				// Caso contr�rio, algo muito estranho est� acontecendo
				header("Location: login.php?e=94");
				
			}
			
			
			// VERIFICA SE USUARIO JA EXISTE
			$prep_stmt = "SELECT id FROM usuarios WHERE recuperar = ? LIMIT 1";
			$stmt = $mysqli->prepare($prep_stmt);
			
			// Verifica se query esta correta
			if ($stmt) {
				
				// Adiciona as variaveis
				$stmt->bind_param('s', $codigo);
				
				// Verifica se query funcionou
				if($stmt->execute()){
					
					// Armazena o resultado
					$stmt->store_result();
					
					// Verifica se existe algum cadastro no banco
					if ($stmt->num_rows == 1) {
						
						// Existe um usu�rio com esse codigo
						
						// EDITA A SENHA DO USUARIO NO BANCO DE DADOS BASICO
						if (empty($error_msg)) {
							
							// Cria um salt aleatorio
							$random_salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));
							
							// Cria uma senha com o salt
							$senha = hash('sha512', $senha . $random_salt);
							
							
							// Cadastrar a nova senha no banco de dados 
							$query = "UPDATE usuarios SET senha='$senha', salt='$random_salt', recuperar='', recuperar_data='0000-00-00 00:00:00' WHERE recuperar='$codigo' ";
							$mysql = mysqli_query($mysqli,$query);
							
							header("Location: ../login.php?e=91");
							
						}else{

							// Erro ao processar solicita��o
							header("Location: login.php?e=94");
								
						}
					
					}else{

						// Erro ao processar solicita��o
						header("Location: login.php?e=94");
							
					}
					
				}else{
						
					// Erro ao processar solicita��o
					header("Location: login.php?e=94");
					
				}
				
				// Fecha a consulta
				$stmt->close();
				
			} else {
				
				// Erro ao processar solicita��o
				header("Location: login.php?e=94");
				
			}
			
			
	}else{
		
		// Erro ao processar solicita��o
		header("Location: login.php?e=94");
		
	}
	
	
?>
