﻿<?php

	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "home";
	
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("config/db_connect.php");
	include_once("config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		

		/* FUNCTIONS */
		// Verificação de acessos
		include_once("functions/acesso/usuario.php");
		include_once("functions/erro/erro.php");
		include_once("functions/count/contar.php");
		
		
		/* SQL */
		// Busca os dados do servico
		include_once("sql/servico/buscar.php");
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		// Busca os dados de paciente
		include_once("sql/paciente/buscar.php");
		// Busca os dados de objetivos
		include_once("sql/objetivo/buscar.php");
		
		
		// BUSCA DADOS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA ID DO PERFIL
		$id_perfil = isset($_GET['id']) ? $_GET['id'] : $usuario['geral']['id_usuario'];
		$id_perfil = preg_replace("/[^0-9]+/", "", $id_perfil);
		
		// BUSCA DADOS DO USUARIO
		$perfil = busca_usuario($mysqli,$id_perfil);
		
		
		// NOMEIA AS PRINCIPAIS TABELAS DE BUSCA
		$tabela_paciente 	= $_SESSION['user_Servico']."_paciente_geral";
		$tabela_equipe 		= $_SESSION['user_Servico']."_".$perfil['geral']['equipe'];
		
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <title>SiSaN | Home</title>
	
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
	
    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
    
</head>


<body class="nav-md">

    <div class="container body">
		
        <div class="main_container" style="background:#3C3C3C">
			
			<!-- SIDEBAR MENU -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- TOPBAR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- PAGE CONTENT -->
            <div class="right_col" role="main">

                <!-- top tiles -->
                <div class="row">
					<div class="page-title">
					
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Home</h3>
                        </div>
						
                        <div class="pull-right hidden-xs">
							<i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;&nbsp;&nbsp;
						</div>
						
                    </div>
                </div>
                <!-- /top tiles -->
				
				<br>
				

				<div class="row">
					<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
						<div class="x_panel">
							
							<div class="x_content">

								<div class="col-md-3 col-sm-3 col-xs-12 profile_left">
									
									
									<div class="profile_img hidden-xs">

										<!-- end of image cropping -->
										<div id="crop-avatar">
											<!-- Current avatar -->
											<div class="avatar-view">
												<img src="<?php echo $perfil['geral']['avatar']; ?>" alt="Avatar">
											</div>
										</div>
										<!-- end of image cropping -->

									</div>
									
									<h3><?php echo $usuario['geral']['nome']; ?></h3>

									<ul class="list-unstyled user_data">
										
										<li>
											<i class="fa fa-stethoscope user-profile-icon"></i> &nbsp; <?php echo $usuario['geral']['cargo']; ?>
										</li>
										
										<li>
											<i class="fa fa-phone user-profile-icon"></i> &nbsp; 
											<?php echo $usuario['contato']['telefone']; ?>
										</li>
										
										<li>
											<i class="fa fa-envelope-o user-profile-icon"></i> &nbsp; 
											<?php echo $usuario['contato']['email']; ?>
										</li>
										
									</ul>
									
									
									
									<?php
										if(acesso_editar_perfil($usuario['acesso'],$usuario,$usuario) == TRUE){
									?>
										
										<br>
										
										<a class="btn btn-primary" href="editar_usuario.php?id=<?php echo $usuario['geral']['id_usuario'];?>">
											<i class="fa fa-edit m-right-xs"></i> &nbsp; Editar &nbsp;
										</a>
										
										
									<?php } ?>
									
									
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
									
									<!-- BLOCOS -->
									<div class="row">
										<div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
											<div class="tile-stats">
												
												<div class="icon">
													<i class="fa fa-medkit"></i>
												</div>
												
												<div class="count">
												<?php
												
												$periodo['inicio'] = date('Y-m-01');
												$periodo['fim'] = date('Y-m-t');
												
												$atendimentos = count_atendimento_usuario($mysqli,$periodo,$_SESSION['user_Codigo']);
												
												echo $atendimentos;
												
												?>
												</div>

												<h3>Atendimento(s)</h3>
												<p>Realizados em <?php echo ajusta_mes(date('m')); ?></p>
												
											</div>
										</div>
										<div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
											<div class="tile-stats">
												
												<div class="icon">
													<i class="fa fa-wheelchair"></i>
												</div>
												
												<div class="count">
												<?php
												
												$pacientes = count_pacientes_usuario($mysqli,$usuario['geral']['equipe'],$_SESSION['user_Codigo']);
												echo $pacientes;
												
												?>
												</div>

												<h3>Paciente(s)</h3>
												<p>Em atendimento</p>
												
											</div>
										</div>
										<div class="animated flipInY col-lg-4 col-md-4 col-sm-6 col-xs-12">
											<div class="tile-stats">
												
												<div class="icon">
													<i class="fa fa-dot-circle-o"></i>
												</div>
												
												<div class="count">
												<?php
												
												$objetivos = count_objetivos_usuario($mysqli,$_SESSION['user_Codigo']);
												echo $objetivos;
												
												?>
												</div>

												<h3>Objetivo(s)</h3>
												<p>Em aberto</p>
											</div>
										</div>
									</div>
									
									<div class="row">
										
										<div class="col-md-6 col-sm-7 col-xs-12">
											<div class="x_panel ui-ribbon-container ">
												
												<div class="x_title">
													<h2>Últimas condutas</h2>
													<div class="clearfix"></div>
												</div>
												
												<div class="x_content">
													
													<table class="table table-hover">
													<tbody>
															
															<?php
															
															$query = "	SELECT * FROM 
																			".$_SESSION['user_Servico']."_terapia 
																		WHERE
																			id_usuario='".$_SESSION['user_Codigo']."' AND 
																			conduta!='Mantida' 
																		ORDER BY 
																			data DESC 
																		LIMIT 
																			5
																		";
															
															$i = 1;
															foreach($mysqli->query($query) as $terapia) {
																
																// FORMATA A DATA
																$d = new DateFormatter($terapia['data']);
																$tempo = $d->formattedInterval();
																
																$paciente = busca_paciente_basico($mysqli, $terapia['id_paciente']);
																
																if($i==1){
																	
																	echo "
																		<tr>
																		<td style='border-top:0px'>
																			<a href='paciente.php?id=".$paciente['id']."' >
																			<small>
																				<i class='pull-right'><small>".$tempo."</small></i>
																				".$paciente['nome']."
																			</small>
																				<br>
																			".$terapia['conduta']."
																			</a>
																		</td>
																		</tr>
																	";
																
																}else{
																	
																	echo "
																		<tr>
																		<td>
																			<a href='paciente.php?id=".$paciente['id']."' >
																			<small>
																				<i class='pull-right'><small>".$tempo."</small></i>
																				".$paciente['nome']."
																			</small>
																				<br>
																			".$terapia['conduta']."
																			</a>
																		</td>
																		</tr>
																	";
																
																}
																
																$i++;
																
															} 
															
															?>
															
													</tbody>
													</table>
													
													<?php
													
														
														if($i==1){
															
															echo "<h3>Nenhuma conduta realizada</h3>";
														
														}
														
													?>
													
												</div>
												
											</div>
										</div>
										
										<div class="col-md-6 col-sm-7 col-xs-12">
											<div class="x_panel ui-ribbon-container ">
												
												<div class="x_title">
													<h2>Objetivos</h2>
													<div class="clearfix"></div>
												</div>
												
												<div class="x_content">
													
													<table class="table table-hover">
													<tbody>
															
															<?php
															
															$query = "	SELECT * FROM 
																			".$_SESSION['user_Servico']."_objetivo 
																		WHERE
																			id_responsavel='".$_SESSION['user_Codigo']."' AND 
																			progresso < 100 
																		ORDER BY 
																			previsao_objetivo
																		LIMIT 5
																		";
															
															$i = 1;
															foreach($mysqli->query($query) as $terapia) {
																
																// FORMATA A DATA
																$d = new DateFormatter($terapia['previsao_objetivo']);
																$tempo = $d->formattedInterval();
																
																$paciente = busca_paciente_basico($mysqli, $terapia['id_paciente']);
																
																if($i==1){
																	
																	if( $terapia['previsao_objetivo'] > date("Y-m-d h:i:sa") ){
																		$cor = 'green';
																	}else{
																		$cor = 'red';
																	}
																	
																	echo "
																		<tr>
																		<td style='border-top:0px'>
																			<a href='paciente.php?id=".$paciente['id']."' >
																			<small>
																				<i class='pull-right text-right'>
																					<small>".$tempo."</small><br>
																					<i class='fa fa-circle ".$cor."'></i>
																				</i>
																				".$paciente['nome']."
																			</small>
																				<br>
																			".$terapia['objetivo']."
																			</a>
																		</td>
																		</tr>
																	";
																
																}else{
																	
																	echo "
																		<tr>
																		<td>
																			<a href='paciente.php?id=".$paciente['id']."' >
																			<small>
																				<i class='pull-right text-right'>
																					<small>".$tempo."</small><br>
																					<i class='fa fa-circle ".$cor."'></i>
																				</i>
																				".$paciente['nome']."
																			</small>
																				<br>
																			".$terapia['objetivo']."
																			</a>
																		</td>
																		</tr>
																	";
																
																}
																
																$i++;
																
															} 
															
															?>
															
													</tbody>
													</table>
													
													<?php
													
														
														if($i==1){
															
															echo "<h3>Nenhum objetivo definido</h3>";
														
														}
														
													?>
													
												</div>
												
											</div>
										</div>
										
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				
                <br />
				

                <!-- FOOTER-->
				<?php include_once("include/layout/footer.php"); ?>
				
				
            </div>
            <!-- /page content -->
		
        </div>
		
    </div>
	
	
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/nicescroll/jquery.nicescroll.min.js"></script>

    <script src="assets/js/custom.js"></script>

    
    <!-- /footer content -->
	
</body>

</html>

<?php

}else{
	
	header("Location: login.php");
	
}

?>