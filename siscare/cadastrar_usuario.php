<?php
	
	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "equipes";
	
	
	// Inclua fun��es e conex�es de banco de dados aqui. Ver 3.1. 
	include_once("config/db_connect.php");
	include_once("config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		

		/* FUNCTIONS */
		// Verifica��o de acessos
		include_once("functions/acesso/usuario.php");
		include_once("functions/erro/erro.php");
		
		
		/* SQL */
		// Busca os dados do servico
		include_once("sql/servico/buscar.php");
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		// Busca os dados de paciente
		include_once("sql/paciente/buscar.php");
		// Busca os dados de objetivos
		include_once("sql/objetivo/buscar.php");
		
		
		// BUSCA DADOS DO SERVICO
		$servico = busca_servico($mysqli,$_SESSION['user_Servico']);
		
		// BUSCA DADOS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// VERIFICA ACESSO PARA CADASTRAR USUARIO
		if( acesso_cadastrar_perfil($mysqli,$usuario,$usuario['geral']['equipe']) ){
			$criar = TRUE;
		}else{
			$criar = FALSE;
			header("Location: equipes.php");
		}
		
		$erro = isset($_GET['e']) ? $_GET['e'] : '';
		$erro_texto = msg_erro($erro);
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SiSaN | Equipes</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
    
	<!--Loading maps css-->
	<link type="text/css" rel="stylesheet" href="assets/css/maps.css">
	
	<script type="text/JavaScript" src="assets/js/form/sha512.js"></script> 
    <script type="text/JavaScript" src="assets/js/form/forms.js"></script>
	
	
</head>


<body class="nav-md">

    <div class="container body">
		
        <div class="main_container" style="background:#3C3C3C">
			
			<!-- MENU DA ESQUERDA -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- BARA SUPERIOR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- CONTEÚDO DA PAGINA -->
            <div class="right_col" role="main">

                <!-- TITULO -->
                <div class="row">
					<div class="page-title">
						
						<!-- NOME DA PAGINA -->
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Cadastrar Usuário</h3>
                        </div>
						
						<!-- CAMINHO DA PAGINA -->
                        <div class="pull-right hidden-xs">
							
							<i class="fa fa-home"></i>&nbsp;<a href="home.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							<a href="equipes.php">Equipes</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							Cadastrar usuário &nbsp;&nbsp;
							
                        </div>
						
                    </div>
                </div>
				<br>
				
				<!-- MENSAGEM DE ERRO -->
				<?php
				
					if($erro_texto!= FALSE){
						echo '<div class="alert alert-danger"><strong>Ops!</strong> '.$erro_texto.'</div>';
					}
					
				?>
				
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
							
							<div class="x_content">
                            <form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/usuario/criar.php">
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 controls">
									
									<h3> Dados básicos </h3>
									
									<!-- NOME -->
									<label class="control-label" for="inputNome"> Nome </label>
									<input type="text" id="inputNome" name="inputNome" class="form-control" required />
									<br>
									
									<!-- E-MAIL -->
									<label class="control-label" for="inputEmail"> E-mail </label>
									<input type="text" id="inputEmail" name="inputEmail" class="form-control" required />
									<br>
									
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- USER-ID -->
											<label class="control-label" for="inputUserID"> User-ID <small><i>Utilizado no login</i></small> </label>
											<input type="text" id="inputUserID" name="inputUserID" class="form-control" required />
											<br>
											
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										</div>
									</div>
									
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- NASCIMENTO -->
											<label class="control-label" for="inputNascimento"> Data de nascimento </label>
											<input type="date" id="inputNascimento" name="inputNascimento" class="form-control" />
											<br>
											
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										</div>
									</div>
									
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- SENHA -->
											<label class="control-label" for="inputSenha"> Senha </label>
											<input type="password" id="inputSenha" name="inputSenha" class="form-control" required />
											<br>
											
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										</div>
									</div>
									
									<div class="row">
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- SENHA2 -->
											<label class="control-label" for="inputSenha2"> Confirmar senha </label>
											<input type="password" id="inputSenha2" name="inputSenha2" class="form-control" required />
											<br>
											
										</div>
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										</div>
									</div>
									
									<h3> Dados profissionais </h3>
									
									<!-- SERVIÇO -->
									<input type="hidden" class="form-control" id="inputServico" name="inputServico" value="<?php echo $_SESSION['user_Servico']; ?>" >
										
									<br>
									
									<!-- EQUIPE -->
									<label class="control-label" for="inputEquipe"> Equipe </label>
									<select class="form-control" id="inputEquipe" name="inputEquipe" required>
										<option></option>
										
										<?php
										
										if($usuario['acesso']['cadastrar_perfil'] == 4){
											
											if($servico['geral']['enf']==1){											
												echo "<option value='enf'>Enfermagem</option>";
											}
											if($servico['geral']['fon']==1){
												echo "<option value='fon'>Fonoaudiologia</option>";
											}
											if($servico['geral']['nut']==1){
												echo "<option value='nut'>Nutrição</option>";
											}
											if($servico['geral']['fir']==1){
												echo "<option value='fir'>Fisioterapia Respiratória</option>";
											}
											if($servico['geral']['fim']==1){
												echo "<option value='fim'>Fisioterapia Motora</option>";
											}
											if($servico['geral']['med']==1){
												echo "<option value='med'>Médico</option>";
											}
											
										}else if($usuario['acesso']['cadastrar_perfil'] == 3){
											
											echo "<option value='".$usuario['geral']['equipe']."'>".equipe($usuario['geral']['equipe'],1)."</option>";
											
										}
										
										?>
										
									</select>
									<br>
									
									<!-- CARGO -->
									<label class="control-label" for="inputCargo"> Cargo </label>
									<input type="text" id="inputCargo" name="inputCargo" class="form-control" />
									<br>
									
								</div>
								
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 controls">
								
									<h3> Contato </h3>
									<br>
									
									<!-- ENDEREÇO -->
									<input id="pac-input" class="controls2" type="text" placeholder="Endereço"></input>
									<div id="map" style="height:300px; width:100%"></div>
									<br>
									
									<label class="control-label" for="inputEndereco"> Endereço </label>
									<input type="text" id="inputEndereco" name="inputEndereco" class="form-control" required />
									<br>
									
									<label class="control-label" for="inputSubject"> Bairro </label>
									<input type="text" id="inputBairro" class="form-control" name="inputBairro" required />
									<br>
									
									<label class="control-label" for="inputSubject"> Cidade </label>
									<input type="text" id="inputCidade" name="inputCidade" class="form-control" required />
									<br>
									
									<input type="hidden" id="inputCoordenada" name="inputCoordenada" class="form-control" required />
									<input type="hidden" id="inputLat" name="inputLat" class="form-control" required />
									<input type="hidden" id="inputLng" name="inputLng" class="form-control" required />
									
									
									<!-- TELEFONE -->
									<label class="control-label" for="inputSubject"> Telefone </label>
									<input type="text" id="inputTelefone" name="inputTelefone" class="form-control" />
									<br>
									
									
									<!-- BOTÃO DE ENVIAR -->
									<button type="submit" class="btn btn-primary" onclick="return regformhash(this.form, this.form.inputUserID, this.form.inputEmail, this.form.inputSenha, this.form.inputSenha2);"> &nbsp; Cadastrar &nbsp; </button>
									
								</div>
							
							</form>
							</div>
							
                        </div>
                    </div>

                </div>
                <br>
				
                <!-- FOOTER-->
                <?php include_once("include/layout/footer.php"); ?>
				
            </div>

        </div>

    </div>
	
	
    <script src="assets/js/bootstrap.min.js"></script>
	
    <!-- bootstrap progress js -->
    <script src="assets/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/nicescroll/jquery.nicescroll.min.js"></script>
    
    <script src="assets/js/custom.js"></script>
	
	<!-- GOOGLE MAPS -->
	<script>
		function initMap() {
		  var map = new google.maps.Map(document.getElementById('map'), {
			center: {lat: -22.9098833, lng: -47.06258120000001},
			zoom: 10
		  });
		  var input = /** @type {!HTMLInputElement} */(
			  document.getElementById('pac-input'));

		  var types = document.getElementById('type-selector');
		  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
		  map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);

		  var autocomplete = new google.maps.places.Autocomplete(input);
		  autocomplete.bindTo('bounds', map);

		  var infowindow = new google.maps.InfoWindow();
		  var marker = new google.maps.Marker({
			map: map,
			anchorPoint: new google.maps.Point(0, -29)
		  });

		  autocomplete.addListener('place_changed', function() {
			infowindow.close();
			marker.setVisible(false);
			var place = autocomplete.getPlace();
			if (!place.geometry) {
			  window.alert("Autocomplete's returned place contains no geometry");
			  return;
			}

			// If the place has a geometry, then present it on a map.
			if (place.geometry.viewport) {
			  map.fitBounds(place.geometry.viewport);
			} else {
			  map.setCenter(place.geometry.location);
			  map.setZoom(17);  // Why 17? Because it looks good.
			}
			marker.setIcon(/** @type {google.maps.Icon} */({
			  url: place.icon,
			  size: new google.maps.Size(71, 71),
			  origin: new google.maps.Point(0, 0),
			  anchor: new google.maps.Point(17, 34),
			  scaledSize: new google.maps.Size(35, 35)
			}));
			marker.setPosition(place.geometry.location);
			marker.setVisible(true);

			var endereco = '';
			if (place.address_components) {
			  
			  endereco = [
				(place.address_components[0] && place.address_components[0].short_name || ''),
				(place.address_components[1] && place.address_components[1].short_name || ''),
				(place.address_components[2] && place.address_components[2].short_name || ''),
			  ].join(' ');
			  
			  bairro = [
				(place.address_components[2] && place.address_components[2].short_name || '')
			  ].join(' ');
			  
			  cidade = [
				(place.address_components[3] && place.address_components[3].short_name || '')
			  ].join(' ');
			  
			}
			
			document.getElementById('inputCoordenada').value = place.geometry.location;
			document.getElementById('inputLat').value = place.geometry.location.lat();
			document.getElementById('inputLng').value = place.geometry.location.lng();
			document.getElementById('inputEndereco').value = place.name;
			document.getElementById('inputBairro').value = bairro;
			document.getElementById('inputCidade').value = cidade;
			
			
			infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
			infowindow.open(map, marker);
		  });
			

		  // setupClickListener('changetype-all', []);
		  // setupClickListener('changetype-address', ['address']);
		  // setupClickListener('changetype-establishment', ['establishment']);
		  // setupClickListener('changetype-geocode', ['geocode']);
		}

    </script>
	
    <!-- GOOGLE MAPS -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOiVaKKO0XJltgm3witPr14h3RnhqiLAY&signed_in=true&libraries=places&callback=initMap"
        async defer>
	</script>
	<!-- [END] GOOGLE MAPS -->
	
	
</body>

</html>

<?php

}else{
	
	header("Location: login.php");
	
}

?>