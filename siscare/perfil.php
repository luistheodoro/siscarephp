﻿<?php

	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "equipes";
	
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("config/db_connect.php");
	include_once("config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("functions/acesso/usuario.php");
		include_once("functions/erro/erro.php");
		include_once("functions/count/contar.php");
		
		
		/* SQL */
		// Busca os dados do servico
		include_once("sql/servico/buscar.php");
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		// Busca os dados de paciente
		include_once("sql/paciente/buscar.php");
		// Busca os dados de objetivos
		include_once("sql/objetivo/buscar.php");
		
		
		// BUSCA DADOS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA ID DO PERFIL
		$id_perfil = isset($_GET['id']) ? $_GET['id'] : $usuario['geral']['id_usuario'];
		$id_perfil = preg_replace("/[^0-9]+/", "", $id_perfil);
		
		// BUSCA DADOS DO USUARIO
		$perfil = busca_usuario($mysqli,$id_perfil);
		
		
		// NOMEIA AS PRINCIPAIS TABELAS DE BUSCA
		$tabela_paciente 	= $_SESSION['user_Servico']."_paciente_geral";
		$tabela_equipe 		= $_SESSION['user_Servico']."_".$perfil['geral']['equipe'];
		
		
		if( acesso_ver_perfil($mysqli,$usuario,$perfil) == FALSE ){
			
			// REDIRECIONA PARA PAGINA DO PACIENTE
			header("Location: equipes.php");
			
		}
		
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <title>SiSaN | Equipes</title>
	
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
	<link href="assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">
	
    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
	

    
</head>


<body class="nav-md">

    <div class="container body">
		
        <div class="main_container" style="background:#3C3C3C">
			
			<!-- SIDEBAR MENU -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- TOPBAR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- PAGE CONTENT -->
            <div class="right_col" role="main">

                <!-- top tiles -->
                <div class="row">
					<div class="page-title">
					
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Perfil</h3>
                        </div>
						
                        <div class="pull-right hidden-xs">
								
								<i class="fa fa-home"></i>&nbsp;<a href="home.php">Home</a>&nbsp;&nbsp;
								
								<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
									<a href="equipes.php">Equipes</a>&nbsp;&nbsp;
									
								<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
									<?php echo $perfil['geral']['nome']; ?>&nbsp;&nbsp;
									
						</div>
						
                    </div>
                </div>
                <!-- /top tiles -->
				
				<br>
				
				<div class="row">
					<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
						<div class="x_panel">
							
							<div class="x_content">

								<div class="col-md-3 col-sm-3 col-xs-12 profile_left">
									
									
									<div class="profile_img hidden-xs">

										<!-- end of image cropping -->
										<div id="crop-avatar">
											<!-- Current avatar -->
											<div class="avatar-view" 
												<?php
													if( acesso_editar_perfil($mysqli,$usuario,$perfil) == TRUE ){
												?>
													title="Alterar imagem"
												<?php } ?>
											>
												<img src="<?php echo $perfil['geral']['avatar']; ?>" alt="Avatar">
											</div>
											
											
											<?php
												if( acesso_editar_perfil($mysqli,$usuario,$perfil) == TRUE ){
											?>
											
											<!-- Cropping modal -->
											<div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
												<div class="modal-dialog modal-lg">
													<div class="modal-content">
														
														<form class="avatar-form" action="functions/crop.php" enctype="multipart/form-data" method="post">
															
															<div class="modal-header">
																<button class="close" data-dismiss="modal" type="button">&times;</button>
																<h4 class="modal-title" id="avatar-modal-label">Alterar imagem</h4>
															</div>
															
															<div class="modal-body">
																<div class="avatar-body">

																	<!-- Upload image and data -->
																	<div class="avatar-upload">
																		<input class="avatar-src" name="avatar_src" type="hidden">
																		<input class="avatar-data" name="avatar_data" type="hidden">
																		<label for="avatarInput">Local upload</label>
																		<input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
																	</div>

																	<!-- Crop and preview -->
																	<div class="row">
																		<div class="col-md-9">
																			<div class="avatar-wrapper"></div>
																		</div>
																		<div class="col-md-3">
																			<div class="avatar-preview preview-lg"></div>
																			<div class="avatar-preview preview-md"></div>
																			<div class="avatar-preview preview-sm"></div>
																		</div>
																	</div>

																	<div class="row avatar-btns">
																		<div class="col-md-9">
																			<div class="btn-group hidden-xs">
																				<button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">Rotate Left</button>
																				<button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15deg</button>
																				<button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30deg</button>
																				<button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45deg</button>
																			</div>
																			<div class="btn-group hidden-xs">
																				<button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">Rotate Right</button>
																				<button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15deg</button>
																				<button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30deg</button>
																				<button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45deg</button>
																			</div>
																		</div>
																		<div class="col-md-3">
																			<button class="btn btn-primary btn-block avatar-save" type="submit">Done</button>
																		</div>
																	</div>
																</div>
															</div>
															
															<div class="modal-footer">
															  <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
															</div>
															
														</form>
														
													</div>
												</div>
											</div>
											<!-- /.modal -->
											
											<?php } ?>
											
											<!-- Loading state -->
											<div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
											
										</div>
										<!-- end of image cropping -->

									</div>
									
									<h3><?php echo $perfil['geral']['nome']; ?></h3>

									<ul class="list-unstyled user_data">
										
										<li>
											<i class="fa fa-stethoscope user-profile-icon"></i> &nbsp; <?php echo $perfil['geral']['cargo']; ?>
										</li>
										
										<li>
											<i class="fa fa-phone user-profile-icon"></i> &nbsp; 
											<?php echo $perfil['contato']['telefone']; ?>
										</li>
										
										<li>
											<i class="fa fa-envelope-o user-profile-icon"></i> &nbsp; 
											<?php echo $perfil['contato']['email']; ?>
										</li>
										
									</ul>
									
									
									
									<?php
										if( acesso_editar_perfil($mysqli,$usuario,$perfil) == TRUE ){
									?>
										
										<br>
										
										<a class="btn btn-primary" href="editar_usuario.php?id=<?php echo $perfil['geral']['id_usuario'];?>">
											<i class="fa fa-edit m-right-xs"></i> &nbsp; Editar &nbsp;
										</a>
										
										
									<?php } ?>
									
									
									
									
									<!-- DIVISOR -->
									<div class="col-sm-10 col-sm-offset-1 xs-hidden controls">
										<br>
										<hr/>
									</div>
									
									
									<!-- DADOS GERAIS -->
									<div class="col-sm-12 hidden-xs controls">
										<ul class="list-unstyled user_data">
											
											<li>
												<h4> <span class="count_top"><i class="fa fa-medkit"></i> &nbsp; Atendimentos no mês</span> </h4>
												
												<?php
												
												$periodo['inicio'] = date('Y-m-01');
												$periodo['fim'] = date('Y-m-t');
												
												$atendimentos = count_atendimento_usuario($mysqli,$periodo,$id_perfil);
												
												echo "<h3> <div class='count'>".$atendimentos."</div> </h3>";
												
												?>
												
											</li>
											
											<br>
											
											<li>
												<h4> <span class="count_top"><i class="fa fa-wheelchair"></i> &nbsp; Pacientes</span> </h4>
												
												<?php
												
												$pacientes = count_pacientes_usuario($mysqli,$perfil['geral']['equipe'],$id_perfil);
												
												echo "<h3> <div class='count'>".$pacientes."</div> </h3>";
												
												?>
												
											</li>
											
										</ul>
									</div>
									
									<div class="col-xs-12 hidden-sm hidden-md hidden-lg controls">
										<ul class="list-unstyled user_data">
											<li>
												<h4> <span class="count_top"><i class="fa fa-medkit"></i> &nbsp; Atendimentos no mês: 
												
												<?php
												
												$periodo['inicio'] = date('Y-m-01');
												$periodo['fim'] = date('Y-m-t');
												
												$atendimentos = count_atendimento_usuario($mysqli,$periodo,$id_perfil);
												
												echo $atendimentos;
												
												?>
												</span> </h4>
												
											</li>
											<li>
												<h4> <span class="count_top"><i class="fa fa-wheelchair"></i> &nbsp; Pacientes: 
												
												<?php
												
												$pacientes = count_pacientes_usuario($mysqli,$perfil['geral']['equipe'],$id_perfil);
												
												echo $pacientes;
												
												?>
												</span> </h4>
												
											</li>
										</ul>
										<br>
										
									</div>
									
								
								
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

									
									<!-- GRÁFICOS -->
									<div class="col-md-8 col-sm-8 hidden-xs text-center">
										<h4> Atendimentos </h4>
										<div id="atendimentos_colaborador" style="width:100%; height:200px;"></div>
									</div>
									
									<div class="col-md-4 col-sm-4 hidden-xs text-center">
										<h4> Principais pacientes </h4>
										<div id="graph_donut" style="width:100%; height:200px;"></div>
										<br><br>
									</div>
									<!-- [FIM] GRÁFICOS -->
									
									
									<div class="col-md-12">
									
										<!-- TABELAS -->
										<div class="" role="tabpanel" data-example-id="togglable-tabs">
											
											<!-- ABAS PARA SELEÇÃO -->
											<ul id="myTab" class="nav nav-tabs" role="tablist">
												
												<li role="presentation" class="active">
													<a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Atividades recentes</a>
												</li>
												<li role="presentation" class="">
													<a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Pacientes</a>
												</li>
												<li role="presentation" class="hidden-xs">
													<a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Mensagens</a>
												</li>
												
											</ul>
											
											<!-- CONTEÚDO DAS ABAS -->
											<div id="myTabContent" class="tab-content">
												
												<!-- ATIVIDADES RECENTES -->
												<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
													
													<br>
													
													
													<ul class="messages list-unstyled">
														
														<!-- the input fields that will hold the variables we will use -->  
														<input type='hidden' id='current_page' />  
														<input type='hidden' id='show_per_page' />  
														
														<!-- An empty div which will be populated using jQuery -->  
														<div id='page_navigation'></div>  
														  
														<!-- Content div. The child elements will be used for paginating(they don't have to be all the same,  
															you can use divs, paragraphs, spans, or whatever you like mixed together). '-->  
														<div id='content'>  
														
														<?php
														
														$query = "SELECT * FROM ".$_SESSION['user_Servico']."_terapia WHERE id_usuario='".$perfil['geral']['id_usuario']."' ORDER BY data DESC LIMIT 200";
														$mysql_query = mysqli_query($mysqli,$query);
														
														$i = 1;
														while( $atividade = mysqli_fetch_array($mysql_query) ){
															
															$timestamp = strtotime($atividade['data']);
															$dia = date("d", $timestamp);
															$mes = ajusta_mes_2( date("m", $timestamp) );
															
															$paciente = busca_paciente($mysqli, $atividade['id_paciente']);
															
														?>
															<div>
															
																<div class="hidden-xs">
																<li>
																	
																	<img src="<?php echo $perfil['geral']['avatar']; ?>" class="avatar" alt="Avatar">
																	
																	<div class="message_date">
																		<h3 class="date text-info"><?php echo $dia; ?></h3>
																		<p class="month"><?php echo $mes; ?></p>
																	</div>
																	
																	<div class="message_wrapper">
																		
																		<h4 class="heading">
																			<?php echo "".$atividade['terapia']; ?> 
																			<small><a href="paciente.php?id=<?php echo $paciente['geral']['id']; ?>"><?php echo $paciente['geral']['nome']; ?></a></small> 
																		</h4>
																		
																		<blockquote class="message">
																			<?php echo "<i>".$atividade['terapia_2']."</i> <br> ".$atividade['evolucao']."<br><br> <b>Conduta: </b>".$atividade['conduta']; ?>
																		</blockquote>
																		</br>
																		
																	</div>
																	
																</li>
																</div>
																
																<div class="hidden-md hidden-sm hidden-lg">
																<li>
																	
																	<div class="message_date">
																		<h3 class="date text-info"><?php echo $dia; ?></h3>
																		<p class="month"><?php echo $mes; ?></p>
																	</div>
																	
																	<div class="message_wrapper_2">
																		
																		<h4 class="heading">
																			<?php echo "".$atividade['terapia']; ?> 
																			<small><?php echo $paciente['geral']['nome']; ?></small> 
																		</h4>
																		
																		<blockquote class="message">
																			<?php echo "<i>".$atividade['terapia_2']."</i> <br> ".$atividade['evolucao']."<br><br> <b>Conduta: </b>".$atividade['conduta']; ?>
																		</blockquote>
																		</br>
																		
																	</div>
																	
																</li>
																</div>
																
															</div>
															
														<?php
															$i++;
														}
														
														?>
														
														</div>  
												  
													</ul>
													<!-- end recent activity -->
													
													<?php 
														
														if($i==1){
															
															//echo "<h4><i>Nenhuma atividade recente.</i></h4>";
															
															echo '
																<br><br>
																<div class="col-lg-8 col-md-8 col-sm-12 col-lg-offset-2 col-md-offset-2">
																	<div class="alert alert-warning alert-dismissible fade in" role="alert">
																		<strong>Ops!</strong> Esse usuário não possui nenhuma atividade recente!
																	</div>
																</div>';
															
														}
													?>

												</div>
												<!-- [FIM] ATIVIDADES RECENTES -->
												
												
												<!-- PACIENTES -->
												<div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
													
													<br>
													
													<!-- start user projects -->
													<table id="tabela_pacientes" class="data table table-striped no-margin" id="tabela-pacientes">
														<thead>
															<tr>
																<th class="hidden-xs">#</th>
																<th>Paciente</th>
																<th>Objetivo</th>
															</tr>
														</thead>
														<tbody>
														
															<?php
															
															$query = "SELECT * FROM ".$tabela_paciente." WHERE ".$perfil['geral']['equipe']."='1' AND  ";
															
															$query = " 	SELECT 
																			*
																		FROM 
																			".$tabela_paciente." 
																			JOIN 
																			".$tabela_equipe."
																			ON 
																			".$tabela_paciente.".id = ".$tabela_equipe.".id_paciente 
																		WHERE 
																			".$perfil['geral']['equipe']."='1' AND
																			".$tabela_equipe.".responsavel = '".$perfil['geral']['id_usuario']."' 
																		ORDER BY 
																			".$tabela_paciente.".nome
																		";
															
															$mysql_query = mysqli_query($mysqli, $query);
															$i=1;
															
															if(gettype($mysql_query) != 'boolean'){
																
																while( $paciente = mysqli_fetch_array($mysql_query) ){
																	
																	$objetivo = busca_objetivo($mysqli,$paciente['id']);
																	
																	if($objetivo){
																		
																		if( $objetivo['previsao_objetivo'] > date("Y-m-d") ){
																			$status = "<i class='fa fa-circle text-success'></i>";
																		}else{
																			$status = "<i class='fa fa-circle text-danger'></i>";
																		}
																	
																	}else{
																	
																		$status = "<small>Nenhum objetivo</small>";
																		
																	}
																	
																	// FORMATA A DATA
																	$d = new DateFormatter($paciente['data_inicio']);
																	$tempo = $d->formattedInterval();
																	
																?>
																
																	<tr onclick="location.href = 'paciente.php?id=<?php echo $paciente['id_paciente']; ?>' " style='cursor: pointer;' >
																		<td class="hidden-xs"><?php echo $i; ?></td>
																		<td><?php echo $paciente['nome']; ?></td>
																		<td><?php echo $objetivo['objetivo'] ." ".$status; ?></td>
																	</tr>
																		
																<?php
																
																	$i++;
																	
																}
																
															}
															
															?>
																
															
															
														</tbody>
													</table>
													<!-- end user projects -->
													
												
												</div>
												<!-- [FIM] PACIENTES -->
												
												
												<div role="tabpanel" class="tab-pane fade hidden-xs" id="tab_content3" aria-labelledby="profile-tab">
													
													<br><br>
													<div class="col-lg-8 col-md-8 col-sm-12 col-lg-offset-2 col-md-offset-2">
														<div class="alert alert-warning alert-dismissible fade in" role="alert">
															<strong>Holy guacamole!</strong> Essa parte não esta pronta ainda!
														</div>
													</div>
													
												</div>
											</div>
										</div>
										
									</div>
										
								</div>
							</div>
						</div>
					</div>
				</div>
				
                <br />
				

                <!-- FOOTER-->
				<?php include_once("include/layout/footer.php"); ?>
				
				
            </div>
            <!-- /page content -->
		
        </div>
		
    </div>
	
	
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/nicescroll/jquery.nicescroll.min.js"></script>

    <script src="assets/js/custom.js"></script>

    
	<!-- image cropping -->
    <script src="assets/js/cropping/cropper.min.js"></script>
    <script src="assets/js/cropping/main.js"></script>
	
	
	<!-- PAGINATION -->
    <script src="assets/js/pagination.js"></script>
	
	
	<!-- GRÁFICO -->
	<script src="assets/js/moris/raphael-min.js"></script>
    <script src="assets/js/moris/morris.js"></script>
	
		<?php
			
			
			
			// DADOS PARA GRAFICO DE BARRAS - NUMERO DE ATENDIMENTOS POR MES
			for ($i=0;$i<=12;$i++){
				
				$query = "SELECT COUNT(*) as total FROM ".$_SESSION['user_Servico']."_terapia WHERE 
								id_usuario 	= '".$perfil['geral']['id_usuario']."' AND 
								MONTH(data) = '".$i."' AND 
								YEAR(data) 	= YEAR( CURRENT_DATE() ) ";
								
				$mysql_query = mysqli_query($mysqli,$query);
				$num = mysqli_fetch_assoc($mysql_query);
				
				if($num['total'] < 1){
					$data[$i] = 0;
				}else{
					$data[$i] = $num['total'];
				}
				
			}
			
			// DADOS PARA GRAFICO DE PIZZA - DISTRIBUIÇÃO DE DIAGNOSTICOS
			$i = 1;
			$outros = 0;
			$qnt_doenca = 7;
			
			$query = " 	SELECT 
							diagnostico_1 as diagnostico_1,
							COUNT(*) as total
						FROM 
							".$tabela_paciente.", ".$tabela_equipe." 
						WHERE 
							".$tabela_paciente.".id = ".$tabela_equipe.".id_paciente AND
							".$tabela_equipe.".responsavel = '".$perfil['geral']['id_usuario']."' 
						GROUP BY 
							diagnostico_1 
						ORDER BY
							total DESC";
							
			
			
			if(gettype($mysqli->query($query)) == 'boolean'){
				
				$doenca[1][1] = '';
				$doenca[1][2] = 0;
				
			}else{
				
				foreach($mysqli->query($query) as $row) {
				
					if($i<=$qnt_doenca){
						
						if( $row['total'] < 1 ){
							$doenca[$i][1] = '';
							$doenca[$i][2] = 0;
						}else{
							$doenca[$i][1] = $row['diagnostico_1'];
							$doenca[$i][2] = $row['total'];
						}
					
					}else{
						$outros = $outros + $row['total'];
					}
					
					$i++;
					
				}
				
				$doenca[$qnt_doenca + 1][1] = 'Outros';
				$doenca[$qnt_doenca + 1][2] = $outros;
				
			}
			
		?>
		
	<script>
		
		$(function () {
			
            var day_data = [
                {
                    "period": "Jan",
                    "Atendimentos": <?php echo $data[1]; ?>
                },
                {
                    "period": "Fev",
                    "Atendimentos": <?php echo $data[2]; ?>
                },
                {
                    "period": "Mar",
                    "Atendimentos": <?php echo $data[3]; ?>
                },
                {
                    "period": "Abr",
                    "Atendimentos": <?php echo $data[4]; ?>
                },
                {
                    "period": "Mai",
                    "Atendimentos": <?php echo $data[5]; ?>
                },
                {
                    "period": "Jun",
                    "Atendimentos": <?php echo $data[6]; ?>
                },
                {
                    "period": "Jul",
                    "Atendimentos": <?php echo $data[7]; ?>
                },
                {
                    "period": "Ago",
                    "Atendimentos": <?php echo $data[8]; ?>
                },
                {
                    "period": "Set",
                    "Atendimentos": <?php echo $data[9]; ?>
                },
                {
                    "period": "Out",
                    "Atendimentos": <?php echo $data[10]; ?>
                },
                {
                    "period": "Nov",
                    "Atendimentos": <?php echo $data[11]; ?>
                },
                {
                    "period": "Dez",
                    "Atendimentos": <?php echo $data[12]; ?>
                }
			];
			
            Morris.Bar({
                element: 'atendimentos_colaborador',
                data: day_data,
                xkey: 'period',
				xLabelMargin: 10,
				behaveLikeLine: true,
				resize: true,
                hideHover: 'auto',
                barColors: ['#286090', '#34495E', '#ACADAC', '#3498DB'],
                ykeys: ['Atendimentos', 'sorned']
            });
			
        });
		
	</script>
	
	<script>
		
		Morris.Donut({
			element: 'graph_donut',
			data: [
			
			<?php
			
				for ($j=1;$j<=$qnt_doenca + 1;$j++){
					
					echo "{label: ' ".$doenca[$j][1]." ', value: ".$doenca[$j][2]." }";
					
					if( $j < $qnt_doenca + 1 ){
						echo ",";
					}
					
				}
			?>
			
			],
			colors: ['#26B99A', '#34495E', '#ACADAC', '#3498DB'],
			formatter: function (y) {
				return y + "%"
			}
		});
    </script>
	
	
	<!-- TABELA DINAMICA -->
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

	<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />
	
	
	<script>
	
		$(document).ready(function() {
			
			$('#tabela_pacientes').DataTable( {
				
				"paging":   		true,
				"bLengthChange": 	true,
				"pageLength": 		20,
				"ordering": 		true,
				"info":     		true,
				"sPaginationType": 	"simple_numbers",
				"order": 			[[ 1, "asc" ]],
				"columnDefs": 		[ { "targets": [0], "orderable": false } ],
				stateSave: 			true,
				
				"dom": '<"left"f><l>rt<"bottom"pi><"clear">',
				
				"language": {
					"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
				},
				
				buttons: [ ]
				
			} );

		} );
		
	</script>
    
    <!-- /footer content -->
	
</body>

</html>

<?php

mysqli_close($mysqli);

}else{
	
	header("Location: login.php");
	
}

?>