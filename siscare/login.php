<?php

	$erro = isset($_GET['e']) ? $_GET['e'] : NULL;
	
	if($erro == 91){
		$sucesso = "Senha alterada com sucesso!";
	}
	
	if($erro == 92){
		$sucesso = "Solicitação enviada! Favor consultar seu e-mail para continuar com a alteração de senha.";
	}
	
	if($erro == 93){
		$alerta = "Não encontramos o e-mail informado, por favor tente novamente.";
	}
	if($erro == 94){
		$alerta = "Houve um erro ao tentar alterar sua senha, por favor inicie o processo novamente.";
	}
	
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SISCARE | Login</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
	
	<script type="text/JavaScript" src="assets/js/form/sha512.js"></script> 
    <script type="text/JavaScript" src="assets/js/form/forms.js"></script>
	
</head>

<body style="background:#F7F7F7;">
    
    <div class="">
        
		<a class="hiddenanchor" id="toregister"></a>
        <a class="hiddenanchor" id="tologin"></a>

		
		<?php
				
		$data_limite = 
		
		$date1 	= date("Y-m-d");
		// os tres primeiros são horas, os tres ultimos são: mes-dia-ano
		$d		= mktime(0, 0, 0, 7, 19, 2016);
		$date2 	= date("Y-m-d", $d);
		$diff 	= strtotime($date2) - strtotime($date1);
		
		if ($diff > 0){
		
		?>
			
			<script type="text/javascript">
				var permanotice, tooltip, _alert;
				$(function () {
					new PNotify({
						title: "Temos novidade!",
						type: "dark",
						text: "Agora você pode escolher a data da terapia.",
						nonblock: {
							nonblock: true
						},
						before_close: function (PNotify) {
							// You can access the notice's options with this. It is read only.
							//PNotify.options.text;

							// You can change the notice's options after the timer like this:
							PNotify.update({
								title: PNotify.options.title + " - Enjoy your Stay",
								before_close: null
							});
							PNotify.queueRemove();
							return false;
						}
					});

				});
			</script>
			
		<?php } ?>
				
		
        <div id="wrapper">
            
			<div id="login" class="animate form">
				
				<?php
				if (!empty($sucesso)) {
					echo "<div class='alert alert-success'><strong>Sucesso!</strong> ".$sucesso."</div>";
				}
				
				if (!empty($alerta)) {
					echo "<div class='alert alert-warning'><strong>Ops!</strong> ".$alerta."</div>";
				}
				?>
				
				
				
				
                <section class="login_content">
                    <form name="login_form" data-parsley-validate class="form-horizontal" method="post" action="config/process_login.php">
                        
						<h1> Entrar </h1>
                        
						<div>
                            <input type="text" 		id="UserID" 	name="UserID" 	class="form-control" placeholder="User-ID / E-mail" required />
                        </div>
						
                        <div>
                            <input type="password" 	id="senha" 		name="senha" 	class="form-control" placeholder="Senha" required />
                        </div>
						
                        <div>
							<button type="button" class="btn btn-primary"  onclick="formhash(this.form, this.form.senha);">Entrar</button>
                            <a href="#toregister" class="to_register" href="#">Esqueci minha senha</a>
                        </div>
						
						<br /><br />
						
                        <div class="clearfix"></div>
						
                        <div class="separator">
							
                            <div class="clearfix"></div>
                            <br />
							
                            <div>
                                <h1><i class="fa fa-cloud round_icon"></i> <i>SIS</i><b>CARE</b></h1>
                                <p>©2015 All Rights Reserved. <i>SIS</i><b>CARE</b> - Sistema de Saúde nas Nuvens. Privacy and Terms</p>
                            </div>
							
                        </div>
						
                    </form>
					
                </section>
				
            </div>
			
			<div id="register" class="animate form">
			
                <section class="login_content">
                    <form name="recuperar_form" data-parsley-validate class="form-horizontal" method="post" action="config/recuperar_senha.php">
					
                        <h1> Recuperar senha </h1>
						
						<br /><br /><br />
						
                        <div>
                            <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="Email" required="" />
                        </div>
                        <div>
                            <button type="submit" class="btn btn-primary">Enviar</button>
							<a href="#tologin" class="to_register" href="#">Fazer login</a>
                        </div>
						
                        <br />
						
                        <div class="clearfix"></div>
						<br />
                        <div class="separator">
							
                            <div class="clearfix"></div>
                            <br />
							
                            <div>
                                <h1><i class="fa fa-cloud round_icon"></i> <i>SIS</i><b>CARE</b></h1>
                                <p>©2015 All Rights Reserved. <i>SIS</i><b>CARE</b> - Sistema de Saúde nas Nuvens. Privacy and Terms</p>
                            </div>
							
                        </div>
						
                    </form>
                    <!-- form -->
                </section>
                <!-- content -->
				
            </div>
            
        </div>
    </div>
	
	<div id="custom_notifications" class="custom-notifications dsp_none">
        <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
        </ul>
        <div class="clearfix"></div>
        <div id="notif-group" class="tabbed_notifications"></div>
    </div>
	
	<!-- PNotify -->
    <script type="text/javascript" src="assets/js/notify/pnotify.core.js"></script>
    <script type="text/javascript" src="assets/js/notify/pnotify.buttons.js"></script>
    <script type="text/javascript" src="assets/js/notify/pnotify.nonblock.js"></script>
	
</body>

</html>