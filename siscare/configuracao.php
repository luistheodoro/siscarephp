﻿<?php

	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "config";
	
	// ZERAR MENSAGEM DE ERRO
	$error_msg = "";
	
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("config/db_connect.php");
	include_once("config/functions.php");
	
	
	sec_session_start();  
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("functions/acesso/usuario.php");
		include_once("functions/erro/erro.php");
		
		
		/* SQL */
		// Busca os dados do servico
		include_once("sql/servico/buscar.php");
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		
		
		// BUSCA DADOS DO SERVICO
		$servico = busca_servico($mysqli,$_SESSION['user_Servico']);
		
		// BUSCA DADOS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
	
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	
    <title>SiSaN | Configurações</title>
	
	
    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">
	
    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
    
</head>


<body class="nav-md">

    <div class="container body">
		
        <div class="main_container" style="background:#3C3C3C">
			
			<!-- SIDEBAR MENU -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- TOPBAR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- PAGE CONTENT -->
            <div class="right_col" role="main">

                <!-- top tiles -->
                <div class="row">
					<div class="page-title">
					
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Configurações</h3>
                        </div>
						
                        <div class="pull-right hidden-xs">
								
								<i class="fa fa-home"></i>&nbsp;<a href="home.php">Home</a>&nbsp;&nbsp;
								
								<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
									Configurações &nbsp;&nbsp;
									
						</div>
						
                    </div>
                </div>
                <!-- /top tiles -->
				
				<br>
				

				<div class="row">
					<div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1">
						<div class="x_panel">
							
							<div class="x_content">

								<div class="col-md-3 col-sm-3 col-xs-12 profile_left">
									
									
									<div class="profile_img hidden-xs">

										<!-- end of image cropping -->
										<div id="crop-avatar">
											<!-- Current avatar -->
											<div class="avatar-view" 
												<?php
													if( acesso_editar_perfil($mysqli,$usuario,$usuario) == TRUE ){
												?>
													title="Alterar imagem"
												<?php } ?>
											>
												<img src="<?php echo $usuario['geral']['imagem']; ?>" alt="Avatar">
											</div>
											
											
											<?php
												if( acesso_editar_perfil($mysqli,$usuario,$usuario) == TRUE ){
											?>
											
											<!-- Cropping modal -->
											<div class="modal fade" id="avatar-modal" aria-hidden="true" aria-labelledby="avatar-modal-label" role="dialog" tabindex="-1">
												<div class="modal-dialog modal-lg">
													<div class="modal-content">
														
														<form class="avatar-form" action="includes/crop.php" enctype="multipart/form-data" method="post">
															
															<div class="modal-header">
																<button class="close" data-dismiss="modal" type="button">&times;</button>
																<h4 class="modal-title" id="avatar-modal-label">Alterar imagem</h4>
															</div>
															
															<div class="modal-body">
																<div class="avatar-body">

																	<!-- Upload image and data -->
																	<div class="avatar-upload">
																		<input class="avatar-src" name="avatar_src" type="hidden">
																		<input class="avatar-data" name="avatar_data" type="hidden">
																		<label for="avatarInput">Local upload</label>
																		<input class="avatar-input" id="avatarInput" name="avatar_file" type="file">
																	</div>

																	<!-- Crop and preview -->
																	<div class="row">
																		<div class="col-md-9">
																			<div class="avatar-wrapper"></div>
																		</div>
																		<div class="col-md-3">
																			<div class="avatar-preview preview-lg"></div>
																			<div class="avatar-preview preview-md"></div>
																			<div class="avatar-preview preview-sm"></div>
																		</div>
																	</div>

																	<div class="row avatar-btns">
																		<div class="col-md-9">
																			<div class="btn-group hidden-xs">
																				<button class="btn btn-primary" data-method="rotate" data-option="-90" type="button" title="Rotate -90 degrees">Rotate Left</button>
																				<button class="btn btn-primary" data-method="rotate" data-option="-15" type="button">-15deg</button>
																				<button class="btn btn-primary" data-method="rotate" data-option="-30" type="button">-30deg</button>
																				<button class="btn btn-primary" data-method="rotate" data-option="-45" type="button">-45deg</button>
																			</div>
																			<div class="btn-group hidden-xs">
																				<button class="btn btn-primary" data-method="rotate" data-option="90" type="button" title="Rotate 90 degrees">Rotate Right</button>
																				<button class="btn btn-primary" data-method="rotate" data-option="15" type="button">15deg</button>
																				<button class="btn btn-primary" data-method="rotate" data-option="30" type="button">30deg</button>
																				<button class="btn btn-primary" data-method="rotate" data-option="45" type="button">45deg</button>
																			</div>
																		</div>
																		<div class="col-md-3">
																			<button class="btn btn-primary btn-block avatar-save" type="submit">Done</button>
																		</div>
																	</div>
																</div>
															</div>
															
															<div class="modal-footer">
															  <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
															</div>
															
														</form>
														
													</div>
												</div>
											</div>
											<!-- /.modal -->
											
											<?php } ?>
												

											<!-- Loading state -->
											<div class="loading" aria-label="Loading" role="img" tabindex="-1"></div>
										</div>
										<!-- end of image cropping -->

									</div>
									
									<h3><?php echo $usuario['geral']['nome']; ?></h3>

									<ul class="list-unstyled user_data">
										
										<li>
											<i class="fa fa-stethoscope user-profile-icon"></i> &nbsp; <?php echo $usuario['geral']['cargo']; ?>
										</li>
										
										<li>
											<i class="fa fa-phone user-profile-icon"></i> &nbsp; 
											<?php echo $usuario['contato']['telefone']; ?>
										</li>
										
										<li>
											<i class="fa fa-envelope-o user-profile-icon"></i> &nbsp; 
											<?php echo $usuario['contato']['email']; ?>
										</li>
										
									</ul>
									
									
									
									<?php
										if( acesso_editar_perfil($mysqli,$usuario,$usuario) == TRUE ){
									?>
										
										<br>
										
										<a class="btn btn-primary" href="editar_usuario.php?id=<?php echo $usuario['geral']['id'];?>">
											<i class="fa fa-edit m-right-xs"></i> &nbsp; Editar &nbsp;
										</a>
										
										
									<?php } ?>
									
									
								</div>
								<div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
									
									
									<div class="row">
									
										<div class="col-md-12">
										
											<div class="x_panel">
												
												<div class="x_title">
													<h4>Módulos de Serviço - <b><?php echo $servico['geral']['nome_servico']; ?></b></h4>
												</div>
												
												<div class="x_content">
													
													<form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/servico/editar.php">
													
														<?php
										
															if($usuario['acesso']['cadastrar_perfil'] == 4){
																
																if($servico['geral']['enf']==1){
																	echo '<input type="checkbox" name="enf" id="enf" checked> Enfermagem <br>';
																}else{
																	echo '<input type="checkbox" name="enf" id="enf"> Enfermagem <br>';
																}
																
																if($servico['geral']['fon']==1){
																	echo '<input type="checkbox" name="fon" id="fon" checked> Fonoaudiologia <br>';
																}else{
																	echo '<input type="checkbox" name="fon" id="fon"> Fonoaudiologia <br>';
																}
																
																if($servico['geral']['nut']==1){
																	echo '<input type="checkbox" name="nut" id="nut" checked> Nuticionista <br>';
																}else{
																	echo '<input type="checkbox" name="nut" id="nut"> Nuticionista <br>';
																}
																
																if($servico['geral']['fir']==1){
																	echo '<input type="checkbox" name="fir" id="fir" checked> Fisioterapia Respiratória <br>';
																}else{
																	echo '<input type="checkbox" name="fir" id="fir"> Fisioterapia Respiratória <br>';
																}
																
																if($servico['geral']['fim']==1){
																	echo '<input type="checkbox" name="fim" id="fim" checked> Fisioterapia Motora <br>';
																}else{
																	echo '<input type="checkbox" name="fim" id="fim"> Fisioterapia Motora <br>';
																}
																
																if($servico['geral']['med']==1){
																	echo '<input type="checkbox" name="med" id="med" checked> Equipe Médica <br>';
																}else{
																	echo '<input type="checkbox" name="med" id="med"> Equipe Médica <br>';
																}
																
															}
															
														?>
														
														<br>
														<button class="btn btn-primary" type="submit">Salvar</button>
													
													</form>
													
													
												</div>
												
											</div>
										</div>
									
									</div>
									
									
									
									<h3>
										Acessos &nbsp; <i><small> <?php echo $_SESSION['user_Acesso']; ?></small></i>
									</h3>
									
									<div class="col-md-10 col-md-offset-1">
										
										<h4> <b> Pacientes </b> </h4>
										<table class="table table-condensed table-responsive">
											<tr>
												<td>
													Visualizar perfil do paciente
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['ver_paciente']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Cadastrar novo paciente
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['cadastrar_paciente']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Editar perfil do paciente
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['editar_paciente']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Avaliar paciente
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['avaliar_paciente']){
															case 0:
																echo 'Nenhum';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Solicitar avaliação
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['solicitar_avaliacao']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Criar terapia
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['criar_terapia']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Gerenciar risco do paciente
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['gerenciar_risco']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Editar especificidades
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['editar_espec']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Ver objetivos
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['ver_objetivo']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Editar objetivos
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['editar_objetivo']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Criar objetivos
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['criar_objetivo']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Finalizar objetivos
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['concluir_objetivo']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Alterar responsável
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['editar_responsavel']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Alta terapêutica
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['alta']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Pacientes que eu atendo';
																break;
															case 2:
																echo 'Pacientes atendidos pelos colaboradores que sou responsável';
																break;
															case 3:
																echo 'Pacientes atendidos pela minha equipe';
																break;
															case 4:
																echo 'Todos os paciente';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											
										</table>
										
										<h4> <b> Colaboradores </b> </h4>
										<table class="table table-condensed table-responsive">
											
											<tr>
												<td>
													Cadastrar colaborador
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['cadastrar_perfil']){
															case 0:
																echo 'Nenhum';
																break;
															case 3:
																echo 'Colaboradores na minha equipe';
																break;
															case 4:
																echo 'Colaboradores em todas equipes';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Editar perfil de colaborador
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['editar_perfil']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Meu perfil';
																break;
															case 2:
																echo 'Colaboradores que sou responsável';
																break;
															case 3:
																echo 'Colaboradores da minha equipe';
																break;
															case 4:
																echo 'Todos os colaboradores';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Visualizar perfil de colaborador
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['ver_perfil']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Meu perfil';
																break;
															case 2:
																echo 'Colaboradores que sou responsável';
																break;
															case 3:
																echo 'Colaboradores da minha equipe';
																break;
															case 4:
																echo 'Todos os colaboradores';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											<tr>
												<td>
													Ver atendimentos
												</td>
												<td>
													<small>
													<?php
														
														switch($usuario['acesso']['ver_atendimentos']){
															case 0:
																echo 'Nenhum';
																break;
															case 1:
																echo 'Meu perfil';
																break;
															case 2:
																echo 'Colaboradores que sou responsável';
																break;
															case 3:
																echo 'Colaboradores da minha equipe';
																break;
															case 4:
																echo 'Todos os colaboradores';
																break;
														}
														
													?>
													</small>
												</td>
											</tr>
											
										</table>
										
										
									</div>
									
									<div class="col-md-3">
										
										
										
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
				
                <br />
				

                <!-- FOOTER-->
				<?php include_once("include/layout/footer.php"); ?>
				
				
            </div>
            <!-- /page content -->
		
        </div>
		
    </div>
	
	
    <script src="assets/js/bootstrap.min.js"></script>

    <!-- chart js -->
    <script src="assets/js/nicescroll/jquery.nicescroll.min.js"></script>
    <script src="assets/js/custom.js"></script>

    
	<!-- image cropping -->
    <script src="assets/js/cropping/cropper.min.js"></script>
    <script src="assets/js/cropping/main.js"></script>
	
	
    <!-- /footer content -->
	
</body>

</html>

<?php

}else{
	
	header("Location: login.php");
	
}

?>