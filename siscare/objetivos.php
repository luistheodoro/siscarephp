﻿<?php
	
	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "objetivos";
	
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("config/db_connect.php");
	include_once("config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("functions/acesso/paciente.php");
		include_once("functions/erro/erro.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		// Busca os dados de paciente
		include_once("sql/paciente/buscar.php");
		
		
		// BUSCA DADOS GERAIS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA MENSAGENS DE ERRO
		$erro = isset($_GET['e']) ? $_GET['e'] : '';
		$erro_texto = msg_erro($erro);
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SiSaN | Objetivos</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
    <link href="assets/css/icheck/flat/green.css" rel="stylesheet" />
	<link href="assets/css/datatables/tools/css/dataTables.tableTools.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="assets/css/maps/jquery-jvectormap-2.0.1.css" />
    
</head>


<body class="nav-md">

    <div class="container body">
		
        <div class="main_container" style="background:#3C3C3C">
			
			<!-- MENU DA ESQUERDA -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- BARA SUPERIOR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- CONTEÚDO DA PAGINA -->
            <div class="right_col" role="main">

                <!-- TITULO -->
                <div class="row">
					<div class="page-title">
						
						<!-- NOME DA PAGINA -->
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Objetivos</h3>
                        </div>
						
						<!-- CAMINHO DA PAGINA -->
                        <div class="pull-right hidden-xs">
								<i class="fa fa-home"></i>&nbsp;<a href="home.php">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
								Objetivos&nbsp;&nbsp;
                        </div>
						
                    </div>
                </div>
				<br>
				
				
				<!-- MENSAGEM DE ERRO -->
				<?php
				
					if($erro_texto!= FALSE){
						echo '<div class="alert alert-danger"><strong>Ops!</strong> '.$erro_texto.'</div>';
					}
					
				?>
				
				
                <div class="row">
					
					<div class="col-md-12">
						<div class="x_panel">
							
							<div class="x_title">
								
								<h2><?php echo equipe($usuario['geral']['equipe'],2); ?></h2>
								
								<!-- ACESSO PARA ADICIONAR OBJETIVO -->
								<?php if( acesso_criar_objetivo_1($mysqli,$usuario) == TRUE ){ ?>
								
									<a class="pull-right label label-primary hidden-xs" data-toggle="modal" data-target=".modal-criar-objetivo">
										<h4> &nbsp; <i class="fa fa-plus"></i> Adicionar objetivo &nbsp; </h4>
									</a>
									
									<a class="pull-right label label-primary hidden-sm hidden-md hidden-lg" data-toggle="modal" data-target=".modal-criar-objetivo">
										<h4> &nbsp; <i class="fa fa-plus"></i> &nbsp; </h4>
									</a>
									
								<?php } ?>
								
								<div class="clearfix"></div>
								
							</div>
							
							<div class="x_content">
								
								<?php
								
								$query = "SELECT * FROM ".$_SESSION['user_Servico']."_objetivo WHERE equipe = '".$usuario['geral']['equipe']."' AND progresso < 100 ORDER BY previsao_objetivo";
								$mysql_query = mysqli_query($mysqli,$query);
								
								if( mysqli_num_rows($mysql_query)>0 ){
								
								?>
								
								<!--
								<table class="table table-striped projects">
								-->
								<table id="tabela_objetivos" class="table table-hover responsive table-condensed">
									
									<thead>
										<tr>
											<th class="hidden-xs">#</th>
											<th>Paciente</th>
											<th class="hidden-xs">Responsável</th>
											<th>Objetivo</th>
											<th class="hidden-xs">Progresso</th>
											<th class="hidden-xs">Status</th>
											<th class="hidden-sm hidden-md hidden-lg"><span>Prazo </span></th>
											<th class="hidden-xs"><span>Prazo </span></th>
											<th></th>
										</tr>
									</thead>
									
									<tbody>
										
										<?php
										
										
										$i = 1;
										while( $objetivo = mysqli_fetch_array($mysql_query) ){
											
											// DADOS DO PACIENTE
											$paciente = busca_paciente($mysqli, $objetivo['id_paciente']);
											
											// FORMATA A DATA
											$d = new DateFormatter($objetivo['inicio_objetivo']);
											$tempo = $d->formattedInterval();
											
											// DADOS DO RESPONSÁVEL
											$responsavel = busca_usuario($mysqli,$objetivo['id_responsavel']);
											
										?>
										
										<tr>
											
											<td class="hidden-xs"><?php echo $i; ?></td>
											
											<td>
												<a href="paciente.php?id=<?php echo $paciente['geral']['id']; ?>"><?php echo $paciente['geral']['nome']; ?></a>
												<br />
												<small class="hidden-xs"><?php echo $tempo; ?></small>
											</td>
											
											<td class="hidden-xs">
												<a href="perfil.php?id=<?php echo $responsavel['geral']['id_usuario']; ?>"> 
													<?php echo $responsavel['geral']['nome']; ?> 
													<br>
													<img src="<?php echo $responsavel['geral']['avatar']; ?>" class="avatar" alt="Avatar">
												</a>
											</td>
											
											<td>
												<p> <?php echo $objetivo['objetivo']; ?> </p>
											</td>
											
											<td class="project_progress hidden-xs">
												<div class="progress progress_sm hidden-xs">
													<div class="progress-bar bg-blue" role="progressbar" data-transitiongoal="<?php echo $objetivo['progresso']; ?>"></div>
												</div>
												<small><?php echo $objetivo['progresso']; ?>%</small>
											</td>
											
											<td class="hidden-xs">
												<?php echo $objetivo['status']; ?>
											</td>
											
											<td class="hidden-sm hidden-md hidden-lg">
											<?php
											
												// FORMATA A DATA
												$d = new DateFormatter($objetivo['previsao_objetivo']);
												$tempo = $d->formattedInterval();
												
												if( $objetivo['previsao_objetivo'] > date("Y-m-d h:i:sa") ){
													$tag = "<span class='text-success' style='font-weight:100'><i class='fa fa-circle'></i></span>";
												}else{
													$tag = "<span class='text-danger' style='font-weight:100'><i class='fa fa-circle'></i></span>";
												}
												
												echo $tag;
												
											?>
											</td>
											
											<td class="hidden-xs">
											<?php
											
												// FORMATA A DATA
												$d = new DateFormatter($objetivo['previsao_objetivo']);
												$tempo = $d->formattedInterval();
												
												if( $objetivo['previsao_objetivo'] > date("Y-m-d h:i:sa") ){
													$tag = "<span class='text-success' style='font-weight:100'><i class='fa fa-circle'></i></span>";
												}else{
													$tag = "<span class='text-danger' style='font-weight:100'><i class='fa fa-circle'></i></span>";
												}
												
												echo "".$tag." <br> ".$tempo."";
												
											?>
											</td>
											
											
											<td>
												
												<?php
													if( acesso_editar_objetivo($mysqli,$usuario,$paciente) == TRUE ){
												?>
													
													<a data-toggle="modal" data-target=".bs-example-modal-md"
															onClick="ajustaModal( 
																		'<?php echo $objetivo['id']; ?>', 
																		'<?php echo $objetivo['inicio_objetivo']; ?>', 
																		'<?php echo $objetivo['objetivo']; ?>', 
																		'<?php echo $paciente['geral']['nome']; ?>', 
																		'<?php echo $objetivo['progresso']; ?>', 
																		'<?php echo $objetivo['status']; ?>'
																		
																	)"
													>
													
														<button title="" class="btn btn-primary btn-xs" type="button" data-original-title="Editar" data-toggle="tooltip" data-placement="right">
															&nbsp; <i class="fa fa-pencil"></i> &nbsp;
														</button>
														
													</a>
												
												<?php } ?>
												
												<?php
													if( acesso_concluir_objetivo($mysqli,$usuario,$paciente) == TRUE ){
												?>
												
													<form id="form-concluir" data-parsley-validate class="form-horizontal" method="post" action="sql/objetivo/concluir.php" onsubmit="return confirm('Deseja concluir esse objetivo?');">
														
														<input type="hidden" name="inputIdObjetivo2" id="inputIdObjetivo2" value="<?php echo $objetivo['id']; ?>">
														
														<button title="" class="btn btn-primary btn-xs" type="submit" data-original-title="Concluído" data-toggle="tooltip" data-placement="right">
															&nbsp; <i class="fa fa-check"></i> &nbsp;
														</button>
														
													</form>
													
													<form id="form-concluir" data-parsley-validate class="form-horizontal" method="post" action="sql/objetivo/excluir.php" onsubmit="return confirm('Deseja excluir definitivamente esse objetivo?');">
														
														<input type="hidden" name="inputIdObjetivo3" id="inputIdObjetivo3" value="<?php echo $objetivo['id']; ?>">
														
														<button title="" class="btn btn-primary btn-xs" type="submit" data-original-title="Excluir" data-toggle="tooltip" data-placement="right">
															&nbsp; <i class="fa fa-times"></i> &nbsp;
														</button>
														
													</form>
													
												<?php } ?>
												
											</td>
										</tr>
										
										<?php 
										
											$i++;
										} 
										
										?>
										
									</tbody>
								</table>
								<!-- FIM DA TABELA DE OBJETIVOS -->
								
								<?php
								
								}else{
									echo '
										<br>
										<h3> Nenhum objetivo estabelecido. </h3>
										<br>
									';
								}
								
								?>
								
								<!-- MODAL EDITAR OBJETIVO -->
								<?php include_once("include/objetivo/editar.php"); ?>
								
								
								<!-- MODAL CRIAR OBJETIVO -->
								<?php include_once("include/objetivo/criar.php"); ?>
								

							</div>
						</div>
					</div>
				</div>
                <br />
				

                <!-- FOOTER-->
				<?php include_once("include/layout/footer.php"); ?>
				
				
            </div>
            <!-- /page content -->
		
        </div>
		
    </div>
	
	
    <script src="assets/js/bootstrap.min.js">										</script>
	
    <!-- bootstrap progress js -->
    <script src="assets/js/progressbar/bootstrap-progressbar.min.js">				</script>
    <script src="assets/js/nicescroll/jquery.nicescroll.min.js">					</script>

    <script src="assets/js/custom.js">												</script>
	
	
	
	
	<!-- TABELA DINAMICA -->
	<script src="//code.jquery.com/jquery-1.12.3.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>

	<link href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet" />
	
	
	<script>
	
		$(document).ready(function() {
			
			$('#tabela_objetivos').DataTable( {
				
				"paging":   		true,
				"bLengthChange": 	true,
				"pageLength": 		20,
				"ordering": 		true,
				"info":     		true,
				"sPaginationType": 	"simple_numbers",
				"order": 			[[ 0, "asc" ]],
				"columnDefs": 		[ { "targets": [4,5,6,7,8], "orderable": false } ],
				stateSave: 			true,
				
				"dom": '<"left"f><l>rt<"bottom"pi><"clear">',
				
				"language": {
					"url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Portuguese.json"
				},
				
				buttons: [ ]
				
			} );

		} );
		
		
	</script>
	
	
	<!-- MODAL -->
	<script>
		
	function ajustaModal(id_objetivo, data_inicio, objetivo, nome_paciente, progresso, status){
		
		document.getElementById("inputIdObjetivo").value 		= id_objetivo;
		document.getElementById("inputStatus").value 			= status;
		document.getElementById("modalDescricao").innerHTML  	= objetivo;
		
		
		var x = document.getElementById("inputProgresso").children[progresso/10];
		x.setAttribute("selected", "selected");

		
		data = data_inicio.split(" ");
		data = data[0].split("-");
		data_inicio = data[2] + "/" + data[1] + "/" + data[0];
		
		
		Paciente = nome_paciente + " <br> Objetivo iniciado em: <i>" + data_inicio + "</i>";
		document.getElementById("modalPaciente").innerHTML   = Paciente;
		
		return;
		
	}
		
	</script>
	
</body>
</html>


<?php

}else{
	
	header("Location: login.php");
	
}

?>