﻿<?php
	
	// CONECTAR AO BANCO DE DADOS
	include("config/db_connect.php");
	
	// RECUPERA O CODIGO
	$codigo = $_GET['id'];
	$codigo = preg_replace("/[^A-Za-z0-9]+/", "", $codigo);
	
		
	// VERIFICA SE EXISTE O CODIGO NO BANCO DE DADOS
	$query 		 = " SELECT COUNT(*) AS total FROM usuarios WHERE recuperar = '".$codigo."' AND recuperar_data > DATE_SUB(NOW(), INTERVAL 1 DAY) ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total 		 = mysqli_fetch_assoc($mysql_query);
	
	// LIBERA A ALTERAÇÃO DA SENHA
	if( $total['total'] == 1 ){
			
?>
			
		<!DOCTYPE html>
		<html lang="en">

		<head>

			<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
			<!-- Meta, title, CSS, favicons, etc. -->
			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1">

			<title>SiSaN | Alterar senha</title>

			<!-- Bootstrap core CSS -->
			<link href="assets/css/bootstrap.min.css" rel="stylesheet">
			<link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
			<link href="assets/css/animate.min.css" rel="stylesheet">

			<!-- Custom styling plus plugins -->
			<link href="assets/css/custom.css" rel="stylesheet">
			<script src="assets/js/jquery.min.js"></script>
			
			<script type="text/JavaScript" src="assets/js/form/sha512.js"></script> 
			<script type="text/JavaScript" src="assets/js/form/forms.js"></script>
			
		</head>

		<body style="background:#F7F7F7;">
			
			<div class="">
				
				<a class="hiddenanchor" id="toregister"></a>
				<a class="hiddenanchor" id="tologin"></a>

				<div id="wrapper">
					
					<div id="login" class="animate form">
						
						<?php
						if (!empty($error_msg)) {
							echo "<div class='alert alert-danger'><strong>Erro!</strong> ".$error_msg."</div>";
						}
						?>
						
						<section class="login_content">
							<form name="login_form" data-parsley-validate class="form-horizontal" method="post" action="config/alterar_senha.php">
								
								<h1> Alterar senha </h1>
								
								<div>
									<input type="password" 	id="senha1" 		name="senha1" 	class="form-control" placeholder="Nova senha" 	required="" />
								</div>
								
								<div>
									<input type="password" 	id="senha2" 		name="senha2" 	class="form-control" placeholder="Confirmar senha" 	required="" />
								</div>
								
								<div>
									<input type="hidden" 	id="inputCodigo" 	name="inputCodigo" 	class="form-control" value="<?php echo $codigo; ?>" />
									<button type="button" class="btn btn-primary"  onclick="formhash_recuperar(this.form, this.form.senha1, this.form.senha2);">Salvar</button>
								</div>
								
								<br /><br />
								
								<div class="clearfix"></div>
								
								<div class="separator">
									
									<div class="clearfix"></div>
									<br />
									
									<div>
										<h1>SiSaN<small><i class="fa fa-cloud"></i></small></h1>
										<p>©2015 All Rights Reserved. SiSaN - Sistema de Saúde nas Nuvens. Privacy and Terms</p>
									</div>
									
								</div>
								
							</form>
							
						</section>
						
					</div>
					
					
				</div>
			</div>

		</body>

		</html>

<?php
		
	}else{
		
		// Erro ao processar solicitação
		header("Location: login.php?e=94");
		
	}

?>