<?php

	// QUAL ITEM EVIDENCIAR NO MENU
	$menu = "equipes";
	
	
	// Inclua fun��es e conexões de banco de dados aqui. Ver 3.1. 
	include_once("config/db_connect.php");
	include_once("config/functions.php");
	
	
	sec_session_start(); 
	

	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verifica��o de acessos
		include_once("functions/acesso/usuario.php");
		include_once("functions/erro/erro.php");
		
		
		/* SQL */
		// Busca os dados do servico
		include_once("sql/servico/buscar.php");
		// Busca os dados de usuario
		include_once("sql/usuario/buscar.php");
		// Busca os dados de paciente
		include_once("sql/paciente/buscar.php");
		// Busca os dados de objetivos
		include_once("sql/objetivo/buscar.php");
		
		
		// BUSCA DADOS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA ID DO PERFIL
		$id_perfil = isset($_GET['id']) ? $_GET['id'] : $usuario['geral']['id_usuario'];
		$id_perfil = preg_replace("/[^0-9]+/", "", $id_perfil);
		$perfil = busca_usuario($mysqli,$id_perfil);
		
		
		if( acesso_editar_perfil($mysqli,$usuario,$perfil) == FALSE ){
		
			// REDIRECIONA PARA PAGINA DO PACIENTE
			header("Location: equipes.php");
			
		}
		
		
?>

<!DOCTYPE html>
<html lang="en">

<head>
    
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>SiSaN | Editar perfil</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/fonts/css/font-awesome.min.css" rel="stylesheet">
    <link href="assets/css/animate.min.css" rel="stylesheet">

    <!-- Custom styling plus plugins -->
    <link href="assets/css/custom.css" rel="stylesheet">
    <script src="assets/js/jquery.min.js"></script>
    
	<!--Loading maps css-->
	<link type="text/css" rel="stylesheet" href="assets/css/maps.css">
	
	
</head>


<body class="nav-md">

    <div class="container body" style="margin-bottom:-30px;">


        <div class="main_container" style="background:#3C3C3C">
			
			<!-- MENU DA ESQUERDA -->
            <?php include_once("include/layout/menu.php"); ?>
			
            <!-- BARA SUPERIOR -->
			<?php include_once("include/layout/topbar.php"); ?>
			
			
            <!-- CONTEÃšDO DA PAGINA -->
            <div class="right_col" role="main">

                <!-- TITULO -->
                <div class="row">
					<div class="page-title">
						
						<!-- NOME DA PAGINA -->
                        <div class="pull-left">
                            <h3> &nbsp; &nbsp; Editar Usuário</h3>
                        </div>
						
                        <!-- CAMINHO DA PAGINA -->
                        <div class="pull-right hidden-xs">
						
							<i class="fa fa-home"></i>&nbsp;<a href="dashboard.html">Home</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							<a href="equipes.php">Equipes</a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							<a href="perfil.php?id=<?php echo $perfil['geral']['id_usuario']; ?>"><?php echo $perfil['geral']['nome']; ?></a>&nbsp;&nbsp;<i class="fa fa-angle-right"></i>&nbsp;&nbsp;
							Editar usuário &nbsp;&nbsp;
							
                        </div>
						
                    </div>
                </div>
				
				<br>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
							
							<div class="x_content">
                            <form id="form-post" data-parsley-validate class="form-horizontal" method="post" action="sql/usuario/editar.php">
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 controls">
									
									<h3> Dados básicos </h3>
									
									<!-- NOME -->
									<label class="control-label" for="inputNome"> Nome </label>
									<input type="text" id="inputNome" name="inputNome" class="form-control" required value="<?php echo $perfil['geral']['nome']; ?>" />
									<br>
									
									<!-- E-MAIL -->
									<label class="control-label" for="inputEmail"> E-mail </label>
									<input type="text" id="inputEmail" name="inputEmail" class="form-control" required value="<?php echo $perfil['contato']['email']; ?>" />
									<br>
									
									<div class="row">
										
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
											
											<!-- NASCIMENTO -->
											<label class="control-label" for="inputNascimento"> Data de nascimento </label>
											<input type="date" id="inputNascimento" name="inputNascimento" class="form-control" value="<?php echo $perfil['geral']['nascimento']; ?>" />
											<br>
											
										</div>
										
										<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
										</div>
										
									</div>
									<br>
									
									<h3> Dados profissionais </h3>
									
									<!-- CARGO -->
									<label class="control-label" for="inputCargo"> Cargo </label>
									<input type="text" id="inputCargo" name="inputCargo" class="form-control" value="<?php echo $perfil['geral']['cargo']; ?>"
										<?php
											if($usuario['acesso']['adm_fon'] != TRUE){
												echo "disabled";
											}
										?>
									/>
									<br>
									
									
									<!-- SUPERIOR -->
									<label class="control-label" for="inputSuperior"> Superior </label>
									<select class="form-control" id="inputSuperior" name="inputSuperior" 
												<?php
													if($usuario['acesso']['adm_fon'] != TRUE){
														echo "disabled";
													}
												?>
									>
									
										<option value='N/A'> 
											N/A
										</option>
										
										<?php
									
										$query = "SELECT 
													nome, id_usuario 
												FROM 
													".$_SESSION['user_Servico']."_usuarios 
												WHERE 
													equipe = '".$perfil['geral']['equipe']."' AND 
													id_usuario != '".$perfil['geral']['id_usuario']."' 
												";
												
										foreach($mysqli->query($query) as $row) { 
											
											if( $row['id_usuario'] == $perfil['geral']['superior'] ){
												
												echo "
													<option selected='selected' value='".$row['id_usuario']."'> 
														".$row['nome']."
													</option>
												";
												
											}else{
												
												echo "
													<option value='".$row['id_usuario']."'> 
														".$row['nome']."
													</option>
												";
												
											}
											
										}
										
										?>
									
									</select>
									<br>
									
									<!-- SUPERIOR -->
									<label class="control-label" for="inputPerfilAcesso"> Perfil de acesso </label>
									<select class="form-control" id="inputPerfilAcesso" name="inputPerfilAcesso" 
												
												<?php
													if( $usuario['acesso']['cadastrar_perfil'] != 4 ){
														echo "disabled";
													}else{
														
														$query = "SELECT 
																	perfil_acesso
																FROM 
																	usuarios
																WHERE
																	id = '".$perfil['geral']['id_usuario']."'
																";
																
														$mysql_query = mysqli_query($mysqli,$query);
														$perfil_acesso = mysqli_fetch_assoc($mysql_query);
														
													}
												?>
									>
									
										
										<?php
									
										$query = "SELECT 
													perfil
												FROM 
													".$_SESSION['user_Servico']."_acessos 
												";
												
										foreach($mysqli->query($query) as $row) { 
											
											if( $row['perfil'] == $perfil_acesso['perfil_acesso'] ){
												
												echo "
													<option selected='selected' value='".$row['perfil']."'> 
														".$row['perfil']."
													</option>
												";
												
											}else{
												
												echo "
													<option value='".$row['perfil']."'> 
														".$row['perfil']."
													</option>
												";
												
											}
											
										}
										
										?>
									
									</select>
									<br>
									
								</div>
								
								
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 controls">
								
									<h3> Contato </h3>
									<br>
									
									
									<!-- ENDEREÇO -->
									<input id="pac-input" class="controls2" type="text" placeholder="Endereço"></input>
									<div id="map" style="height:300px; width:100%"></div>
									<br>
									
									<label class="control-label" for="inputEndereco"> Endereço </label>
									<input type="text" id="inputEndereco" name="inputEndereco" class="form-control" value="<?php echo $perfil['contato']['endereco']; ?>" />
									<br>
									
									<label class="control-label" for="inputSubject"> Bairro </label>
									<input type="text" id="inputBairro" class="form-control" name="inputBairro" value="<?php echo $perfil['contato']['bairro']; ?>" />
									<br>
									
									<label class="control-label" for="inputSubject"> Cidade </label>
									<input type="text" id="inputCidade" name="inputCidade" class="form-control" value="<?php echo $perfil['contato']['cidade']; ?>" />
									<br>
									
									<input type="hidden" id="inputCoordenada" name="inputCoordenada" class="form-control" required />
									<input type="hidden" id="inputLat" name="inputLat" class="form-control" required value="<?php echo $perfil['contato']['lat']; ?>" />
									<input type="hidden" id="inputLng" name="inputLng" class="form-control" required value="<?php echo $perfil['contato']['lng']; ?>" />
									
									
									<!-- TELEFONE -->
									<label class="control-label" for="inputSubject"> Telefone </label>
									<input type="text" id="inputTelefone" name="inputTelefone" class="form-control" value="<?php echo $perfil['contato']['telefone']; ?>" />
									<br>
									
									<input type="hidden" id="inputIdUsuario" name="inputIdUsuario" class="form-control" required value="<?php echo $perfil['geral']['id_usuario']; ?>" />
									
									<!-- BOTÃƒO DE ENVIAR -->
									<button type="submit" class="btn btn-primary"> &nbsp; Salvar &nbsp; </button>
									
								</div>
							
							</form>
							</div>
							
                        </div>
                    </div>

                </div>
                <br />
				

                <!-- FOOTER-->
                <?php include_once("include/layout/footer.php"); ?>
				
				
            </div>
            <!-- /page content -->

        </div>

    </div>
	
	
    <script src="assets/js/bootstrap.min.js"></script>
	
    <!-- bootstrap progress js -->
    <script src="assets/js/progressbar/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/nicescroll/jquery.nicescroll.min.js"></script>
	
    <script src="assets/js/custom.js"></script>

    
	<!-- GOOGLE MAPS -->
	<script>
		
		function initMap() {

			
			  var map = new google.maps.Map(document.getElementById('map'), {
				center: {lat: <?php echo $perfil['contato']['lat']; ?>, lng: <?php echo $perfil['contato']['lng']; ?>},
				zoom: 15
			  });
			  
				var location = new google.maps.LatLng(<?php echo $perfil['contato']['lat']; ?>, <?php echo $perfil['contato']['lng']; ?>);
				var marker = new google.maps.Marker({
					position: location,
					map: map
				});
			
			  var input = /** @type {!HTMLInputElement} */(
				  document.getElementById('pac-input'));
	
			  var types = document.getElementById('type-selector');
			  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
			  map.controls[google.maps.ControlPosition.TOP_LEFT].push(types);
	
			  var autocomplete = new google.maps.places.Autocomplete(input);
			  autocomplete.bindTo('bounds', map);
	
			  var infowindow = new google.maps.InfoWindow();
			  var marker = new google.maps.Marker({
				map: map,
				anchorPoint: new google.maps.Point(0, -29)
			  });
	
			  autocomplete.addListener('place_changed', function() {
				infowindow.close();
				marker.setVisible(false);
				var place = autocomplete.getPlace();
				if (!place.geometry) {
				  window.alert("Autocomplete's returned place contains no geometry");
				  return;
				}
	
				// If the place has a geometry, then present it on a map.
				if (place.geometry.viewport) {
				  map.fitBounds(place.geometry.viewport);
				} else {
				  map.setCenter(place.geometry.location);
				  map.setZoom(17);  // Why 17? Because it looks good.
				}
				marker.setIcon(/** @type {google.maps.Icon} */({
				  url: place.icon,
				  size: new google.maps.Size(71, 71),
				  origin: new google.maps.Point(0, 0),
				  anchor: new google.maps.Point(17, 34),
				  scaledSize: new google.maps.Size(35, 35)
				}));
				marker.setPosition(place.geometry.location);
				marker.setVisible(true);
	
				var endereco = '';
				if (place.address_components) {
				  
				  endereco = [
					(place.address_components[0] && place.address_components[0].short_name || ''),
					(place.address_components[1] && place.address_components[1].short_name || ''),
					(place.address_components[2] && place.address_components[2].short_name || ''),
				  ].join(' ');
				  
				  bairro = [
					(place.address_components[2] && place.address_components[2].short_name || '')
				  ].join(' ');
				  
				  cidade = [
					(place.address_components[3] && place.address_components[3].short_name || '')
				  ].join(' ');
				  
				}
				
				document.getElementById('inputCoordenada').value = place.geometry.location;
				document.getElementById('inputLat').value = place.geometry.location.lat();
				document.getElementById('inputLng').value = place.geometry.location.lng();
				document.getElementById('inputEndereco').value = place.name;
				document.getElementById('inputBairro').value = bairro;
				document.getElementById('inputCidade').value = cidade;
				
				
				infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
				infowindow.open(map, marker);
			  });
	
			  // Sets a listener on a radio button to change the filter type on Places
			  // Autocomplete.
			  function setupClickListener(id, types) {
				var radioButton = document.getElementById(id);
				radioButton.addEventListener('click', function() {
				  autocomplete.setTypes(types);
				});
			  }
	
			  // setupClickListener('changetype-all', []);
			  // setupClickListener('changetype-address', ['address']);
			  // setupClickListener('changetype-establishment', ['establishment']);
			  // setupClickListener('changetype-geocode', ['geocode']);
		  
		}

    </script>
	
    <!-- GOOGLE MAPS -->
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOiVaKKO0XJltgm3witPr14h3RnhqiLAY&signed_in=true&libraries=places&callback=initMap"
        async defer>
	</script>
	<!-- [END] GOOGLE MAPS -->
	
    
    <!-- /footer content -->
	
</body>

</html>

<?php

}else{
	
	header("Location: login.php");
	
}

?>