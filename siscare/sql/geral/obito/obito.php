﻿<?php
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include("../../../config/db_connect.php");
	include("../../../config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("../../../functions/acesso/paciente.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../../usuario/buscar.php");
		// Busca os dados de paciente
		include_once("../../paciente/buscar.php");
		
		
		$id_paciente  	= isset($_POST['inputIDPaciente']) 	? $_POST['inputIDPaciente'] 	: header("Location: ../../../pacientes.php");
		$id_paciente   	= preg_replace("/[^0-9]+/", "", $id_paciente);
		
		// BUSCA DADOS BASICOS DO PACIENTE
		$paciente = busca_paciente($mysqli, $id_paciente);
		
		
		// BUSCA DADOS GERAIS DO USUARIO
		$usuario   = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		$alta = isset($_POST['inputAlta']) 	? $_POST['inputAlta'] 	: header("Location: ../../../pacientes.php");
		
		
		// VERIFICA ACESSO PARA DAR ALTA PARA O PACIENTE
		if( acesso_obito($mysqli,$paciente,$usuario) == TRUE ){
			
			// FINALIZA AS ATIVIDADES DE FONOAUDIOLOGIA
			$query = " UPDATE ".$_SESSION['user_Servico']."_fon SET data_termino = NOW(), alta = '".$alta."', alterado_por = '".$_SESSION['user_Codigo']."', data_alteracao = NOW() WHERE id_paciente = '".$id_paciente."' AND alta IS NULL ";
			$mysqli_query = mysqli_query($mysqli,$query)  or die(mysqli_error($mysqli));
			
			
			// INATIVA OS DADOS GERAIS DO PACIENTE
			$query = " UPDATE ".$_SESSION['user_Servico']."_paciente_geral SET data_atualizacao = NOW(), fon = '0', status='obito' WHERE id = '".$id_paciente."' ";
			$mysqli_query = mysqli_query($mysqli,$query)  or die(mysqli_error($mysqli));
			
			// FINALIZA OS OBJETIVOS
			$query = " DELETE FROM ".$_SESSION['user_Servico']."_objetivo WHERE id_paciente = '".$id_paciente."' AND progresso < 100";
			$mysqli_query = mysqli_query($mysqli,$query)  or die(mysqli_error($mysqli));
			
			// RETORNA PARA OS PACIENTES
			header("Location: ../../../pacientes.php");
			
		}
		
	}
	
?>