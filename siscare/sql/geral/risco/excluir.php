<?php

	// Inclua fun��es e conex�es de banco de dados aqui. Ver 3.1. 
	include_once("../../../config/db_connect.php");
	include_once("../../../config/functions.php");
	
	
	sec_session_start(); 
	

	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verifica��o de acessos
		include_once("../../../functions/acesso/paciente.php");
		// Ajusta os checkboxs para terapias adicionais
		include_once("../../../functions/terapia/terapia_adicional_fon.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../../usuario/buscar.php");
		// Busca os dados de paciente
		include_once("../../paciente/buscar.php");
		
		
		// RECUPERA OS DADOS DO USUARIO
		$usuario 	= busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA O ID E DADOS DO PACIENTE
		$id_paciente 	= isset($_POST['inputIDPaciente']) ? $_POST['inputIDPaciente'] : header("Location: ../../../pacientes.php") ;
		$id_paciente 	= preg_replace("/[^0-9]+/", "", $id_paciente);
		$paciente 		= busca_paciente($mysqli, $id_paciente);
		
		
		// VERIFICA ACESSO PARA GERENCIAR RISCO
		if( acesso_gerenciar_risco($mysqli,$usuario,$paciente) == TRUE ){
			
			
			/* INPUTS DE ENTRADA */
			// Dados do risco
			$id_risco = filter_input(INPUT_POST, 'inputExcluirRisco', 	FILTER_SANITIZE_STRING);
			
			
			// DELETA DO BANCO DE DADOS
			$query = "	DELETE FROM ".$_SESSION['user_Servico']."_risco WHERE id = '".$id_risco."' ";
			$mysql_query = mysqli_query($mysqli,$query) or die(mysqli_error($mysqli));
			
			
			// REDIRECIONA - Concluido
			header("Location:../../../paciente.php?id=".$id_paciente);
			
			
		}else{
			
			// REDIRECIONA - Acesso n�o permitido
			header("Location:../../../paciente.php?id=".$id_paciente."&tab=fon&e=211");
		
		}
		
	}else{
	
		// REDIRECIONA - Falha de login
		header("Location: ../../../login.php");
	
	}


?>

