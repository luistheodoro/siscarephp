﻿<?php
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("../../../config/db_connect.php");
	include_once("../../../config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("../../../functions/acesso/paciente.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../../usuario/buscar.php");
		// Busca os dados de paciente
		include_once("../../paciente/buscar.php");
		
		
		// RECUPERA OS DADOS DO USUARIO
		$usuario 	= busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA O ID E DADOS DO PACIENTE
		$id_paciente 	= isset($_POST['inputIDPaciente']) ? $_POST['inputIDPaciente'] : header("Location: ../../../pacientes.php") ;
		$id_paciente 	= preg_replace("/[^0-9]+/", "", $id_paciente);
		$paciente 		= busca_paciente($mysqli, $id_paciente);
		
		
		// VERIFICA ACESSO PARA EDITAR RESPONSAVEL
		if( acesso_editar_responsavel($mysqli,$usuario,$paciente) == TRUE ){	
			
			
			// RECUPERA INPUTS
			$responsavel	= isset($_POST['inputResponsavel'])	? $_POST['inputResponsavel'] 	: NULL ;
			$equipe	   		= isset($_POST['inputEquipe'])		? $_POST['inputEquipe'] 		: NULL ;
			
			
			// DETERMINA O NOME DA TABELA
			$tabela = $_SESSION['user_Servico']."_".$equipe;
			
			// EDITA RESPONSAVEL DO PACIENTE
			$query = " UPDATE ".$tabela." SET responsavel = '".$responsavel."', alterado_por = '".$_SESSION['user_Codigo']."', data_alteracao = NOW() WHERE id_paciente = '".$id_paciente."' ";
			$mysqli_query = mysqli_query($mysqli,$query)  or die(mysqli_error($mysqli));
		
			// ATUALIZAR OBJETIVO ATUAL
			$query = "	UPDATE
							".$_SESSION['user_Servico']."_objetivo 
						SET
							id_responsavel	= '".$responsavel."'
						WHERE
							progresso		< '100' AND
							id_paciente		= '".$id_paciente."'
						";
			$mysql_query_objetivo = mysqli_query($mysqli,$query);
			
			
			// REDIRECIONA - Concluido
			header("Location: ../../../paciente.php?id=".$id_paciente."&tab=".$equipe);
		
		}else{
			
			// REDIRECIONA - Acesso não permitido
			header("Location: ../../../paciente.php?id=".$id_paciente."&tab=".$equipe."&e=214");
			
		}
		
	}else{
	
		// REDIRECIONA - Falha de login
		header("Location: ../../../login.php");
	
	}
	
	
?>