﻿<?php
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("../../../config/db_connect.php");
	include_once("../../../config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("../../../functions/acesso/paciente.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../../usuario/buscar.php");
		// Busca os dados de paciente
		include_once("../../paciente/buscar.php");
		
		
		// RECUPERA OS DADOS DO USUARIO
		$usuario 	= busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA O ID E DADOS DO PACIENTE
		$id_paciente 	= isset($_POST['inputIDPaciente']) ? $_POST['inputIDPaciente'] : header("Location: ../../../pacientes.php") ;
		$id_paciente 	= preg_replace("/[^0-9]+/", "", $id_paciente);
		$paciente 		= busca_paciente($mysqli, $id_paciente);
		
		
		// VERIFICA ACESSO PARA EDITAR ESPEC
		if( acesso_editar_espec($mysqli,$usuario,$paciente) == TRUE ){
			
			
			// RECUPERA INPUTS
			$degenerativo   = isset($_POST['inputDegenerativo']) 	? $_POST['inputDegenerativo'] 	: 0 ;
			$paliativo		= isset($_POST['inputPaliativo']) 		? $_POST['inputPaliativo'] 		: 0 ;
			$terminalidade	= isset($_POST['inputTerminalidade']) 	? $_POST['inputTerminalidade'] 	: 0 ;
			$prognostico	= isset($_POST['inputPrognostico']) 	? $_POST['inputPrognostico'] 	: $paciente['espec']['prognostico'] ;
			$VAA        	= isset($_POST['inputVAA']) 			? $_POST['inputVAA'] 	 		: $paciente['espec']['VAA'] ;
			$TQT       		= isset($_POST['inputTQT']) 			? $_POST['inputTQT'] 	 		: $paciente['espec']['TQT'] ;
			$dieta      	= isset($_POST['inputDieta']) 			? $_POST['inputDieta'] 	 		: $paciente['espec']['dieta'] ;
			$liquido		= isset($_POST['inputLiquido']) 		? $_POST['inputLiquido'] 		: $paciente['espec']['liquido'] ;
			
			
			// INSERE NOVA ESPEC PARA PACIENTE
			$query = "INSERT INTO ".$_SESSION['user_Servico']."_espec (
											id_paciente, 
											modificado_por, 
											modificado_em, 
											degenerativo, 
											paliativo,
											terminalidade,
											prognostico,
											VAA,
											TQT,
											dieta,
											liquido
										) VALUES (
											'".$id_paciente."', 
											'".$_SESSION['user_Codigo']."', 
											NOW(), 
											'".$degenerativo."', 
											'".$paliativo."', 
											'".$terminalidade."',
											'".$prognostico."',
											'".$VAA."',
											'".$TQT."',
											'".$dieta."',
											'".$liquido."'
										)";
										
			$mysqli_query = mysqli_query($mysqli,$query);
			
			// REDIRECIONA - Concluido
			header("Location: ../../../paciente.php?id=".$id_paciente);
		
		}else{
			
			// REDIRECIONA - Acesso não permitido
			header("Location: ../../../paciente.php?id=".$id_paciente."&tab=fon&e=213");
			
		}
		
	}else{
	
		// REDIRECIONA - Falha de login
		header("Location: ../../../login.php");
		
	}
	
?>