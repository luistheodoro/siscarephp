﻿<?php


//-------------------------------------------------	USUARIO	---------------------------------------------------------------------------------//


function busca_usuario($mysqli,$id_usuario){
	
	$id_usuario = preg_replace("/[^0-9]+/", "", $id_usuario);
	
	$usuario['geral']    = busca_usuario_geral($mysqli,$id_usuario);
	$usuario['contato']  = busca_usuario_contato($mysqli,$id_usuario);
	$usuario['acesso']   = acesso($mysqli);
	
	return $usuario;
	
}


	// BUSCA DADOS GERAIS DO USUARIO
	function busca_usuario_geral($mysqli,$id_usuario){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_usuarios WHERE id_usuario = '".$id_usuario."' LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		$usuario = mysqli_fetch_assoc($mysql_query);
		
		if($usuario['imagem']!=""){
			$imagem = $usuario['imagem'];
			$avatar = explode(".",$usuario['avatar']);
			$avatar = $avatar[0].".png";
		}else{
			$imagem = "images/avatar/profile-pic.png";
			$avatar = $imagem;
		}
		
		$usuario['imagem'] = $imagem;
		$usuario['avatar'] = $avatar;
		
		
		
		return $usuario;
		
	}


	// BUSCA DADOS DE CONTATO DO USUARIO
	function busca_usuario_contato($mysqli,$id_usuario){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_contatos WHERE id_usuario = '".$id_usuario."' LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		$contato = mysqli_fetch_assoc($mysql_query);
		
		$query = "SELECT email FROM usuarios WHERE id = '".$id_usuario."' LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		$email = mysqli_fetch_assoc($mysql_query);
		
		$contato['email'] = $email['email'];
		
		return $contato;
		
	}


	// BUSCA ACESSOS DO USUARIO
	function acesso($mysqli){
		
		// Busca senha do usuario no banco de dados basico
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_acessos WHERE perfil = '".$_SESSION['user_Acesso']."' LIMIT 1";
		$mysql = mysqli_query($mysqli,$query);
		
		$acesso = mysqli_fetch_assoc($mysql);
		
		return $acesso;
		
	}



?>