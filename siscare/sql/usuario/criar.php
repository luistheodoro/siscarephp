<?php


	// Inclua fun��es e conex�es de banco de dados aqui. Ver 3.1. 
	include_once("../../config/db_connect.php");
	include_once("../../config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verifica��o de acessos
		include_once("../../functions/acesso/usuario.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../usuario/buscar.php");
		
		
		// RECUPERA OS DADOS DO USUARIO
		$usuario 	= busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		// RECUPERA A EQUIPE QUE SERA CRIADO O NOVO USUARIO
		$equipe		= filter_input(INPUT_POST, 'inputEquipe', FILTER_SANITIZE_STRING);
		
		
		// VERIFICA ACESSO PARA CADASTRAR USUARIO NA EQUIPE
		if( acesso_cadastrar_perfil($mysqli,$usuario,$equipe) == FALSE ){
			
			header("Location:../../equipes.php");
			
		}else{
			
			/* --- CADASTRAR DADOS BASICOS --- */
				
			// RECUPERA VALORES DO FORMULARIO
			$codigo_servico	= filter_input(INPUT_POST, 'inputServico'		, FILTER_SANITIZE_STRING);
			$UserID			= filter_input(INPUT_POST, 'inputUserID'		, FILTER_SANITIZE_STRING);
			$acesso			= "basico";
			
			
			$email = filter_input(INPUT_POST, 'inputEmail', FILTER_SANITIZE_EMAIL);
			$email = filter_var($email, FILTER_VALIDATE_EMAIL);
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				// Email inv�lido
				$error_msg .= '<p class="error">O endere�o de email digitado n�o � v�lido</p>';
				header('Location: ../../cadastrar_usuario.php?e=124');
			}
			
			
			$senha = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
			if (strlen($senha) != 128) {
				// A senha com hash deve ter 128 caracteres.
				// Caso contr�rio, algo muito estranho est� acontecendo
				$error_msg .= '<p class="error">Senha n�o confere.</p>';
				header('Location: ../../cadastrar_usuario.php?e=123');
			}
			
			
			// VERIFICA SE USUARIO JA EXISTE
			$prep_stmt = "SELECT id FROM usuarios WHERE email = ? OR user_id = ? LIMIT 1";
			$stmt = $mysqli->prepare($prep_stmt);
			
			// Verifica se query esta correta
			if ($stmt) {
				
				// Adiciona as variaveis
				$stmt->bind_param('ss', $email, $UserID);
				
				// Verifica se query funcionou
				if($stmt->execute()){
					
					// Armazena o resultado
					$stmt->store_result();
					
					// Verifica se existe algum cadastro no banco
					if ($stmt->num_rows == 1) {
						// Um usu�rio com esse email j� esixte
						$error_msg .= '<p class="error">Usu�rio com esse e-mail/UserID j� existe.</p>';
						header('Location: ../../cadastrar_usuario.php?e=122');
					}
					
				}else{
					die('Error : ('. $mysqli->errno .') '. $mysqli->error);
					$error_msg .= '<p class="error">Erro no banco de dados</p>';
				}
				
				// Fecha a consulta
				$stmt->close();
				
			} else {
				$error_msg .= '<p class="error">Erro no banco de dados</p>';
			}
			
			// CADASTRA O USUARIO NO BANCO DE DADOS BASICO
			if (empty($error_msg)) {
				
				// Cria um salt aleatorio
				$random_salt = hash('sha512', uniqid(openssl_random_pseudo_bytes(16), TRUE));
				
				// Cria uma senha com o salt
				$senha = hash('sha512', $senha . $random_salt);
				
				// Data do cadastro
				$data_now = date("Y-m-d H:i:s");
				
				
				// Cadastrar o usuario no banco de dados 
				$prep_stmt = "INSERT INTO usuarios (data, codigo_servico, perfil_acesso, user_id, email, senha, salt) VALUES (?, ?, ?, ?, ?, ?, ?)";
				$stmt = $mysqli->prepare($prep_stmt);
				
				// Verifica se query esta correta
				if ($stmt) {
					
					// bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
					$stmt->bind_param('sisssss', $data_now, $codigo_servico, $acesso, $UserID, $email, $senha, $random_salt);

					if($stmt->execute()){
						
						// Codigo gerado para o paciente
						$codigo_usuario = $stmt->insert_id;
						
						print 'Success! ID of last inserted record is : ' .$stmt->insert_id .'<br />';
						$success_msg = TRUE;
						
					}else{
						die('Error : ('. $mysqli->errno .') '. $mysqli->error);
						$error_msg .= '<p class="error">Erro no banco de dados</p>';
					}
					
					$stmt->close();
				
				} else {
					$error_msg .= '<p class="error">Erro no banco de dados</p>';
				}
				
			}else{
				
				header('Location: ../../cadastrar_usuario.php?e=121');
				
			}
			
			
			
			/* --- CADASTRAR DADOS COMPLETOS --- */
				
			// RECUPERA VALORES DO FORMULARIO
			$nome				= filter_input(INPUT_POST, 'inputNome'			, FILTER_SANITIZE_STRING);
			$nascimento			= filter_input(INPUT_POST, 'inputNascimento'	, FILTER_SANITIZE_STRING);
			$equipe				= filter_input(INPUT_POST, 'inputEquipe'		, FILTER_SANITIZE_STRING);
			$cargo	 			= filter_input(INPUT_POST, 'inputCargo'			, FILTER_SANITIZE_STRING);
			$imagem				= '';
			
			// CADASTRA DADOS DO USUARIO
			if ($success_msg == TRUE) {
				
				// Cadastrar o usuario no banco de dados 
				$prep_stmt = "INSERT INTO ".$codigo_servico."_usuarios (id_usuario, codigo_servico, data_atualizacao, nome, imagem, nascimento, equipe, cargo) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
				$stmt = $mysqli->prepare($prep_stmt);
				
				// Verifica se query esta correta
				if ($stmt) {
					
					// bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
					$stmt->bind_param('ssssssss', $codigo_usuario, $codigo_servico, $data_now, $nome, $imagem, $nascimento, $equipe, $cargo);

					if($stmt->execute()){
						
						// Atualiza lista de servi�os
						$query 	= "UPDATE servicos SET ".$equipe." = '1' WHERE codigo_servico = '".$codigo_servico."' ";
						$sql	= mysqli_query($mysqli,$query);
						
						print 'Success! ID of last inserted record is : ' .$stmt->insert_id .'<br />';
						$success_msg = TRUE;
						
					}else{
						die('Error : ('. $mysqli->errno .') '. $mysqli->error);
						$error_msg .= '<p class="error">Erro no banco de dados</p>';
						$success_msg = FALSE;
					}
					
					$stmt->close();
				
				} else {
					$error_msg .= '<p class="error">Erro no banco de dados</p>';
				}
			
			}
				
				
			
			/* --- CADASTRAR CONTATO --- */
				
			// RECUPERA VALORES DO FORMULARIO
			$endereco		= filter_input(INPUT_POST, 'inputEndereco'		, FILTER_SANITIZE_STRING);
			$bairro			= filter_input(INPUT_POST, 'inputBairro'		, FILTER_SANITIZE_STRING);
			$cidade	 		= filter_input(INPUT_POST, 'inputCidade'		, FILTER_SANITIZE_STRING);
			$telefone		= filter_input(INPUT_POST, 'inputTelefone'		, FILTER_SANITIZE_STRING);
			$lat			= filter_input(INPUT_POST, 'inputLat'			, FILTER_SANITIZE_STRING);
			$lng			= filter_input(INPUT_POST, 'inputLng'			, FILTER_SANITIZE_STRING);
			$coordenada		= filter_input(INPUT_POST, 'inputCoordenada'	, FILTER_SANITIZE_STRING);
			
			if($lat==""){
				$lat = "-22.9039465";
			}
			
			if($lng==""){
				$lng = "-47.0690783";
			}
			
			// CADASTRA DADOS DE CONTATO DO USUARIO
			if ($success_msg == TRUE) {
			
				// Cadastrar o usuario no banco de dados 
				$prep_stmt = "INSERT INTO ".$codigo_servico."_contatos (id_usuario, data_atualizacao, endereco, bairro, cidade, coordenada, lat, lng, telefone) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
				$stmt = $mysqli->prepare($prep_stmt);
				
				// Verifica se query esta correta
				if ($stmt) {
					
					// bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
					$stmt->bind_param('sssssssss', $codigo_usuario, $data_now, $endereco, $bairro, $cidade, $coordenada, $lat, $lng, $telefone);

					if($stmt->execute()){
						print 'Success! ID of last inserted record is : ' .$stmt->insert_id .'<br />';
						$success_msg = TRUE;
					}else{
						die('Error : ('. $mysqli->errno .') '. $mysqli->error);
						$error_msg .= '<p class="error">Erro no banco de dados2</p>';
					}
					
					$stmt->close();
				
				} else {
					$error_msg .= '<p class="error">Erro no banco de dados1</p>';
				}
			
			}
			
			echo $error_msg;
			
			if( empty($error_msg) AND $success_msg == TRUE ){
				header("Location:../../equipes.php");
			}
		
		}
		
	}else{
		
		header('Location: ../../cadastrar_usuario.php?e=121');
		
	}




?>

