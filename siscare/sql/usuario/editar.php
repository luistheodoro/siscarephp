<?php


	// Inclua fun��es e conex�es de banco de dados aqui. Ver 3.1. 
	include_once("../../config/db_connect.php");
	include_once("../../config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {

		
		/* FUNCTIONS */
		// Verifica��o de acessos
		include_once("../../functions/acesso/usuario.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../usuario/buscar.php");
		
		
		// RECUPERA OS DADOS DO USUARIO
		$usuario 	= busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		/* --- CADASTRAR DADOS BASICOS --- */
			
			// RECUPERA VALORES DO FORMULARIO
			$id_usuario 	= filter_input(INPUT_POST, 'inputIdUsuario'		, FILTER_SANITIZE_EMAIL);
			$perfil_acesso	= filter_input(INPUT_POST, 'inputPerfilAcesso'	, FILTER_SANITIZE_STRING);
			
			$email			= filter_input(INPUT_POST, 'inputEmail'			, FILTER_SANITIZE_EMAIL);
			$email 			= filter_var($email, FILTER_VALIDATE_EMAIL);
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				// Email inv�lido
				$error_msg .= '<p class="error">O endere�o de email digitado n�o � v�lido</p>';
			}
			
			
		// RECUPERA ID DO PERFIL, FILTRA POR NUMEROS
		$id_usuario   	= preg_replace("/[^0-9]+/", "", $id_usuario);
		$perfil   = busca_usuario($mysqli,$id_usuario);
		
		
		// VERIFICA ACESSO PARA EDITAR DADOS DO USUARIO
		if( acesso_editar_perfil($mysqli,$usuario,$perfil) == FALSE ){
		
			// REDIRECIONA PARA PAGINA DE EQUIPES
			header("Location: equipes.php");
			
		}else{
				
				// VERIFICA SE USUARIO JA EXISTE
				$prep_stmt = "SELECT id FROM usuarios WHERE email = ? LIMIT 2";
				$stmt = $mysqli->prepare($prep_stmt);
				
				// Verifica se query esta correta
				if ($stmt) {
					
					// Adiciona as variaveis
					$stmt->bind_param('s', $email);
					
					// Verifica se query funcionou
					if($stmt->execute()){
						
						// Armazena o resultado
						$stmt->store_result();
						
						// Verifica se existe algum cadastro no banco
						if ($stmt->num_rows > 1) {
							// Um usu�rio com esse email j� esixte
							$error_msg .= '<p class="error">E-mail j� existe.</p>';
						}
						
					}else{
						die('Error : ('. $mysqli->errno .') '. $mysqli->error);
						$error_msg .= '<p class="error">Erro no banco de dados</p>';
					}
					
					// Fecha a consulta
					$stmt->close();
					
				} else {
					$error_msg .= '<p class="error">Erro no banco de dados</p>';
				}
				
				
				// CADASTRA O USUARIO NO BANCO DE DADOS BASICO
				if (empty($error_msg)) {
					
					// Cadastrar o usuario no banco de dados 
					$prep_stmt = "UPDATE usuarios SET email = ?, perfil_acesso = ? WHERE id = ? ";
					$stmt = $mysqli->prepare($prep_stmt);
					
					// Verifica se query esta correta
					if ($stmt) {
						
						// bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
						$stmt->bind_param('sss', $email, $perfil_acesso, $id_usuario);

						if($stmt->execute()){
							
							$success_msg = TRUE;
							
						}else{
							die('Error : ('. $mysqli->errno .') '. $mysqli->error);
							$error_msg .= '<p class="error">Erro no banco de dados2</p>';
						}
						
						$stmt->close();
					
					} else {
						$error_msg .= '<p class="error">Erro no banco de dados1</p>';
					}
					
				}
			
			
			
			/* --- CADASTRAR DADOS COMPLETOS --- */
				
				// RECUPERA VALORES DO FORMULARIO
				$nome				= filter_input(INPUT_POST, 'inputNome'			, FILTER_SANITIZE_STRING);
				$nascimento			= filter_input(INPUT_POST, 'inputNascimento'	, FILTER_SANITIZE_STRING);
				$equipe				= filter_input(INPUT_POST, 'inputEquipe'		, FILTER_SANITIZE_STRING);
				$cargo	 			= filter_input(INPUT_POST, 'inputCargo'			, FILTER_SANITIZE_STRING);
				$superior			= filter_input(INPUT_POST, 'inputSuperior'		, FILTER_SANITIZE_STRING);
				
				// CADASTRA DADOS DO USUARIO
				if ($success_msg == TRUE) {
				
					// Atualiza o usuario no banco de dados 
					$query = "	UPDATE 
									".$_SESSION['user_Servico']."_usuarios 
								SET 
									data_atualizacao 	= NOW(),
									nome				= '$nome', 
									nascimento			= '$nascimento', 
									cargo				= '$cargo', 
									superior			= '$superior'
								WHERE 
									id_usuario = '".$id_usuario."' 
								";
					$mysql_query = mysqli_query($mysqli,$query);
					
					// ATUALIZA O NOME DO USUARIO NA SESSION
					if($id_usuario == $_SESSION['user_Codigo']){
						$_SESSION['user_Nome'] = $nome;
					}
					
					
				}
				
				
			
			/* --- CADASTRAR CONTATO --- */
				
				// RECUPERA VALORES DO FORMULARIO
				$endereco		= filter_input(INPUT_POST, 'inputEndereco'		, FILTER_SANITIZE_STRING);
				$bairro			= filter_input(INPUT_POST, 'inputBairro'		, FILTER_SANITIZE_STRING);
				$cidade	 		= filter_input(INPUT_POST, 'inputCidade'		, FILTER_SANITIZE_STRING);
				$telefone		= filter_input(INPUT_POST, 'inputTelefone'		, FILTER_SANITIZE_STRING);
				$lat			= filter_input(INPUT_POST, 'inputLat'			, FILTER_SANITIZE_STRING);
				$lng			= filter_input(INPUT_POST, 'inputLng'			, FILTER_SANITIZE_STRING);
				
				// CADASTRA DADOS DE CONTATO DO USUARIO
				if ($success_msg == TRUE) {
				
					$query = "UPDATE 
								".$_SESSION['user_Servico']."_contatos 
							SET 
								endereco	='$endereco', 
								bairro		='$bairro', 
								cidade		='$cidade', 
								telefone	='$telefone', 
								lat			='$lat', 
								lng			='$lng'
							WHERE 
								id_usuario = '".$id_usuario."' 
							";
					$mysql_query = mysqli_query($mysqli,$query);
				
				}
			
			
			if( empty($error_msg) AND $success_msg == TRUE ){
				header("Location:../../perfil.php?id=".$id_usuario);
			}
			
			echo $error_msg;
		
		}
		
	}


?>

