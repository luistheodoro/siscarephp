<?php


	// Inclua fun��es e conex�es de banco de dados aqui. Ver 3.1. 
	include_once("../../config/db_connect.php");
	include_once("../../config/functions.php");
	

	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verifica��o de acessos
		include_once("../../functions/acesso/paciente.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../usuario/buscar.php");
		// Busca os dados de paciente
		include_once("../paciente/buscar.php");
		
		
		// RECUPERA OS DADOS DO USUARIO
		$usuario 	= busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA O ID E DADOS DO PACIENTE
		$id_paciente 	= isset($_POST['inputID']) ? $_POST['inputID'] : header("Location: ../../pacientes.php") ;
		$id_paciente 	= preg_replace("/[^0-9]+/", "", $id_paciente);
		$paciente 		= busca_paciente($mysqli, $id_paciente);
		
		
		if( acesso_editar_perfil_paciente($mysqli,$usuario,$paciente) == TRUE ){
			
			
			// RECUPERA VALORES DO FORMULARIO
			// Dados pessoais
			$nome				= filter_input(INPUT_POST, 'inputNome'			, FILTER_SANITIZE_STRING);
			$nascimento			= filter_input(INPUT_POST, 'inputNascimento'	, FILTER_SANITIZE_STRING);
			$sexo				= filter_input(INPUT_POST, 'inputSexo'			, FILTER_SANITIZE_STRING);
			$diagnostico1		= filter_input(INPUT_POST, 'inputDiagnostico1'	, FILTER_SANITIZE_STRING);
			$diagnostico2		= filter_input(INPUT_POST, 'inputDiagnostico2'	, FILTER_SANITIZE_STRING);
			$diagnostico3		= filter_input(INPUT_POST, 'inputDiagnostico3'	, FILTER_SANITIZE_STRING);
			$responsavel		= filter_input(INPUT_POST, 'inputResponsavel'	, FILTER_SANITIZE_STRING);
			$relacao			= filter_input(INPUT_POST, 'inputRelacao'		, FILTER_SANITIZE_STRING);
			$cuidador			= filter_input(INPUT_POST, 'inputCuidador'		, FILTER_SANITIZE_STRING);
			
			// Endereco
			$endereco			= filter_input(INPUT_POST, 'inputEndereco'		, FILTER_SANITIZE_STRING);
			$bairro				= filter_input(INPUT_POST, 'inputBairro'		, FILTER_SANITIZE_STRING);
			$cidade	 			= filter_input(INPUT_POST, 'inputCidade'		, FILTER_SANITIZE_STRING);
			$lat				= filter_input(INPUT_POST, 'inputLat'			, FILTER_SANITIZE_STRING);
			$lng				= filter_input(INPUT_POST, 'inputLng'			, FILTER_SANITIZE_STRING);
			$coordenada			= filter_input(INPUT_POST, 'inputCoordenada'	, FILTER_SANITIZE_STRING);
			$referencia			= filter_input(INPUT_POST, 'inputReferencia'	, FILTER_SANITIZE_STRING);
			$distancia			= filter_input(INPUT_POST, 'inputDistancia'		, FILTER_SANITIZE_STRING);
			$distancia 			= str_replace(",",".",$distancia);
			
			// Contato
			$telefone1			= filter_input(INPUT_POST, 'inputTelefone1'		, FILTER_SANITIZE_STRING);
			$nome1				= filter_input(INPUT_POST, 'inputNome1'			, FILTER_SANITIZE_STRING);
			$telefone2			= filter_input(INPUT_POST, 'inputTelefone2'		, FILTER_SANITIZE_STRING);
			$nome2				= filter_input(INPUT_POST, 'inputNome2'			, FILTER_SANITIZE_STRING);
			$telefone3			= filter_input(INPUT_POST, 'inputTelefone3'		, FILTER_SANITIZE_STRING);
			$nome3				= filter_input(INPUT_POST, 'inputNome3'			, FILTER_SANITIZE_STRING);
			
			// DADOS ADICIONAIS
			// Data do cadastro
			$data_now 	= date("Y-m-d H:i:s");
			
			
			// ATUALIZAR BANCO DE DADOS
			
			// Dados gerais do paciente
			$query = "	UPDATE
							".$_SESSION['user_Servico']."_paciente_geral 
						SET
							data_atualizacao = NOW(),
							nome             = '".$nome."', 
							nascimento       = '".$nascimento."', 
							sexo             = '".$sexo."', 
							diagnostico_1    = '".$diagnostico1."', 
							diagnostico_2    = '".$diagnostico2."',
							diagnostico_3    = '".$diagnostico3."',
							responsavel      = '".$responsavel."',
							relacao          = '".$relacao."',
							cuidador         = '".$cuidador."'
						WHERE
							id = '".$id_paciente."'
						";
			$mysql_query_geral = mysqli_query($mysqli,$query);
			
			
			// Dados de contato do paciente
			$query = "	UPDATE 
							".$_SESSION['user_Servico']."_paciente_contato 
						SET
							data_atualizacao = NOW(), 
							endereco         = '".$endereco."',
							bairro           = '".$bairro."',
							cidade           = '".$cidade."',
							referencia       = '".$referencia."',
							kilometragem     = '".$distancia."',
							telefone_1       = '".$telefone1."',
							nome_1           = '".$nome1."',
							telefone_2       = '".$telefone2."',
							nome_2           = '".$nome2."',
							telefone_3       = '".$telefone3."',
							nome_3           = '".$nome3."',
							coordenada       = '".$coordenada."',
							lng              = '".$lng."',
							lat              = '".$lat."'
						WHERE
							id_paciente = '".$id_paciente."'
						";
			$mysql_query_contato = mysqli_query($mysqli,$query);
				
			
			// Verifica se cadastrou os dados gerais
			if( $mysql_query_geral == TRUE ){
				
				// Verifica se cadastrou os dados de contato
				if( $mysql_query_contato == TRUE ){
				
					// REDIRECIONA - Concluido
					header("Location:../../paciente.php?id=".$id_paciente);
				
				}else{
						
					// REDIRECIONA - Erro de banco de dados
					header("Location: ../../paciente.php?id=".$id_paciente."&tab=fon&e=1062");
						
				}
				
			}else{
				
				// REDIRECIONA - Erro de banco de dados
				header("Location: ../../paciente.php?id=".$id_paciente."&tab=fon&e=1061");
				
			}
			
			
		}else{
		
			// REDIRECIONA - Acesso n�o permitido
			header("Location: ../../paciente.php?id=".$id_paciente."&tab=fon&e=204");
			
		}
		
		
	}else{
		
		// REDIRECIONA - Falha de login
		header("Location: ../../login.php");
		
	}


?>

