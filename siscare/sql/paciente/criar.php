<?php


	// Inclua fun��es e conex�es de banco de dados aqui. Ver 3.1. 
	include_once("../../config/db_connect.php");
	include_once("../../config/functions.php");
	

	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verifica��o de acessos
		include_once("../../functions/acesso/paciente.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../usuario/buscar.php");
		
		
		// RECUPERA OS DADOS DO USUARIO
		$usuario 	= busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// VERIFICA ACESSO PARA CADASTRAR PACIENTE
		if( acesso_cadastrar_paciente($usuario) == TRUE ){
		
			
			// RECUPERA VALORES DO FORMULARIO
			// Dados pessoais
			$nome				= filter_input(INPUT_POST, 'inputNome'			, FILTER_SANITIZE_STRING);
			$nascimento			= filter_input(INPUT_POST, 'inputNascimento'	, FILTER_SANITIZE_STRING);
			$sexo				= filter_input(INPUT_POST, 'inputSexo'			, FILTER_SANITIZE_STRING);
			$diagnostico1		= filter_input(INPUT_POST, 'inputDiagnostico1'	, FILTER_SANITIZE_STRING);
			$diagnostico2		= filter_input(INPUT_POST, 'inputDiagnostico2'	, FILTER_SANITIZE_STRING);
			$diagnostico3		= filter_input(INPUT_POST, 'inputDiagnostico3'	, FILTER_SANITIZE_STRING);
			$responsavel		= filter_input(INPUT_POST, 'inputResponsavel'	, FILTER_SANITIZE_STRING);
			$relacao			= filter_input(INPUT_POST, 'inputRelacao'		, FILTER_SANITIZE_STRING);
			$cuidador			= filter_input(INPUT_POST, 'inputCuidador'		, FILTER_SANITIZE_STRING);
			
			// Endereco
			$endereco			= filter_input(INPUT_POST, 'inputEndereco'		, FILTER_SANITIZE_STRING);
			$bairro				= filter_input(INPUT_POST, 'inputBairro'		, FILTER_SANITIZE_STRING);
			$cidade	 			= filter_input(INPUT_POST, 'inputCidade'		, FILTER_SANITIZE_STRING);
			$lat				= filter_input(INPUT_POST, 'inputLat'			, FILTER_SANITIZE_STRING);
			$lng				= filter_input(INPUT_POST, 'inputLng'			, FILTER_SANITIZE_STRING);
			$coordenada			= filter_input(INPUT_POST, 'inputCoordenada'	, FILTER_SANITIZE_STRING);
			$referencia			= filter_input(INPUT_POST, 'inputReferencia'	, FILTER_SANITIZE_STRING);
			$distancia			= filter_input(INPUT_POST, 'inputDistancia'		, FILTER_SANITIZE_STRING);
			$distancia 			= str_replace(",",".",$distancia);
			
			// Contato
			$telefone1			= filter_input(INPUT_POST, 'inputTelefone1'		, FILTER_SANITIZE_STRING);
			$nome1				= filter_input(INPUT_POST, 'inputNome1'			, FILTER_SANITIZE_STRING);
			$telefone2			= filter_input(INPUT_POST, 'inputTelefone2'		, FILTER_SANITIZE_STRING);
			$nome2				= filter_input(INPUT_POST, 'inputNome2'			, FILTER_SANITIZE_STRING);
			$telefone3			= filter_input(INPUT_POST, 'inputTelefone3'		, FILTER_SANITIZE_STRING);
			$nome3				= filter_input(INPUT_POST, 'inputNome3'			, FILTER_SANITIZE_STRING);
			
			// DADOS ADICIONAIS
			// Data do cadastro
			$data_now 	= date("Y-m-d H:i:s");
			// Status do paciente
			$status		= "ativo";
			// Codigo do usuario
			$user_codigo = $_SESSION['user_Codigo'];
			
			
			$query = "SELECT COUNT(id) as total FROM ".$_SESSION['user_Servico']."_paciente_geral WHERE nome = '".$nome."' AND nascimento = '".$nascimento."' LIMIT 1";
			$mysql_query = mysqli_query($mysqli, $query);
			$total = mysqli_fetch_assoc($mysql_query);
			
			
			// CADASTRAR NO BANCO DE DADOS
			if ($total['total'] == 0) {
				
				// Dados gerais do paciente
				$prep_stmt = "INSERT INTO ".$_SESSION['user_Servico']."_paciente_geral 
									(data_cadastro, 
									data_atualizacao,
									status, 
									nome, 
									nascimento, 
									sexo, 
									diagnostico_1, 
									diagnostico_2,
									diagnostico_3,
									responsavel,
									relacao,
									cuidador,
									enf,
									fon,
									nut,
									fir,
									fim,
									med) 
										VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, '0', '0', '0', '0', '0', '0')";
				$stmt = $mysqli->prepare($prep_stmt);
				
				// Verifica se query esta correta
				if ($stmt) {
					
					// bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
					$stmt->bind_param('ssssssssssss', 
											$data_now, 
											$data_now, 
											$status, 
											$nome, 
											$nascimento, 
											$sexo, 
											$diagnostico1, 
											$diagnostico2, 
											$diagnostico3,
											$responsavel,
											$relacao,
											$cuidador
											);
	
					if($stmt->execute()){
						
						// Codigo gerado para o paciente
						$id_paciente = $stmt->insert_id;
						
						print 'Success! ID of last inserted record is : ' .$stmt->insert_id .'<br />';
						
					}else{
						
						die('Error : ('. $mysqli->errno .') '. $mysqli->error);
						
						// REDIRECIONA - Erro de banco de dados
						header("Location: ../../paciente.php?e=1051");
						
					}
					
					$stmt->close();
				
				} else {
					
					// REDIRECIONA - Erro de banco de dados
					header("Location: ../../paciente.php?e=1051");
					
				}
				
				
				// Dados de contato do paciente
				$prep_stmt = "INSERT INTO ".$_SESSION['user_Servico']."_paciente_contato 
									(id_paciente, 
									data_atualizacao, 
									endereco,
									bairro,
									cidade,
									referencia,
									kilometragem,
									telefone_1,
									nome_1,
									telefone_2,
									nome_2,
									telefone_3,
									nome_3,
									coordenada,
									lng,
									lat) 
										VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				$stmt = $mysqli->prepare($prep_stmt);
				
				// Verifica se query esta correta
				if ($stmt) {
					
					// bind parameters for markers, where (s = string, i = integer, d = double,  b = blob)
					$stmt->bind_param('ssssssssssssssss', 
											$id_paciente, 
											$data_now,
											$endereco,
											$bairro,
											$cidade,
											$referencia,
											$distancia,
											$telefone1,
											$nome1,
											$telefone2,
											$nome2,
											$telefone3,
											$nome3,
											$coordenada,
											$lng,
											$lat);
	
					if($stmt->execute()){
						
						print 'Success! ID of last inserted record is : ' .$stmt->insert_id .'<br />';
						$success_msg = TRUE;
						
					}else{
						
						die('Error : ('. $mysqli->errno .') '. $mysqli->error);
						
						// REDIRECIONA - Erro de banco de dados
						header("Location: ../../pacientes.php?e=1052");
						
					}
					
					$stmt->close();
				
				} else {
					
					// REDIRECIONA - Erro de banco de dados
					header("Location: ../../pacientes.php?e=1052");
					
				}
				
				
				// REDIRECIONA - Concluido
				header("Location:../../paciente.php?id=".$id_paciente);
			
			}else{
				
				header("Location: ../../pacientes.php?e=203");
				
			}
			
		}else{
		
			header("Location: ../../pacientes.php?e=203");
			
		}
		
	}else{
		
		header("Location: ../../login.php");
		
	}


?>

