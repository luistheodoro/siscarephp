﻿<?php



//-------------------------------------------------	PACIENTE ---------------------------------------------------------------------------------//

function busca_paciente($mysqli,$id_paciente){
	
	$id_paciente = preg_replace("/[^0-9]+/", "", $id_paciente);
	
	$paciente['geral']   = busca_paciente_basico($mysqli, $id_paciente);
	$paciente['contato'] = busca_paciente_contato($mysqli, $id_paciente);
	$paciente['espec']   = busca_paciente_espec($mysqli, $id_paciente);
	$paciente['escala']  = busca_paciente_escala($mysqli, $id_paciente);
	$paciente['fon']     = busca_paciente_fon($mysqli, $id_paciente);
	
	// Ultimas condutas
	$paciente['conduta']['fon'] = busca_paciente_conduta($mysqli, $id_paciente, 'fon');
	
	return $paciente;
	
}


	// BUSCA DADOS BASICOS DO PACIENTE
	function busca_paciente_basico($mysqli, $id_paciente){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_paciente_geral WHERE id = '".$id_paciente."' LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		$paciente_geral = mysqli_fetch_assoc($mysql_query);
		
		$paciente_geral['diagnostico'] = diagnostico($paciente_geral['diagnostico_1'], $paciente_geral['diagnostico_2'], $paciente_geral['diagnostico_3']);
		
		return $paciente_geral;
		
	}


	// BUSCA DADOS DE CONTATO DO PACIENTE
	function busca_paciente_contato($mysqli, $id_paciente){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_paciente_contato WHERE id_paciente = '".$id_paciente."' LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		$paciente_contato = mysqli_fetch_assoc($mysql_query);
		
		if($paciente_contato['lat']==NULL){
			$paciente_contato['lat'] = 0;
		}
		
		if($paciente_contato['lng']==NULL){
			$paciente_contato['lng'] = 0;
		}
		
		return $paciente_contato;
		
	}


	// BUSCA DADOS ESPECIFICOS DO PACIENTE
	function busca_paciente_espec($mysqli, $id_paciente){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_espec WHERE id_paciente = '".$id_paciente."' ORDER BY modificado_em DESC LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		
		if( mysqli_num_rows($mysql_query) > 0 ){
			$paciente_espec = mysqli_fetch_assoc($mysql_query);
			return $paciente_espec;
		}else{
			return FALSE;
		}
		
		
	}
	
	
	// BUSCA ESCALAS DO PACIENTE
	function busca_paciente_escala($mysqli, $id_paciente){
	
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_escala WHERE id_paciente = '".$id_paciente."' ORDER BY data DESC LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
	
		if( mysqli_num_rows($mysql_query) > 0 ){
			$paciente_escala = mysqli_fetch_assoc($mysql_query);
			return $paciente_escala;
		}else{
			return FALSE;
		}
	
	
	}
	
	
	// BUSCA ULTIMA CONDUTA DO PACIENTE
	function busca_paciente_conduta($mysqli, $id_paciente, $equipe){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_terapia WHERE conduta!='Mantida' AND equipe='".$equipe."' AND id_paciente = '".$id_paciente."' ORDER BY data DESC LIMIT 1 ";
		$mysql_query = mysqli_query($mysqli,$query);
		
		if( mysqli_num_rows($mysql_query) > 0 ){
			$paciente_conduta = mysqli_fetch_assoc($mysql_query);
			return $paciente_conduta;
		}else{
			return FALSE;
		}
		
	}
	

	// BUSCA DADOS DE FONOAUDIOLOGIA DO PACIENTE
	function busca_paciente_fon($mysqli, $id_paciente){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_fon WHERE id_paciente = '".$id_paciente."' LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		$paciente_fon = mysqli_fetch_assoc($mysql_query);
		
		return $paciente_fon;
		
	}


?>