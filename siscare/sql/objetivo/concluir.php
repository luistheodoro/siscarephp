<?php


	// Inclua fun��es e conex�es de banco de dados aqui. Ver 3.1. 
	include_once("../../config/db_connect.php");
	include_once("../../config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verifica��o de acessos
		include_once("../../functions/acesso/paciente.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../usuario/buscar.php");
		// Busca os dados de paciente
		include_once("../paciente/buscar.php");
		// Busca os dados do objetivo
		include_once("buscar.php");
		
		
		// RECUPERA OS DADOS DO USUARIO
		$usuario 	= busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA VALORES DO FORMULARIO
		$id			= filter_input(INPUT_POST, 'inputIdObjetivo2'	, FILTER_SANITIZE_STRING);
		$id 		= preg_replace("/[^0-9]+/", "", $id);
		
		
		$objetivo 	= busca_objetivo_id($mysqli,$id);
		
		
		// BUSCA DADOS BASICOS DO PACIENTE
		$paciente	= busca_paciente($mysqli, $objetivo['id_paciente']);
			
		
		if( acesso_concluir_objetivo($mysqli,$usuario,$paciente) == TRUE ){
			
			// Conclui o objetivo
			$query = "	UPDATE
							".$_SESSION['user_Servico']."_objetivo 
						SET
							progresso		= '100',
							fim_objetivo	= NOW()
						WHERE
							id = '".$id."'
						";
			
			$mysql_query_objetivo = mysqli_query($mysqli,$query);
			
			if( $mysql_query_objetivo == TRUE ){
				
				// REDIRECIONA - Concluido
				header("Location:../../objetivos.php");
				
			}else{
				
				// REDIRECIONA - Erro de cadastro
				header("Location:../../objetivos.php?e=113");
			
			}
		
		}else{
			
			// REDIRECIONA - Acesso n�o permitido
			header("Location:../../objetivos.php?e=223");
			
		}
		
	}else{
		
		// REDIRECIONA - Falha de login
		header("Location:../../login.php");
		
	}


?>

