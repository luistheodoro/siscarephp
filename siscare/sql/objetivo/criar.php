<?php


	// Inclua fun��es e conex�es de banco de dados aqui. Ver 3.1. 
	include_once("../../config/db_connect.php");
	include_once("../../config/functions.php");


	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verifica��o de acessos
		include_once("../../functions/acesso/paciente.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../usuario/buscar.php");
		// Busca os dados de paciente
		include_once("../paciente/buscar.php");
		
		
		// RECUPERA OS DADOS DO USUARIO
		$usuario 	= busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// RECUPERA O ID E DADOS DO PACIENTE
		$id_paciente 	= isset($_POST['inputPaciente']) ? $_POST['inputPaciente'] : header("Location: ../../pacientes.php") ;
		$id_paciente 	= preg_replace("/[^0-9]+/", "", $id_paciente);
		$paciente 		= busca_paciente($mysqli, $id_paciente);
		
		
		// VERIFICA ACESSO PARA CRIAR OBJETIVO
		if( acesso_criar_objetivo_2($mysqli,$usuario,$paciente) == TRUE ){
			
			
			// RECUPERA VALORES DO FORMULARIO
			// Dados do objetivo
			$objetivo			= filter_input(INPUT_POST, 'inputObjetivo'		, FILTER_SANITIZE_STRING);
			$meta				= filter_input(INPUT_POST, 'inputMeta'			, FILTER_SANITIZE_STRING);
			$equipe				= filter_input(INPUT_POST, 'inputEquipe'		, FILTER_SANITIZE_STRING);
			
			
			$tabela_equipe = $_SESSION['user_Servico']."_".$equipe;
			
			$query = "SELECT responsavel FROM ".$tabela_equipe." WHERE id_paciente = '".$id_paciente."' AND data_termino IS NULL ";
			$mysql_query = mysqli_query($mysqli,$query);
			$responsavel = mysqli_fetch_assoc($mysql_query);
			
			
			// CADASTRA O NOVO OBJETIVO
			$query = "	INSERT INTO ".$_SESSION['user_Servico']."_objetivo 
							(id_paciente, equipe, id_responsavel, objetivo, progresso, inicio_objetivo, previsao_objetivo) 
						VALUES 
							('".$paciente['geral']['id']."', '".$equipe."', '".$responsavel['responsavel']."', '".$objetivo."', '0', NOW(), '".$meta."')
						";
			$mysql_query_objetivo = mysqli_query($mysqli,$query) or die(mysqli_error($mysqli));
			
		
			if( $mysql_query_objetivo == TRUE ){
				
				// REDIRECIONA - Concluido
				header("Location:../../objetivos.php");
				
			}else{
				
				// REDIRECIONA - Erro de cadastro
				header("Location:../../objetivos.php?e=111");
			
			}
		
		}else{
			
			// REDIRECIONA - Acesso n�o permitido
			header("Location:../../objetivos.php?e=221");
			
		}
		
	}else{
		
		// REDIRECIONA - Falha de login
		header("Location:../../login.php");
		
	}


?>

