﻿<?php

	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("../../config/db_connect.php");
	include_once("../../config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
			
		$codigo_servico 	= $_POST['codigo_servico'];
		
		// proteção XSS conforme imprimimos este valor
		$codigo_servico 	= preg_replace("/[^0-9]+/", "", $codigo_servico);
		$_SESSION['user_Servico2'] = $codigo_servico;
		
		header("Location: ../../restrito.php");
		
	}else{
		
		header("Location: ../../login.php");
		
	}

?>