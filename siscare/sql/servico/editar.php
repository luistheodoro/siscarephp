<?php


	// Inclua fun��es e conex�es de banco de dados aqui. Ver 3.1. 
	include_once("../../config/db_connect.php");
	include_once("../../config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {

		
		/* FUNCTIONS */
		// Verifica��o de acessos
		include_once("../../functions/acesso/usuario.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../usuario/buscar.php");
		
		
		// RECUPERA OS DADOS DO USUARIO
		$usuario 	= busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		
		// VERIFICA ACESSO PARA EDITAR DADOS DO USUARIO
		if( $usuario['acesso']['cadastrar_perfil'] != 4 ){
		
			// REDIRECIONA PARA PAGINA DE CONFIGURACAO
			header("Location: ../../configuracao.php");
			
		}else{
		
		/* --- CADASTRAR DADOS BASICOS --- */
			
			// RECUPERA VALORES DO FORMULARIO
			$enf = isset($_POST['enf']) ? 1 : 0 ;
			$nut = isset($_POST['nut']) ? 1 : 0 ;
			$fon = isset($_POST['fon']) ? 1 : 0 ;
			$fir = isset($_POST['fir']) ? 1 : 0 ;
			$fim = isset($_POST['fim']) ? 1 : 0 ;
			$med = isset($_POST['med']) ? 1 : 0 ;
			
			$codigo_servico = isset($_POST['codigo_servico']) ? $_POST['codigo_servico'] : FALSE ;
			$codigo_servico	= preg_replace("/[^0-9]+/", "", $codigo_servico);
			
			$num_usuarios 	= isset($_POST['num_usuarios']) ? $_POST['num_usuarios'] : FALSE ;
			$num_usuarios	= preg_replace("/[^0-9]+/", "", $num_usuarios);
			
			
			if($codigo_servico != FALSE){
				
				/* --- CADASTRAR DADOS COMPLETOS --- */
				
					// Atualiza o usuario no banco de dados 
					$query = "	UPDATE 
									servicos 
								SET 
									usuarios = '$num_usuarios',
									enf = '$enf',
									nut = '$nut', 
									fon = '$fon', 
									fir = '$fir',
									fim = '$fim',
									med = '$med'
								WHERE 
									codigo_servico = '".$codigo_servico."' 
								";
					$mysql_query = mysqli_query($mysqli,$query);
					
					header("Location:../../restrito.php");
			}
			
		}
		
	}


?>

