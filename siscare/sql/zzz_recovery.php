﻿<?php



//-------------------------------------------------	PACIENTE ---------------------------------------------------------------------------------//

function busca_paciente($mysqli,$id_paciente){

	$paciente['geral']   = busca_paciente_basico($mysqli, $id_paciente);
	$paciente['contato'] = busca_paciente_contato($mysqli, $id_paciente);
	$paciente['espec']   = busca_paciente_espec($mysqli, $id_paciente);
	$paciente['fon']     = busca_paciente_fon($mysqli, $id_paciente);
	
	return $paciente;
	
}


	// BUSCA DADOS BASICOS DO PACIENTE
	function busca_paciente_basico($mysqli, $id_paciente){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_paciente_geral WHERE id = '".$id_paciente."' LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		$paciente_geral = mysqli_fetch_assoc($mysql_query);
		
		$paciente_geral['diagnostico'] = diagnostico($paciente_geral['diagnostico_1'], $paciente_geral['diagnostico_2'], $paciente_geral['diagnostico_3']);
		
		return $paciente_geral;
		
	}


	// BUSCA DADOS DE CONTATO DO PACIENTE
	function busca_paciente_contato($mysqli, $id_paciente){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_paciente_contato WHERE id_paciente = '".$id_paciente."' LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		$paciente_fon = mysqli_fetch_assoc($mysql_query);
		
		return $paciente_fon;
		
	}


	// BUSCA DADOS ESPECIFICOS DO PACIENTE
	function busca_paciente_espec($mysqli, $id_paciente){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_espec WHERE id_paciente = '".$id_paciente."' ORDER BY id DESC LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		
		if( mysqli_num_rows($mysql_query) > 0 ){
			$paciente_espec = mysqli_fetch_assoc($mysql_query);
			return $paciente_espec;
		}else{
			return FALSE;
		}
		
		
	}


	// BUSCA DADOS DE FONOAUDIOLOGIA DO PACIENTE
	function busca_paciente_fon($mysqli, $id_paciente){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_fon WHERE id_paciente = '".$id_paciente."' LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		$paciente_fon = mysqli_fetch_assoc($mysql_query);
		
		return $paciente_fon;
		
	}



//-------------------------------------------------	USUARIO	---------------------------------------------------------------------------------//


function busca_usuario($mysqli,$id_usuario){

	$usuario['geral']    = busca_usuario_geral($mysqli,$id_usuario);
	$usuario['contato']  = busca_usuario_contato($mysqli,$id_usuario);
	$usuario['acesso']   = acesso($mysqli);
	
	return $usuario;
	
}


	// BUSCA DADOS GERAIS DO USUARIO
	function busca_usuario_geral($mysqli,$id_usuario){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_usuarios WHERE id_usuario = '".$id_usuario."' LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		$usuario = mysqli_fetch_assoc($mysql_query);
		
		if($usuario['imagem']!=""){
			$imagem = $usuario['imagem'];
			$avatar = explode(".",$usuario['avatar']);
			$avatar = $avatar[0].".png";
		}else{
			$imagem = "images/avatar/profile-pic.png";
			$avatar = $imagem;
		}
		
		$usuario['imagem'] = $imagem;
		$usuario['avatar'] = $avatar;
		
		
		
		return $usuario;
		
	}


	// BUSCA DADOS DE CONTATO DO USUARIO
	function busca_usuario_contato($mysqli,$id_usuario){
		
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_contatos WHERE id_usuario = '".$id_usuario."' LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		$contato = mysqli_fetch_assoc($mysql_query);
		
		$query = "SELECT email FROM usuarios WHERE id = '".$id_usuario."' LIMIT 1";
		$mysql_query = mysqli_query($mysqli,$query);
		$email = mysqli_fetch_assoc($mysql_query);
		
		$contato['email'] = $email['email'];
		
		return $contato;
		
	}


	// BUSCA ACESSOS DO USUARIO
	function acesso($mysqli){
		
		// Busca senha do usuario no banco de dados basico
		$query = "SELECT * FROM ".$_SESSION['user_Servico']."_acessos WHERE perfil = '".$_SESSION['user_Acesso']."' LIMIT 1";
		$mysql = mysqli_query($mysqli,$query);
		
		$acesso = mysqli_fetch_assoc($mysql);
		
		return $acesso;
		
	}



//-------------------------------------------------	OBJETIVO ---------------------------------------------------------------------------------//


function objetivo($mysqli,$id_paciente){
	
	$query = "SELECT * FROM ".$_SESSION['user_Servico']."_objetivo WHERE id_paciente = '".$id_paciente."' AND progresso < 100 ";
	$mysql_query = mysqli_query($mysqli,$query);
	$objetivo = mysqli_fetch_assoc($mysql_query);
	return $objetivo;
	
}

function objetivo_id($mysqli,$id_objetivo){
	
	$query = "SELECT * FROM ".$_SESSION['user_Servico']."_objetivo WHERE id = '".$id_objetivo."' ";
	$mysql_query = mysqli_query($mysqli,$query);
	$objetivo = mysqli_fetch_assoc($mysql_query);
	return $objetivo;
	
}


//-------------------------------------------------	CONTAR ---------------------------------------------------------------------------------//


function count_atendimento_usuario($mysqli,$periodo,$id_usuario){
	
	$query = "SELECT COUNT(id) as total FROM ".$_SESSION['user_Servico']."_terapia WHERE id_usuario = '".$id_usuario."' AND data BETWEEN '".$periodo['inicio']."' AND '".$periodo['fim']."' ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);
	
	return $total['total'];
	
}


function count_atendimento_equipe($mysqli,$periodo,$equipe){
	
	$query = "SELECT COUNT(id) as total FROM ".$_SESSION['user_Servico']."_terapia WHERE equipe = '".$equipe."' AND data BETWEEN '".$periodo['inicio']."' AND '".$periodo['fim']."' ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);
	
	return $total['total'];
	
}


function count_pacientes_usuario($mysqli,$equipe,$id_usuario){
	
	$tabela = $_SESSION['user_Servico']."_".$equipe;
	
	$query = "SELECT COUNT(id) as total FROM ".$tabela." WHERE responsavel = '".$id_usuario."' AND data_termino IS NULL ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);
	
	return $total['total'];
	
}


function count_pacientes_equipe($mysqli,$equipe){
	
	$tabela = $_SESSION['user_Servico']."_".$equipe;
	
	$query = "SELECT COUNT(id) as total FROM ".$tabela." WHERE data_termino IS NULL ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);
	
	return $total['total'];
	
}


function count_objetivos_usuario($mysqli,$id_usuario){
	
	$tabela = $_SESSION['user_Servico']."_objetivo";
	
	$query = "SELECT COUNT(id) as total FROM ".$tabela." WHERE id_responsavel = '".$id_usuario."' AND fim_objetivo IS NULL ";
	$mysql_query = mysqli_query($mysqli,$query);
	$total = mysqli_fetch_assoc($mysql_query);
	
	return $total['total'];
	
}


?>