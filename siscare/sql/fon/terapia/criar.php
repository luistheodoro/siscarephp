﻿<?php
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include("../../../config/db_connect.php");
	include("../../../config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("../../../functions/acesso/paciente.php");
		// Ajusta os checkboxs para terapias adicionais
		include_once("../../../functions/terapia/terapia_adicional_fon.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../../usuario/buscar.php");
		// Busca os dados de paciente
		include_once("../../paciente/buscar.php");
		
		
		// RECUPERA OS DADOS DO USUARIO
		$usuario = busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		// RECUPERA O ID E DADOS DO PACIENTE
		$id_paciente   	= $_POST['inputIDPaciente'];
		$id_paciente 	= preg_replace("/[^0-9]+/", "", $id_paciente);
		$paciente 		= busca_paciente($mysqli, $id_paciente);
		
		
		// VERIFICA SE O USUARIO TEM ACESSO PARA CRIAR UMA TERAPIA
		if( acesso_criar_terapia($mysqli,$usuario,$paciente) == TRUE ){
			
			
			// INPUTS DE ENTRADA
			// Terapia
			$terapia       = isset($_POST['inputTerapiaPrincipal']) ? $_POST['inputTerapiaPrincipal'] 	: NULL ;
			$evolucao      = isset($_POST['inputEvolucao']) 		? $_POST['inputEvolucao'] 			: NULL ;
			$conduta       = isset($_POST['inputConduta']) 			? $_POST['inputConduta'] 			: NULL ;
			$comentario    = isset($_POST['inputComentario']) 		? $_POST['inputComentario'] 		: NULL ;
			$equipe        = isset($_POST['inputEquipe']) 			? $_POST['inputEquipe']				: NULL ;
			$DataTerapia   = isset($_POST['inputDataTerapia']) 		? $_POST['inputDataTerapia']		: date('Y-m-d') ;
			// Dieta liberada
			$dieta         = isset($_POST['inputDieta']) 			? $_POST['inputDieta'] 				: NULL ;
			$liquido       = isset($_POST['inputLiquido']) 			? $_POST['inputLiquido'] 			: NULL ;
			// Dieta atual
			$dieta_atual   = isset($_POST['dietaAtual']) 			? $_POST['dietaAtual'] 				: NULL ;
			$liquido_atual = isset($_POST['liquidoAtual']) 			? $_POST['liquidoAtual'] 			: NULL ;
			// Terapias adicionais
			$adicional[1] = isset($_POST['adicional_1']) 			? $_POST['adicional_1'] 			: NULL ;
			$adicional[2] = isset($_POST['adicional_2']) 			? $_POST['adicional_2'] 			: NULL ;
			$adicional[3] = isset($_POST['adicional_3']) 			? $_POST['adicional_3'] 			: NULL ;
			$adicional[4] = isset($_POST['adicional_4']) 			? $_POST['adicional_4'] 			: NULL ;
			$adicional[5] = isset($_POST['adicional_5']) 			? $_POST['adicional_5'] 			: NULL ;
			$adicional[6] = isset($_POST['adicional_6']) 			? $_POST['adicional_6']			 	: NULL ;
			$adicional[7] = isset($_POST['adicional_7']) 			? $_POST['adicional_7'] 			: NULL ;
			$adicional[8] = isset($_POST['adicional_8']) 			? $_POST['adicional_8'] 			: NULL ;
			// O que foi avaliado
			$aval_dieta   = isset($_POST['aval_dieta']) 			? $_POST['aval_dieta'] 				: NULL ;
			$aval_liquido = isset($_POST['aval_liquido']) 			? $_POST['aval_liquido'] 			: NULL ;
			// Escalas
			$ASHA_NOMS	  = isset($_POST['inputASHA_NOMS']) 		? $_POST['inputASHA_NOMS'] 			: NULL ;
			$BRADEN		  = isset($_POST['inputBraden']) 			? $_POST['inputBraden'] 			: NULL ;
			
			
			$primeiro = TRUE;
			
			for ($i=1;$i<=8;$i++){
				
				if( $adicional[$i] != NULL ){
					
					if($primeiro == TRUE){
						$adicional_final = terapia_adicional_fon($i);
						$primeiro = FALSE;
					}else{
						$adicional_final = $adicional_final." / ".terapia_adicional_fon($i);
					}
					
				}
				
			}
			
			if($aval_dieta == TRUE){
				$aval_dieta = 1;
			}else{
				$aval_dieta = 0;
			}
			
			if($aval_liquido == TRUE){
				$aval_liquido = 1;
			}else{
				$aval_liquido = 0;
			}
			
			
			// VERIFICA SE TERAPIA JA FOI CRIADA
			$query = "SELECT COUNT(id) as total FROM ".$_SESSION['user_Servico']."_terapia WHERE id_paciente = '".$id_paciente."' AND id_usuario = '".$_SESSION['user_Codigo']."' AND TIMESTAMPDIFF(SECOND, data, NOW() ) <= '60' LIMIT 1";
			$mysql_query = mysqli_query($mysqli, $query);
			$total = mysqli_fetch_assoc($mysql_query);
			
			
			// CADASTRA A TERAPIA
			if ($total['total'] == 0) {
				
				$query = "INSERT INTO ".$_SESSION['user_Servico']."_terapia (
												id_paciente, 
												id_usuario, 
												data, 
												equipe, 
												terapia,
												terapia_2,
												evolucao, 
												conduta, 
												comentario,
												aval_dieta,
												aval_liquido
											) VALUES (
												'".$id_paciente."', 
												'".$usuario['geral']['id_usuario']."', 
												'".$DataTerapia."', 
												'".$usuario['geral']['equipe']."', 
												'".$terapia."', 
												'".$adicional_final."',
												'".$evolucao."', 
												'".$conduta."', 
												'".$comentario."',
												'".$aval_dieta."',
												'".$aval_liquido."'
											)";
											
				$mysql_query = mysqli_query($mysqli,$query);
				
				
				// ATUALIZA AS ESCALAS DO PACIENTE
				$query = "INSERT INTO ".$_SESSION['user_Servico']."_escala (
												id_paciente,
												id_usuario,
												data,
												ASHA_NOMS,
												BRADEN
											) VALUES (
												'".$id_paciente."',
												'".$usuario['geral']['id_usuario']."',
												'".$DataTerapia."',
												'".$ASHA_NOMS."',
												'".$BRADEN."'
											)";
				
				$mysql_query = mysqli_query($mysqli,$query);
				
				
				// CADASTRA A CONDUTA (SE HOUVER)
				if($conduta != 'Mantida'){
					
					$paciente_espec	= busca_paciente_espec($mysqli, $id_paciente);
					
					if($conduta == "Suspensão de dieta via oral"){
						$dieta     = "Suspenso" ;
						$liquido   = "Suspenso" ;
					}
					
					$query = "INSERT INTO ".$_SESSION['user_Servico']."_espec (
												id_paciente, 
												modificado_por, 
												modificado_em, 
												degenerativo, 
												paliativo,
												terminalidade,
												prognostico,
												VAA,
												TQT, 
												dieta, 
												liquido
											) VALUES (
												'".$id_paciente."', 
												'".$usuario['geral']['id_usuario']."', 
												'".$DataTerapia."',
												'".$paciente['espec']['degenerativo']."', 
												'".$paciente['espec']['paliativo']."', 
												'".$paciente['espec']['terminalidade']."', 
												'".$paciente['espec']['prognostico']."', 
												'".$paciente['espec']['VAA']."',
												'".$paciente['espec']['TQT']."', 
												'".$dieta."', 
												'".$liquido."'
											)";
					
					$mysql_query = mysqli_query($mysqli,$query) or die(mysqli_error($mysqli));
					
				}
				
				// REDIRECIONA - Concluido
				header("Location: ../../../paciente.php?id=".$id_paciente."&tab=".$equipe);
			
			}else{
				
				// REDIRECIONA - Terapia ja cadastrada
				header("Location: ../../../paciente.php?id=".$id_paciente."&tab=".$equipe);
				
			}
			
		}else{
			
			// REDIRECIONA - Acesso não permitido
			header("Location: ../../../paciente.php?id=".$id_paciente."&tab=fon&e=202");
			
		}
		
	}else{
		
		// REDIRECIONA - Falha de login
		header("Location: ../../../login.php");
		
	}
	
	
?>