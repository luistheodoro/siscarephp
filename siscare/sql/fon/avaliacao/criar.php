﻿<?php
	
	// Inclua funções e conexões de banco de dados aqui. Ver 3.1. 
	include_once("../../../config/db_connect.php");
	include_once("../../../config/functions.php");
	
	
	sec_session_start(); 
	
	
	// VERIFICA SE USUARIO ESTA LOGADO
	if(login_check($mysqli) == TRUE) {
		
		
		/* FUNCTIONS */
		// Verificação de acessos
		include_once("../../../functions/acesso/paciente.php");
		// Ajusta os checkboxs para terapias adicionais
		include_once("../../../functions/terapia/terapia_adicional_fon.php");
		
		
		/* SQL */
		// Busca os dados de usuario
		include_once("../../usuario/buscar.php");
		// Busca os dados de paciente
		include_once("../../paciente/buscar.php");
		
		
		// RECUPERA OS DADOS DO USUARIO
		$usuario 	= busca_usuario($mysqli,$_SESSION['user_Codigo']);
		
		// RECUPERA O ID E DADOS DO PACIENTE
		$id_paciente 	= isset($_POST['inputIDPaciente']) ? $_POST['inputIDPaciente'] : header("Location: ../../../pacientes.php") ;
		$id_paciente 	= preg_replace("/[^0-9]+/", "", $id_paciente);
		$paciente 		= busca_paciente($mysqli, $id_paciente);
		
		
		// VERIFICA SE O USUARIO TEM ACESSO PARA CRIAR UMA AVALIAÇÃO
		if( acesso_criar_avaliacao($mysqli,$usuario,$paciente) == TRUE ){
		
			
			// VERIFICA SE O PACIENTE JA POSSUI ALGUMA ESPECIFICIDADE
			if($paciente['espec'] == FALSE){
			
				$paciente['espec']['degenerativo'] 	= 0; 
				$paciente['espec']['paliativo']    	= 0; 
				$paciente['espec']['terminalidade']	= 0; 
				$paciente['espec']['prognostico']  	= "";  
				$paciente['espec']['VAA']          	= ""; 
				$paciente['espec']['TQT']          	= "";  
				
			}
			
			
			/* INPUTS DE ENTRADA */
			// Avaliação
			$dieta_atual        = isset($_POST['inputDietaAtual']) 		    ? $_POST['inputDietaAtual'] 	     : NULL ;
			$liquido_atual 	    = isset($_POST['inputLiquidoAtual']) 	    ? $_POST['inputLiquidoAtual'] 	     : NULL ;
			$motivo_avaliacao   = isset($_POST['inputMotivoAvaliacao']) 	? $_POST['inputMotivoAvaliacao']     : NULL ;
			$frequencia	        = isset($_POST['inputFrequencia']) 		    ? $_POST['inputFrequencia'] 	     : NULL ;
			$ASHA_NOMS	        = isset($_POST['inputASHA_NOMS']) 		    ? $_POST['inputASHA_NOMS'] 		     : NULL ;
			$comentario         = isset($_POST['inputComentario']) 		    ? $_POST['inputComentario'] 		 : NULL ;
			// Terapia
			$terapia       		= isset($_POST['inputTerapiaPrincipal'])    ? $_POST['inputTerapiaPrincipal']	 : NULL ;
			$conduta       		= isset($_POST['inputConduta']) 		    ? $_POST['inputConduta']	         : NULL ;
			// Terapias adicionais
			$adicional[1]       = isset($_POST['adicional_1'])              ? $_POST['adicional_1']              : NULL ;
			$adicional[2]       = isset($_POST['adicional_2'])              ? $_POST['adicional_2']              : NULL ;
			$adicional[3]       = isset($_POST['adicional_3'])              ? $_POST['adicional_3']              : NULL ;
			$adicional[4]       = isset($_POST['adicional_4'])              ? $_POST['adicional_4']              : NULL ;
			$adicional[5]       = isset($_POST['adicional_5'])              ? $_POST['adicional_5']              : NULL ;
			$adicional[6]       = isset($_POST['adicional_6'])              ? $_POST['adicional_6']              : NULL ;
			$adicional[7]       = isset($_POST['adicional_7'])              ? $_POST['adicional_7']              : NULL ;
			$adicional[8]       = isset($_POST['adicional_8'])              ? $_POST['adicional_8']              : NULL ;
			// Escalas
			$ASHA_NOMS	  		= isset($_POST['inputASHA_NOMS']) 			? $_POST['inputASHA_NOMS'] 			 : NULL ;
			$BRADEN		  		= isset($_POST['inputBraden']) 				? $_POST['inputBraden'] 			 : NULL ;
			
			
			$primeiro = TRUE;
			$adicional_final = "";
			
			for ($i=1;$i<=8;$i++){
				
				if( $adicional[$i] != NULL ){
					
					if($primeiro == TRUE){
						$adicional_final = terapia_adicional_fon($i);
						$primeiro = FALSE;
					}else{
						$adicional_final = $adicional_final." / ".terapia_adicional_fon($i);
					}
					
				}
				
			}
			
			
			if($conduta == "Mantida"){
				
				$dieta     = $dieta_atual;
				$liquido   = $liquido_atual;
				
			}else{
				
				if($conduta == "Suspensão de dieta via oral"){
					$dieta     = "Suspenso" ;
					$liquido   = "Suspenso" ;
				}else{
					$dieta     = isset($_POST['inputDieta']) 	? $_POST['inputDieta'] 		: NULL ;
					$liquido   = isset($_POST['inputLiquido']) 	? $_POST['inputLiquido'] 	: NULL ;
				}
				
			}
			
			
			
			
			/* CADASTRO NO BANCO DE DADOS */
			
			// VERIFICA SE AVALIAÇÃO JA FOI CRIADA
			$query = "SELECT COUNT(id) as total FROM ".$_SESSION['user_Servico']."_fon WHERE id_paciente = '".$id_paciente."' AND TIMESTAMPDIFF(SECOND, data_inicio, NOW() ) <= '60' LIMIT 1";
			$mysql_query = mysqli_query($mysqli, $query);
			$total = mysqli_fetch_assoc($mysql_query);
			
			
			// CADASTRA A AVALIAÇÃO
			if ($total['total'] == 0) {
					
				$query = "INSERT INTO ".$_SESSION['user_Servico']."_fon (
												id_paciente, 
												responsavel, 
												data_inicio, 
												frequencia,
												dieta_inicial,
												liquido_inicial, 
												motivo_avaliacao, 
												comentario
											) VALUES (
												'".$id_paciente."', 
												'".$usuario['geral']['id_usuario']."',  
												NOW(), 
												'$frequencia', 
												'$dieta_atual',
												'$liquido_atual', 
												'$motivo_avaliacao', 
												'$comentario'
											)";
											
				$mysql_query = mysqli_query($mysqli,$query) or die(mysqli_error($mysqli));
				
				$query = "UPDATE ".$_SESSION['user_Servico']."_paciente_geral SET fon='1' WHERE id='".$id_paciente."' ";
				$mysql_query = mysqli_query($mysqli,$query) or die(mysqli_error($mysqli));
				
				
				// CADASTRA A TERAPIA
				$query = "INSERT INTO ".$_SESSION['user_Servico']."_terapia (
												id_paciente, 
												id_usuario, 
												data, 
												equipe, 
												terapia,
												terapia_2,
												conduta, 
												comentario
											) VALUES (
												'".$id_paciente."', 
												'".$usuario['geral']['id_usuario']."', 
												NOW(), 
												'".$usuario['geral']['equipe']."', 
												'$terapia', 
												'$adicional_final',
												'$conduta', 
												'$comentario'
											)";
											
				$mysql_query = mysqli_query($mysqli,$query) or die(mysqli_error($mysqli));
				
				
				// CADASTRA A ESPECIFICIDADE
				$query = "INSERT INTO ".$_SESSION['user_Servico']."_espec (
											id_paciente, 
											modificado_por, 
											modificado_em, 
											degenerativo, 
											paliativo,
											terminalidade,
											prognostico,
											VAA,
											TQT, 
											dieta, 
											liquido
										) VALUES (
											'".$id_paciente."', 
											'".$usuario['geral']['id_usuario']."', 
											NOW(), 
											'".$paciente['espec']['degenerativo']."', 
											'".$paciente['espec']['paliativo']."', 
											'".$paciente['espec']['terminalidade']."', 
											'".$paciente['espec']['prognostico']."', 
											'".$paciente['espec']['VAA']."',
											'".$paciente['espec']['TQT']."', 
											'$dieta', 
											'$liquido'
										)";
				
				$mysql_query = mysqli_query($mysqli,$query) or die(mysqli_error($mysqli));
				
				
				// ATUALIZA AS ESCALAS DO PACIENTE
				$query = "INSERT INTO ".$_SESSION['user_Servico']."_escala (
												id_paciente,
												id_usuario,
												data,
												ASHA_NOMS,
												BRADEN
											) VALUES (
												'".$id_paciente."',
												'".$usuario['geral']['id_usuario']."',
												NOW(),
												'".$ASHA_NOMS."',
												'".$BRADEN."'
											)";
					
				$mysql_query = mysqli_query($mysqli,$query);
				
				
				// REDIRECIONA - Concluido
				header("Location: ../../../paciente.php?id=".$id_paciente."&tab=fon");
				
			}else{
				
				// REDIRECIONA - Concluido
				header("Location: ../../../paciente.php?id=".$id_paciente);
				
			}
			
		}else{
			
			// REDIRECIONA - Acesso não permitido
			header("Location: ../../../paciente.php?id=".$id_paciente."&tab=fon&e=202");
			
		}
		
	}else{
		
		// REDIRECIONA - Falha de login
		header("Location: ../../../login.php");
		
	}
	
?>